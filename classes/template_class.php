<?php

/**
 * Template class containing template class
 * 
 * @package projex
 * @author Sunnyboy Mathole <sunny@innovatorshill.co.za>
 * @copyright (c) 2016, Innovators Hill
 * @license 
 */

class template_class
{
    protected $file;
    protected $folder;
    protected $variables;
    protected $data = array();
    
    /**
     * Constructor
     * 
     * The constructor with all variables for the page template
     */
    public function __construct() 
    {
        $this->variables = array();  
    }	
    /**
     * @access public
     * Method to set folder name and filename
     * 
     * @param string $folder directory location
     * @param string $file file name without extension name     * 
     * @return void
     */
    public function setView($folder, $file, $data = null){
        $this->folder = $folder;
        $this->file = $file;
        $this->data = $data;
        $this->render($data);
    } 
    /**
     * @access public
     * Method to set set variables for the page
     * 
     * @param string $key
     * @param string $value
     * @return void
     */
    public function setVariables($key, $value){
           $this->variable[$key] = $value; 
    }
    /**
     * @access public
     * Method to call the file name
     * @param array $data
     * @return void
     */
    public function render($data = null){
        extract($this->variables);
        $this->data = $data;
        $filename = ROOT . "/frontend/" .$this->folder ."/" .$this->file .".php";
        if(file_exists($filename)){
            include($filename);
        }
    }
  
}  