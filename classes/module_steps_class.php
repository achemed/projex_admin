<?php

 trait module_steps_class
 {


 	public function addHelpModuleSteps($moduleData = array()){

        global $db;
        global $objCompanies;
        $is_success = false;
        $sql = "INSERT INTO sheqintel_master.tbl_module_steps (
                                                help_id,
                                                description,
                                                parent_id, 
                                                created_by
                                          
                            ) VALUES ("
            . $db->sqs($moduleData['help_id']) . ", "
            . $db->sqs($moduleData['description']) . ", "
            . $db->sqs($moduleData['position']) . " , "
            . $db->sqs($_SESSION['user_id']) . ")";
        $is_success  = $db->query($sql);

        if($is_success){           
            return $db->insertId();
        }else{
        	return false;
        }        
    }
    public function editHelpModuleSteps($data = array())
    {
        global $db;
        if(!is_null($data)){
            $sql = "UPDATE sheqintel_master.tbl_module_steps 
            SET parent_id = ".$db->sqs($data['parent_id']).",
            description = ".$db->sqs($data['description']).",
            WHERE id = ".$db->sqs($data['id']);
            $result = $db->query($sql);
            if($result){
                return true;
            } else {
                return false;
            }            
        } else {
            return "Required information is missing";
        }
    }
    public function addHelpModuleStepsUpdate($processFlowData = array())
    {

        global $db;     
        $addFlowResults = false;
        $position = $processFlowData["position"] + 1;        
        $sql = "SELECT * FROM sheqintel_master.tbl_module_steps WHERE help_id = ".$db->sqs($processFlowData["help_id"])
                . " and parent_id  = ".$db->sqs($position);
        $result = $db->getRow($sql);
        if($result)
        {
            $sql_check = "SELECT * FROM sheqintel_master.tbl_module_steps WHERE help_id = ".$db->sqs($processFlowData["help_id"])
                . " and parent_id  >= ".$db->sqs($position);
            $position_results = $db->getAll($sql_check);
            if($position_results)
            {
                $update_result = false;
                foreach($position_results as $row)
                {
                    $new_position = $row["parent_id"] + 1;                    
                    $sql = "UPDATE sheqintel_master.tbl_module_steps SET parent_id  = ".$db->sqs($new_position)." where id = ".$db->sqs($row["id"]);
                    $update_result = $db->query($sql);
                }
                if($update_result)
                {
                    if(!empty($processFlowData['id']))
                    {
                        $processFlowData['parent_id'] = $position;
                         $addFlowResults = $this->editHelpModuleSteps($processFlowData);
                    }
                    else
                    {
                    	$processFlowData['position'] = $position;
                    	$addFlowResults = $this->addHelpModuleSteps($processFlowData);
                    }
                }
            }
        }
        else
        {
            if(!empty($processFlowData['id']))
            {
                $processFlowData['parent_id'] = $position;
                $addFlowResults = $this->editHelpModuleSteps($processFlowData);
            }
             else
             {
                    $processFlowData['position'] = $position;
                    $addFlowResults = $this->addHelpModuleSteps($processFlowData);
             }
        }
        if ($addFlowResults) {
            return true;
        } else {
            return false;
        }
    }
 	public function getAllModuleSteps($help_id = null){

        global $db;
        $sql = "SELECT sheqintel_master.tbl_module_steps.*
                  FROM sheqintel_master.tbl_module_steps WHERE help_id =" . $db->sqs($help_id)." ORDER BY parent_id asc";
        $result = $db->getAll($sql);
        if ($result) {
            return $result;
        } else {
            return false;
        }

    }
 }

?>