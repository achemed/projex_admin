<?php

/**
 * Class containing all permissions functionality
 * l
 * @package projex
 * @author Brian Douglas<douglas@innovatorshill.co.za>
 * @copyright (c) 2016, Innovators Hill
 * @license
 */

include_once ('module_steps_class.php');
include_once ('module_files_class.php');

class module_class
{

    /**
     * Method to initiate the class
     *
     * @access public
     */
     use module_steps_class,module_files_class;

    public function __construct()
    {

    }

    /**
     * Get all comapnies
     * @return bool
     */
    public function getAllModuleHelp()
    {

        global $db;
        $sql = "SELECT *
                  FROM tbl_module_help";
        $result = $db->getAll($sql);
        if ($result) {
            return $result;
        } else {
            return false;
        }
    }

    public function getModuleHelpInfo($id = null)
    {
        global $db;
        $sql = "SELECT tbl_module_help.*
                  FROM tbl_module_help WHERE id =" . $db->sqs($id);
        $result = $db->getRow($sql);
        if ($result) {
            return $result;
        } else {
            return false;
        }
    }

    public function getModuleDetailsInfo($id = null){

        global $db;
        $sql = "SELECT tbl_module_details.*
                  FROM tbl_module_details WHERE help_id =" . $db->sqs($id);
        $result = $db->getRow($sql);
        if ($result) {
            return $result;
        } else {
            return false;
        }

    }

        public function getModuleRecommendedSettingsInfo($id = null){

        global $db;
        $sql = "SELECT sheqintel_master.tbl_module_recommended_settings.*
                  FROM sheqintel_master.tbl_module_recommended_settings WHERE help_id =" . $db->sqs($id);
        $result = $db->getRow($sql);
        if ($result) {
            return $result;
        } else {
            return false;
        }

    }


    //Attachments
    public function getModuleAttachmentsInfo($id = null){

        global $db;
        $sql = "SELECT sheqintel_master.tbl_module_attachments.*
                  FROM sheqintel_master.tbl_module_attachments WHERE id =" . $db->sqs($id);
        $result = $db->getRow($sql);
        if ($result) {
            return $result;
        } else {
            return false;
        }

    }
    public function getAllModuleAttachmentsInfo($help_id = null){

        global $db;
        $sql = "SELECT sheqintel_master.tbl_module_attachments.*
                  FROM sheqintel_master.tbl_module_attachments WHERE help_id =" . $db->sqs($help_id);
        $result = $db->getAll($sql);
        if ($result) {
            return $result;
        } else {
            return false;
        }

    }

    
    public function addHelpModuleAttachments($moduleData = array()){

        global $db;
        global $objCompanies;
        $is_success = false;
        $sql = "INSERT INTO sheqintel_master.tbl_module_attachments (
                                                help_id,
                                                description,
                                                filename, 
                                                created_by
                                          
                            ) VALUES ("
            . $db->sqs($moduleData['help_id']) . ", "
            . $db->sqs($moduleData['description']) . ", "
            . $db->sqs($moduleData['filename']) . " , "
            . $db->sqs($_SESSION['user_id']) . ")";
        $is_success  = $db->query($sql);

        if($is_success){           
            $is_success = true;
        }
        return $is_success;
    }

    public function editHelpModuleAttachments($moduleData = array()){
        global $db;
        global $objCompanies;
        $is_success = false;
        $sql = "UPDATE sheqintel_master.tbl_module_attachments SET 
                                                description = ". $db->sqs($moduleData['description']) . ",
                                                filename= ". $db->sqs($moduleData['filename']) . "
                           WHERE id=".$db->sqs($moduleData['id']);
        $is_success  = $db->query($sql);

        if($is_success){
            $success = false;            
        }
        return $is_success;
    }
    //End Attachments


    public function updateHelpModuleRecommendedSettings($moduleData =  array()){

        global $db;
        global $objCompanies;
        $is_sucess = false;

        if(!$this->getModuleRecommendedSettingsInfo($moduleData['help_id'])){
            $sql = "INSERT INTO sheqintel_master.tbl_module_recommended_settings (
                                                `help_id`,
                                                `notes`,
                                                created_by
                                          
                            ) VALUES ("
                . $db->sqs($moduleData['help_id']) . ", "
                . $db->sqs($moduleData['notes']) . " , "
                . $db->sqs($_SESSION['user_id']) . ")";
            $is_sucess = $db->query($sql);

        }else{
            $SQL = "UPDATE sheqintel_master.tbl_module_recommended_settings SET notes =" . $db->sqs($moduleData['notes']) . "
                 WHERE help_id = " . $db->sqs($moduleData['help_id']);
            $is_sucess = $db->query($SQL);
        }

        if($is_sucess) {
            $is_sucess = true;
        }
        return $is_sucess;

    }

    public function addHelpModuleNewFunction($moduleData = array()){

        global $db;
        global $objCompanies;
        $is_success = false;
        $sql = "INSERT INTO sheqintel_master.tbl_module_help (
                                                module_name,
                                                module_description, 
                                                created_by
                                          
                            ) VALUES ("
            . $db->sqs($moduleData['module_name']) . ", "
            . $db->sqs($moduleData['description']) . " , "
            . $db->sqs($_SESSION['user_id']) . ")";
        $is_success  = $db->query($sql);

        if($is_success){           
            $is_success = true;
        }
        return $is_success;
    }

    public function editHelpModuleNewFunction($moduleData = array()){
        global $db;
        global $objCompanies;
        $is_success = false;
        $sql = "UPDATE sheqintel_master.tbl_module_help SET 
                                                module_name = ". $db->sqs($moduleData['module_name']) . ",
                                                module_description= ". $db->sqs($moduleData['description']) . "
                           WHERE id=".$db->sqs($moduleData['help_id']);
        $is_success  = $db->query($sql);

        if($is_success){
            $success = false;            
        }
        return $is_success;
    }

    public function addHelpModule($moduleData = array()){

        global $db;
        global $objCompanies;
        $is_success = false;
        $db->startTransaction();
        $sql = "INSERT INTO tbl_module_help (
                                                module_name,
                                                module_description, 
                                                created_by
                                          
                            ) VALUES ("
            . $db->sqs($moduleData['module_name']) . ", "
            . $db->sqs($moduleData['description']) . " , "
            . $db->sqs($_SESSION['user_id']) . ")";
        $is_success  = $db->query($sql);

        if($is_success){
            $success = false;
            $companies = $objCompanies->getAllClientCompanies();
            foreach ($companies as $company) {
                $db->selectDatabase($company['db_name']);
                $sql = "INSERT INTO tbl_module_help (
                                                module_name,
                                                module_description, 
                                                created_by
                                          
                            ) VALUES ("
                    . $db->sqs($moduleData['module_name']) . ", "
                    . $db->sqs($moduleData['description']) . " , "
                    . $db->sqs($_SESSION['user_id']) . ")";
                $success  = $db->query($sql);
            }
            if($success){
                $db->commit();
                $db->selectDatabase(MASTER_DB);
            }else{
                $db->rollBack();
            }

        }
        $db->freeResult();
        $db->selectDatabase(MASTER_DB);
        return $is_success;
    }

    /**
     * @param array $moduleData
     * @return bool
     */
    public function editHelpModule($moduleData = array()){
        global $db;
        global $objCompanies;
        $is_success = false;
        $db->startTransaction();
        $sql = "UPDATE tbl_module_help SET 
                                                module_name = ". $db->sqs($moduleData['module_name']) . ",
                                                module_description= ". $db->sqs($moduleData['description']) . "
                           WHERE id=".$db->sqs($moduleData['help_id']);
        $is_success  = $db->query($sql);

        if($is_success){
            $success = false;
            $companies = $objCompanies->getAllClientCompanies();
            foreach ($companies as $company) {
                $db->selectDatabase($company['db_name']);
                $sql = "UPDATE tbl_module_help SET 
                                                module_name = ". $db->sqs($moduleData['module_name']) . ",
                                                module_description= ". $db->sqs($moduleData['description']) . "
                           WHERE id = ".$db->sqs($moduleData['help_id']);

                $success  = $db->query($sql);
            }
            if($success){
                $db->commit();
                $db->selectDatabase(MASTER_DB);
            }else{
                $db->rollBack();
            }
        }
        $db->freeResult();
        $db->selectDatabase(MASTER_DB);
        return $is_success;
    }

    /**
     * @param array $moduleData
     * @return bool
     */
    public function updateHelpModule($moduleData =  array()){

        global $db;
        global $objCompanies;
        $is_sucess = false;

        if(!$this->getModuleDetailsInfo($moduleData['help_id'])){
            $sql = "INSERT INTO sheqintel_master.tbl_module_details (
                                                `help_id`,
                                                `notes`,
                                                created_by
                                          
                            ) VALUES ("
                . $db->sqs($moduleData['help_id']) . ", "
                . $db->sqs($moduleData['notes']) . " , "
                . $db->sqs($_SESSION['user_id']) . ")";
            $is_sucess = $db->query($sql);

        }else{
            $SQL = "UPDATE sheqintel_master.tbl_module_details SET notes =" . $db->sqs($moduleData['notes']) . "
                 WHERE help_id = " . $db->sqs($moduleData['help_id']);
            $is_sucess = $db->query($SQL);
        }

        if($is_sucess) {
            $success = true;          
        }
        return $is_sucess;

    }

    public function  useExists($helpId = null,$userId = null ){

        global $db;
        $sql = "SELECT tbl_module_detail_read.*
                  FROM tbl_module_detail_read WHERE help_id =" . $db->sqs($helpId)." AND user_id =".$db->sqs($userId);
        $result = $db->getRow($sql);
        if ($result) {
            return $result;
        } else {
            return false;
        }
    }
}