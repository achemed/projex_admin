<?php

/**
 * Class containing all permissions functionality
 * l
 * @package projex
 * @author Brian Douglas<douglas@innovatorshill.co.za>
 * @copyright (c) 2016, Innovators Hill
 * @license
 */

class update_class
{

    /**
     * Method to initiate the class
     *
     * @access public
     */
    public function __construct()
    {

    }

    /**
     * Get all comapnies
     * @return bool
     */
    public function getAllClientUpdates($companyId = null){

        global $db;
        $sql = "SELECT tbl_updates_log.*, 
                  tbl_client_companies.db_name,
                  tbl_client_companies.company_name
                  FROM tbl_updates_log  
                INNER JOIN tbl_client_companies ON tbl_client_companies.id = tbl_updates_log.company_id
         WHERE tbl_client_companies.id = ".$db->sqs($companyId);

        $result = $db->getAll($sql);
        if($result){
            return $result;
        }else{
            return false;
        }
    }

    /**
     * Method to update database
     * @param array $updateData
     */
    public function runQuickDatabaseUpdate($updateData =  array()){

        global $db;
        global $objCompanies;

        $success = false;

        foreach($updateData['company'] as $key=>$value){

            $companyInfo = $objCompanies->getCompanyInfo($value);
           if($companyInfo){
               $db->selectDatabase($companyInfo['db_name']);
               //$results = $db->query($db->sqs($updateData['script']));
               $db->query('SET FOREIGN_KEY_CHECKS=0');
               foreach (explode(";\n", $updateData['script']) as $sql) {
                   $sql = trim($sql);
                   if ($sql) {
                       $results = $db->query($sql);
                   }
               }
               $db->query('SET FOREIGN_KEY_CHECKS=1');
               if($results){
                   $db->selectDatabase(MASTER_DB);
                   $this->updateLog($updateData['script'],$value);
                   $this->updateLastUpdated($value);
                   $success = true;
               }else{
                   $success = false;
               }
            }
        }


        $db->freeResult();
        $db->selectDatabase(MASTER_DB);
        return $success;

    }


    function updateLog($SQL = null, $companyId = null){
        global $db;

        $AuditSQL = "INSERT INTO tbl_updates_log (
									query_string,
									created_by,
									company_id)
						VALUES(" . $db->sqs($SQL) . ",
						    " . $db->sqs($_SESSION['user_id']) . ",
							   " . $db->sqs($companyId). ")";
        $results = $db->query($AuditSQL);
        if($results){
            return true;
        }else{
            return false;
        }
    }

    function updateLastUpdated($companyId = null)
    {
        global $db;
        $SQL = "UPDATE tbl_client_companies SET last_db_updated = NOW(), modified_by =" . $db->sqs($_SESSION['user_id']) . "
                 WHERE id = " . $db->sqs($companyId);
        $results = $db->query($SQL);
        if ($results) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * install downloaded update/ this replaces old file with new ones
     * @return bool
     */
    function installUpdate2($installData =  array())
    {


        if (!isset($installData['build'])) {
            return false;
        }

        $zip = new ZipArchive;
        if ($zip->open("./file/updates/sheqintel-" . $installData['build'] . "-update.zip") === true) {
            $res = $zip->extractTo('./');
            $zip->close();
        }

    }

    /**
     * downloads database update and updates database
     * @param $build
     * @return bool
     */
    function upgradeDatabase($build)
    {

        global $db;

        if ($file_content) {
            //Get compannies
            /*$db->query('USE ' . $db->database . ';');// company database

            $file_content = file("./file/updates/sheqintel-" . $installData['build'] . "-update.sql")
            foreach (explode(";\n", $file_content) as $sql) {
                $sql = trim($sql);
                if ($sql) {
                    $db->query($sql);
                }
            }*/
        }

        return true;
    }


    private function runQuery($sql_line = null){

        global $db;
        $sql_array = array();
        $query     = "";

        // add ";" at the end of file to catch last sql query
        //if (substr($sql_dump[count($sql_dump) - 1], -1) != ";")
        //{
        //    $sql_dump[count($sql_dump) - 1] .= ";";
      //  }

       // foreach ($sql_dump as $sql_line)
       // {
            $tsl = trim(utf8_decode($sql_line));
            $tsl = trim($sql_line);
            if (($sql_line != "") && (substr($tsl, 0, 2) != "--") && (substr($tsl, 0, 1) != "?") && (substr($tsl, 0, 1) != "#"))
            {
                $query .= $sql_line;
                if (preg_match("/;\s*$/", $sql_line))
                {
                    if (strlen(trim($query)) > 5)
                    {
                        if (!@$db->query($query))
                        {
                            return false;
                        }
                    }
                    $query = "";
                }
            }
       // }
        return true;
    }
}