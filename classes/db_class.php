<?php

/**
 * Base DB class containing all database . All DB interaction must be done via its functions
 * 
 * @package projex
 * @author Sunnyboy Mathole <sunny@innovatorshill.co.za>
 * @copyright (c) 2016, Innovators Hill
 * @license 
 */

class db_class
{
    
    /**
     * DB Configuration variabels
     * 
     * This array needs to have the following parameters defined:
     * username, password, host, database
     *
     * @var array
     * @access private
     */
    private $db_config = array(
            'host' => '',
            'username' => '',
            'password' => '',
            'database' => '',
            'port' => '',
            'socket' => '',
            'html_enc' => true,
            'error_level' => LOG_EMERG, 					// Stores what level this files logs
            'logging_file' => 'C:\xampp\htdocs\logs\PHP_errors.log'	// The file wher everthying gets logged
    );

    /**
     * The current connection to the database
     *
     * @var MySQL link identifier
     * @access private
     */
    private $connection;

    private $queryResult;

    /**
     * Count the number of queries run
     *
     * @var Integer
     * @access public
     */
    public $queries = 0;

    /**
     * Constructor sets up {@link $db_config} 
     *
     * @param array $db_config
     */
    public function __construct ( $db_config ) 
    {
        foreach( $db_config AS $k => $v ) {
                $this->db_config[$k] = $v;
        }
    }

    /**
     * Make a connection to server and database
     *
     * @return boolean
     */
    public function connect()
    {
        $this->connection = @mysqli_connect($this->db_config['host'],$this->db_config['username'],$this->db_config['password'],$this->db_config['database'],$this->db_config['port']);
        if(!$this->connection){
                $this->toLog('connect','Could not connect to DB '.$this->db_config['host'],LOG_EMERG);

                return false;	
        } else {
                $this->toLog('connect','Connected to DB '.$this->db_config['host'],LOG_DEBUG);
        }
        //Change the current db.
        if ( $this->db_config['database'] ) {
            if ( !@mysqli_select_db($this->connection, $this->db_config['database']) ) {
                $this->toLog('connect','Could not select database '.$this->db_config['database'],LOG_ERR);

                mysqli_close($this->connection);
                        return false;
            }
        }else {
                $this->toLog('connect','No database given to select',LOG_ERR);
                return false;
        }

        $this->toLog('connect','Database successfully selected '.$this->db_config['database'],LOG_DEBUG);
        return true;
    }

    /**
     * Method to select database
     * @param $databaseName
     * @return bool
     */
    public function selectDatabase($databaseName = ''){

        if(!@mysqli_select_db($this->connection, $databaseName)){

            echo mysqli_error($this->connection)."<br /><br />";
            echo 'Could not select database '.$databaseName."<br />";
            exit();
            $this->toLog('connect','Could not select database '.$databaseName,LOG_ERR);
            //mysqli_close($this->connection);
            return false;
        }else{
            return true;
        }
    }

    /**
     * Checks to see if there is a connection to the database
     *
     * @return boolean
     */
    public function isConnected() 
    {
        if ( $this->connection ) {
                return true;
        } else {
                return false;
        }
    }

    /**
     * Retrieve a single entry in a table
     *
     * @param String $query
     * @return Boolean | String | Integer
     */
    public function getOne($query)
    {
        $this->queries++;
        $res = mysqli_query($this->connection, $query);
        if (!$res) {

                $this->toLog('getOne',mysqli_error($this->connection),LOG_ERR, $query);
                return false;
        }

        $this->toLog('getOne','Success',LOG_DEBUG, $query);


        $temp = mysqli_fetch_array($res);
        mysqli_free_result($res); 
        return $temp[0];
    }

    /**
     * Set up database
     * @param $file
     * @return bool
     */
	public function setupTables($file)
	{
		if($this->connection)
		{
			if(mysqli_multi_query($this->connection,$file)){

			    return true;
            }else{
			    return false;
            }

		}
		return false;
	}

	public function nexResults(){
        mysqli_next_result($this->connection);
    }
    /**
     * Returns all retrieved records
     *
     * @param String $query
     * @param String $index_col
     * @return Array
     */
    public function getAll($query,$index_col=false)
    {
        $this->queries++;
        $res = mysqli_query($this->connection, $query);
        if (!$res) {
                $this->toLog('getAll',mysqli_error($this->connection),LOG_ERR, $query);
                return false;
        }
        $this->toLog('getAll','Success',LOG_DEBUG, $query);

        $temp_arr = array();
        if($index_col===false){
                while($temp = mysqli_fetch_assoc($res)){
                        $temp_arr[] = $temp;
                }
        } else {
                while($temp = mysqli_fetch_assoc($res)){
                        $temp_arr[$temp[$index_col]] = $temp;
                }
        }
        mysqli_free_result($res); 

        return $temp_arr;
    }

    /**
     * Returns the first row returned from query
     *
     * @param String $query
     * @return Array
     */
    public function getRow($query)
    {
        $this->queries++;
        $res = mysqli_query($this->connection, $query);
        if (!$res) {
                $this->toLog('getRow',mysqli_error($this->connection),LOG_ERR, $query);		           
                return false;
        }

        $this->toLog('getRow','Success',LOG_DEBUG, $query);

        $temp = mysqli_fetch_assoc($res);
        mysqli_free_result($res); 
        return $temp;
    }

    /**
     * Executes a query, returns true if query was successfull else false
     *
     * @param String $query
     * @return Boolean
     */
    public function query($query,$storeResult = false)
    {
        $this->queries++;
        $res = mysqli_query($this->connection, $query);

        if($storeResult){
            $this->queryResult = $res;
        }

        if (!$res) {

                echo mysqli_error($this->connection) ." <br />".$query ."<br />";
                die();
                exit();
                $this->toLog('query',mysqli_error($this->connection),LOG_ERR, $query);
                return false;
        }

        @$this->toLog('query','Success',LOG_DEBUG, $query);

        return true;
    }

    public function fetchRow( $result_type = MYSQL_ASSOC ) 
    {
        return mysqli_fetch_array( $this->queryResult, $result_type );
    }

    public function getRowsReturned() 
    {
        return mysqli_num_rows( $this->queryResult );
    }

    public function freeResult() 
    {
       /* if( isset( $this->queryResult ) ) {
                mysqli_free_result($this->queryResult);
                $this->nexResults();
                $this->queryResult = NULL;
        }
        return true; */

        while (mysqli_more_results($this->connection) && mysqli_next_result($this->connection)) {

            $dummyResult = mysqli_use_result($this->connection);

            if ($dummyResult instanceof mysqli_result) {
                mysqli_free_result($this->connection);
            }
        }
        return true;
    }

    /**
     * Returns the number of effected rows from the previous query
     *
     * @return Integer
     */
    public function affectedRows() 
    {
        return mysqli_affected_rows($this->connection);
    }

    /**
     * Returns the id of the last inserted query
     *
     * @return Integer
     */
    public function insertId() 
    {
        return mysqli_insert_id($this->connection);
    }


    /**
     * start transaction
     * @return bool
     */
    public function startTransaction() {

        return mysqli_autocommit($this->connection,FALSE);
    }

    /**
     *  Starts a Transaction
     * @return bool
     */
    public function commit() {

        $return = mysqli_commit($this->connection);
        mysqli_autocommit($this->connection,TRUE);
        return $return;
    }

    /**
     * Roolback a Transaction ie discard all the changes
     * @return bool
     */
    public function rollBack() {

        $return = mysqli_rollback($this->connection);
        mysqli_autocommit($this->connection,TRUE);
        return $return;
    }


    /**
     * Escape the parsed string to be valid html
     *
     * @param  String | Integer $value
     * @param  Boolean $quote default true Quote the vlaue
     * @param  Boolean $html_smart default true Also html encode it
     * @return String
     */
    public function sqs( $value, $quote=true, $html_enc=false )
    {

        if(( !is_null($html_enc) && $html_enc) || (is_null($html_enc) && $this->db_config['html_enc'])) {
                $value=htmlentities($value);
        }

        if( $quote ){
                if( is_numeric( $value ) ) {
                        return $value;
                } else {
                        return "'".mysqli_real_escape_string($this->connection, $value)."'";
                }
        } else {
                return mysqli_real_escape_string($this->connection, $value);
        }
    }
	
    /**
     * Records the qurey and messages to a log file
     *
     * @param String $func
     * @param String $txt
     * @param Integer $requestedLogLevel
     * @param String $sqlQuery
     */
    private function toLog( $func, $txt, $requestedLogLevel = LOG_NOTICE, $sqlQuery = '' ) 
    {

	if ( $this->db_config['error_level'] == 0 || $requestedLogLevel > $this->db_config['error_level'] ) {
			return;
	}
        
    	$warn_banner = '';
    	switch($requestedLogLevel) {
    		case LOG_EMERG:
    				$warn_banner = 'EMERGENCY';
    			break;
    		case LOG_ALERT:
    				$warn_banner = 'ALERT';
    			break;
    		case LOG_CRIT:
    				$warn_banner = 'CRITICAL';
    			break; 
    		case LOG_ERR:
    				$warn_banner = 'ERROR';
    			break;
    		case LOG_WARNING:
    				$warn_banner = 'WARNING';
    			break;
    		case LOG_NOTICE:
    				$warn_banner = 'NOTICE';
    			break;
    		case LOG_INFO:
    				$warn_banner = 'INFO';
    			break;
    		case LOG_DEBUG:
					$warn_banner = 'DEBUG';
    			break;
    		default:
    				$warn_banner = 'UNKOWN';
    			break;
    	}
    	
    	$msg = (!empty( $sqlQuery ) )?$sqlQuery."\n\t":'';
    	$msg .= $txt; 
    	$txtToLog = '['.date("Ymd H:i:s").']['.$warn_banner.'] ' . $msg . "\n";
    	error_log($txtToLog, 3, $this->db_config['logging_file']);
    }
    
    /**
     * Destruct function
     * 
     */
    public function __destruct()
    {
 	$this->freeResult();
        if ( $this->connection ) {
            mysqli_close( $this->connection );
        }
    }
       
}        