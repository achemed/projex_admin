<?php
/**
 * Class containing all methods company files
 *
 * @package projex
 * @author Brian Douglas <douglas@innovatorshill.co.za>
 * @copyright (c) 2016, Innovators Hill
 * @license
 */
include_once('filemanager.php');
trait company_files
{


    /**
     * Method to update company files
     * @param array $fileInfo
     * @param array $folderInfo
     * @return bool|int
     */
    public function addCompanyFile($fileInfo = array(), $folderInfo = array(),$fileSize = 15)
    {


        global $db;
        $folderId  = null;
        $target_file = null;


        if(!empty($folderInfo)){
            $target_file = $folderInfo['folder_path'];
            $folderId = $folderInfo['id'];
        }

        if($target_file) {

            $upload = Upload::factory($target_file,FOLDER_DIRECTORY_ROOT);
            $upload->file($fileInfo['uploadDoc']);
            //set max. file size (in mb)
            $upload->set_max_file_size($fileSize);
            //set allowed mime types
            $upload->set_allowed_mime_types(array('image/jpeg',
                'image/png',
                'image/gif',
                'image/bmp',
                'image/svg+xml',
                'application/pdf',
                'application/octet-stream',
                'application/msword',
                'application/vnd.ms-excel',
                'application/vnd.openxmlformats-officedocument.wordprocessingml.document'
            ));


            $fileDetailsArr = $upload->upload();

            //insert file details the documents
            //isset($fileDetailsArr['filename']) &&
            if(!empty($fileDetailsArr['errors'])){
                print_r($fileDetailsArr['errors']);
                exit;
            }
            if($fileDetailsArr['filename'] !='') {
                $addDocumentSql = "INSERT INTO tbl_file_documents(
                                                original_filename,	
                                                file_name,
                                                file_extension,
                                                file_size,
                                                local_file_path,
                                                created_by,
                                                modified_by,
                                                folders_id)
                                    VALUES(
                                    " . $db->sqs($fileDetailsArr['original_filename']) . ",
                                     " . $db->sqs($fileDetailsArr['filename']) . ",
                                    " . $db->sqs($fileDetailsArr['extension']) . ",
                                    " . $db->sqs($fileDetailsArr['size_in_mb']) . ",
                                    " . $db->sqs($folderInfo['folder_path']) . ",
                                    " . $db->sqs($_SESSION['user_id']) . ",
                                    " . $db->sqs($_SESSION['user_id']) . ",
                                    " . $db->sqs($folderId) . "
                                    )";

                $docResults = $db->query($addDocumentSql);

                if ($docResults) {
                    return $db->insertId();
                } else {
                    return false;
                }
            }else{
                return false;
            }
        }else{
            return false;
        }

    }

    public function addDifferentNamesCompanyFile($fileInfo = array(), $folderInfo = array(),$fileSize = 15)
    {


        global $db;
        $folderId  = null;
        $target_file = null;


        if(!empty($folderInfo)){
            $target_file = $folderInfo['folder_path'];
            $folderId = $folderInfo['id'];
        }

        if($target_file) {

            $upload = Upload::factory($target_file,FOLDER_DIRECTORY_ROOT);
            $upload->file($fileInfo);
            //set max. file size (in mb)
            $upload->set_max_file_size($fileSize);
            //set allowed mime types
            $upload->set_allowed_mime_types(array('image/jpeg',
                'image/png',
                'image/gif',
                'image/bmp',
                'image/svg+xml',
                'application/pdf',
                'application/octet-stream',
                'application/msword',
                'application/vnd.ms-excel',
                'application/vnd.openxmlformats-officedocument.wordprocessingml.document'
            ));


            $fileDetailsArr = $upload->upload();

            //insert file details the documents
            if($folderInfo['folder_path'] =='' ||  $fileDetailsArr['filename'] ==''){
                return false;
            }
            $addDocumentSql = "INSERT INTO tbl_file_documents(
                                                original_filename,	
                                                file_name,
                                                file_extension,
                                                file_size,
                                                local_file_path,
                                                created_by,
                                                modified_by,
                                                folders_id)
                                    VALUES(
                                    " . $db->sqs($fileDetailsArr['original_filename']) . ",
                                     " . $db->sqs($fileDetailsArr['filename']) . ",
                                    " . $db->sqs($fileDetailsArr['extension']) . ",
                                    " . $db->sqs($fileDetailsArr['size_in_mb']) . ",
                                    " . $db->sqs($folderInfo['folder_path']) . ",
                                    " . $db->sqs($_SESSION['user_id']) . ",
                                    " . $db->sqs($_SESSION['user_id']) . ",
                                    " . $db->sqs($folderId) . "
                                    )";

            $docResults = $db->query($addDocumentSql);

            if ($docResults) {
                return $db->insertId();
            } else {
                return false;
            }
        }else{
            return false;
        }
    }


    /**
     * Method to update company files
     * @param array $fileInfo
     * @param array $folderInfo
     * @param null $angle
     * @return bool
     */
    public function editCompanyFile($fileInfo = array(), $folderInfo = array(),$angle = null)
    {

        global $db;

        if(empty($folderInfo['local_file_path']) && !is_null($angle)) {

            $updateDocumentSql = "UPDATE   tbl_file_documents SET  
                                      angle  = " . $db->sqs($angle) . ",
                                      modified_by  = " . $db->sqs($_SESSION['user_id']) . "
                                  WHERE id = " . $db->sqs($folderInfo['id']);

            $updareResults = $db->query($updateDocumentSql);

            if ($updareResults) {
                return true;
            } else {
                return false;
            }

        }elseif($folderInfo['local_file_path']) {

            unlink(FOLDER_DIRECTORY_ROOT.$folderInfo['local_file_path'].$folderInfo['file_name'].'.'.$folderInfo['file_extension']);

            $upload = Upload::factory($folderInfo['local_file_path'], FOLDER_DIRECTORY_ROOT);
            $upload->file($fileInfo['uploadDoc']);
            //set max. file size (in mb)
            $upload->set_max_file_size(15);
            //set allowed mime types
            $upload->set_allowed_mime_types(array('image/jpeg',
                'image/png',
                'image/gif',
                'image/bmp',
                'image/svg+xml',
                'application/pdf',
                'application/octet-stream',
                'application/msword',
                'application/vnd.ms-excel',
                'application/vnd.openxmlformats-officedocument.wordprocessingml.document'
            ));


            $fileDetailsArr = $upload->upload();

            //update file details the documents
            $updateDocumentSql = "UPDATE   tbl_file_documents SET 
                                                original_filename =  " . $db->sqs($fileDetailsArr['original_filename']) . ",
                                                file_name =  " . $db->sqs($fileDetailsArr['filename']) . ",
                                                file_extension = " . $db->sqs($fileDetailsArr['extension']) . ",
                                                file_size = " . $db->sqs($fileDetailsArr['size_in_mb']) . ",";

            if($angle){
                $updateDocumentSql .=" angle  = " . $db->sqs($angle) . ",";
            }
            $updateDocumentSql .= "  modified_by  = " . $db->sqs($_SESSION['user_id']) . "
                                  WHERE id = ".$db->sqs($folderInfo['id']);

            $updareResults = $db->query($updateDocumentSql);

            if ($updareResults) {
                return true;
            } else {
                return false;
            }
        }elseif($folderInfo['local_file_path'] =='' && $folderInfo['id'] >0) {
            $fileSize = 5;
            if(!empty($folderInfo)){
                $target_file = $folderInfo['folder_path'];
                $folderId = $folderInfo['id'];
            }

            if($target_file) {
                //insert file details the documents
                if ($folderInfo['folder_path'] == '') {
                    return false;
                }
                $upload = Upload::factory($target_file, FOLDER_DIRECTORY_ROOT);
                $upload->file($fileInfo['uploadDoc']);
                //set max. file size (in mb)
                $upload->set_max_file_size(15);
                //set allowed mime types
                $upload->set_allowed_mime_types(array('image/jpeg',
                    'image/png',
                    'image/gif',
                    'image/bmp',
                    'image/svg+xml',
                    'application/pdf',
                    'application/octet-stream',
                    'application/msword',
                    'application/vnd.ms-excel',
                    'application/vnd.openxmlformats-officedocument.wordprocessingml.document'
                ));


                $fileDetailsArr = $upload->upload();

                //update file details the documents
                $updateDocumentSql = "UPDATE   tbl_file_documents SET 
                                                original_filename =  " . $db->sqs($fileDetailsArr['original_filename']) . ",
                                                file_name =  " . $db->sqs($fileDetailsArr['filename']) . ",
                                                file_extension = " . $db->sqs($fileDetailsArr['extension']) . ",
                                                file_size = " . $db->sqs($fileDetailsArr['size_in_mb']) . ",";

                if ($angle) {
                    $updateDocumentSql .= " angle  = " . $db->sqs($angle) . ",";
                }
                $updateDocumentSql .= "  modified_by  = " . $db->sqs($_SESSION['user_id']) . "
                                  WHERE id = " . $db->sqs($folderInfo['id']);

                $updareResults = $db->query($updateDocumentSql);

                if ($updareResults) {
                    return true;
                } else {
                    return false;
                }
            }else{
                return false;
            }
        }else{

            return false;
        }
    }

    /**
     * Method to update company files
     * @param array $fileInfo
     * @param array $folderInfo
     * @param null $angle
     * @return bool
     */
    public function editSpecificCompanyFile($fileInfo = array(), $folderInfo = array(),$angle = null)
    {

        global $db;

        if(empty($folderInfo['local_file_path']) && !is_null($angle)) {

            $updateDocumentSql = "UPDATE   tbl_file_documents SET  
                                      angle  = " . $db->sqs($angle) . ",
                                      modified_by  = " . $db->sqs($_SESSION['user_id']) . "
                                  WHERE id = " . $db->sqs($folderInfo['id']);

            $updareResults = $db->query($updateDocumentSql);

            if ($updareResults) {
                return true;
            } else {
                return false;
            }

        }elseif($folderInfo['local_file_path']) {

            unlink(FOLDER_DIRECTORY_ROOT.$folderInfo['local_file_path'].$folderInfo['file_name'].'.'.$folderInfo['file_extension']);

            $upload = Upload::factory($folderInfo['local_file_path'], FOLDER_DIRECTORY_ROOT);
            $upload->file($fileInfo);
            //set max. file size (in mb)
            $upload->set_max_file_size(15);
            //set allowed mime types
            $upload->set_allowed_mime_types(array('image/jpeg',
                'image/png',
                'image/gif',
                'image/bmp',
                'image/svg+xml',
                'application/pdf',
                'application/octet-stream',
                'application/msword',
                'application/vnd.ms-excel',
                'application/vnd.openxmlformats-officedocument.wordprocessingml.document'
            ));


            $fileDetailsArr = $upload->upload();

            //update file details the documents
            $updateDocumentSql = "UPDATE   tbl_file_documents SET 
                                                original_filename =  " . $db->sqs($fileDetailsArr['original_filename']) . ",
                                                file_name =  " . $db->sqs($fileDetailsArr['filename']) . ",
                                                file_extension = " . $db->sqs($fileDetailsArr['extension']) . ",
                                                file_size = " . $db->sqs($fileDetailsArr['size_in_mb']) . ",";

            if($angle){
                $updateDocumentSql .=" angle  = " . $db->sqs($angle) . ",";
            }
            $updateDocumentSql .= "  modified_by  = " . $db->sqs($_SESSION['user_id']) . "
                                  WHERE id = ".$db->sqs($folderInfo['id']);

            $updareResults = $db->query($updateDocumentSql);

            if ($updareResults) {
                return true;
            } else {
                return false;
            }
        }elseif($folderInfo['local_file_path'] =='' && $folderInfo['id'] >0) {
            $fileSize = 5;
            if(!empty($folderInfo)){
                $target_file = $folderInfo['folder_path'];
                $folderId = $folderInfo['id'];
            }

            if($target_file) {
                //insert file details the documents
                if ($folderInfo['folder_path'] == '') {
                    return false;
                }
                $upload = Upload::factory($target_file, FOLDER_DIRECTORY_ROOT);
                $upload->file($fileInfo);
                //set max. file size (in mb)
                $upload->set_max_file_size(15);
                //set allowed mime types
                $upload->set_allowed_mime_types(array('image/jpeg',
                    'image/png',
                    'image/gif',
                    'image/bmp',
                    'image/svg+xml',
                    'application/pdf',
                    'application/octet-stream',
                    'application/msword',
                    'application/vnd.ms-excel',
                    'application/vnd.openxmlformats-officedocument.wordprocessingml.document'
                ));


                $fileDetailsArr = $upload->upload();

                //update file details the documents
                $updateDocumentSql = "UPDATE   tbl_file_documents SET 
                                                original_filename =  " . $db->sqs($fileDetailsArr['original_filename']) . ",
                                                file_name =  " . $db->sqs($fileDetailsArr['filename']) . ",
                                                file_extension = " . $db->sqs($fileDetailsArr['extension']) . ",
                                                file_size = " . $db->sqs($fileDetailsArr['size_in_mb']) . ",";

                if ($angle) {
                    $updateDocumentSql .= " angle  = " . $db->sqs($angle) . ",";
                }
                $updateDocumentSql .= "  modified_by  = " . $db->sqs($_SESSION['user_id']) . "
                                  WHERE id = " . $db->sqs($folderInfo['id']);

                $updareResults = $db->query($updateDocumentSql);

                if ($updareResults) {
                    return true;
                } else {
                    return false;
                }
            }else{
                return false;
            }
        }else{

            return false;
        }
    }


    /**
     * Method to view
     * @param null $fileId
     * @return array|bool
     */
    public function getCompanyFileInfo($fileId = null){


        global $db;

        $selectedFileSql = "SELECT *
                            FROM tbl_file_documents
                            WHERE id =".$db->sqs($fileId);

        $getFileResult = $db->getRow($selectedFileSql);

        if($getFileResult){
            return $getFileResult;
        }else{
            return false;
        }

    }

    /**
     * Method to get company image
     * @param null $fileExtension
     * @param null $fileSize
     * @return string
     */
    public function getCompanyFileImage($fileExtension = null, $fileSize = null){

        if(is_null($fileSize)){
            $fileSize = 48;
        }

        $iconFilePath = ICON_IMAGE_FOLDER . $fileSize . 'px/' .$fileExtension . '.png';
        $iconUrl   = SITE_IMAGE_PATH . $iconFilePath;

        if(is_null($fileExtension) || $fileExtension ==''){

            return  SITE_IMAGE_PATH .ICON_IMAGE_FOLDER . $fileSize . 'px/_page.png';
            // }
            //elseif(!file_exists($iconUrl) && !is_null($fileExtension)){

            //return SITE_IMAGE_PATH .ICON_IMAGE_FOLDER . $fileSize . 'px/_page.png';

        }else{

            return $iconUrl;
        }


    }

}
