<?php
/**
 * Company class containing all company methods
 *
 * @package projex
 * @author Brian Douglas <douglas@innovatorshill.co.za>
 * @copyright (c) 2016, Innovators Hill
 * @license
 */


class company{

    /**
     * company constructor.
     *Initialize the class
     */
    function  __construct()
    {

    }

    /**
 * @param null $name
 * @param array $clientData
 * @return bool
 */
    public function addClientCompany($name = null,$clientData=array()){

        global $db;
        $sql = "INSERT INTO tbl_client_companies (
                                                db_name,
                                                company_name,
                                                package_id,
                                                created_by
                                          
                            ) VALUES (".$db->sqs($name).","
            .$db->sqs($clientData['company_name']).","
            .$db->sqs($clientData['package_id']).","
            .$db->sqs($_SESSION['user_id']).")";
        $result = $db->query($sql);
        if($result){
            return $db->insertId();
        }else{
            return false;
        }
    }

    /**
     * Get all comapnies
     * @return bool
     */
    public function getAllClientCompanies($isActive = true){

        global $db;
        $sql = "SELECT *  FROM tbl_client_companies WHERE";

        if($isActive){
            $sql .= "  is_active =1";
        }else{
            $sql .= " is_active =0";
        }
        $result = $db->getAll($sql);
        if($result){
            return $result;
        }else{
            return false;
        }
    }

    /**
     * Get all comapnies
     * @return bool
     */
    public function getCompanyInfo($companyId = null){

        global $db;
        $sql = "SELECT *  FROM tbl_client_companies WHERE id = ".$db->sqs($companyId);

        $result = $db->getRow($sql);
        if($result){
            return $result;
        }else{
            return false;
        }
    }





    /**
     * Method to add a new company
     * @param $companyData
     * @param int $createdBy
     * @return bool
     */
    public function addCompany($companyData,$createdBy = 0){

        global $objFolders;
        global $db;

        $parentFolderId = null;
        $success = false;

        $date = new DateTime();
        $dateModified = date_format($date, 'Y-m-d H:i:s');

        if($createdBy == 0){
            $createdBy = $_SESSION["user_id"];
        }
		//$db_name = $db->sqs($companyData['db_name']);
        $addCompanySql = "INSERT INTO tbl_companies ( 
                                                      company_name,
                                                      vat_number,
                                                      company_logo,
                                                      sars_certificate,
                                                      due_dilligence,
                                                      publicliability_insurance,
                                                      pty_ck_documents,
                                                      ohs_legal_compliance_certificate,
                                                      pty_number,
                                                      office_address,
                                                      street,
                                                      suburb,
                                                      town,
                                                      postal_code,
                                                      office_tel_number,
                                                      cell_number,
                                                      fax_number,
                                                      office_email,
                                                      financial_department_email,
                                                      marketing_department_email,
                                                      country_id,
                                                      region_id,
                                                      created_by,
                                                      date_created,
                                                      modified_by,
                                                      date_modified)
                          value (".$db->sqs($companyData['company_name']).",
                           ".$db->sqs($companyData['vat_number']).",
                           ".$db->sqs($companyData['logo']).",
                           null,
                           null,
                           null,
                           null,
                           null,
                           ".$db->sqs($companyData['pty_number']).",
                           ".$db->sqs($companyData['building_name']).",
                           ".$db->sqs($companyData['street']).",
                           ".$db->sqs($companyData['suburb']).",
                           ".$db->sqs($companyData['town']).",
                           ".$db->sqs($companyData['postalCode']).",
                           ".$db->sqs($companyData['office_number']).",
                           ".$db->sqs($companyData['cell_number']).",
                           ".$db->sqs($companyData['fax_number']).",
                           ".$db->sqs($companyData['email']).",
                           null,
                           null,
                           ".$db->sqs($companyData['country_id']).",
                           ".$db->sqs($companyData['region_id']).",
                           ".$db->sqs($createdBy).",
                           ".$db->sqs($dateModified).",
                           ".$db->sqs($createdBy).",
                           ".$db->sqs($dateModified).")";


        $addCompanyResults = $db->query($addCompanySql);

        if($addCompanyResults){
/*
            $branchData = array();
            $branchData['companyId'] = $db->insertId();
            $branchData['branchName'] = $companyData['headOfficeName'];
            $branchData['building_name'] = $companyData['building_name'];
            $branchData['street'] = $companyData['street'];
            $branchData['suburb'] = $companyData['suburb'];
            $branchData['town'] = $companyData['town'];
            $branchData['postalCode'] = $companyData['postalCode'];

            $branchData['officeNumber'] = $companyData['office_number'];
            $branchData['faxNumber'] = $companyData['fax_number'];
            $branchData['officeEmail'] = $companyData['email'];
            $branchData['isHeadOffice'] = 1;

            $branchData['countryId'] = $companyData['country_id'];
            $branchData['regionId'] = $companyData['region_id'];
            $branchData['createdBy'] = $createdBy;


            //add company folder
            $folderData = array(
                "folderName"=>$companyData['company_name'],
                "parentId" =>"NULL",
                "companyId" =>$branchData['companyId'],
                "userId"=>$createdBy
            );

            $parentFolderId = $objFolders->addNewFolder($folderData);

            $success = $objFolders->createFolderDirectory($folderData,$parentFolderId);

            if(!is_null($parentFolderId)){
                     $branchData['parentId'] = $parentFolderId;
             }

            if($this->addBranch($branchData)){
                $success = true;
            }
*/
            return $db->insertId();
        }else{
            return false;
        }

    }



    /**
     * Method to add a new branch
     * @param array $branchData
     * @return bool
     */
    public function addBranch($branchData = array()){

        global $db;


        $date = new DateTime();
        $dateModified = date_format($date, 'Y-m-d H:i:s');

        $addBranchSql = "INSERT INTO tbl_branches (
                                                company_id,	    
												branch_name	,    
												branch_address,
												street,
                                                suburb,
                                                town,
                                                postal_code,
												branch_tel_number,    
												branch_fax_number,	    
												branch_email,	    
												is_head_office,
												country_id,	
												region_id,
												created_by,	    
												date_created,	    
												modified_by,	    
												date_modified)
                             value (
                                    ".$db->sqs($branchData['companyId']).",
                                    ".$db->sqs($branchData['branchName']).",
                                    ".$db->sqs($branchData['building_name']).",
                                    ".$db->sqs($branchData['street']).",
                                    ".$db->sqs($branchData['suburb']).",
                                    ".$db->sqs($branchData['town']).",
                                    ".$db->sqs($branchData['postalCode']).",
                                    ".$db->sqs($branchData['officeNumber']).",
                                    ".$db->sqs($branchData['faxNumber']).",
                                    ".$db->sqs($branchData['officeEmail']).",
                                    ".$db->sqs($branchData['isHeadOffice']).",
                                    ".$db->sqs($branchData['countryId']).",
                                    ".$db->sqs($branchData['regionId']).",
                                    ".$db->sqs($branchData['createdBy']).",
                                    ".$db->sqs($dateModified).",
                                    ".$db->sqs($branchData['createdBy']).",
                                    ".$db->sqs($dateModified).
            ")";

        $addBranchResults = $db->query($addBranchSql);

        if($addBranchResults){
            $branchId =  $db->insertId();

            $folderData = array(
                //"db_name"=>$branchData['db_name'],
                "folderName"=>$branchData['branchName'],
                "parentId" => $branchData['parentId'],
                "companyId" =>$branchData['companyId'],
                "branchId" =>$branchId,
                "created_by"=>$branchData['createdBy'],
                "modified_by"=>$branchData['createdBy']
            );
            if($this->createBranchFolders($folderData)){

                return true;
            }else{

                return false;
            }
        }else{
            return false;
        }
    }

    /**
     * Method to add branch folders
     * @param array $folderData
     * @return bool|int
     */
    public  function createBranchFolders($folderData = array()){

        global $objFolders;
        $success = false;
        $parentFolderId = null;
        $companyFolderId = null;

       // $db_name = $folderData['db_name'];

        $companyFolderId = $objFolders->getCompanyFolderId($folderData['companyId']);

        if(!is_null($companyFolderId)) {
            $folderData['parentId'] = $companyFolderId;
            //second create branch folder
            $parentFolderId = $objFolders->addNewFolder($folderData);

            $success = $objFolders->createFolderDirectory($folderData,$parentFolderId,$folderData['parentId']);

            //create branch sub folders
            if (!is_null($parentFolderId)) {

                $mainFolderGroup = array(
                    "Documents",
                    "Employees",
                    "Suppliers",
                    "Resources",
                    "SHEQTeams",
                    "Inspections",
                    "Logo",
                    "Training",
                    "Non-Conformance");

                $defaultGroupFolders = array("OHS (Health AND Safety)", "E (Environment)", "Q (Quality)", "F (Food)");

                $defaultTypesFolders = array("Policy Statements", "Policy", "Procedures", "Instructions (SHEQ)", "Appointments","Forms-Checklists (SHEQ)","Forms-Registers");

                $defaultResourceCategoriesFolders = array("Fixed Assets", "Movable Assets", "Raw Materials", "Final Products","Consumables","Emergency Equipment","Motorized Equipment");


                //add main folders
                foreach ($mainFolderGroup as $key => $mainFolderName) {

                    if($mainFolderName == "Documents"){
                        //create document folders
                        $folderGroupData = array(
                           // "db_name"=>$db_name,
                            "folderName" => $mainFolderName,
                            "parentId" => $parentFolderId,
                            "companyId" => $folderData['companyId'],
                            "branchId" => $folderData['branchId'],
                            "userId" => $folderData['createdBy'],
                            "modified_by" => $folderData['createdBy']
                        );

                        $parentMainFolderId = $objFolders->addNewFolder($folderGroupData);

                        $success = $objFolders->createFolderDirectory($folderGroupData,$parentMainFolderId,$parentFolderId);
                        //add a group
                        foreach ($defaultGroupFolders as $key => $groupFolderName) {

                            $folderGroupData = array(
                                //"db_name"=>$db_name,
                                "folderName" => $groupFolderName,
                                "parentId" => $parentMainFolderId,
                                "companyId" => $folderData['companyId'],
                                "branchId" => $folderData['branchId'],
                                "userId" => $folderData['createdBy'],
                                "modified_by" => $folderData['createdBy']
                            );

                            $parentGroupFolderId = $objFolders->addNewFolder($folderGroupData);

                            $success = $objFolders->createFolderDirectory($folderGroupData,$parentGroupFolderId,$parentMainFolderId);

                            if ($parentGroupFolderId){
                                foreach ($defaultTypesFolders as $key => $value) {

                                    $folderData = array(
                                       // "db_name"=>$db_name,
                                        "folderName" => $value,
                                        "parentId" => $parentGroupFolderId,
                                        "companyId" => $folderGroupData['companyId'],
                                        "branchId" => $folderGroupData['branchId'],
                                        "userId" => $folderGroupData['createdBy'],
                                        "modified_by" => $folderGroupData['createdBy']
                                    );

                                    $folderId = $objFolders->addNewFolder($folderData);

                                    $success = $objFolders->createFolderDirectory($folderData,$folderId,$parentGroupFolderId);
                                }
                            }
                        }

                    }elseif($mainFolderName == "SHEQTeams"){

                        //create sheqteam folder folders
                        $folderGroupData = array(
                            //"db_name"=>$db_name,
                            "folderName" => $mainFolderName,
                            "parentId" => $parentFolderId,
                            "companyId" => $folderData['companyId'],
                            "branchId" => $folderData['branchId'],
                            "userId" => $folderData['createdBy'],
                            "modified_by" => $folderData['createdBy']
                        );

                        $parentMainFolderId = $objFolders->addNewFolder($folderGroupData);

                        $success = $objFolders->createFolderDirectory($folderGroupData,$parentMainFolderId,$parentFolderId);

                        //add a group
                        foreach ($defaultGroupFolders as $key => $groupFolderName) {

                            $folderGroupData = array(
                                //"db_name"=>$db_name,
                                "folderName" => $groupFolderName,
                                "parentId" => $parentMainFolderId,
                                "companyId" => $folderData['companyId'],
                                "branchId" => $folderData['branchId'],
                                "userId" => $folderData['createdBy'],
                                "modified_by" => $folderData['createdBy']
                            );

                            $parentGroupFolderId = $objFolders->addNewFolder($folderGroupData);

                            $success = $objFolders->createFolderDirectory($folderGroupData,$parentGroupFolderId,$parentMainFolderId);

                        }

                    }elseif($mainFolderName == "Resources"){
                        //create resources folder folders

                        $folderGroupData = array(
                            //"db_name"=>$db_name,
                            "folderName" => $mainFolderName,
                            "parentId" => $parentFolderId,
                            "companyId" => $folderData['companyId'],
                            "branchId" => $folderData['branchId'],
                            "userId" => $folderData['createdBy'],
                            "modified_by" => $folderData['createdBy']
                        );

                        $parentMainFolderId = $objFolders->addNewFolder($folderGroupData);

                        $success = $objFolders->createFolderDirectory($folderGroupData,$parentMainFolderId,$parentFolderId);
                        //add a category
                        foreach ($defaultResourceCategoriesFolders as $key => $categoryFolderName) {

                            $folderGroupData = array(
                               // "db_name"=>$db_name,
                                "folderName" => $categoryFolderName,
                                "parentId" => $parentMainFolderId,
                                "companyId" => $folderData['companyId'],
                                "branchId" => $folderData['branchId'],
                                "userId" => $folderData['createdBy'],
                                "modified_by" => $folderData['createdBy']
                            );

                            $parentCategoryFolderId = $objFolders->addNewFolder($folderGroupData);
                            $success = $objFolders->createFolderDirectory($folderGroupData,$parentCategoryFolderId,$parentMainFolderId);

                        }

                    }elseif($mainFolderName == "Inspections") {

                        //create Inspections folder folders
                        $folderGroupData = array(
                           // "db_name"=>$db_name,
                            "folderName" => $mainFolderName,
                            "parentId" => $parentFolderId,
                            "companyId" => $folderData['companyId'],
                            "branchId" => $folderData['branchId'],
                            "userId" => $folderData['createdBy'],
                            "modified_by" => $folderData['createdBy']
                        );

                        $parentMainFolderId = $objFolders->addNewFolder($folderGroupData);

                        $success = $objFolders->createFolderDirectory($folderGroupData,$parentMainFolderId,$parentFolderId);

                        //add a group
                        foreach ($defaultGroupFolders as $key => $groupFolderName) {

                            $folderGroupData = array(
                                //"db_name"=>$db_name,
                                "folderName" => $groupFolderName,
                                "parentId" => $parentMainFolderId,
                                "companyId" => $folderData['companyId'],
                                "branchId" => $folderData['branchId'],
                                "userId" => $folderData['createdBy'],
                                "modified_by" => $folderData['createdBy']
                            );

                            $parentGroupFolderId = $objFolders->addNewFolder($folderGroupData);

                            $success = $objFolders->createFolderDirectory($folderGroupData,$parentGroupFolderId,$parentMainFolderId);

                        }

                    }elseif($mainFolderName == "Logo") {

                        //create Inspections folder folders
                        $folderGroupData = array(
                            //"db_name"=>$db_name,
                            "folderName" => $mainFolderName,
                            "parentId" => $parentFolderId,
                            "companyId" => $folderData['companyId'],
                            "branchId" => $folderData['branchId'],
                            "userId" => $folderData['createdBy'],
                            "modified_by" => $folderData['createdBy'],
                            "is_hidden" => 1
                        );

                        $parentMainFolderId = $objFolders->addNewFolder($folderGroupData);
                        $success = $objFolders->createFolderDirectory($folderGroupData,$parentMainFolderId,$parentFolderId);

                    }else{
                        //Create main folders
                        $folderGroupData = array(
                            //"db_name"=>$db_name,
                            "folderName" => $mainFolderName,
                            "parentId" => $parentFolderId,
                            "companyId" => $folderData['companyId'],
                            "branchId" => $folderData['branchId'],
                            "userId" => $folderData['createdBy'],
                            "modified_by" => $folderData['createdBy']
                        );

                        $parentMainFolderId = $objFolders->addNewFolder($folderGroupData);

                        $success = $objFolders->createFolderDirectory($folderGroupData,$parentMainFolderId,$parentFolderId);

                    }

                }

                return $success;
            }
            return $success;
        }
        return $success;
    }




    /**
     * Method to update company info
     * @param $companyData
     * @param int $modifiedBy
     * @return bool
     */
    public function editCompany($companyData,$modifiedBy = 0){

        global $db;
        global $objFolders;

        $success = false;

        $date = new DateTime();
        $dateModified =  date_format($date, 'Y-m-d H:i:s');

        if($modifiedBy == 0){
            $modifiedBy = $_SESSION["user_id"];
        }

        $editCompanyInfoSql = "UPDATE tbl_companies SET 
                                                      company_name = ".$db->sqs($companyData['company_name']).",
                                                      vat_number = ".$db->sqs($companyData['vat_number']).",
                                                      company_logo = ".$db->sqs($companyData['logo']).",
                                                      sars_certificate = '',
                                                      due_dilligence = '',
                                                      publicliability_insurance = '',
                                                      pty_ck_documents = '',
                                                      ohs_legal_compliance_certificate = '',
                                                      pty_number = ".$db->sqs($companyData['pty_number']).",
                                                      office_address = ".$db->sqs($companyData['building_name']).",
                                                      street = ".$db->sqs($companyData['street']).",
                                                      suburb = ".$db->sqs($companyData['suburb']).",
                                                      town = ".$db->sqs($companyData['town']).",
                                                      postal_code = ".$db->sqs($companyData['postalCode']).",
                                                      office_tel_number =".$db->sqs($companyData['office_number']).",
                                                      cell_number = ".$db->sqs($companyData['cell_number']).",
                                                      fax_number =".$db->sqs($companyData['fax_number']).",
                                                      office_email = ".$db->sqs($companyData['email']).",
                                                      financial_department_email = '',
                                                      marketing_department_email = '',
                                                      country_id = ".$db->sqs($companyData['country_id']).",
                                                      region_id = ".$db->sqs($companyData['region_id']).",
                                                      modified_by=".$db->sqs($modifiedBy).",
                                                      date_modified=".$db->sqs($dateModified)."

                                                    WHERE id=".$db->sqs($companyData['company_id']);

        $editCompanyInfoResults = $db->query($editCompanyInfoSql);

        if($editCompanyInfoResults){
            return true;
        }else{
            return false;
        }



    }

}