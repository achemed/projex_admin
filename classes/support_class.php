<?php
/**
 * Created by PhpStorm.
 * User: Douglas
 * Date: 30/11/2018
 * Time: 1:34 PM
 */
class support_class
{

    public function getAllTickets(){

        global $db;
        $sql = "SELECT * FROM tbl_support ";
        $response = $db->getAll($sql);
        if($response){
            return $response;
        }else{
            return false;
        }
    }

    public function replySupportTicket($data = array()){
        global $db;
        $addOrderSql = "UPDATE ".$data['db_name'].".tbl_support SET
                                                  `status_id` = 2,
                                                  `response`=".$db->sqs($data['description'])."
                                                  WHERE id = ".$db->sqs($data['support_id']);
        $results = $db->query($addOrderSql);
        if($results){

            $addOrderSql = "UPDATE tbl_support SET 
                                                  `status_id` = 2,
                                                  `response`=".$db->sqs($data['description'])."
                                                  WHERE client_support_id = ".$db->sqs($data['support_id']);
            $results = $db->query($addOrderSql);
            return true;
        }else{
            return false;
        }
    }
}