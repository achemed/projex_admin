<?php

/**
 * Class containing all methods providing asset functionality
 * 
 * @package projex
 * @author Sunnyboy Mathole <sunny@innovatorshill.co.za>
 * @copyright (c) 2016, Innovators Hill
 * @license 
 */
trait sheqclub_main_types_class {
    /**
     * Method to add asset category
     * @access public
     * @global $db
     * @param string $name
     * @param string $description
     */
    public function addSheqClubMainType($dataUser = array())
    {
        global $db;
        if(isset($dataUser)){
            $sql = "INSERT INTO tbl_sheqclub_main_types (sheqclub_main_id,sheqclub_section,type_name,created_by) VALUES (".
            $db->sqs($dataUser["sheqclub_main_id"]).",".
			$db->sqs($dataUser["sheqclub_section"]).",".
            $db->sqs($dataUser["type_name"]).",". $db->sqs($_SESSION['user_id']) . ")";
            $response = $db->query($sql);
            if($response){
                return true;
            }else{
                return false;
            }  
        } else {
            return false;
        }
    }
    
    /**
     * Method to edit asset category
     * @access public
     * @global $db
     * @param int $id
     * @param string $name
     * @param string $description
     */
	  public function editSheqClubMainType($dataUser = array())
    {
        global $db;
        if(isset($dataUser)){
            $sql = "UPDATE tbl_sheqclub_main_types SET
			type_name = " . $db->sqs($dataUser["type_name"]). ",			
			modified_by = " . $db->sqs($_SESSION['user_id']). " WHERE id = ".$db->sqs($dataUser["id"]);
            $response = $db->query($sql);
            if($response){
                return $response;
            }else{
                return false;
            }  
        } else {
            return false;
        }
    }
    public function editSheqClubMainTypeStatus($id,$status)
    {
        global $db;
        if(isset($id)){
            $sql = "UPDATE tbl_sheqclub_main_types SET is_published = " . $db->sqs($status). ", 
            modified_by = " . $db->sqs($_SESSION['user_id']). " WHERE id = ".$db->sqs($id);
            $response = $db->query($sql);
            if($response){
                return $response;
            }else{
                return false;
            }  
        } else {
            return false;
        }
    }
    
	
   /**
     * Method to activate asset category
     * @access public
     * @global $db
     * @param int $id
     */
    public function activateSheqClubMainType($id)
    {
        global $db;
        if(!is_null($id)){
            $sql = "UPDATE tbl_sheqclub_main_types SET is_active = 1 WHERE id = ".$db->sqs($id);
            $result = $db->query($sql);
            if($result){
                return true;
            } else {
                return false;
            }            
        } else {
            return "Required information is missing";
        }  
    }
    /**
     * Method to deactivate asset category
     * @access public
     * @global $db
     * @param int $id
     */
    public function deactivateSheqClubMainType($id)
    {
        global $db;
        if(!is_null($id)){
            $sql = "UPDATE tbl_sheqclub_main_types SET is_active = 0 WHERE id = ".$db->sqs($id);
            $result = $db->query($sql);
            if($result){
                return true;
            } else {
                return false;
            }            
        } else {
            return "Required information is missing";
        }
    }
    /**
     * Method to get all asset category
     * @access public
     * @global $db
     * @param boolean $activeOnly
     */

    public function getSheqClubMainTypeInfo($id)
    {
        global $db;
        $sql = "SELECT * FROM tbl_sheqclub_main_types WHERE is_active = 1 and id = ".$db->sqs($id);
       
        $response = $db->getRow($sql);
        if($response) {
            return $response;
        } else {
            return false;
        }
    } 

    public function getAllSheqClubMainType($activeOnly = true,$sheqclub_section = null,$sheqclub_main_id = null)
    {
        global $db;
        $sql = "SELECT * FROM tbl_sheqclub_main_types WHERE ";
        if($activeOnly){
            $sql .= " is_active = 1 ";
        }else{
            $sql .= " is_active = 0 ";
        }

        if(!empty($sheqclub_section))
        {
            $sql .= " and sheqclub_section = ".$db->sqs($sheqclub_section);
        }
        if(!empty($sheqclub_main_id))
        {
            $sql .= " and sheqclub_main_id = ".$db->sqs($sheqclub_main_id);
        }

        $response = $db->getAll($sql);        
        if($response) {
            return $response;
        } else {
            return false;
        }
    } 
}
