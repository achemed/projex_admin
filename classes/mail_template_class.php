<?php

/**
 * Class containing all package functionality
 *
 * @package projex
 * @author Brian Douglas<douglas@innovatorshill.co.za>
 * @copyright (c) 2016, Innovators Hill
 * @license
 */

class mail_template_class
{

    /**
     * Method to initiate the class
     *
     * @access public
     */
    public function __construct()
    {

    }

    /**
     * @return array|bool
     */
    public function getAllTemplates(){
        global $db;
        $sql = "SELECT *  FROM tbl_mail_template ";
        $result = $db->getAll($sql);
        if($result){
            return $result;
        }else{
            return false;
        }
    }

    /**
     * @param null $packageId
     * @return bool
     */
    public function getTemplateInfo($packageId = null){
        global $db;
        $sql = "SELECT *  FROM tbl_mail_template  WHERE id=".$db->sqs($packageId);
        $result = $db->getRow($sql);
        if($result){
            return $result;
        }else{
            return false;
        }
    }

    /**
     * @param array $data
     * @return bool
     */
    public function addTemplate($data = array())
    {
        global $db;

        $addPackageSql = "INSERT INTO tbl_mail_template (
                                                template_name,	    
												description,
												content
												)
                             value (
                                    ".$db->sqs($data['template_name']).",
                                    ".$db->sqs($data['description']).",
                                    ".$db->sqs($data['content']).")";

        $results = $db->query($addPackageSql);

        if($results){
            return true;
        }else{
            return false;
        }
    }

    /**
     * @param array $data
     * @return bool
     */
    public function editTemplate($data =  array()){
        global $db;

        $upPackageSql = "UPDATE tbl_mail_template SET 
												content=".$db->sqs($data['content'])."
												WHERE id = ".$db->sqs($data['template_id']);

        $results = $db->query($upPackageSql);

        if($results){
            return true;
        }else{
            return false;
        }

    }



}