<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of actions_class
 * Emmanuel van der Westhuizen
 * @author Innovatorshill
 */
class actions
{
    //put your code here
    public function __construct() {
        
    }
    public static function hasCreatedAction($data = array())
    {
        global $db;
        $status = false;
        $sql = "insert into tbl_system_actions(source_id,source_type,action,action_taken,action_taken_id,performed_by,branch_id,company_id,created_by)"
                . "VALUES(".$db->sqs($data['source_id']).","
                .$db->sqs($data['source_type']).","
                .$db->sqs($data['action']).","
                .$db->sqs($data['action_taken']).","
                .$db->sqs($data['action_taken_id'])."," .$db->sqs($data['performed_by']).","                 
                .$db->sqs($_SESSION['branch_id']).","
                .$db->sqs($_SESSION['company_id']).","
                .$db->sqs($_SESSION['user_id']).")";
        
       $createActionResult = $db->query($sql);
       if($createActionResult == true)
       {
            $status = true;
       }    
       return $status;
    }

    //update action status
   public static function updateActionStatus($data = array())
    {
        global $db;
        $status = false;
        $sql = "update tbl_system_actions set status = ".$db->sqs($data['status']).", modified_by = ".$db->sqs($_SESSION['user_id'])." "
                . " where id = ".$db->sqs($data['id'])." and company_id = ".$db->sqs($_SESSION['company_id']);                        
       $updateResult = $db->query($sql);
       if($updateResult == true)
       {
            $status = true;
       }    
       return $status;
    }
    //signs of action
   public static function signOffAction($data = array())
   {
        global $db;
        $status = false;
        $sql = "update tbl_system_actions set action_signed_off = 1, action_signed_by = ".$db->sqs($_SESSION['user_id'])." "
                . " where id = ".$db->sqs($data['id'])." and company_id = ".$db->sqs($_SESSION['company_id']);                        
       $updateResult = $db->query($sql);
       if($updateResult == true)
       {
            $status = true;
       }    
       return $status;
    }
    public static function viewActionsBySourceId($source)
    {
        global $db;
        $sql = "select DATE_FORMAT(tbl_system_actions.date_created,'%m/%d/%Y') as dt,tbl_system_actions.* from tbl_system_actions where source_id = ".$db->sqs($source)." and company_id = ".$db->sqs($_SESSION['company_id']); 
        $viewResult = $db->getAll($sql);
        return $viewResult;
    }
    ///////////////////////////////////Notifications//////////////////////////////////////////////////////////
    public static function getMailingList($list_type)
    {
         global $db;    
         switch($list_type)
         {
             case "inbox":
                 $sql = "SELECT * from tbl_jobs_list where recipient = ".$db->sqs($_SESSION["user_id"])." and company_id = ".$db->sqs($_SESSION["company_id"]);
                 break;
             case "outbox":
                 $sql = "SELECT * from tbl_jobs_list where sender = ".$db->sqs($_SESSION["user_id"])." and company_id = ".$db->sqs($_SESSION["company_id"]);
                 break;
             case "trash":
                 $sql = "SELECT * from tbl_jobs_list where (recipient = ".$db->sqs($_SESSION["user_id"]).") or (sender = ".$db->sqs($_SESSION["user_id"]).") "
                     . "and is_active = 0 and company_id = ".$db->sqs($_SESSION["company_id"]);
                 break;
        }        
        $response = $db->getAll($sql);   
        //echo $sql;
        if($response){
            return $response;
        }else{
            return false;
        }  
    }
    public static function addToRequestList($data)
    {
        global $db;
        $status = false;
        $sql = "insert into tbl_jobs_list(source_id,source_name,sender,recipient,status,subject,branch_id,company_id,created_by)"
                . "VALUES(".$db->sqs($data['source_id']).","
                .$db->sqs($data['source_name']).","
                .$db->sqs($_SESSION['user_id']).","
                .$db->sqs($data['recipient']).","
                .$db->sqs($data['subject']).","
                .$db->sqs($data['status']).","                
                .$db->sqs($_SESSION['branch_id']).","
                .$db->sqs($_SESSION['company_id']).","
                .$db->sqs($_SESSION['user_id']).")";        
       $createActionResult = $db->query($sql);
       if($createActionResult == true)
       {
            $status = true;
       }    
       return $status;

    }
}
