<?php

/**
 * Class containing all methods providing asset functionality
 * 
 * @package projex
 * @author Sunnyboy Mathole <sunny@innovatorshill.co.za>
 * @copyright (c) 2016, Innovators Hill
 * @license 
 */
trait sheq_daily_safety_topic_description_class {
    /**
     * Method to add asset category
     * @access public
     * @global $db
     * @param string $name
     * @param string $description
     */
    public function addDailySafetyTopicDescription($dataUser = array())
    {
        global $db;
        if(isset($dataUser)){
            $sql = "INSERT INTO tbl_daily_safety_topics_description (topic_id, notes,created_by) VALUES (". $db->sqs($dataUser["topic_id"]) .",". $db->sqs($dataUser["notes"]) .",".$db->sqs($_SESSION['user_id']) . ")";
            $response = $db->query($sql);
            if($response){
                return true;
            }else{
                return false;
            }  
        } else {
            return false;
        }
    }
    
    /**
     * Method to edit asset category
     * @access public
     * @global $db
     * @param int $id
     * @param string $name
     * @param string $description
     */
	  public function editDailySafetyTopicDescription($dataUser = array())
    {
        global $db;
        if(isset($dataUser)){
            $sql = "UPDATE tbl_daily_safety_topics_description SET notes = " . $db->sqs($dataUser["notes"]). ",	
			modified_by = " . $db->sqs($_SESSION['user_id']). " WHERE topic_id = ".$db->sqs($dataUser["topic_id"]);
            $response = $db->query($sql);
            if($response){
                return $response;
            }else{
                return false;
            }  
        } else {
            return false;
        }
    }

    
   /**
     * Method to activate asset category
     * @access public
     * @global $db
     * @param int $id
     */
    public function activateDailySafetyTopicDescription($id)
    {
        global $db;
        if(!is_null($id)){
            $sql = "UPDATE tbl_daily_safety_topics_description SET is_active = 1 WHERE id = ".$db->sqs($id);
            $result = $db->query($sql);
            if($result){
                return true;
            } else {
                return false;
            }            
        } else {
            return "Required information is missing";
        }  
    }
    /**
     * Method to deactivate asset category
     * @access public
     * @global $db
     * @param int $id
     */
    public function deactivateDailySafetyTopicDescription($id)
    {
        global $db;
        if(!is_null($id)){
            $sql = "UPDATE tbl_daily_safety_topics_description SET is_active = 0 WHERE id = ".$db->sqs($id);
            $result = $db->query($sql);
            if($result){
                return true;
            } else {
                return false;
            }            
        } else {
            return "Required information is missing";
        }
    }
    /**
     * Method to get all asset category
     * @access public
     * @global $db
     * @param boolean $activeOnly
     */

    public function getDailySafetyTopicDescriptionInfo($id)
    {
        global $db;
        $sql = "SELECT * FROM tbl_daily_safety_topics_description WHERE is_active = 1 and id = ".$db->sqs($id);
       
        $response = $db->getRow($sql);
        if($response) {
            return $response;
        } else {
            return false;
        }
    } 

    public function getAllDailySafetyTopicDescription($topic_id = null,$activeOnly = true)
    {
        global $db;
        $sql = "SELECT * FROM tbl_daily_safety_topics_description WHERE ";
        if($activeOnly){
            $sql .= " is_active = 1 ";
        }else{
            $sql .= " is_active = 0 ";
        }

        $sql .= " and topic_id = ".$db->sqs($topic_id);

        $response = $db->getRow($sql);
        if($response) {
            return $response;
        } else {
            return false;
        }
    } 
}
