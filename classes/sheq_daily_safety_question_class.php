<?php

/**
 * Class containing all methods providing asset functionality
 * 
 * @package projex
 * @author Sunnyboy Mathole <sunny@innovatorshill.co.za>
 * @copyright (c) 2016, Innovators Hill
 * @license 
 */
trait sheq_daily_safety_question_class {
    /**
     * Method to add asset category
     * @access public
     * @global $db
     * @param string $name
     * @param string $description
     */
    public function addDailySafetyTopicQuestion($dataUser = array())
    {
        global $db;
        if(isset($dataUser)){
            $sql = "INSERT INTO tbl_daily_safety_topics_questions (topic_id,question, answer,created_by) VALUES (". $db->sqs($dataUser["topic_id"]) . ",". $db->sqs($dataUser["question"]) . ",". $db->sqs($dataUser["answer"]) .",".$db->sqs($_SESSION['user_id']) . ")";
            $response = $db->query($sql);
            if($response){
                return true;
            }else{
                return false;
            }  
        } else {
            return false;
        }
    }
    
    /**
     * Method to edit asset category
     * @access public
     * @global $db
     * @param int $id
     * @param string $name
     * @param string $description
     */
	  public function editDailySafetyTopicQuestion($dataUser = array())
    {
        global $db;
        if(isset($dataUser)){
            $sql = "UPDATE tbl_daily_safety_topics_questions SET question = " . $db->sqs($dataUser["question"]). ",
			answer = " . $db->sqs($dataUser["answer"]). ",			
			modified_by = " . $db->sqs($_SESSION['user_id']). " WHERE id = ".$db->sqs($dataUser["id"]);
            $response = $db->query($sql);
            if($response){
                return $response;
            }else{
                return false;
            }  
        } else {
            return false;
        }
    }
	
   /**
     * Method to activate asset category
     * @access public
     * @global $db
     * @param int $id
     */
    public function activateDailySafetyTopicQuestion($id)
    {
        global $db;
        if(!is_null($id)){
            $sql = "UPDATE tbl_daily_safety_topics_questions SET is_active = 1 WHERE id = ".$db->sqs($id);
            $result = $db->query($sql);
            if($result){
                return true;
            } else {
                return false;
            }            
        } else {
            return "Required information is missing";
        }  
    }
    /**
     * Method to deactivate asset category
     * @access public
     * @global $db
     * @param int $id
     */
    public function deactivateDailySafetyTopicQuestion($id)
    {
        global $db;
        if(!is_null($id)){
            $sql = "UPDATE tbl_daily_safety_topics_questions SET is_active = 0 WHERE id = ".$db->sqs($id);
            $result = $db->query($sql);
            if($result){
                return true;
            } else {
                return false;
            }            
        } else {
            return "Required information is missing";
        }
    }
    /**
     * Method to get all asset category
     * @access public
     * @global $db
     * @param boolean $activeOnly
     */

    public function getDailySafetyTopicQuestionInfo($id)
    {
        global $db;
        $sql = "SELECT * FROM tbl_daily_safety_topics_questions WHERE is_active = 1 and id = ".$db->sqs($id);
       
        $response = $db->getRow($sql);
        if($response) {
            return $response;
        } else {
            return false;
        }
    } 

    public function getAllDailySafetyTopicQuestion($topic_id = null,$activeOnly = true)
    {
        global $db;
        $sql = "SELECT * FROM tbl_daily_safety_topics_questions WHERE ";
        if($activeOnly){
            $sql .= " is_active = 1 ";
        }else{
            $sql .= " is_active = 0 ";
        }

        $sql .= " and topic_id = ".$db->sqs($topic_id);

        $response = $db->getAll($sql);
        if($response) {
            return $response;
        } else {
            return false;
        }
    } 
}
