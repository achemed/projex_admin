<?php
/**
 * Folder class containing all Folder methods
 *
 * @package projex
 * @author Brian Douglas <douglas@innovatorshill.co.za>
 * @copyright (c) 2016, Innovators Hill
 * @license
 */

class folders_class
{


    /**
     * Initialise folder class
     * folder  constructor.
     */
    function __construct()
    {

    }



    /**
     * @param null $folderId
     * @param null $companyId
     * @return array|bool
     */
    public function getFoldersAndFiles($folderId  = null, $companyId = null,$branchId = null){
        global $db;


        $this->autoCreateFolders($companyId,$branchId);
        $clause = "";
        if(isset($folderId) && ($folderId != -1))
        {
            $clause = 'tbl_document_folders.parent_id = '.$db->sqs($folderId);
        }else{
            $clause = 'tbl_document_folders.parent_id IS NULL';
        }

        $allFolderFiles = "SELECT tbl_document_folders.id, folder_name, 
                                    (SELECT COUNT(document_folders_child.id) AS total
                                     FROM tbl_document_folders document_folders_child
                                     WHERE document_folders_child.parent_id = tbl_document_folders.id) AS subFolderCount,
                                 (SELECT COUNT(tbl_documents.id) AS total FROM tbl_documents 
                                 WHERE tbl_documents.folder_id = tbl_document_folders.id ) AS documentsCount
                                 FROM tbl_document_folders WHERE ".$clause."
                                 AND company_id = ".$db->sqs($companyId)."
                                 AND (branch_id =".$db->sqs($branchId)." OR branch_id = 0)
                                  ORDER BY folder_name";

        $getAllFoldersResults = $db->getAll($allFolderFiles);


        if($getAllFoldersResults){

            return $getAllFoldersResults;
        }else{

            return false;
        }

    }

    /**
     * @param null $folderId
     * @param null $companyId
     * @return bool|int|String
     */
    public function getRootFolder($folderId  = null, $companyId = null){

        global $db;

        $clause = "";
        if(isset($folderId) && ($folderId != -1))
        {
            $clause = 'tbl_document_folders.id  = '.$db->sqs($folderId);
        }else{
            $clause = 'tbl_document_folders.parent_id IS NULL';
        }

        $allFolderFiles = "SELECT tbl_document_folders.id, folder_name, 
                                    (SELECT COUNT(document_folders_child.id) AS total
                                     FROM tbl_document_folders document_folders_child
                                     WHERE document_folders_child.parent_id = tbl_document_folders.id) AS subFolderCount,
                                 (SELECT COUNT(tbl_documents.id) AS total FROM tbl_documents 
                                 WHERE tbl_documents.folder_id = tbl_document_folders.id ) AS documentsCount
                                 FROM tbl_document_folders WHERE ".$clause." AND company_id = ".$db->sqs($companyId)." ORDER BY folder_name";

        // echo $allFolderFiles;
        $getAllFoldersResults = $db->getOne($allFolderFiles);


        if($getAllFoldersResults){

            return $getAllFoldersResults;
        }else{

            return false;
        }

    }

    /**
     * Method to get folder info
     * @param null $folderId
     * @param null $companyId
     * @return array|bool
     */
    public  function  getFolderInfo($folderId = null,$companyId= null){

        global $db;


        $clause = "";
        if(isset($companyId) && !is_null($companyId))
        {
            $clause = " AND company_id = ".$db->sqs($companyId);
        }

        $getFolderInfo = "SELECT *        
                           FROM tbl_document_folders
                           WHERE id =".$db->sqs($folderId)." {$clause} ";


        $folderInfoResults = $db->getRow($getFolderInfo);

        if($folderInfoResults){

            return $folderInfoResults;
        }else{

            return false;
        }

    }

    /**
     * Method to get folder stats
     * @param null $folderId
     * @param null $companyId
     * @return array|bool
     */
    public function getFoldersStats($folderId  = null, $companyId = null){
        global $db;


        $getFolderStats = "SELECT tbl_document_folders.id, folder_name, 
                                    (SELECT COUNT(document_folders_child.id) AS total
                                     FROM tbl_document_folders document_folders_child
                                     WHERE document_folders_child.parent_id = tbl_document_folders.id) AS subFolderCount,
                                 (SELECT COUNT(tbl_documents.id) AS total FROM tbl_documents 
                                 WHERE tbl_documents.folder_id = tbl_document_folders.id ) AS documentsCount
                                 FROM tbl_document_folders WHERE tbl_document_folders.id = ".$db->sqs($folderId)." AND company_id = ".$db->sqs($companyId)." ORDER BY folder_name";


        $folderResults = $db->getRow($getFolderStats);

        if($folderResults){

            return $folderResults;
        }else{

            return false;
        }

    }


    /**
     * Method to get folder name
     * @param null $folderId
     * @return array|bool
     */
    public  function  getFolderName($folderId = null){

        global $db;

        $getFolderName = "SELECT folder_name       
                           FROM tbl_document_folders
                           WHERE id =".$db->sqs($folderId);


        $folderNameResults = $db->getOne($getFolderName);

        if($folderNameResults){

            return $folderNameResults;
        }else{

            return false;
        }

    }

    /**
     * Method to get company folder id(Parent
     * @param null $companyId
     * @return bool|int|String
     */
    function  getCompanyFolderId($companyId = null){

        global $db;

        $getCompanyFolderId = "SELECT id       
                           FROM tbl_document_folders
                           WHERE company_id =".$db->sqs($companyId) ." AND parent_id IS NULL";


        $companyFolderIdResults = $db->getOne($getCompanyFolderId);

        if($companyFolderIdResults){

            return $companyFolderIdResults;
        }else{

            return false;
        }

    }
    /**
     * Method to get all folder directories
     * @param null $companyId
     * @return array|bool
     *
     */
    public function getAllFolderCompany($companyId= null){


        global $db;

        $getAllFolders = "SELECT *        
                           FROM tbl_document_folders
                           WHERE company_id =".$db->sqs($companyId);


        $allFolderResults = $db->getAll($getAllFolders);

        if($allFolderResults){

            return $allFolderResults;
        }else{

            return false;
        }

    }


    /**
     * Method to add a new folder
     * @param array $folderData
     * @return bool|int
     */
    public  function  addNewFolder($folderData = array()){

        global $db;

        $parentId  = "NULL";
        if($folderData['parentId'] > 0)
        {
            $parentId = $db->sqs($folderData['parentId']);
        }


        if(!$this->existsFolder($folderData,$parentId)) {
            // print_r($folderData);
            // echo "here";
            // exit;
            $addFolderSql = "INSERT INTO tbl_document_folders(
                                                folder_name,
                                                parent_id,
                                                company_id,
                                                branch_id,
                                                created_by
                                                ) VALUES(
                                                 " . $db->sqs($folderData['folderName']) . ",
                                                  " . $parentId . ",
                                                  " . $db->sqs($folderData['companyId']) . ",
                                                  " . $db->sqs($folderData['branchId']) . ",
                                                  " . $db->sqs($folderData['userId']) . "
                                                )";

            $addFolderResult = $db->query($addFolderSql);

            if ($addFolderResult) {
                return $db->insertId();
            } else {
                return false;
            }
        }else{
            return false;
        }

    }


    /**
     * Method to update folder info
     * @param array $folderData
     * @return bool
     */
    public  function  editFolder($folderData = array()){

        global $db;

        $parentId = null;
        if($folderData['parentId'] > 0)
        {
            $parentId = $db->sqs($folderData['parentId']);
        }else{

            $parentId  = "NULL";
        }
        $sql = "UPDATE tbl_document_folders SET 
                          folder_name =" . $db->sqs($folderData['folderName']). ", 
                          parent_id =".$parentId. ", 
                          modified_by =" . $db->sqs($folderData['userId']). "
                WHERE id = " . $db->sqs($folderData['folderId']);

        $isUpdateResults = $db->query($sql);
        if($isUpdateResults){
            return true;
        }else{
            return false;
        }

    }

    /**
     * Method to get folder url
     * @param null $folderId
     * @return bool|int|String
     */
    public function getParentFolderUrl($folderId = null){


        global $db;

        $getCompanyFolderUrl = "SELECT folder_path       
                           FROM tbl_document_folders
                           WHERE id =".$db->sqs($folderId) ;


        $companyFolderUrlResults = $db->getOne($getCompanyFolderUrl);

        if($companyFolderUrlResults){

            return $companyFolderUrlResults;
        }else{

            return false;
        }

    }



    /**
     * Method to add a folder directory
     * @param array $directoryData
     * @param null $folderId
     * @param null $parentFolderId
     * @return bool
     */
    public function createFolderDirectory($directoryData = array(),$folderId = null,$parentFolderId = null)
    {
        global $db;

        $success = false;
        $folderPath = null;


        $parentFolderUrl = $this->getParentFolderUrl($parentFolderId);


        if($parentFolderUrl && $parentFolderId){

            //$folderPath = $this->createFolderLink($folderId,$directoryData['companyId'],$directoryData['companyName']);
            $folderPath = $this->createFolderLink($parentFolderUrl,$directoryData['folderName']);
        }else{

            //$folderPath = $this->createFolderLink($folderId,$directoryData['companyId'],$directoryData['companyName']);
            $folderPath = $this->createFolderLink(null,$directoryData['folderName']);
        }




        // save to file
        $fullFolderPath = FOLDER_DIRECTORY_ROOT . '/' . $folderPath;

        // make sure the folder path exists
        if (!is_dir($fullFolderPath)) {

            $createPath = mkdir($fullFolderPath, 0777, true);
            if(!$createPath)
            {
                return $success;

            }else{
                $sql = "UPDATE tbl_document_folders SET 
                          folder_path =" . $db->sqs($folderPath). ",
                          modified_by =" . $db->sqs($directoryData['created_by']). "
                          WHERE id = " . $db->sqs($folderId);

                $isUpdateResults = $db->query($sql);
                if($isUpdateResults){
                    return true;
                }else{
                    return false;
                }
            }

            return $success;
        }

        return $success;
    }


    public function autoCreateFolders($companyId = null,$branchId = null ){


        global $db;

        $clause = "";
        /*if(isset($folderId) && ($folderId != -1))
        {
            $clause = 'tbl_document_folders.parent_id = '.$db->sqs($folderId);
        }else{
            $clause = 'tbl_document_folders.parent_id IS NULL';
        }
        */

        $allFolderFiles = "SELECT  folder_name, tbl_document_folders.folder_path
                    
                                 FROM tbl_document_folders WHERE ".$clause."
                                  company_id = ".$db->sqs($companyId)."
                                 AND (branch_id =".$db->sqs($branchId)." OR branch_id = 0)
                                  ORDER BY tbl_document_folders.id";

        $getAllFoldersResults = $db->getAll($allFolderFiles);


        if($getAllFoldersResults){
            foreach ($getAllFoldersResults as $folder){
                // save to file
                $fullFolderPath = FOLDER_DIRECTORY_ROOT . '/' . $folder['folder_path'];

                // make sure the folder path exists
                if (!is_dir($fullFolderPath)) {
                    $createPath = mkdir($fullFolderPath, 0777, true);
                }
            }

            return true;
        }else{

            return false;
        }

    }


    /**
     *  Method to get upload directory
     * @param null $moduleName
     * @param null $groupId
     * @param null $typeId
     * @param null $categoryId
     * @param null $folderNameOption
     * @return array|bool
     */
    public function getFolderDirectoryPath($moduleName = null,$groupId = null,$typeId = null,$categoryId = null,$folderNameOption =  null){

        global $db;
        global $objDocument;
        global $objAssets;
        global $objSheqTeam;

        $searchClause = "";
        //SELECT id,folder_path FROM tbl_document_folders WHERE folder_name LIKE '%%' AND (folder_path LIKE '%%' AND folder_path LIKE '%Documents%') AND company_id =32 AND branch_id =57
        if($groupId && $typeId){
            //get the type
            $typeFolderName = $objDocument->getDocumentTypeName($typeId);
            $typeFolderName = $this->formatFolderNameForUrl($typeFolderName);

            $groupFolderName = $objSheqTeam->getSheqTeamCategory($groupId);
            $groupFolderName = $this->formatFolderNameForUrl($groupFolderName['sheqteam_name']);

            $groupFolderName = str_replace('&', 'AND', $groupFolderName);
            $searchClause .= " folder_name LIKE '%".$typeFolderName."%' AND (folder_path LIKE '%".$groupFolderName."%'  AND folder_path LIKE '%".$moduleName."%') ";

        }elseif($folderNameOption && $groupId) {


            $groupFolderName = $objSheqTeam->getSheqTeamCategory($groupId);
            $groupFolderName = $this->formatFolderNameForUrl($groupFolderName['sheqteam_name']);
            $groupFolderName = str_replace('&', 'AND', $groupFolderName);
            $searchClause .= " folder_name LIKE '%".$folderNameOption."%' AND (folder_path LIKE '%".$groupFolderName."%'  AND folder_path LIKE '%".$moduleName."%') ";

        }elseif ($folderNameOption){

            $searchClause .= " folder_name LIKE '%".$folderNameOption."%'  AND folder_path LIKE '%".$moduleName."%' ";

        }elseif($groupId){

            $folderName = $objSheqTeam->getSheqTeamCategory($groupId);
            $folderName = $this->formatFolderNameForUrl($folderName['sheqteam_name']);


            //$searchClause .= " folder_name =".$db->sqs($folderName)." AND folder_path LIKE '%".$moduleName."%'";
            $searchClause .= " folder_name LIKE '%".$folderName."%' AND folder_path LIKE '%".$moduleName."%' ";

        }elseif($typeId){

            //get the type
            $folderName = $objDocument->getDocumentTypeName($typeId);
            $folderName = $this->formatFolderNameForUrl($folderName);

            //$searchClause .= " folder_name =".$db->sqs($folderName)." AND folder_path LIKE '%".$moduleName."%'";
            $searchClause .= " folder_name LIKE '%".$folderName."%' AND folder_path LIKE '%".$moduleName."%' ";

        }elseif($categoryId){

            //get the type
            $folderName = $objAssets->getAssetTypeByCategory($categoryId);
            $folderName = $this->formatFolderNameForUrl($folderName['name']);
            //$searchClause .= " folder_name =".$db->sqs($folderName)." AND folder_path LIKE '%".$moduleName."%'";
            $searchClause .= " folder_name LIKE '%".$folderName."%' AND folder_path LIKE '%".$moduleName."%' ";

        }else{

            $folderName = $this->formatFolderNameForUrl($moduleName);
            $searchClause .= "  folder_name =".$db->sqs($folderName);

        }

        $getCompanyFolderUrl = "SELECT id,folder_path       
                                       FROM tbl_document_folders
                                       WHERE  {$searchClause} AND company_id =".$db->sqs($_SESSION['company_id']) ."
                                        AND branch_id =".$db->sqs($_SESSION['branch_id']) ;



        // echo $getCompanyFolderUrl;
        $companyFolderUrlResults = $db->getRow($getCompanyFolderUrl);

        if($companyFolderUrlResults){

            return $companyFolderUrlResults;
        }else{

            return false;
        }

    }

    /**
     * Method to format directory name
     * @param null $folderName
     * @return mixed
     */
    public function formatFolderNameForUrl($folderName = null)
    {
        return str_replace(array(" ", "\"", "'", ";", "#", "%"), "_", strip_tags($folderName));
    }


    /**
     * Method to create links to folders
     * @param null $folderUrl
     * @param null $folderName
     * @param null $folderId
     * @return string
     */
    private function createFolderLink($folderUrl = null,$folderName = null){


        $folderName = trim($folderName);
        if($folderUrl && $folderName){
            return $folderUrl.$this->formatFolderNameForUrl($folderName).'/';
        }else{
            return $this->formatFolderNameForUrl($folderName).'/';
        }

    }
    /**
     * Method to create links to folders
     * @param null $folderId
     * @param null $companyId
     * @param null $companyName
     * @return string
     */
    private function createFolderLinks($folderId = null,$companyId = null,$companyName = null){


        $folderName = $this->getFolderName($folderId);

        if($folderName){
            $folderName = $this->formatFolderNameForUrl($folderName);
            if($companyName && $companyId){
                return 'folder/'.$this->formatFolderNameForUrl($companyName).'_'.$companyId.'/'.intval($folderId).'/'.$folderName;
            }
            return 'folder/'.intval($folderId).'/'.$folderName;
        }else{
            return 'folder/'.intval($folderId);

        }

    }

    /**
     * Method to list all the folders
     * @param null $folderId
     * @param null $companyId
     * @param null $branchId
     * @return array|bool
     */
    public function folderDetailsView($folderId = null,$companyId = null,$branchId = null){

        global $db;
        $searchType = null;
        $addClause = '';

        $foldersClause = "WHERE 1=1 ";
        if($folderId)
        {
            $nodeId = $folderId;
            switch($nodeId)
            {
                case 'recent':
                    $searchType = 'recent';
                    $foldersClause .= ' AND 1=2'; // disable
                    break;
                case 'trash':
                    $searchType = 'trash';
                    $foldersClause .= ' AND 1=2'; // disable
                    break;
                case 'all':
                    $searchType = 'all';
                    $foldersClause .= ' AND 1=2'; // disable
                    break;
                case '-1':
                    $searchType = 'root';
                    $foldersClause .= " AND tbl_document_folders.parent_id IS NULL";// AND tbl_document_folders.company_id = ".$db->sqs($companyId)

                    break;
                default:
                    $searchType = 'folder';
                    $foldersClause .= " AND tbl_document_folders.parent_id = ".(int)$nodeId;
                    break;
            }
        }
        $getAllFolders = "SELECT tbl_document_folders.id,
                                        tbl_document_folders.created_by,
                                        tbl_document_folders.parent_id,
                                        tbl_document_folders.folder_name,
                                        (SELECT COUNT(tbl_documents.id) AS documentsCount
                                             FROM tbl_documents WHERE tbl_documents.folder_id = tbl_document_folders.id ) AS documentsCount
                                 FROM tbl_document_folders ". $foldersClause ."
                                 AND tbl_document_folders.company_id = ".$db->sqs($companyId)."
                                 AND (tbl_document_folders.branch_id = ".$db->sqs($branchId)." OR tbl_document_folders.branch_id = 0)  ORDER BY folder_name";



        $allFolderResults = $db->getAll($getAllFolders);

        if($allFolderResults){

            return $allFolderResults;
        }else{

            return false;
        }
    }

    /**
     * Method to list all the files
     * @param null $folderId
     * @param null $companyId
     * @param null $branchId
     * @return array|bool
     */
    public function folderFilesDetailsView($folderId = null,$companyId = null,$branchId = null){

        global $db;
        $searchType = null;
        $addClause = "";
        $orderBy = null;
        $foldersClause = "WHERE 1=1 ";
        if($folderId)
        {
            $nodeId = $folderId;
            switch($nodeId)
            {
                case 'recent':
                    $searchType = 'recent';
                    $foldersClause .= ' AND 1=2'; // disable
                    $orderBy .=" ORDER BY tbl_document_folders.date_created  DESC";
                    break;
                case 'trash':
                    $searchType = 'trash';
                    $foldersClause .= ' AND 1=2'; // disable
                    break;
                case 'all':
                    $searchType = 'all';
                    $foldersClause .= ' AND 1=2'; // disable
                    break;
                case '-1':
                    $searchType = 'root';
                    $foldersClause .= " AND tbl_document_folders.parent_id IS NULL";

                    break;
                default:
                    $searchType = 'folder';
                    $foldersClause .= " AND tbl_document_folders.id = ".(int)$nodeId;
                    //$foldersClause .= " AND tbl_document_folders.id = ".(int)$nodeId;
                    break;
            }
        }
        $getAllFiles = "SELECT  tbl_documents.*, 
                                        tbl_document_folders.created_by,
                                        tbl_document_folders.parent_id,
                                        tbl_document_folders.folder_name
                                FROM tbl_document_folders
                                INNER JOIN tbl_documents ON  tbl_documents.folder_id = tbl_document_folders.id 
                                ". $foldersClause ." AND tbl_document_folders.company_id = ".$db->sqs($companyId)." 
                               AND (tbl_document_folders.branch_id = ".$db->sqs($branchId)." OR tbl_document_folders.branch_id = 0)  {$orderBy}";


        //echo $getAllFiles;
        //  exit;
        $allFieldsResults = $db->getAll($getAllFiles);

        if($allFieldsResults){

            return $allFieldsResults;
        }else{

            return false;
        }
    }


    /**
     * Get file/document information
     * @param null $fileId
     * @param null $companyId
     * @return bool|int|String
     */
    public function getFileInformation($fileId = null,$companyId = null){

        global $db;
        $getFileInfo = "SELECT tbl_documents.*, tbl_document_folders.id as folderId,
                                        tbl_document_folders.folder_path,
                                        tbl_document_folders.parent_id,
                                        tbl_document_folders.folder_name
                                FROM tbl_documents
                                INNER JOIN tbl_document_folders ON  tbl_documents.folder_id = tbl_document_folders.id 
                                WHERE tbl_documents.id=".$db->sqs($fileId)." AND tbl_document_folders.company_id = ".$db->sqs($companyId);

        $fileResults = $db->getRow($getFileInfo);

        if($fileResults){

            return $fileResults;
        }else{

            return false;
        }

    }

    /**
     * Method to remove selected folder
     * @param null $folderId
     * @return bool
     */
    public function removeSelectedFolder($folderId = null){

        global $db;

        if (!empty($folderId)) {

            $subFoldersSql = "SELECT *        
                           FROM tbl_document_folders
                           WHERE parent_id =".$db->sqs($folderId);


            $subFoldersResults = $db->getAll($subFoldersSql);

            //Okay now we remove subfolders and move documents to root folder
            if($subFoldersResults)
            {
                foreach($subFoldersResults AS $subFolder)
                {
                    self::removeSelectedFolder($subFolder['id']);
                }
            }

            $sqlUpdate = "UPDATE  tbl_documents SET folder_id = NULL WHERE  folder_id=". $db->sqs($folderId);
            $updateResults = $db->query($sqlUpdate);

            if($updateResults) {
                $sql = "DELETE FROM tbl_document_folders WHERE id = " . $db->sqs($folderId);
                $results = $db->query($sql);
                if ($results) {
                    return true;
                } else {
                    return false;
                }
            }
        }
        return false;

    }

    /**
     * check if folder exists
     * @param null $folderData
     * @param null $parentId
     * @return bool
     */
    private function existsFolder($folderData = null,$parentId =null)
    {
        global $db;

        $sql = "SELECT id FROM  tbl_document_folders 
                  WHERE folder_name = ".$db->sqs($folderData['folderName'])." 
                  AND parent_id = ".$parentId." 
                  AND company_id=".$db->sqs($folderData['companyId'])." AND branch_id = ".$db->sqs($folderData['branchId']);

        //echo  $sql;
        //exit;
        $response = $db->getOne($sql);

        if(count($response)>0){
            return TRUE;
        } else {
            return FALSE;
        }
    }


    /**
     * Return folder Id if folder exists
     * @param null $folderData
     * @param null $parentId
     * @return bool
     */
    public function existsFolderId($folderData = null)
    {
        global $db;

        $parentId  = "NULL";
        if($folderData['parentId'] > 0)
        {
            $parentId = $db->sqs($folderData['parentId']);
        }

        $sql = "SELECT id FROM tbl_document_folders 
                  WHERE folder_name = ".$db->sqs($folderData['folderName'])." 
                  AND parent_id = ".$parentId." 
                  AND company_id=".$db->sqs($folderData['companyId'])." AND branch_id = ".$db->sqs($folderData['branchId']);

        $response = $db->getOne($sql);

        if(count($response)>0){
            return $response;
        } else {
            return FALSE;
        }
    }
}
