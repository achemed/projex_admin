<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of module_type_class
 *
 * @author Innovatorshill
 */
trait module_files_class {
    //put your code here
    
    public function addModuleFile($fileData = array())
    {
        global $db;
        if(isset($fileData)){           
            $sql = "INSERT INTO sheqintel_master.tbl_upload_files(section_id,filename,file_type,file_size,file_section,created_by)
                                VALUES(                                                 
                                                  " . $db->sqs($fileData["section_id"]) . ",
                                                  " . $db->sqs($fileData["filename"]) . ",
                                                  " . $db->sqs($fileData["file_type"]) . ",
                                                  " . $db->sqs($fileData["file_size"]) . ",
                                                  " . $db->sqs($fileData["file_section"]) . ",
                                                  " . $db->sqs($_SESSION['user_id']) . ")";
        $result = $db->query($sql);
        if ($result) {
            return $result;
        } else {
            return false;
        }         
      }
   }
        
    public function deleteModuleFile($fileData = array())
    {
        global $db;
        if(!is_null($fileData)){           
            $sql = "UPDATE sheqintel_master.tbl_upload_files SET is_active = 0 WHERE id = ".$db->sqs($fileData["id"]);
            $result = $db->query($sql);
            if($result){
                return true;
            } else {
                return false;
            }            
        }
    }
  
    /**
     * Method to get incident types
     * @access public
     * @global $db
     * @param int $id
     */
    public function getAllModuleFilesBySectionID($file_section,$id)
    {
        global $db;
        if(!is_null($id)){
            $sql = "SELECT * FROM sheqintel_master.tbl_upload_files WHERE is_active = 1";
            if(!empty($id))
            {
                $sql .= " and section_id = ".$id;
            }
            if(!empty($file_section))
            {
                $sql .= " and file_section = ".$db->sqs($file_section);
            }            
            $response = $db->getAll($sql);
            if($response) {
                return $response;
            } else {
                return false;
            } 
        } else {
            return "Required information is missing";
        }
    }
    public function getModuleFile($fileData = array())
    {
        global $db;
        if(!is_null($fileData)){
            $sql = "SELECT * FROM sheqintel_master.tbl_upload_files WHERE id = " . $db->sqs($fileData["id"]);
            $response = $db->getRow($sql);
            if($response) {
                return $response;
            } else {
                return false;
            } 
        } else {
            return "Required information is missing";
        }
    }
    /**
     * Method to get all incident types
     * @access public
     * @global $db
     * @param int $categoryid
     * @param boolean $activeOnly
     */ 
}
