<?php

/**
 * Class containing all user related methods
 *
 * @package sheqonline
 * @author Warren Windvogel <warren@innovatorshill.co.za>
 * @copyright (c) 2016, Innovators Hill
 * @license
 */

class company_client_class
{

    public function connectToClient(){


    }

    /**
     * Method to format database name
     * @param null $DatabaseName
     * @return mixed
     */
    public function formatDatabase($DatabaseName = null)
    {
        return str_replace(array(" ", "\"", "'", ";", "#", "%"), "_", strip_tags($DatabaseName));
    }
    
    

    public function addClient($clientData = array()){

        global $objCompanies;
        global $objClientUser;
        global $db;

        $db->startTransaction();
        if(isset($clientData['username']) && isset($clientData['password'])){

            if($this->existClientDb($clientData['db_name'])){
                return 'Database already exists.';
            }

            if($this->existsClient($clientData['username'],$clientData['username'])){
                return 'The username is already registered.';
            } else {

                $name = $this->formatDatabase(DB_PREFIX . $clientData['db_name']);

                //add company first

                $clientCompanyId = $objCompanies->addClientCompany($name, $clientData);

                if ($clientCompanyId){
                    $sql = "INSERT INTO tbl_clients (
                                                role_id,
                                                firstname, 
                                                lastname,
                                                username, 
                                                password,
                                                email,
                                                company_id,
                                                created_by
                                          
                            ) VALUES (2,"
                        . $db->sqs($clientData['firstname']) . ", "
                        . $db->sqs($clientData['lastname']) . " , "
                        . $db->sqs($clientData['username']) . ", "
                        . $db->sqs(md5($clientData['password'])) . ","
                        . $db->sqs($clientData['email']) . ", "
                        . $db->sqs($clientCompanyId) . ","
                        . $db->sqs($_SESSION['user_id']) . ")";

                /*$sql = "INSERT INTO tbl_clients (
                                                role_id,
                                                firstname, 
                                                lastname,
                                                username, 
                                                password,
                                                email,
                                                db_name,
                                                company_name,
                                                created_by
                                          
                            ) VALUES (2,"
                    .$db->sqs($clientData['firstname']).", "
                    .$db->sqs($clientData['lastname'])." , "
                    .$db->sqs($clientData['username']).", "
                    .$db->sqs(md5($clientData['password'])).","
                    .$db->sqs($clientData['email']).", "
                    .$db->sqs($name).","
                    .$db->sqs($clientData['company_name']).","
                    .$db->sqs($_SESSION['user_id']).")";*/

                $result = $db->query($sql);
                if ($result) {
                    $clientData['client_user_Id'] = $db->insertId();
                    //return true;
                    $sql = "CREATE DATABASE $name";
                    $db_create = $db->query($sql);
                    if ($db_create) {
                        if ($result) {
                            if ($db->selectDatabase($name)) {
                                //runs import script
                                $file_name = BASE_URL . "/clean_script.sql";
                                $sqlSource = file_get_contents($file_name);
                                //$conn = mysqli_connect("localhost","root", "ureyus22", $name);

                                if ($db->setupTables($sqlSource)) {

                                    //add user as main user on the user db
                                    //add company
                                    if ($db->freeResult()) {
                                        $companyData = array(
                                            "company_name" => $clientData['company_name'],
                                            "headOfficeName" => NULL,
                                            "numberofBranch" => NULL,
                                            "vat_number" => NULL,
                                            "logo" => 0, //$_POST['logo'];
                                            "pty_number" => NULL,
                                            "building_name" => NULL,
                                            "street" => '',
                                            "suburb" => '',
                                            "town" => '',
                                            "postalCode" => 0000,
                                            "office_number" => NULL,
                                            "cell_number" => NULL,
                                            "fax_number" => NULL,
                                            "email" => $clientData['email'],
                                            "country_id" => 1,
                                            "region_id" => 1,
                                            "client_db" => $name
                                        );

                                        $companyId = $objCompanies->addCompany($companyData);
                                        $clientData['companyId'] = $companyId;
                                        $clientData['db_name'] = $name;
                                        $objClientUser->addUser($clientData);
                                    }

                                } else {
                                    $db->rollBack();
                                }
                            }
                            if ($db->freeResult()) {
                                $db->selectDatabase(MASTER_DB);
                            }
                            $db->commit();
                            return true;
                        } else {
                            $db->rollBack();
                            return false;
                        }
                    } else {
                        $db->rollBack();
                        return false;
                    }
                }
            }else{//client company is created
                    return 'Failed to add client company.';
              }
            }//end client doesnt exist if
        } else {
            return 'Required information missing.';
        }
    }

    /**
     * Method to get all company clients
     * @return array|bool
     */
    public function getAllCompanyClients($is_active = null){
        global $db;

        /*$sql = "SELECT
                    id,
                    firstname, 
                    lastname,
                    username, 
                    password,
                    email,
                    db_name,
                    company_name,
                    is_active,
                    date_created,
                    created_by
                    FROM tbl_clients WHERE role_id = 2";*/

        $sql = "SELECT 
                    tbl_client_companies.id,
                    tbl_client_companies.db_name,
                    tbl_client_companies.company_name,
                    tbl_client_companies.is_active,
                    tbl_client_companies.date_created,
                    tbl_client_companies.created_by,
                    tbl_client_companies.last_db_updated,
                    tbl_client_companies.package_id,
                    tbl_client_companies.company_club_member,
                    tbl_clients.firstname,
                    tbl_clients.lastname,
                    tbl_clients.username
                    FROM tbl_client_companies 
                    INNER JOIN tbl_clients ON tbl_clients.company_id = tbl_client_companies.id
                    WHERE tbl_clients.role_id = 2";

                    if(!empty($is_active))
                    {
                        $sql .= " and tbl_client_companies.is_active = 0";
                    }
                    else{
                        $sql .= " and tbl_client_companies.is_active = 1";
                    }
        $response = $db->getAll($sql);
        if($response){
            return $response;
        } else {
            return FALSE;
        }

    }

    public function getClientGroups($db_name = null){

        global $db;

        $sql = "SELECT * FROM {$db_name}.tbl_sheqteam_groups";

        $response = $db->getAll($sql);
        if($response){
            return $response;
        } else {
            return FALSE;
        }

    }

    /**
     * Method to get all company clients
     * @return array|bool
     */
    public function getClientInfo($clientId = null){
        global $db;

        $sql = "SELECT *
                    FROM tbl_client_companies 
                    WHERE tbl_client_companies.id = ".$db->sqs($clientId);
        $response = $db->getRow($sql);
        if($response){
            return $response;
        } else {
            return FALSE;
        }
    }


    public function updateClientPackage($clientData = array()){
        global $db;

        $sql = "UPDATE  tbl_client_companies
                    SET  package_id = ".$db->sqs($clientData['package_id'])."
                    WHERE id = ".$db->sqs($clientData['client_id']);
        $response = $db->query($sql);
        if($response){
            return $response;
        } else {
            return FALSE;
        }

    }

    /**
     * Method to check if client exists
     * @param null $userName
     * @param null $email
     * @return bool
     */
    private function existsClient($userName = null , $email = null){

        global $db;

        $sql = "SELECT id FROM tbl_clients  WHERE username = ".$db->sqs($userName) ." OR email =".$db->sqs($email) ;
        $response = $db->getRow($sql);
        if(is_array($response) && count($response)>0){
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     * Method to check if client database exists
     * @param null $clientDb
     * @return bool
     */
    private function existClientDb($clientDb = null){

        global $db;

        $clientDb = $this->formatDatabase(DB_PREFIX.$clientDb);

        $sql = "SELECT db_name FROM tbl_clients  WHERE db_name = ".$db->sqs($clientDb) ;

        $response = $db->getRow($sql);
        if(is_array($response) && count($response)>0){
            return TRUE;
        } else {
            return FALSE;
        }
    }

}