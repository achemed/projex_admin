<?php
/**
 * Regions class containing all region methods
 *
 * @package projex
 * @author Brian Douglas <douglas@innovatorshill.co.za>
 * @copyright (c) 2016, Innovators Hill
 * @license
 */

class legal_class
{


    /**
     * Initialise service class
     * providers constructor.
     */
    function __construct()
    {

    }

    /**
     * Method to get all companies
     * @return array|bool
     */
    public function getAllLegalCountries()
    {

        global $db;

        $allCountriesSql = "SELECT * FROM tbl_legal_country";

        $getAllCountriesResults = $db->getAll($allCountriesSql);


        if ($getAllCountriesResults) {

            return $getAllCountriesResults;
        } else {

            return false;
        }

    }

    public function addLegal($data = array()){
        global $db;

        $addLegalSql = "INSERT INTO tbl_legal_country (
                                                country_id,	    
												description)
                             value (
                                    ".$db->sqs($data['country_id']).",
                                    ".$db->sqs($data['description']).")";

        $results = $db->query($addLegalSql);

        if($results){
            return true;
        }else{
            return false;
        }

    }

    /**
     * @param array $data
     * @return bool
     */
    public function addLegalRequirement($data = array()){
        global $db;

        $addLegalSql = "INSERT INTO tbl_legal_details (
                                                legal_id,
                                                header,	    
                                                sub_no,	   
												details)
                             value (
                                    ".$db->sqs($data['legal_id']).",
                                    ".$db->sqs($data['header']).",
                                    ".$db->sqs($data['sub_no']).",
                                    ".$db->sqs($data['description']).")";

        $results = $db->query($addLegalSql);

        if($results){
            return true;
        }else{
            return false;
        }

    }

    /**
     * @param array $data
     * @return bool
     */
    public function addLegalDefinition($data = array()){

        global $db;

        $addLegalSql = "INSERT INTO tbl_legal_definition (
                                                legal_id,
                                                content,	    
                                                definition)
                             value (
                                    ".$db->sqs($data['legal_id']).",
                                    ".$db->sqs($data['content']).",
                                    ".$db->sqs($data['definition']).")";

        $results = $db->query($addLegalSql);

        if($results){
            return true;
        }else{
            return false;
        }
    }


    public function addLegalAbout($data = array()){
        global $db;

        $addLegalSql = "INSERT INTO tbl_legal_about (
                                                legal_id,
                                                about,	    
                                                effective_date)
                             value (
                                    ".$db->sqs($data['legal_id']).",
                                    ".$db->sqs($data['about']).",
                                    ".$db->sqs($data['effective_date']).")";

        $results = $db->query($addLegalSql);

        if($results){
            return true;
        }else{
            return false;
        }

    }

    /**
     * @param array $data
     * @return bool
     */
    public function editLegalAbout($data = array()){
        global $db;

        $sql = "UPDATE tbl_legal_about SET      about= ".$db->sqs($data['about']).",	    
                                                effective_date=".$db->sqs($data['effective_date'])."
                                                WHERE id =".$db->sqs($data['about_id']);

        $results = $db->query($sql);

        if($results){
            return true;
        }else{
            return false;
        }
    }


    /**
     * @param null $id
     * @return array|bool
     */
    public function getLegalCountryInfo($id = null){
        global $db;

        $sql = "SELECT * FROM tbl_legal_country WHERE country_id =".$db->sqs($id);

        $results = $db->getAll($sql);
        if ($results) {

            return $results;
        } else {

            return false;
        }

    }

    public function getLegalnfo($id = null){
        global $db;

        $sql = "SELECT * FROM tbl_legal_country WHERE id =".$db->sqs($id);

        $results = $db->getRow($sql);
        if ($results) {

            return $results;
        } else{
            return false;
        }
    }

    public  function getLegalDetails($id = null){
        global $db;

        $sql = "SELECT * FROM tbl_legal_details WHERE legal_id =".$db->sqs($id);

        $results = $db->getAll($sql);
        if ($results) {

            return $results;
        } else{
            return false;
        }
    }

    public function getLegalDefinitions($id = null){
        global $db;

        $sql = "SELECT * FROM tbl_legal_definition WHERE legal_id =".$db->sqs($id);

        $results = $db->getAll($sql);
        if ($results) {

            return $results;
        } else{
            return false;
        }
    }

    public function getLegalAbout($id = null){
        global $db;

        $sql = "SELECT * FROM tbl_legal_about WHERE legal_id =".$db->sqs($id);

        $results = $db->getRow($sql);
        if ($results) {

            return $results;
        } else{
            return false;
        }

    }

}