<?php
/**
 * Regions class containing all region methods
 *
 * @package projex
 * @author Brian Douglas <douglas@innovatorshill.co.za>
 * @copyright (c) 2016, Innovators Hill
 * @license
 */

class regions
{


    /**
     * Initialise service class
     * providers constructor.
     */
    function __construct()
    {

    }

    /**
     * Method to get all companies
     * @return array|bool
     */
    public function getAllCountries()
    {

        global $db;

        $allCountriesSql = "SELECT * FROM tbl_countries";

        $getAllCountriesResults = $db->getAll($allCountriesSql);


        if ($getAllCountriesResults) {

            return $getAllCountriesResults;
        } else {

            return false;
        }
    }

    public function getCountryInfo($id = null)
    {
        global $db;
        if(!is_null($id)){
            $sql = "SELECT * FROM tbl_countries WHERE id = " . $db->sqs($id);
            $response = $db->getRow($sql);
            if($response) {
                return $response;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
}