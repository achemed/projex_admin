<?php

/**
 * Class containing all user related methods
 *
 * @package sheqonline
 * @author Warren Windvogel <warren@innovatorshill.co.za>
 * @copyright (c) 2016, Innovators Hill
 * @license
 */

class company_client_users_class
{

    public function addUser($userData = array()){
        global $db;
        //$name = DB_PREFIX.$userData['db_name'];

        $name = $userData['db_name'];
        $sql = "INSERT INTO tbl_users (
                            username,
                            email,
                            role_id,
                            is_allowed_global_view,
                            firstname,
                            lastname,
                            password,
                            ip_address,
                            last_login,
                            db_name,
                            client_id,
                            company_id,
                            created_by)
               VALUES("
                .$db->sqs($userData['username']).", "
                .$db->sqs($userData['email']).",
                 2,1,"
                .$db->sqs($userData['firstname']).", "
                .$db->sqs($userData['lastname']).", "
                .$db->sqs(md5($userData['password'])).","
                .$db->sqs($_SERVER['SERVER_ADDR']).",
                    NOW(),"
                .$db->sqs($name).","
               .$db->sqs($userData['client_user_Id']).","
                .$db->sqs($userData['companyId']).","
                .$db->sqs($_SESSION['user_id']).")";

        $results = $db->query($sql);

        if($results){
            return true;
        }else{
            return false;
        }
    }
}