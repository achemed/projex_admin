<?php
/**
 * Created by PhpStorm.
 * User: Douglas
 * Date: 30/11/2018
 * Time: 1:34 PM
 */
include_once ('sheq_daily_safety_class.php');
include_once ('sheq_daily_safety_question_class.php');
include_once ('sheq_daily_safety_topic_description_class.php');
//main
include_once ('sheqclub_main_class.php');
include_once ('sheqclub_main_types_class.php');
include_once ('sheqclub_main_files_class.php');
class sheq_class
{
    use sheq_daily_safety_class,sheq_daily_safety_question_class,sheq_daily_safety_topic_description_class,sheqclub_main_class,sheqclub_main_types_class,sheqclub_main_files_class;
}