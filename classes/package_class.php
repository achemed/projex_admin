<?php

/**
 * Class containing all package functionality
 *
 * @package projex
 * @author Brian Douglas<douglas@innovatorshill.co.za>
 * @copyright (c) 2016, Innovators Hill
 * @license
 */

class package_class
{

    /**
     * Method to initiate the class
     *
     * @access public
     */
    public function __construct()
    {

    }

    /**
     * @return array|bool
     */
    public function getAllPackages(){
        global $db;
        $sql = "SELECT *  FROM tbl_package_plan ";
        $result = $db->getAll($sql);
        if($result){
            return $result;
        }else{
            return false;
        }
    }

    /**
     * @param null $packageId
     * @return bool
     */
    public function getPackageInfo($packageId = null){
        global $db;
        $sql = "SELECT *  FROM tbl_package_plan  WHERE id=".$db->sqs($packageId);
        $result = $db->getRow($sql);
        if($result){
            return $result;
        }else{
            return false;
        }
    }

    /**
     * @param array $data
     * @return bool
     */
    public function addPackage($data = array())
    {
        global $db;

        $addPackageSql = "INSERT INTO tbl_package_plan (
                                                package_name,	    
												package_details)
                             value (
                                    ".$db->sqs($data['package_name']).",
                                    ".$db->sqs($data['description']).")";

        $results = $db->query($addPackageSql);

        if($results){
            return true;
        }else{
            return false;
        }
    }



    /**
     * @param array $data
     */
    public function updatePackage($data = array()){
        global $db;

    }


}