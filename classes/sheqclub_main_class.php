<?php

/**
 * Class containing all methods providing asset functionality
 * 
 * @package projex
 * @author Sunnyboy Mathole <sunny@innovatorshill.co.za>
 * @copyright (c) 2016, Innovators Hill
 * @license 
 */
trait sheqclub_main_class {
    /**
     * Method to add asset category
     * @access public
     * @global $db
     * @param string $name
     * @param string $description
     */
    public function addSheqClubMain($dataUser = array())
    {
        global $db;
        if(isset($dataUser)){
            $sql = "INSERT INTO tbl_sheqclub_main (topic,sheqclub_section,created_by) VALUES (".
            $db->sqs($dataUser["topic"]).",".
			$db->sqs($dataUser["sheqclub_section"]).",". $db->sqs($_SESSION['user_id']) . ")";
            $response = $db->query($sql);
            if($response){
                return true;
            }else{
                return false;
            }  
        } else {
            return false;
        }
    }
    
    /**
     * Method to edit asset category
     * @access public
     * @global $db
     * @param int $id
     * @param string $name
     * @param string $description
     */
	  public function editSheqClubMain($dataUser = array())
    {
        global $db;
        if(isset($dataUser)){
            $sql = "UPDATE tbl_sheqclub_main SET
			topic = " . $db->sqs($dataUser["topic"]). ",			
			modified_by = " . $db->sqs($_SESSION['user_id']). " WHERE id = ".$db->sqs($dataUser["id"]);
            $response = $db->query($sql);
            if($response){
                return $response;
            }else{
                return false;
            }  
        } else {
            return false;
        }
    }
    public function editSheqClubMainStatus($id,$status)
    {
        global $db;
        if(isset($id)){
            $sql = "UPDATE tbl_sheqclub_main SET is_published = " . $db->sqs($status). ", 
            modified_by = " . $db->sqs($_SESSION['user_id']). " WHERE id = ".$db->sqs($id);
            $response = $db->query($sql);
            if($response){
                return $response;
            }else{
                return false;
            }  
        } else {
            return false;
        }
    }
    
	
   /**
     * Method to activate asset category
     * @access public
     * @global $db
     * @param int $id
     */
    public function activateSheqClubMain($id)
    {
        global $db;
        if(!is_null($id)){
            $sql = "UPDATE tbl_sheqclub_main SET is_active = 1 WHERE id = ".$db->sqs($id);
            $result = $db->query($sql);
            if($result){
                return true;
            } else {
                return false;
            }            
        } else {
            return "Required information is missing";
        }  
    }
    /**
     * Method to deactivate asset category
     * @access public
     * @global $db
     * @param int $id
     */
    public function deactivateSheqClubMain($id)
    {
        global $db;
        if(!is_null($id)){
            $sql = "UPDATE tbl_sheqclub_main SET is_active = 0 WHERE id = ".$db->sqs($id);
            $result = $db->query($sql);
            if($result){
                return true;
            } else {
                return false;
            }            
        } else {
            return "Required information is missing";
        }
    }
    /**
     * Method to get all asset category
     * @access public
     * @global $db
     * @param boolean $activeOnly
     */

    public function getSheqClubMainInfo($id)
    {
        global $db;
        $sql = "SELECT * FROM tbl_sheqclub_main WHERE is_active = 1 and id = ".$db->sqs($id);
       
        $response = $db->getRow($sql);
        if($response) {
            return $response;
        } else {
            return false;
        }
    } 

    public function getAllSheqClubMain($activeOnly = true,$sheqclub_section = null)
    {
        global $db;
        $sql = "SELECT * FROM tbl_sheqclub_main WHERE ";
        if($activeOnly){
            $sql .= " is_active = 1 ";
        }else{
            $sql .= " is_active = 0 ";
        }

        if(!empty($sheqclub_section))
        {
            $sql .= " and sheqclub_section = ".$db->sqs($sheqclub_section);
        }

        $response = $db->getAll($sql);        
        if($response) {
            return $response;
        } else {
            return false;
        }
    } 
}
