<?php

/**
 * Class containing all methods providing asset functionality
 * 
 * @package projex
 * @author Sunnyboy Mathole <sunny@innovatorshill.co.za>
 * @copyright (c) 2016, Innovators Hill
 * @license 
 */
trait sheq_daily_safety_class {
    /**
     * Method to add asset category
     * @access public
     * @global $db
     * @param string $name
     * @param string $description
     */
    public function addDailySafetyTopic($dataUser = array())
    {
        global $db;
        if(isset($dataUser)){
            $sql = "INSERT INTO tbl_daily_safety_topics (daily_date, topic,created_by) VALUES (". $db->sqs($dataUser["daily_date"]) . ",".
			$db->sqs($dataUser["topic"]).",". $db->sqs($_SESSION['user_id']) . ")";
            $response = $db->query($sql);
            if($response){
                return true;
            }else{
                return false;
            }  
        } else {
            return false;
        }
    }
    
    /**
     * Method to edit asset category
     * @access public
     * @global $db
     * @param int $id
     * @param string $name
     * @param string $description
     */
	  public function editDailySafetyTopic($dataUser = array())
    {
        global $db;
        if(isset($dataUser)){
            $sql = "UPDATE tbl_daily_safety_topics SET daily_date = " . $db->sqs($dataUser["daily_date"]). ",
			topic = " . $db->sqs($dataUser["topic"]). ",			
			modified_by = " . $db->sqs($_SESSION['user_id']). " WHERE id = ".$db->sqs($dataUser["id"]);
            $response = $db->query($sql);
            if($response){
                return $response;
            }else{
                return false;
            }  
        } else {
            return false;
        }
    }
    public function editDailySafetyTopicStatus($id,$status)
    {
        global $db;
        if(isset($id)){
            $sql = "UPDATE tbl_daily_safety_topics SET is_published = " . $db->sqs($status). ", 
            modified_by = " . $db->sqs($_SESSION['user_id']). " WHERE id = ".$db->sqs($id);
            $response = $db->query($sql);
            if($response){
                return $response;
            }else{
                return false;
            }  
        } else {
            return false;
        }
    }
    
	
   /**
     * Method to activate asset category
     * @access public
     * @global $db
     * @param int $id
     */
    public function activateDailySafetyTopic($id)
    {
        global $db;
        if(!is_null($id)){
            $sql = "UPDATE tbl_daily_safety_topics SET is_active = 1 WHERE id = ".$db->sqs($id);
            $result = $db->query($sql);
            if($result){
                return true;
            } else {
                return false;
            }            
        } else {
            return "Required information is missing";
        }  
    }
    /**
     * Method to deactivate asset category
     * @access public
     * @global $db
     * @param int $id
     */
    public function deactivateDailySafetyTopic($id)
    {
        global $db;
        if(!is_null($id)){
            $sql = "UPDATE tbl_daily_safety_topics SET is_active = 0 WHERE id = ".$db->sqs($id);
            $result = $db->query($sql);
            if($result){
                return true;
            } else {
                return false;
            }            
        } else {
            return "Required information is missing";
        }
    }
    /**
     * Method to get all asset category
     * @access public
     * @global $db
     * @param boolean $activeOnly
     */

    public function getDailySafetyTopicInfo($id)
    {
        global $db;
        $sql = "SELECT * FROM tbl_daily_safety_topics WHERE is_active = 1 and id = ".$db->sqs($id);
       
        $response = $db->getRow($sql);
        if($response) {
            return $response;
        } else {
            return false;
        }
    } 

    public function getAllDailySafetyTopic($activeOnly = true)
    {
        global $db;
        $sql = "SELECT * FROM tbl_daily_safety_topics WHERE ";
        if($activeOnly){
            $sql .= " is_active = 1 ";
        }else{
            $sql .= " is_active = 0 ";
        }

        $response = $db->getAll($sql);
        if($response) {
            return $response;
        } else {
            return false;
        }
    } 
}
