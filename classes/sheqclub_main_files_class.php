<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of module_type_class
 *
 * @author Innovatorshill
 */
trait sheqclub_main_files_class 
{
    //put your code here
    
    public function addSheqClubMainTypeFile($fileData = array())
    {
        global $db;
        if(isset($fileData)){           
            $sql = "INSERT INTO tbl_sheqclub_main_types_files(sheqclub_main_id,sheqclub_main_types_id,sheqclub_section,description,filename,file_type,file_size,created_by)
                                VALUES(
          " . $db->sqs($fileData['sheqclub_main_id']) . ",
          " . $db->sqs($fileData['sheqclub_main_types_id']) . ",
          " . $db->sqs($fileData["sheqclub_section"]) . ",
          " . $db->sqs($fileData["description"]) . ",
          " . $db->sqs($fileData["filename"]) . ",
          " . $db->sqs($fileData["file_type"]) . ",
          " . $db->sqs($fileData["file_size"]) . ",
          " . $db->sqs($_SESSION['user_id']) . ")";
        $result = $db->query($sql);
        if ($result) {
            return $result;
        } else {
            return false;
        }         
      }
   }
        
    public function deleteSheqClubMainTypeFile($fileData = array())
    {
        global $db;
        if(!is_null($fileData)){           
            $sql = "UPDATE tbl_sheqclub_main_types_files SET is_active = 0 WHERE id = ".$db->sqs($fileData["id"]);
            $result = $db->query($sql);
            if($result){
                return true;
            } else {
                return false;
            }            
        }
    }
  
    /**
     * Method to get incident types
     * @access public
     * @global $db
     * @param int $id
     */    
    public function getAllSheqClubMainTypeFiles($file_section,$type_id)
    {
        global $db;
        if(!is_null($type_id)){
            $sql = "SELECT * FROM tbl_sheqclub_main_types_files WHERE is_active = 1";
            if(!empty($type_id))
            {
                $sql .= " and sheqclub_main_types_id = ".$type_id;
            }
            if(!empty($file_section))
            {
                $sql .= " and sheqclub_section = ".$db->sqs($file_section);
            }            
            $response = $db->getAll($sql);
            if($response) {
                return $response;
            } else {
                return false;
            } 
        } else {
            return "Required information is missing";
        }
    }
    public function getSheqClubMainTypeFile($fileData = array())
    {
        global $db;
        if(!is_null($fileData)){
            $sql = "SELECT * FROM tbl_sheqclub_main_types_files WHERE id = " . $db->sqs($fileData["id"]);
            $response = $db->getRow($sql);
            if($response) {
                return $response;
            } else {
                return false;
            } 
        } else {
            return "Required information is missing";
        }
    }
    /**
     * Method to get all incident types
     * @access public
     * @global $db
     * @param int $categoryid
     * @param boolean $activeOnly
     */ 
}
