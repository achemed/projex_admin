<?php
/**
 * Controller class containing methods to process all users related actions
 *
 * @package projex
 * @author Brian Douglas <douglas@innovatorshill.co.za>
 * @copyright (c) 2016, Innovators Hill
 * @license
 */

//Include the base controller
include_once 'controller.php';

class updates extends controller
{



    /**
     * Method to login
     * @return void
     */
    public static function viewAllUpdates()
    {
        global $objClient;
        global $objTemplate;



        $data['allClients'] = $objClient->getAllCompanyClients();
         $objTemplate->setVariables('title', 'Updates');
         $objTemplate->setView('templates/utilities', 'index_tpl', $data);
    }


    /**
     * Method to run quick updates
     */
    public static function runQuickUpdate(){

        global $objUpdates;
        if(!empty($_POST)){



            $companyScript = array("company"=>$_POST['companyId'],
                            "script"=>$_POST['script']
            );

            $response = $objUpdates->runQuickDatabaseUpdate($companyScript);

            if(is_string($response)){

                $data['type'] = 'success';
                $data['message'] = $response;
                controller::nextPage('viewAllUpdates','updates', $data);

            }elseif($response){

                $data['type'] = 'success';
                $data['message'] = 'Updates executed successfully';
                controller::nextPage('viewAllUpdates','updates', $data);
            }else{
                $data['type'] = 'error';
                $data['message'] = 'Could not run system update';
                controller::nextPage('viewAllUpdates','updates', $data);
            }
        }
    }
}