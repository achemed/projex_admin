<?php
/**
 * Controller class containing methods to process all client actions
 *
 * @package projex
 * @author Brian Douglas <douglas@innovatorshill.co.za>
 * @copyright (c) 2016, Innovators Hill
 * @license
 */

//Include the base controller
include_once 'controller.php';

class clients extends controller{


    /**
     * Method to view all clients
     */
    public static function viewAllClients(){
        global $objClient;
        $data = array();
        global $objTemplate;
        global$objPackages;
        $data['allClients'] = $objClient->getAllCompanyClients();
        $data['packages'] = $objPackages->getAllPackages();
        $objTemplate->setVariables('title','client Info');
        $objTemplate->setView('templates/client','index_tpl',$data);
    }
    public static function viewAllInActiveClients(){
        global $objClient;
        $data = array();
        global $objTemplate;
        global$objPackages;
        $val = "InActive";
        $data['allClients'] = $objClient->getAllCompanyClients($val);
        $data['packages'] = $objPackages->getAllPackages();
        $objTemplate->setVariables('title','client Info');
        $objTemplate->setView('templates/client','in_active',$data);
    }


    public static function viewAllTransactLogs()
    {
        global $objTemplate;
        global $objClient;

        $data = array();

        $data['allClients'] = $objClient->getAllCompanyClients();
        $objTemplate->setVariables('title','Company info');
        $objTemplate->setView('templates/client','index_logs_tpl',$data);
    }

    /**
     *
     */
    public static function viewClientDetails()
    {
        global $objClient;
        global $objTemplate;
        $data['clientInfo'] = $objClient->getClientInfo($_GET['id']);
        $data['groups'] = $objClient->getClientGroups($data['clientInfo']['db_name']);
        $objTemplate->setVariables('title', 'Details');
        $objTemplate->setView('templates/client', 'details_tpl', $data);
    }



    public static function addClient(){
        global $objClient;


        if(!empty($_POST)){
            $userDetailsArr['firstname'] = $_POST['firstname'];
            $userDetailsArr['lastname'] = $_POST['lastname'];
            $userDetailsArr['username'] = $_POST['username'];
            $userDetailsArr['email'] = $_POST['username'];
            $userDetailsArr['password'] = $_POST['password'];
            $userDetailsArr['package_id'] = $_POST['package_id'];
            $userDetailsArr['db_name'] = $_POST['db_name'];
            $userDetailsArr['company_name'] = $_POST['company_name'];
            $response = $objClient->addClient($userDetailsArr);


            if(is_string($response)){

                $data['type'] = 'success';
                $data['message'] = $response;
                controller::nextPage('viewAllClients','clients', $data);

            }elseif($response){

                $data['type'] = 'success';
                $data['message'] = 'Admin user successfully created';
                controller::nextPage('viewAllClients','clients', $data);
            }
            else{
                $data['type'] = 'error';
                $data['message'] = 'Could not create administrator account';
                controller::nextPage('viewAllClients','clients', $data);
            }
        }
        else
        {

            $data['type'] = 'error';
            $data['message'] = 'Could not create administrator account';
            controller::nextPage('viewAllClients','clients', $data);
        }
    }

    public static function editClientPackage(){



                global $objClient;


        if(!empty($_POST)){
            $userDetailsArr['package_id'] = $_POST['package_id'];
            $userDetailsArr['client_id'] = $_POST['client_id'];
            $response = $objClient->updateClientPackage($userDetailsArr);


            if(is_string($response)){

                $data['type'] = 'success';
                $data['message'] = $response;
                controller::nextPage('viewAllClients','clients', $data);

            }elseif($response){

                $data['type'] = 'success';
                $data['message'] = 'Package success updated';
                controller::nextPage('viewAllClients','clients', $data);
            }
            else{
                $data['type'] = 'error';
                $data['message'] = 'Could not update packages';
                controller::nextPage('viewAllClients','clients', $data);
            }
        }
        else
        {

            $data['type'] = 'error';
            $data['message'] = 'Could update package';
            controller::nextPage('viewAllClients','clients', $data);
        }

    }

}