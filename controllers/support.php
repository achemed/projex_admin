<?php
/**
 * Controller class containing methods to process all users related actions
 *
 * @package projex
 * @author Brian Douglas <douglas@innovatorshill.co.za>
 * @copyright (c) 2016, Innovators Hill
 * @license
 */

//Include the base controller
include_once 'controller.php';

class support extends controller
{

    /**
     * var  The companies object
     * @access private
     */

    public function constructor()
    {

    }

    public static function viewAllSupportTickets()
    {

        global $objTemplate;
        global $objProducts;
        global $objSupport;

        $data['tickets'] = $objSupport->getAllTickets();
        $objTemplate->setVariables('title', 'Stock');
        $objTemplate->setView('templates/support', 'index_tpl', $data);
    }

    /**
     *
     */
    public static function replySupportTicket(){

        global $objSupport;
        if(!empty($_POST)){

            $processData = array(
                "description"=>$_POST['description'],
                "db_name"=>$_POST['db_name'],
                "support_id"=>$_POST['support_id']

            );

            $response = $objSupport->replySupportTicket($processData);

            if($response){
                $data['type'] = 'success';
                $data['message'] = 'Support added successfully';
                controller::nextPage("viewAllSupportTickets","support",$data);

            }else{
                $data['type'] = 'error';
                $data['message'] = 'Could not add support';
                controller::nextPage("viewAllSupportTickets","support",$data);
            }
        }else{

            $data['type'] = 'warning';
            $data['message'] = 'Please fill required fields';
            controller::nextPage("viewAllSupportTickets","support",$data);
        }
    }

    public static function viewSupportFiles(){

    }
}