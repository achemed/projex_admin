<?php


trait sheq_daily_safety
{
				public static function view_sheq_daily_safety_topics()
			    {
			        global $objTemplate;
			        global $objSheq;
        			$data['allDailySafetyTopics'] = $objSheq->getAllDailySafetyTopic();
			        $objTemplate->setVariables('title', 'All Projects');
			        $objTemplate->setView('templates/sheq/DailySafety', 'topics_tpl',$data);
			    }
			    public static function view_sheq_daily_safety_topics_details()
			    {
			        global $objTemplate;
			        global $objSheq;
        			$data['topicInfo'] = $objSheq->getDailySafetyTopicInfo($_GET['id']);
        			$data['allQuesetions'] = $objSheq->getAllDailySafetyTopicQuestion($_GET['id']);
			        $objTemplate->setVariables('title', 'All Projects');
			        $objTemplate->setView('templates/sheq/DailySafety', 'topic_details_tpl',$data);
			    }
			    

			    public static function addDailySafetyTopic()
    		   {
			        if(isset($_POST["btnAdd"]))
			        {
			        	global $objSheq;
			            $data = array();
			            $data["daily_date"] = $_POST["daily_date"];	
			            $data["topic"] = $_POST["topic"];		
			            $result = $objSheq->addDailySafetyTopic($data);                			            

			            if($result)
			            {
			               $data = array();
			               $data['type'] = 'success';
			               $data['message'] = 'Succeeded ';			
			               controller::nextPage("view_sheq_daily_safety_topics","sheq",$data);
			            }
			            else
			            {                                  
			               $data = array();
			               $data['type'] = 'error';
			               $data['message'] = 'Failed adding';  			
			               controller::nextPage("view_sheq_daily_safety_topics","sheq",$data);
			            }
			        }
			    }	
			    public static function updateDailySafetyTopic()
    		   {
			        if(isset($_POST["btnUpdate"]))
			        {
			        	global $objSheq;
			            $data = array();
			            $data["id"] = $_POST["row_id"];	
			            $data["daily_date"] = $_POST["daily_date"];	
			            $data["topic"] = $_POST["topic"];	
			            $result = $objSheq->editDailySafetyTopic($data);                			            

			            if($result)
			            {
			               $data = array();
			               $data['type'] = 'success';
			               $data['message'] = 'Succeeded updating';			               			               
			               controller::nextPage("view_sheq_daily_safety_topics","sheq",$data);
			            }
			            else
			            {                                  
			               $data = array();
			               $data['type'] = 'error';
			               $data['message'] = 'Failed updating Details';  			                			               
			               controller::nextPage("view_sheq_daily_safety_topics","sheq",$data);
			            }
			        }
			    }



			    //quesetions
			   public static function addDailySafetyTopicQuestions()
    		   {
			        if(isset($_POST["btnAdd"]))
			        {
			        	global $objSheq;
			            $data = array();
			            $data["question"] = $_POST["question"];	
			            $data["answer"] = $_POST["answer"];		
			            $data["topic_id"] = $_POST["topic_id"];	
			            $result = $objSheq->addDailySafetyTopicQuestion($data);                			            

			            if($result)
			            {
			               $data = array();
			               $data['type'] = 'success';
			               $data['message'] = 'Succeeded ';	
			               $data['id'] = $_POST["topic_id"];		
			               controller::nextPage("view_sheq_daily_safety_topics_details","sheq",$data);
			            }
			            else
			            {                                  
			               $data = array();
			               $data['type'] = 'error';
			               $data['message'] = 'Failed adding'; 
			               $data['id'] = $_POST["topic_id"]; 			
			               controller::nextPage("view_sheq_daily_safety_topics_details","sheq",$data);
			            }
			        }
			    }


			    public static function updateDailySafetyTopicDescription()
    		   {
			        if(isset($_POST["btnAdd"]))
			        {
			        	global $objSheq;
			            $data = array();
			            $data["notes"] = $_POST["docContent"];		
			            $data["topic_id"] = $_POST["topic_id"];	
			            $exist = $objSheq->getAllDailySafetyTopicDescription($_POST["topic_id"]);
			            if(empty($exist))
			            {
			            	$result = $objSheq->addDailySafetyTopicDescription($data);
			            }
			            else{
			            	$result = $objSheq->editDailySafetyTopicDescription($data);
			            }


			            if($result)
			            {
			               $data = array();
			               $data['type'] = 'success';
			               $data['message'] = 'Succeeded ';	
			               $data['id'] = $_POST["topic_id"];		
			               controller::nextPage("view_sheq_daily_safety_topics_details","sheq",$data);
			            }
			            else
			            {                                  
			               $data = array();
			               $data['type'] = 'error';
			               $data['message'] = 'Failed adding'; 
			               $data['id'] = $_POST["topic_id"]; 			
			               controller::nextPage("view_sheq_daily_safety_topics_details","sheq",$data);
			            }
			        }
			    }


			    public static function update_daily_safety_topic_status()
			    {
			        global $objSheq;
			         if(isset($_POST['topic_id']))
			         {
			             $result = $objSheq->editDailySafetyTopicStatus($_POST["topic_id"],$_POST["status"]);
			             if($result)
			             {
			                 echo 200;
			             }
			         }
			    }


			}


			   