<?php
/**
 * Controller class containing methods to process all users related actions
 *
 * @package projex
 * @author Brian Douglas <douglas@innovatorshill.co.za>
 * @copyright (c) 2016, Innovators Hill
 * @license
 */

//Include the base controller
include_once 'controller.php';
include_once 'sheq_daily_safety.php';
include_once 'sheq_main_club.php';

class sheq extends controller
{

    /**
     * var  The companies object
     * @access private
     */
    use sheq_daily_safety,sheq_main_club;

    public function constructor()
    {

    }

    public static function viewAllSHEQOptions()
    {

        global $objTemplate;    
        $objTemplate->setVariables('title', 'Stock');
        $objTemplate->setView('templates/sheq', 'index_tpl');
    }    
    public static function view_sheq_documents()
    {
        global $objTemplate;
        global $objSheq;
        $objTemplate->setVariables('title', 'All Projects');
        $objTemplate->setView('templates/sheq/Documents', 'index_tpl');
    }
    public static function view_sheq_legal()
    {
        global $objTemplate;
        global $objSheq;
        $objTemplate->setVariables('title', 'All Projects');
        $objTemplate->setView('templates/sheq/Legal', 'index_tpl');
    }
    public static function view_sheq_training()
    {
        global $objTemplate;
        global $objSheq;
        $objTemplate->setVariables('title', 'All Projects');
        $objTemplate->setView('templates/sheq/Training', 'index_tpl');
    }
    public static function view_sheq_posters()
    {
        global $objTemplate;
        global $objSheq;
        $objTemplate->setVariables('title', 'All Projects');
        $objTemplate->setView('templates/sheq/Posters', 'index_tpl');
    }
    public static function view_sheq_toolbox_talks()
    {
        global $objTemplate;
        global $objSheq;
        $objTemplate->setVariables('title', 'All Projects');
        $objTemplate->setView('templates/sheq/ToolboxTalks', 'index_tpl');
    }

    ////Templates///    
    public static function view_more_info()
    {
        global $objTemplate;
        global $objSheq;        
        $section = null;
        if(isset($_GET['section']))
        {
            $section = $_GET['section'];
        }
        if(isset($data['sheqclub_section']))
        {
            $section = $_POST['sheqclub_section'];
        }        
        $objTemplate->setVariables('title', 'All Projects');        
        $objTemplate->setView('templates/sheq/'.$section, 'templates_tpl');
    }
    public static function view_more_info_files()
    {
        global $objTemplate;
        global $objSheq;        
        $section = null;
        if(isset($_GET['section']))
        {
            $section = $_GET['section'];
        }
        if(isset($data['sheqclub_section']))
        {
            $section = $_POST['sheqclub_section'];
        }        
        $objTemplate->setVariables('title', 'All Projects');        
        $objTemplate->setView('templates/sheq/'.$section, 'docs_tpl');
    }



}