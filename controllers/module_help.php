<?php
/**
 * Controller class containing methods to process all users related actions
 *
 * @package projex
 * @author Brian Douglas <douglas@innovatorshill.co.za>
 * @copyright (c) 2016, Innovators Hill
 * @license
 */

//Include the base controller
include_once 'controller.php';

class module_help extends controller
{


    /**
     * Method to login
     * @return void
     */
    public static function viewAllModules()
    {
        global $objModules;
        global $objTemplate;

        $data['moduleHelp'] = $objModules->getAllModuleHelp();
        $objTemplate->setVariables('title', 'Updates');
        $objTemplate->setView('templates/module_help', 'index_tpl', $data);
    }
    public static function viewAllModuleAbout()
    {
        global $objModules;
        global $objTemplate;        
        $objTemplate->setVariables('title', 'Updates');
        $objTemplate->setView('templates/module_help', 'about_tpl');
    }
    public static function viewAllModuleRecommendedSettings()
    {
        global $objModules;
        global $objTemplate;        
        $objTemplate->setVariables('title', 'Updates');
        $objTemplate->setView('templates/module_help', 'recommendedSettings_tpl');
    }
    public static function viewAllModuleSteps()
    {
        global $objModules;
        global $objTemplate;        
        $objTemplate->setVariables('title', 'Updates');
        $objTemplate->setView('templates/module_help', 'steps_tpl');
    }
    


    /**
     *
     */
    public static function modalViewModuleDetails(){

        global $objModules;
        global $objTemplate;

        $data['moduleInfo'] = $objModules->getModuleHelpInfo($_GET['id']);
        $data['moduleDetails'] = $objModules->getModuleDetailsInfo($_GET['id']);
        $objTemplate->setVariables('title', 'Details');
        $objTemplate->setView('templates/module_help', 'details_tpl', $data);
    }

    public static function addHelpModule(){

        global $objModules;
        if(!empty($_POST)){

            $moduleData  = array("module_name"=>"",
                "description"=>$_POST['description']
            );

            $response = $objModules->addHelpModuleNewFunction($moduleData);

            if(is_string($response)){

                $data['type'] = 'success';
                $data['message'] = $response;
                controller::nextPage('viewAllModules','module_help', $data);

            }elseif($response){

                $data['type'] = 'success';
                $data['message'] = 'Added successfully';
                controller::nextPage('viewAllModules','module_help', $data);
            }else{
                $data['type'] = 'error';
                $data['message'] = 'Could not run system update';
                controller::nextPage('viewAllModules','module_help', $data);
            }
        }
    }


    public static function addHelpModuleSteps(){

        global $objModules;
        if(!empty($_POST)){
           
            $data = array();
            $data['help_id'] = $_POST["help_id"];
            $data['description'] = $_POST["description"];   
            $data['position'] = $_POST["position"];

            $data['id'] = 0;
            $row_id = 0;
            if(!empty($_POST['id']))
            {
              $row_id = $_POST['id'];
              $data['id'] = $_POST['id'];
            }

            $response = false;
            if(!empty($_POST["process_first"]))
            {
                $response = $objModules->addHelpModuleSteps($data);
                $row_id = $response;
            }
            else
            {
                $response = $objModules->addHelpModuleStepsUpdate($data);
            }            
         
            if($response)
            {
                if($row_id > 0)
                {
                    //you may add upload the multifiles
                    if(!empty($_FILES['uploadPhoto']))
                    {
                      $files = $_FILES['uploadPhoto']['name'];
                      $count = count($_FILES['uploadPhoto']['name']);                
                      $counter = 0;
                      foreach($files as $name)
                      {
                          if($_FILES['uploadPhoto']["error"][$counter] == 0)
                          {
                              $time = microtime();
                              $target = ROOT.SITE_UPLOADS_PATH;
                              $target = $target . basename($time.$_SESSION["user_id"].$name);
                              $encrypted_file = $_FILES['uploadPhoto']['tmp_name'][$counter];                          
                              if(move_uploaded_file($encrypted_file,$target))
                              {
                                 $data["filename"] =  $time.$_SESSION["user_id"].$name;
                                 $data["file_type"] = $_FILES['uploadPhoto']["type"][$counter];
                                 $data["file_size"] = $_FILES['uploadPhoto']["size"][$counter];
                                 $data["file_section"] = "Module Helper";  
                                 $data["section_id"] = $row_id;
                                 $result = $objModules->addModuleFile($data);  
                                 if($result)
                                 {
                                     $status = 200;
                                 }
                              }
                          }    
                          $counter++;
                      }
                    }
                }

                $data['type'] = 'success';
                $data['message'] = 'Added successfully';
                $data['id'] = $_POST['help_id'];
                controller::nextPage('viewAllModuleSteps','module_help', $data);
            }else{
                $data['type'] = 'error';
                $data['message'] = 'Could not run system update';
                $data['id'] = $_POST['help_id'];
                controller::nextPage('viewAllModuleSteps','module_help', $data);
            }
        }
    }


    public static function addHelpModuleAttachment(){

        global $objModules;
        if(!empty($_POST)){

            $fileInfo = "";

            if(!empty($_FILES['uploadPhoto']))
            {
                //check the file size, check the               
                $target = ROOT.SITE_UPLOADS_PATH;
                 $time = microtime(true);
                $target = $target . basename($_SESSION['user_id'].'_'.$time.$_FILES['uploadPhoto']['name']);
                $encrypted_file = $_FILES['uploadPhoto']['tmp_name'];            
                if(move_uploaded_file($encrypted_file,$target))
                {
                    $fileInfo = $_SESSION['user_id'].'_'.$time.$_FILES['uploadPhoto']['name'];    
                }          
            }
            else
            {
                $fileInfo = "";
            }

            $moduleData  = array("filename"=>$fileInfo,
                "description"=>$_POST['description'],
                "help_id"=>$_POST['help_id']
            );

            $response = $objModules->addHelpModuleAttachments($moduleData);

            if(is_string($response)){

                $data['type'] = 'success';
                $data['message'] = $response;
                $data['id'] = $_POST['help_id'];
                controller::nextPage('viewAllModuleAbout','module_help', $data);

            }elseif($response){

                $data['type'] = 'success';
                $data['message'] = 'Added successfully';
                $data['id'] = $_POST['help_id'];
                controller::nextPage('viewAllModuleAbout','module_help', $data);
            }else{
                $data['type'] = 'error';
                $data['message'] = 'Could not run system update';
                $data['id'] = $_POST['help_id'];
                controller::nextPage('viewAllModuleAbout','module_help', $data);
            }
        }
    }

    public static function editHelpModule(){

        global $objModules;
        if(!empty($_POST)){

            $moduleData  = array("module_name"=>"",
                "description"=>$_POST['description'],
                "help_id"=>$_POST['help_id']
            );

            $response = $objModules->editHelpModuleNewFunction($moduleData);

            if(is_string($response)){

                $data['type'] = 'success';
                $data['message'] = $response;
                controller::nextPage('viewAllModules','module_help', $data);

            }elseif($response){

                $data['type'] = 'success';
                $data['message'] = 'Updated  successfully';
                controller::nextPage('viewAllModules','module_help', $data);
            }else{
                $data['type'] = 'error';
                $data['message'] = 'Could not run system update';
                controller::nextPage('viewAllModules','module_help', $data);
            }
        }
    }

    public static function updateHelpModule(){

        global $objModules;
        if(!empty($_POST)){

            $moduleData  = array("notes"=>$_POST['docContent'],
                "help_id"=>$_POST['help_id']
            );

            $response = $objModules->updateHelpModule($moduleData);

            if(is_string($response)){
                $data['id'] = $_POST['help_id'];
                $data['type'] = 'success';
                $data['message'] = $response;
                controller::nextPage('viewAllModuleAbout','module_help', $data);

            }elseif($response){
                $data['id'] = $_POST['help_id'];
                $data['type'] = 'success';
                $data['message'] = 'Updates executed successfully';
                controller::nextPage('viewAllModuleAbout','module_help', $data);
            }else{
                $data['id'] = $_POST['help_id'];
                $data['type'] = 'error';
                $data['message'] = 'Could not run system update';
                controller::nextPage('viewAllModuleAbout','module_help', $data);
            }
        }
    }



    public static function updateHelpModuleRecommendedSettings(){

        global $objModules;
        if(!empty($_POST)){

            $moduleData  = array("notes"=>$_POST['docContent'],
                "help_id"=>$_POST['help_id']
            );

            $response = $objModules->updateHelpModuleRecommendedSettings($moduleData);

            if(is_string($response)){
                $data['id'] = $_POST['help_id'];
                $data['type'] = 'success';
                $data['message'] = $response;
                controller::nextPage('viewAllModuleRecommendedSettings','module_help', $data);

            }elseif($response){
                $data['id'] = $_POST['help_id'];
                $data['type'] = 'success';
                $data['message'] = 'Updates executed successfully';
                controller::nextPage('viewAllModuleRecommendedSettings','module_help', $data);
            }else{
                $data['id'] = $_POST['help_id'];
                $data['type'] = 'error';
                $data['message'] = 'Could not run system update';
                controller::nextPage('viewAllModuleRecommendedSettings','module_help', $data);
            }
        }
    }
}