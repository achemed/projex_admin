<?php
/**
 * Controller class containing methods to process all users related actions
 *
 * @package projex
 * @author Brian Douglas <douglas@innovatorshill.co.za>
 * @copyright (c) 2016, Innovators Hill
 * @license
 */

//Include the base controller
include_once 'controller.php';

class mail_template extends controller
{

    /**
     * Method to login
     * @return void
     */
    public static function viewAllTemplates()
    {
        global $objMailTemp;
        global $objTemplate;
        //$data['packages'] = $objPackages->getAllCompanyClients();
        $data['templates'] = $objMailTemp->getAllTemplates();
        $objTemplate->setVariables('title', 'package');
        $objTemplate->setView('templates/mail', 'index_tpl', $data);
    }
    /**
     * Method to login
     * @return void
     */
    public static function viewTemplateDetails()
    {
        global $objMailTemp;
        global $objTemplate;
        $data['templateInfo'] = $objMailTemp->getTemplateInfo($_GET['id']);
        $objTemplate->setVariables('title', 'Details');
        $objTemplate->setView('templates/mail', 'details_tpl', $data);
    }


    public static function addTemplate(){

        global $objMailTemp;
        global $objTemplate;
        if(!empty($_POST)){

            $data = array("template_name"=>$_POST['template_name'],
                "description"=>$_POST['description'],
                "content"=>htmlentities($_POST['content'])
            );
            $response = $objMailTemp->addTemplate($data);

            if(is_string($response)){
                $data['type'] = 'success';
                $data['message'] = $response;
                controller::nextPage('viewAllTemplates','mail_template', $data);

            }elseif($response){

                $data['type'] = 'success';
                $data['message'] = 'Template added successfully';
                controller::nextPage('viewAllTemplates','mail_template', $data);
            }else{
                $data['type'] = 'error';
                $data['message'] = 'Could not add template';
                controller::nextPage('viewAllTemplates','mail_template', $data);
            }
        }
    }

    public static function editTemplate(){

        global $objMailTemp;
        global $objTemplate;
        if(!empty($_POST)){

            $data = array("template_name"=>$_POST['template_name'],
                "description"=>$_POST['description'],
                "content"=>htmlentities($_POST['content']),
                "template_id"=>$_POST['template_id']
            );
            $response = $objMailTemp->editTemplate($data);

            if(is_string($response)){
                $data['type'] = 'success';
                $data['message'] = $response;
                controller::nextPage('viewAllTemplates','mail_template', $data);

            }elseif($response){

                $data['type'] = 'success';
                $data['message'] = 'Template added successfully';
                controller::nextPage('viewAllTemplates','mail_template', $data);
            }else{
                $data['type'] = 'error';
                $data['message'] = 'Could not add template';
                controller::nextPage('viewAllTemplates','mail_template', $data);
            }
        }
    }


}