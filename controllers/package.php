<?php
/**
 * Controller class containing methods to process all users related actions
 *
 * @package projex
 * @author Brian Douglas <douglas@innovatorshill.co.za>
 * @copyright (c) 2016, Innovators Hill
 * @license
 */

//Include the base controller
include_once 'controller.php';

class package extends controller
{

    /**
     * Method to login
     * @return void
     */
    public static function viewAllPackages()
    {
        global $objPackages;
        global $objTemplate;
        //$data['packages'] = $objPackages->getAllCompanyClients();
        $data['packages'] = $objPackages->getAllPackages();
        $objTemplate->setVariables('title', 'package');
        $objTemplate->setView('templates/packages', 'index_tpl', $data);
    }


    /**
     * Method to login
     * @return void
     */
    public static function viewPackageDetails()
    {
        global $objPackages;
        global $objTemplate;
        $data['parent_modules'] = package_module_class::getAllModules();
        $data['global_parent_modules'] = package_module_class::getAllGlobalModules();
        $data['packageInfo'] = $objPackages->getPackageInfo($_GET['id']);
        $objTemplate->setVariables('title', 'Details');
        $objTemplate->setView('templates/packages', 'details_tpl', $data);
    }



    public static function addPackage(){

        global $objPackages;
        global $objTemplate;
        if(!empty($_POST)){

            $companyScript = array("package_name"=>$_POST['package_name'],
                "description"=>$_POST['description']
            );
            $response = $objPackages->addPackage($companyScript);

            if(is_string($response)){

                $data['type'] = 'success';
                $data['message'] = $response;
                controller::nextPage('viewAllPackages','package', $data);

            }elseif($response){

                $data['type'] = 'success';
                $data['message'] = 'package executed successfully';
                controller::nextPage('viewAllPackages','package', $data);
            }else{
                $data['type'] = 'error';
                $data['message'] = 'Could not run system update';
                controller::nextPage('viewAllPackages','package', $data);
            }
        }
    }

    /**
     *
     */
    public static function editPackage(){
        global $objPackages;
        global $objTemplate;
        if(!empty($_POST)){

            $companyScript = array("package_name"=>$_POST['package_name'],
                "description"=>$_POST['description']
            );
            $response = $objPackages->updatePackage($companyScript);

            if(is_string($response)){

                $data['type'] = 'success';
                $data['message'] = $response;
                controller::nextPage('viewAllPackages','package', $data);

            }elseif($response){

                $data['type'] = 'success';
                $data['message'] = 'package executed successfully';
                controller::nextPage('viewAllPackages','package', $data);
            }else{
                $data['type'] = 'error';
                $data['message'] = 'Could not run system update';
                controller::nextPage('viewAllPackages','package', $data);
            }
        }
    }

    public static function addPackageModules(){

        if(!empty($_POST)){

            $packageData = array("package_id"=>$_POST['package_id']
            );

            $main_permission = array();
            if(isset($_POST['permissions'])){
                $main_permission = $_POST['permissions'];
            }
            $sub_permission = array();
            if(isset($_POST['sub_permissions'])){
                $sub_permission = $_POST['sub_permissions'];
            }
/*
            $permissions_actions = array();
            if(isset($_POST['permissions_actions'])){
                $permissions_actions = $_POST['permissions_actions'];
            }*/


            $global_main_permission = array();
            if(isset($_POST['global_permissions'])){
                $global_main_permission = $_POST['global_permissions'];
            }
            $global_sub_permission = array();
            if(isset($_POST['global_sub_permissions'])){
                $global_sub_permission = $_POST['global_sub_permissions'];
            }

           /* $global_permissions_actions = array();
            if(isset($_POST['global_permissions_actions'])){
                $global_permissions_actions = $_POST['global_permissions_actions'];
            }*/

            $response = package_module_class::addPackageModules($main_permission,$sub_permission,$global_main_permission,$global_sub_permission,$packageData);
            if($response){
                $data['id'] = $_POST['package_id'];
                $data['type'] = 'success';
                $data['message'] = 'Role added successfully';
                controller::nextPage("viewPackageDetails","package",$data);

            }else{
                $data['id'] = $_POST['package_id'];
                $data['type'] = 'error';
                $data['message'] = 'Could not role';
                controller::nextPage("viewPackageDetails","package",$data);
            }
        }
    }
}