<?php
/**
 * Controller class containing methods to process all client actions
 *
 * @package projex
 * @author Brian Douglas <douglas@innovatorshill.co.za>
 * @copyright (c) 2016, Innovators Hill
 * @license
 */

//Include the base controller
include_once 'controller.php';

class legal extends controller
{


    /**
     * Method to view all clients
     */
    public static function viewAllLegals()
    {
        global $objClient;
        $data = array();
        global $objTemplate;
        global $objRegions;
        global $objLegal;


        $data['countries'] = $objRegions->getAllCountries();
        $data['legals'] = $objLegal->getAllLegalCountries();
        $objTemplate->setVariables('title', 'Legal Info');
        $objTemplate->setView('templates/legal', 'index_tpl', $data);
    }

    public static function viewLegalCountyDetails(){

        global $objClient;
        $data = array();
        global $objTemplate;
        global $objRegions;
        global $objLegal;

        $data['country_id'] = $_GET['id'];
        $data['countryLegalInfo'] = $objLegal->getLegalCountryInfo($_GET['id']);
        $objTemplate->setVariables('title', 'Legal Info');
        $objTemplate->setView('templates/legal', 'details_tpl', $data);

    }

    public static function viewLegalDetails(){

        global $objClient;
        $data = array();
        global $objTemplate;
        global $objRegions;
        global $objLegal;

        $data['legal_id'] = $_GET['id'];
        $data['legalInfo'] = $objLegal->getLegalnfo($_GET['id']);
        $data['detailsInfo'] = $objLegal->getLegalDetails($_GET['id']);
        $objTemplate->setVariables('title', 'Legal Info');
        $objTemplate->setView('templates/legal', 'legal_details_tpl', $data);

    }

    public static function viewLegalAbout(){

        global $objClient;
        $data = array();
        global $objTemplate;
        global $objRegions;
        global $objLegal;

        $data['legal_id'] = $_GET['id'];
        $data['legalInfo'] = $objLegal->getLegalnfo($_GET['id']);
        $data['aboutInfo'] = $objLegal->getLegalAbout($_GET['id']);
        $objTemplate->setVariables('title', 'Legal Info');
        $objTemplate->setView('templates/legal', 'legal_about_details_tpl', $data);

    }

    public static function viewAboutLegalDefinitions(){

        global $objClient;
        $data = array();
        global $objTemplate;
        global $objRegions;
        global $objLegal;

        $data['legal_id'] = $_GET['id'];
        $data['legalInfo'] = $objLegal->getLegalnfo($_GET['id']);
        $data['definitionInfo'] = $objLegal->getLegalDefinitions($_GET['id']);
        $objTemplate->setVariables('title', 'Legal Info');
        $objTemplate->setView('templates/legal', 'legal_definition_details_tpl', $data);

    }

    public static function addLegal(){

        global $objLegal;
        global $objTemplate;
        if(!empty($_POST)){

            $companyScript = array("country_id"=>$_POST['country_id'],
                "description"=>$_POST['description']
            );
            $response = $objLegal->addLegal($companyScript);

            if(is_string($response)){
                $data['id'] = $_POST['country_id'];
                $data['type'] = 'success';
                $data['message'] = $response;
                controller::nextPage('viewLegalCountyDetails','legal', $data);

            }elseif($response){
                $data['id'] = $_POST['country_id'];
                $data['type'] = 'success';
                $data['message'] = 'package executed successfully';
                controller::nextPage('viewLegalCountyDetails','legal', $data);
            }else{
                $data['id'] = $_POST['country_id'];
                $data['type'] = 'error';
                $data['message'] = 'Could not run system update';
                controller::nextPage('viewLegalCountyDetails','legal', $data);
            }
        }

    }

    public static function addLegalRequirement(){

        global $objLegal;
        global $objTemplate;
        if(!empty($_POST)){

            $companyScript = array("header"=>$_POST['header'],
                "sub_no"=>$_POST['sub_no'],
                 "description"=>$_POST['description'],
                "legal_id"=>$_POST['legal_id']
            );
            $response = $objLegal->addLegalRequirement($companyScript);

            if(is_string($response)){
                $data['id'] = $_POST['legal_id'];
                $data['type'] = 'success';
                $data['message'] = $response;
                controller::nextPage('viewLegalDetails','legal', $data);

            }elseif($response){
                $data['id'] = $_POST['legal_id'];
                $data['type'] = 'success';
                $data['message'] = 'package executed successfully';
                controller::nextPage('viewLegalDetails','legal', $data);
            }else{
                $data['id'] = $_POST['country_id'];
                $data['type'] = 'error';
                $data['message'] = 'Could not run system update';
                controller::nextPage('viewLegalDetails','legal', $data);
            }
        }
    }

    public function addLegalDefinition(){
        global $objLegal;
        global $objTemplate;
        if(!empty($_POST)){

            $companyScript = array("content"=>$_POST['content'],
                "definition"=>$_POST['definition'],
                "legal_id"=>$_POST['legal_id']
            );
            $response = $objLegal->addLegalDefinition($companyScript);

            if(is_string($response)){
                $data['id'] = $_POST['legal_id'];
                $data['type'] = 'success';
                $data['message'] = $response;
                controller::nextPage('viewAboutLegalDefinitions','legal', $data);

            }elseif($response){
                $data['id'] = $_POST['legal_id'];
                $data['type'] = 'success';
                $data['message'] = 'package executed successfully';
                controller::nextPage('viewAboutLegalDefinitions','legal', $data);
            }else{
                $data['id'] = $_POST['country_id'];
                $data['type'] = 'error';
                $data['message'] = 'Could not run system update';
                controller::nextPage('viewAboutLegalDefinitions','legal', $data);
            }
        }
    }

    public static function addLegalAbout(){
        global $objLegal;
        global $objTemplate;
        if(!empty($_POST)){

            $companyScript = array("about"=>$_POST['about'],
                "effective_date"=>$_POST['effective_date'],
                "legal_id"=>$_POST['legal_id']
            );
            $response = $objLegal->addLegalAbout($companyScript);

            if(is_string($response)){
                $data['id'] = $_POST['legal_id'];
                $data['type'] = 'success';
                $data['message'] = $response;
                controller::nextPage('viewLegalAbout','legal', $data);

            }elseif($response){
                $data['id'] = $_POST['legal_id'];
                $data['type'] = 'success';
                $data['message'] = 'package executed successfully';
                controller::nextPage('viewLegalAbout','legal', $data);
            }else{
                $data['id'] = $_POST['country_id'];
                $data['type'] = 'error';
                $data['message'] = 'Could not run system update';
                controller::nextPage('viewLegalAbout','legal', $data);
            }
        }

    }

    public static function editLegalAbout(){
        global $objLegal;
        global $objTemplate;
        if(!empty($_POST)){

            $companyScript = array("about"=>$_POST['about'],
                "effective_date"=>$_POST['effective_date'],
                "about_id"=>$_POST['about_id'],
                "legal_id"=>$_POST['legal_id']
            );
            $response = $objLegal->editLegalAbout($companyScript);

            if(is_string($response)){
                $data['id'] = $_POST['legal_id'];
                $data['type'] = 'success';
                $data['message'] = $response;
                controller::nextPage('viewLegalAbout','legal', $data);

            }elseif($response){
                $data['id'] = $_POST['legal_id'];
                $data['type'] = 'success';
                $data['message'] = 'package executed successfully';
                controller::nextPage('viewLegalAbout','legal', $data);
            }else{
                $data['id'] = $_POST['country_id'];
                $data['type'] = 'error';
                $data['message'] = 'Could not run system update';
                controller::nextPage('viewLegalAbout','legal', $data);
            }
        }

    }

}