<?php
/**
 * Modal class containing methods to process all modal requests
 *
 * @package projex
 * @author Brian Douglas<douglas@innovatorshill.co.za>
 * @copyright (c) 2016, Innovators Hill
 * @license
 */
class modal_request
{


    public function __construct()
    {

    }


    /**
     * Method to call employee edit modal
     */
    public static function removeDetailsFile()
    {
        global $objTemplate;
        global $objProject;
        global $objDocument; 
        global $objOppurtunities;
        global $objClient;
        global $objMaintenance;
        global $objCriticalControlPoint;
        global $objNonConformance;
        global $objCorrectiveAction;
        global $objMaintenance;
        global $objModules;
        
        $statusInfo = "";
        $fileData["id"] = array();
        $fileData["id"] = $_POST["ncid"];

        $result =  $objModules->deleteModuleFile($fileData);        
        if($result)
        {
            $statusInfo = "succeed";
        }
        echo $statusInfo; 
    }
    public static function viewSignedOffDetails()
    {
        global $objTemplate;
        global $objProject;
        global $objDocument; 
        global $objOppurtunities;
        global $objClient;
        global $objMaintenance;
        global $objCriticalControlPoint;
        global $objNonConformance;
        global $objCorrectiveAction;
        global $objMaintenance;
        global $objRiskAssessment;
        global $objTasks;
        global $objAssets;
        global $objModules;
                
        switch($_GET['signOffSection'])
        {


          case "ModuleHelper":
                $data['signOffSection'] = "ModuleHelper";
                $data['allAnswers'] = array();
                //$data['history'] = "True";
                $data['allFiles'] =  $objModules->getAllModuleFilesBySectionID("Module Helper",$_GET["id"]);        
            break;           
        }        
        $objTemplate->setView('templates/generics', 'view_signed_off_details', $data);
    }
    public static function modalAddModuleStep()
    {

        global $objTemplate;
        global $objCompanies;
        $data['id'] = $_GET['id'];
        $objTemplate->setVariables('title', 'Update');
        $objTemplate->setView('templates/module_help/modals', 'add_module_step', $data);
    }

    public static function modalAddModuleHelpAttachment()
    {

        global $objTemplate;
        global $objCompanies;
        $data['id'] = $_GET['id'];
        $objTemplate->setVariables('title', 'Update');
        $objTemplate->setView('templates/module_help/modals', 'add_module_attachment', $data);
    }

    public static function modalQuickUpdateCompaniesScript()
    {

        global $objTemplate;
        global $objCompanies;


        $data['companies'] = $objCompanies->getAllClientCompanies();

        $objTemplate->setVariables('title', 'Update Companies');
        $objTemplate->setView('templates/utilities/modals', 'update_company_script_m', $data);
    }

    public static function modalViewUpdateStrings(){


        global $objTemplate;
        global $objUpdates;


        $data['allUpdates'] = $objUpdates->getAllClientUpdates($_GET['id']);

        $objTemplate->setVariables('title', 'Update Companies');
        $objTemplate->setView('templates/utilities/modals', 'view_update_scripts_m', $data);
    }

    public static function modalAddModuleHelp(){

        global $objTemplate;
        $objTemplate->setVariables('title', 'Update');
        $objTemplate->setView('templates/module_help/modals', 'add_module_help_m', $data=[]);
    }

    public static function modalEditModuleHelp(){
        global $objTemplate;
        global $objModules;

        $data['helperInfo'] = $objModules->getModuleHelpInfo($_GET['id']);
        $objTemplate->setVariables('title', 'Update');
        $objTemplate->setView('templates/module_help/modals', 'edit_module_help_m', $data);
    }

    public static function modalAddPackage(){
        global $objTemplate;

        $objTemplate->setVariables('title', 'Update');
        $objTemplate->setView('templates/packages/modals', 'add_package_m', $data=[]);

    }

    public static function modalEditClientDetails(){

        global $objTemplate;
        global $objClient;
        global $objPackages;

        $data['packages'] = $objPackages->getAllPackages();
        $data['clientInfo'] = $objClient->getClientInfo($_GET['id']);
        $objTemplate->setVariables('title', 'Update Companies');
        $objTemplate->setView('templates/client/modals', 'edit_client_m', $data);
    }

    /**
     *
     */
    public static function modalAddMailTemplate(){

        global $objTemplate;

        $objTemplate->setVariables('title', 'Mail');
        $objTemplate->setView('templates/mail/modals', 'add_template_m', $data=[]);
    }

    public static function modalReplySupportTicket(){

        global $objTemplate;

        $data['db_name'] = $_GET['db_name'];
        $data['id'] = $_GET['id'];
        $objTemplate->setVariables('title', 'Mail');
        $objTemplate->setView('templates/support/modals', 'reply_ticket_m', $data);
    }

    public static function modalAddLegalCountry(){

        global $objTemplate;
        global $objRegions;
        $data['countries'] = $objRegions->getAllCountries();
        $objTemplate->setVariables('title', 'Mail');
        $objTemplate->setView('templates/legal/modals', 'add_legal_m', $data);
    }

    public static function modalAddDailySafetyTopic(){

        global $objTemplate;
        global $objRegions;        
        $objTemplate->setVariables('title', 'Mail');
        $objTemplate->setView('templates/sheq/modals', 'add_daily_safety_topic_m');
    }

    public static function modalAddSheqClubMainTopic(){

        global $objTemplate;
        global $objRegions;      
        $data = array();  
        $data['section'] = $_GET['section'];
        $data['return_url'] = $_GET['returnUrl'];
        $objTemplate->setVariables('title', 'Mail');
        $objTemplate->setView('templates/sheq/modals', 'add_club_main_topic_m',$data);
    }
    public static function modalAddSheqClubMainTopicType(){

        global $objTemplate;
        global $objRegions;      
        $data = array();  
        $data['section'] = $_GET['section'];
        $data['id'] = $_GET['id'];
        $objTemplate->setVariables('title', 'Mail');
        $objTemplate->setView('templates/sheq/modals', 'add_club_main_topic_type_m',$data);
    }
    public static function modalAddSheqClubMainTopicTypeFile(){

        global $objTemplate;
        global $objRegions;      
        $data = array();  
        $data['section'] = $_GET['section'];
        $data['id'] = $_GET['id'];
        $objTemplate->setVariables('title', 'Mail');
        $objTemplate->setView('templates/sheq/modals', 'add_club_main_topic_type_file_m',$data);
    }

    public static function modalAddDailySafetyTopicQuestion(){

        global $objTemplate;
        global $objRegions;        
        $data['topic_id'] = $_GET['id'];
        $objTemplate->setVariables('title', 'Mail');
        $objTemplate->setView('templates/sheq/modals', 'add_daily_safety_topic_question_m',$data);
    }

    public static function modalAddLegalRequirements(){

        global $objTemplate;
        global $objRegions;
        $data['legal_id'] = $_GET['id'];
        $objTemplate->setVariables('title', 'Mail');
        $objTemplate->setView('templates/legal/modals', 'add_legal_requirement_m', $data);

    }

    public static function modalAddLegalDefinition(){

        global $objTemplate;
        global $objRegions;
        $data['legal_id'] = $_GET['id'];
        $objTemplate->setVariables('title', 'Definition');
        $objTemplate->setView('templates/legal/modals', 'add_legal_definition_m', $data);
    }

    public static function modalAddLegalAbout(){

        global $objTemplate;
        global $objRegions;
        $data['legal_id'] = $_GET['id'];
        $objTemplate->setVariables('title', 'Definition');
        $objTemplate->setView('templates/legal/modals', 'add_legal_about_m', $data);
    }

    public static function modalEditLegalAbout(){

        global $objTemplate;
        global $objLegal;

        $data['aboutInfo'] = $objLegal->getLegalAbout($_GET['id']);
        $objTemplate->setVariables('title', 'Definition');
        $objTemplate->setView('templates/legal/modals', 'edit_legal_about_m', $data);
    }
}