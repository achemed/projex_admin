<?php

/**
 * Base controller class
 * 
 * @package projex
 * @author Sunnyboy Mathole <sunny@innovatorshill.co.za>
 * @copyright (c) 2016, Innovators Hill
 * @license 
 */
class controller 
{
    
    /**
     * Function to pass a user to another action. Will be used to pass users to 
     * a new action after forms have been submitted rather than returning a page in the form processing action.
     * 
     * @param string $action The action the user will be passed to
     * @param string $module The module or section of functionality the request relates to
     * @param array  $params This will not be available as yet and will be added later should there be a need for it
     * @return void
     */
    
    public static function nextPage($action = 'defaultAction', $module = 'default', $params = [])
    {
        $string = '';
        if(is_array($params) && !empty($params)){
            foreach($params as $key=>$value){
                if(count($params) > 1){
                    $string .= '&' .$key.'='.urlencode($value) . '&';
                }else{
                    $string .='&'. $key.'='.urlencode($value);
                }  
            }
        }         
        $url = BASE_URL.'/index.php?action='.$action.'&module='.$module.$string;
        header("Location: $url");
    }
    
    /**
     * Is AJAX request?
     *
     * Test to see if a request contains the HTTP_X_REQUESTED_WITH header.
     *
     * @return 	bool
     */
    public static function is_ajax_request()
    {
        return ( ! empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) === 'xmlhttprequest');
    }

    /**
     * Function for a default action.
     * @return void
     */
    public static function defaultAction() 
    {    
        global $objTemplate;
        $data = array();
        
        if(isset($_SESSION['loggedin'])){
            self::nextPage('dashboard', 'auth');
        } else {
            $objTemplate->setVariables('title', 'Login');
            $objTemplate->setView('templates', 'login_tpl', $data);            
        }    
   
    }
}

