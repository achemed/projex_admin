<?php


trait sheq_main_club
{					
				public static function addMainClubTopic()
    		   {
			        if(isset($_POST["btnAdd"]))
			        {
			        	global $objSheq;
			            $data = array();
			            $data["sheqclub_section"] = $_POST["sheqclub_section"];	
			            $data["topic"] = $_POST["topic"];		
			            $result = $objSheq->addSheqClubMain($data);                			            

			            if($result)
			            {
			               $data = array();
			               $data['type'] = 'success';
			               $data['message'] = 'Succeeded ';				
			               controller::nextPage($_POST['return_url'],"sheq",$data);
			            }
			            else
			            {                                  
			               $data = array();
			               $data['type'] = 'error';
			               $data['message'] = 'Failed adding';  			
			               controller::nextPage($_POST['return_url'],"sheq",$data);
			            }
			        }
			    }

			    public static function addMainClubTopicType()
    		   {
			        if(isset($_POST["btnAdd"]))
			        {
			        	global $objSheq;
			            $data = array();
			            $data["sheqclub_main_id"] = $_POST["sheqclub_main_id"];	
			            $data["sheqclub_section"] = $_POST["sheqclub_section"];
			            $data["type_name"] = $_POST["topic"];		
			            $result = $objSheq->addSheqClubMainType($data);

			            //$return_url ='templates/sheq/'.$_POST['sheqclub_section'];              			            

			            if($result)
			            {
			               $data = array();
			               $data['type'] = 'success';
			               $data['message'] = 'Succeeded ';		
			               $data['id'] = $_POST['sheqclub_main_id'];
			               $data['section'] = $_POST['sheqclub_section'];	
			               controller::nextPage('view_more_info',"sheq",$data);
			            }
			            else
			            {                                  
			               $data = array();
			               $data['type'] = 'error';
			               $data['message'] = 'Failed adding';  
			               $data['id'] = $_POST['sheqclub_main_id'];
			               $data['section'] = $_POST['sheqclub_section'];
			               controller::nextPage('view_more_info',"sheq",$data);
			            }
			        }
			    }



			    public static function addMainClubTopicTypeFile()
    		   {
			        if(isset($_POST["btnAdd"]))
			        {
			        	global $objSheq;
			        	$topicTypeInfo = $objSheq->getSheqClubMainTypeInfo($_POST['type_id']);
                $topicInfo = $objSheq->getSheqClubMainInfo($topicTypeInfo['sheqclub_main_id']);

			        	$result = false;
			        	if(!empty($_FILES['uploadPhoto']))
                        {
                            $files = $_FILES['uploadPhoto']['name'];
                            $count = count($_FILES['uploadPhoto']['name']);
                            //print_r($_FILES['uploadPhoto']);                            
                            $counter = 0;
                            foreach($files as $name)
                            {
                                if($_FILES['uploadPhoto']["error"][$counter] == 0)
                                {                                    
                                    $time = microtime();
                                    $target = ROOT.SITE_UPLOADS_PATH;
                                    $target = $target . basename($time.$_SESSION["user_id"].$name);
                                    $encrypted_file = $_FILES['uploadPhoto']['tmp_name'][$counter];
                                    echo $encrypted_file;
                                    if(move_uploaded_file($encrypted_file,$target))
                                    {
                   $data["filename"] =  $time.$_SESSION["user_id"].$name;
                   $data["file_type"] = $_FILES['uploadPhoto']["type"][$counter];
                   $data["file_size"] = $_FILES['uploadPhoto']["size"][$counter];
                   
                   $data["sheqclub_section"] = $_POST['sheqclub_section'];
                   $data['sheqclub_main_types_id'] = $_POST['type_id'];
                   $data['description'] = $_POST['description'];
                   $data['sheqclub_main_id'] = $topicInfo['id'];

                   $result = $objSheq->addSheqClubMainTypeFile($data);                                         
                                    }
                                }    
                                $counter++;
                            }
                        }
			            if($result)
			            {
			               $data = array();
			               $data['type'] = 'success';
			               $data['message'] = 'Succeeded ';		
			               $data['id'] = $_POST['type_id'];
			               $data['section'] = $_POST['sheqclub_section'];	
			              controller::nextPage('view_more_info_files',"sheq",$data);
			            }
			            else
			            {                                  
			               $data = array();
			               $data['type'] = 'error';
			               $data['message'] = 'Failed adding';  
			               $data['id'] = $_POST['type_id'];
			               $data['section'] = $_POST['sheqclub_section'];
			               controller::nextPage('view_more_info_files',"sheq",$data);
			            }
			        }
			    }


			    	


			     public static function update_main_club_topic_status()
			    {
			        global $objSheq;
			         if(isset($_POST['topic_id']))
			         {
			             $result = $objSheq->editSheqClubMainStatus($_POST["topic_id"],$_POST["status"]);
			             if($result)
			             {
			                 echo 200;
			             }
			         }
			    }
			    
			}?>