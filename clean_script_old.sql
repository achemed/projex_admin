-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 13, 2017 at 12:06 PM
-- Server version: 5.7.19-0ubuntu0.16.04.1
-- PHP Version: 7.0.22-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sheqintel_master`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_actions`
--

CREATE TABLE `tbl_actions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `action_name` varchar(50) NOT NULL,
  `module_name` varchar(50) DEFAULT 'system',
  `description` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_appointments`
--

CREATE TABLE `tbl_appointments` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `settings_id` bigint(20) UNSIGNED NOT NULL,
  `appointed_employee_id` bigint(20) UNSIGNED NOT NULL,
  `appointment_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `appointment_expiry_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `is_approved` tinyint(1) NOT NULL DEFAULT '0',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  `document_path` varchar(255) NOT NULL,
  `date_created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` bigint(20) DEFAULT NULL,
  `date_modified` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_appointmnet_settings`
--

CREATE TABLE `tbl_appointmnet_settings` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `appointment_group_id` bigint(20) UNSIGNED NOT NULL,
  `company_id` bigint(20) UNSIGNED NOT NULL,
  `doc_id` bigint(20) UNSIGNED DEFAULT NULL,
  `appointment_name` varchar(45) NOT NULL,
  `renewal_frequency` int(11) DEFAULT NULL,
  `date_created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` bigint(20) DEFAULT NULL,
  `date_modified` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_assets`
--

CREATE TABLE `tbl_assets` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `department_id` bigint(20) UNSIGNED NOT NULL,
  `location_id` bigint(20) NOT NULL,
  `categorytype_id` bigint(20) UNSIGNED NOT NULL,
  `resourcetype_id` bigint(20) UNSIGNED NOT NULL,
  `description` varchar(255) NOT NULL,
  `manufacture_name` varchar(255) NOT NULL,
  `company_asset_number` varchar(255) NOT NULL,
  `serial_number` varchar(255) NOT NULL,
  `date_purchased` timestamp NULL DEFAULT NULL,
  `asset_type` int(11) NOT NULL DEFAULT '1',
  `cost` varchar(100) NOT NULL,
  `notes` text,
  `question_answers` varchar(255) DEFAULT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1',
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NULL DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_assets_involved_cause`
--

CREATE TABLE `tbl_assets_involved_cause` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cause_name` varchar(255) NOT NULL,
  `cause_description` varchar(255) NOT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1',
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NULL DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_assets_involved_effects`
--

CREATE TABLE `tbl_assets_involved_effects` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `effects_name` varchar(255) NOT NULL,
  `effects_description` varchar(255) NOT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1',
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NULL DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_assets_involved_type`
--

CREATE TABLE `tbl_assets_involved_type` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `involvement_name` varchar(255) NOT NULL,
  `involvement_description` varchar(255) NOT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1',
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NULL DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_assets_thirdparty`
--

CREATE TABLE `tbl_assets_thirdparty` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `incident_id` bigint(20) UNSIGNED NOT NULL,
  `assetType` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `company_asset_number` varchar(100) NOT NULL,
  `effect_on_asset` varchar(20) NOT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1',
  `cause` varchar(250) NOT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NULL DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_assets_thirdparty_cause`
--

CREATE TABLE `tbl_assets_thirdparty_cause` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cause_name` varchar(255) NOT NULL,
  `cause_description` varchar(255) NOT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1',
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NULL DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_assets_thirdparty_effects`
--

CREATE TABLE `tbl_assets_thirdparty_effects` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `effects_name` varchar(255) NOT NULL,
  `effects_description` varchar(255) NOT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1',
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NULL DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_asset_category`
--

CREATE TABLE `tbl_asset_category` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1',
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NULL DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_asset_document`
--

CREATE TABLE `tbl_asset_document` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `asset_id` bigint(20) UNSIGNED NOT NULL,
  `document_name` varchar(250) NOT NULL,
  `description` varchar(250) NOT NULL,
  `document_location` varchar(250) NOT NULL,
  `filename` varchar(250) NOT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT '1',
  `created_by` bigint(20) UNSIGNED NOT NULL,
  `modified_by` bigint(20) UNSIGNED NOT NULL,
  `date_modified` timestamp NULL DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_asset_documents`
--

CREATE TABLE `tbl_asset_documents` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `description` text,
  `source_id` bigint(20) NOT NULL,
  `document` text,
  `due_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `is_active` int(11) DEFAULT '1',
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_asset_financial`
--

CREATE TABLE `tbl_asset_financial` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `asset_id` bigint(20) NOT NULL,
  `date_purchased` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `purchase_price` varchar(255) DEFAULT NULL,
  `gl_reference` varchar(255) DEFAULT NULL,
  `asset_life` varchar(255) DEFAULT NULL,
  `asset_residual_value` varchar(255) DEFAULT NULL,
  `asset_exact_value` varchar(255) DEFAULT NULL,
  `asset_insured_for` varchar(255) DEFAULT NULL,
  `branch_id` bigint(20) DEFAULT NULL,
  `company_id` bigint(20) DEFAULT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1',
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_asset_life_span`
--

CREATE TABLE `tbl_asset_life_span` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `asset_id` bigint(20) NOT NULL,
  `date_purchased` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `asset_life` varchar(255) DEFAULT NULL,
  `sell_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `months_to_sell` varchar(255) NOT NULL,
  `branch_id` bigint(20) DEFAULT NULL,
  `company_id` bigint(20) DEFAULT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1',
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_asset_material`
--

CREATE TABLE `tbl_asset_material` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(4) NOT NULL DEFAULT '1',
  `is_active` tinyint(4) NOT NULL DEFAULT '1',
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NULL DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_asset_photo`
--

CREATE TABLE `tbl_asset_photo` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `asset_id` bigint(20) UNSIGNED NOT NULL,
  `document_location` varchar(250) NOT NULL,
  `filename` varchar(250) NOT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT '1',
  `created_by` bigint(20) UNSIGNED NOT NULL,
  `modified_by` bigint(20) UNSIGNED NOT NULL,
  `date_modified` timestamp NULL DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_asset_questions`
--

CREATE TABLE `tbl_asset_questions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `sort` int(11) NOT NULL,
  `is_active` int(11) DEFAULT '1',
  `created_by` bigint(20) UNSIGNED NOT NULL,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NULL DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_asset_relocate`
--

CREATE TABLE `tbl_asset_relocate` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `asset_id` bigint(20) DEFAULT NULL,
  `department_id` bigint(20) DEFAULT NULL,
  `location_id` bigint(20) DEFAULT NULL,
  `prev_department` bigint(20) DEFAULT NULL,
  `prev_location` bigint(20) DEFAULT NULL,
  `is_active` int(11) DEFAULT '1',
  `created_by` bigint(20) DEFAULT NULL,
  `date_created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` bigint(20) DEFAULT NULL,
  `date_modified` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_asset_som`
--

CREATE TABLE `tbl_asset_som` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `description` text,
  `source_id` bigint(20) NOT NULL,
  `is_active` int(11) DEFAULT '1',
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_asset_type`
--

CREATE TABLE `tbl_asset_type` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `category_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1',
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NULL DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_audit_type`
--

CREATE TABLE `tbl_audit_type` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `audit_name` varchar(120) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_awareness`
--

CREATE TABLE `tbl_awareness` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type_id` bigint(20) NOT NULL,
  `group_id` bigint(20) DEFAULT NULL,
  `description` varchar(250) DEFAULT NULL,
  `file_name` text,
  `branch_id` bigint(20) DEFAULT NULL,
  `company_id` bigint(20) DEFAULT NULL,
  `is_active` int(11) DEFAULT '1',
  `created_by` bigint(20) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_awareness_induction_material`
--

CREATE TABLE `tbl_awareness_induction_material` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type_id` bigint(20) NOT NULL,
  `group_id` bigint(20) NOT NULL,
  `description` varchar(250) DEFAULT NULL,
  `file_name` text,
  `branch_id` bigint(20) DEFAULT NULL,
  `company_id` bigint(20) DEFAULT NULL,
  `is_active` int(11) DEFAULT '1',
  `created_by` bigint(20) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_awareness_posters`
--

CREATE TABLE `tbl_awareness_posters` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type_id` bigint(20) NOT NULL,
  `group_id` bigint(20) DEFAULT NULL,
  `description` varchar(250) DEFAULT NULL,
  `file_name` text,
  `branch_id` bigint(20) DEFAULT NULL,
  `company_id` bigint(20) DEFAULT NULL,
  `is_active` int(11) DEFAULT '1',
  `created_by` bigint(20) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_awareness_program`
--

CREATE TABLE `tbl_awareness_program` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `year_col` int(11) NOT NULL,
  `month_col` varchar(20) DEFAULT NULL,
  `group_id` bigint(20) DEFAULT NULL,
  `awareness_topic` varchar(255) DEFAULT NULL,
  `link_toolbox_talks` varchar(255) DEFAULT NULL,
  `link_posters` varchar(255) DEFAULT NULL,
  `link_induction` varchar(255) DEFAULT NULL,
  `outcomes` text,
  `attendance_file` text NOT NULL,
  `branch_id` bigint(20) DEFAULT NULL,
  `company_id` bigint(20) DEFAULT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1',
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_awareness_program_responsible_persons`
--

CREATE TABLE `tbl_awareness_program_responsible_persons` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `department_id` bigint(20) DEFAULT NULL,
  `responsible_user` bigint(20) DEFAULT NULL,
  `attendance_register` text NOT NULL,
  `status` varchar(255) DEFAULT NULL,
  `awareness_program_id` bigint(20) DEFAULT NULL,
  `branch_id` bigint(20) DEFAULT NULL,
  `company_id` bigint(20) DEFAULT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1',
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_awareness_types`
--

CREATE TABLE `tbl_awareness_types` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type_name` varchar(250) DEFAULT NULL,
  `branch_id` bigint(20) DEFAULT NULL,
  `company_id` bigint(20) DEFAULT NULL,
  `is_active` int(11) DEFAULT '1',
  `created_by` bigint(20) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_bee_levels`
--

CREATE TABLE `tbl_bee_levels` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `level_name` varchar(145) DEFAULT NULL,
  `level_description` text,
  `created_by` bigint(20) DEFAULT NULL,
  `modified_by` bigint(20) DEFAULT NULL,
  `date_created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `date_modified` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_bee_levels`
--

INSERT INTO `tbl_bee_levels` (`id`, `level_name`, `level_description`, `created_by`, `modified_by`, `date_created`, `date_modified`) VALUES
(1, 'Level 1 (> 100)', NULL, 1, 1, '2017-08-14 06:10:31', NULL),
(2, 'Level 2 (> 85 - <100)', NULL, 1, 1, '2017-08-14 06:10:31', NULL),
(3, 'Level 3 (75 - 85)', NULL, 1, 1, '2017-08-14 06:11:36', NULL),
(4, 'Level 4 ( 65 - 75)', NULL, 1, 1, '2017-08-14 06:11:36', NULL),
(5, 'Level 5 (55 - 65)', NULL, 1, 1, '2017-08-14 06:12:44', NULL),
(6, 'Level 6 (45 - 55)', NULL, 1, 1, '2017-08-14 06:12:44', NULL),
(7, 'Level 7 (40 - 45', NULL, 1, 1, '2017-08-14 06:13:32', NULL),
(8, 'level 8 (30 -40 )', NULL, 1, 1, '2017-08-14 06:13:32', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_blood_type`
--

CREATE TABLE `tbl_blood_type` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type_name` varchar(145) DEFAULT NULL,
  `type_description` text,
  `created_by` bigint(20) DEFAULT NULL,
  `modified_by` bigint(20) DEFAULT NULL,
  `date_created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `date_modified` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_blood_type`
--

INSERT INTO `tbl_blood_type` (`id`, `type_name`, `type_description`, `created_by`, `modified_by`, `date_created`, `date_modified`) VALUES
(1, 'O', 'Blood Type O', 1, 1, '2017-07-31 04:39:25', NULL),
(2, 'A', 'Blood Type B', 1, 1, '2017-07-31 04:39:25', NULL),
(3, 'B', 'Blood Type B', 1, 1, '2017-07-31 04:39:52', NULL),
(4, 'AB', 'Blood Type AB', 1, 1, '2017-07-31 04:40:30', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_branches`
--

CREATE TABLE `tbl_branches` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `company_id` bigint(20) UNSIGNED NOT NULL,
  `branch_name` varchar(255) NOT NULL,
  `branch_address` text NOT NULL,
  `street` varchar(145) NOT NULL,
  `suburb` varchar(145) NOT NULL,
  `town` varchar(145) NOT NULL,
  `postal_code` varchar(145) NOT NULL,
  `branch_tel_number` varchar(20) NOT NULL,
  `branch_fax_number` varchar(20) NOT NULL,
  `branch_email` varchar(120) NOT NULL,
  `is_head_office` tinyint(1) DEFAULT '0',
  `country_id` varchar(6) NOT NULL,
  `region_id` bigint(20) UNSIGNED NOT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_created` timestamp NULL DEFAULT NULL,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_cities`
--

CREATE TABLE `tbl_cities` (
  `id` bigint(20) UNSIGNED DEFAULT NULL,
  `country` char(6) DEFAULT NULL,
  `region` char(9) DEFAULT NULL,
  `name` varchar(150) DEFAULT NULL,
  `latitude` double DEFAULT NULL,
  `longitude` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_clients`
--

CREATE TABLE `tbl_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_type` bigint(20) UNSIGNED DEFAULT NULL,
  `branch_id` bigint(20) UNSIGNED NOT NULL,
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  `created_by` bigint(20) UNSIGNED NOT NULL,
  `modified_by` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_client_company`
--

CREATE TABLE `tbl_client_company` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `company_name` varchar(120) DEFAULT NULL,
  `pty_number` varchar(50) DEFAULT NULL,
  `vat_number` varchar(50) DEFAULT NULL,
  `office_number` varchar(45) DEFAULT NULL,
  `contact_person` varchar(255) DEFAULT NULL,
  `company_contact_number` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `website` varchar(120) DEFAULT NULL,
  `company_address` varchar(255) DEFAULT NULL,
  `company_town` varchar(120) NOT NULL,
  `company_province` varchar(45) DEFAULT NULL,
  `country_id` varchar(6) NOT NULL,
  `region_id` int(11) UNSIGNED NOT NULL,
  `created_by` bigint(20) UNSIGNED NOT NULL,
  `date_created` timestamp NULL DEFAULT NULL,
  `modified_by` bigint(20) UNSIGNED NOT NULL,
  `date_modified` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_client_individual`
--

CREATE TABLE `tbl_client_individual` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `client_name` varchar(120) DEFAULT NULL,
  `client_surname` varchar(120) DEFAULT NULL,
  `client_gender` int(11) DEFAULT NULL,
  `client_email` varchar(120) DEFAULT NULL,
  `contact_number` varchar(45) DEFAULT NULL,
  `client_address` varchar(120) DEFAULT NULL,
  `client_town` varchar(120) DEFAULT NULL,
  `province` varchar(45) DEFAULT NULL,
  `country_id` varchar(6) NOT NULL,
  `region_id` int(11) NOT NULL,
  `created_by` bigint(20) UNSIGNED NOT NULL,
  `date_created` timestamp NULL DEFAULT NULL,
  `modified_by` bigint(20) UNSIGNED NOT NULL,
  `date_modified` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_client_types`
--

CREATE TABLE `tbl_client_types` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type_name` varchar(120) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_client_types`
--

INSERT INTO `tbl_client_types` (`id`, `type_name`) VALUES
(1, 'Company'),
(2, 'Individual');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_companies`
--

CREATE TABLE `tbl_companies` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `company_name` varchar(255) NOT NULL,
  `vat_number` varchar(50) DEFAULT NULL,
  `company_logo` bigint(20) DEFAULT NULL,
  `sars_certificate` varchar(255) DEFAULT NULL,
  `due_dilligence` varchar(255) DEFAULT NULL,
  `publicliability_insurance` varchar(255) DEFAULT NULL,
  `pty_ck_documents` varchar(255) DEFAULT NULL,
  `ohs_legal_compliance_certificate` varchar(255) DEFAULT NULL,
  `pty_number` varchar(255) DEFAULT NULL,
  `office_address` varchar(255) DEFAULT NULL,
  `street` varchar(145) NOT NULL,
  `suburb` varchar(145) NOT NULL,
  `town` varchar(145) NOT NULL,
  `postal_code` varchar(145) NOT NULL,
  `office_tel_number` varchar(20) DEFAULT NULL,
  `cell_number` varchar(20) DEFAULT NULL,
  `fax_number` varchar(20) DEFAULT NULL,
  `office_email` varchar(120) DEFAULT NULL,
  `financial_department_email` varchar(120) DEFAULT NULL,
  `marketing_department_email` varchar(120) DEFAULT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_created` timestamp NULL DEFAULT NULL,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `country_id` varchar(6) NOT NULL,
  `region_id` bigint(20) UNSIGNED NOT NULL,
  `date_modified` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_company_docs`
--

CREATE TABLE `tbl_company_docs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `document_type` varchar(50) DEFAULT NULL,
  `description` varchar(250) DEFAULT NULL,
  `file_name` text,
  `responsible_person` bigint(20) DEFAULT NULL,
  `expire_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `resources` text NOT NULL,
  `occupations` text NOT NULL,
  `materials` text NOT NULL,
  `appointments` text NOT NULL,
  `customers` text NOT NULL,
  `branch_id` bigint(20) DEFAULT NULL,
  `company_id` bigint(20) NOT NULL,
  `is_active` int(11) DEFAULT '1',
  `created_by` bigint(20) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_compliance`
--

CREATE TABLE `tbl_compliance` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `law_standard` varchar(50) DEFAULT NULL,
  `description` varchar(250) DEFAULT NULL,
  `responsible_person` bigint(20) DEFAULT NULL,
  `branch_id` bigint(20) DEFAULT NULL,
  `company_id` bigint(20) DEFAULT NULL,
  `is_active` int(11) DEFAULT '1',
  `created_by` bigint(20) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_compliance_details`
--

CREATE TABLE `tbl_compliance_details` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `compliance_id` bigint(20) NOT NULL,
  `header` varchar(250) DEFAULT NULL,
  `section` varchar(250) DEFAULT NULL,
  `sub_section` varchar(250) DEFAULT NULL,
  `legal_snip` text,
  `control_measure` text,
  `frequency` varchar(50) DEFAULT NULL,
  `resources` varchar(250) DEFAULT NULL,
  `occupations` varchar(250) DEFAULT NULL,
  `products` varchar(250) DEFAULT NULL,
  `appointments` text NOT NULL,
  `materials` text NOT NULL,
  `customers` text NOT NULL,
  `branch_id` bigint(20) DEFAULT NULL,
  `company_id` bigint(20) DEFAULT NULL,
  `is_active` int(11) DEFAULT '1',
  `created_by` bigint(20) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_compliance_documents`
--

CREATE TABLE `tbl_compliance_documents` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `compliance_id` bigint(20) NOT NULL,
  `description` varchar(250) DEFAULT NULL,
  `expiry_date` timestamp NULL DEFAULT NULL,
  `file_name` text NOT NULL,
  `resources` varchar(250) DEFAULT NULL,
  `occupations` varchar(250) DEFAULT NULL,
  `products` varchar(250) DEFAULT NULL,
  `materials` text NOT NULL,
  `appointments` text NOT NULL,
  `customers` text NOT NULL,
  `branch_id` bigint(20) DEFAULT NULL,
  `company_id` bigint(20) DEFAULT NULL,
  `is_active` int(11) DEFAULT '1',
  `created_by` bigint(20) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_contractor_control`
--

CREATE TABLE `tbl_contractor_control` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `supplier_id` bigint(20) DEFAULT NULL,
  `job_description` varchar(250) DEFAULT NULL,
  `quote_no` varchar(50) DEFAULT NULL,
  `invoice` varchar(50) DEFAULT NULL,
  `po_num` varchar(250) DEFAULT NULL,
  `start_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `responsible_person` bigint(20) DEFAULT NULL,
  `hazard_id` varchar(20) DEFAULT NULL,
  `risk_level` varchar(20) DEFAULT NULL,
  `docs_recieved_status` varchar(20) DEFAULT NULL,
  `docs_recieved_notes` text,
  `docs_recieved_start_date` text,
  `docs_recieved_rules` text,
  `docs_recieved_date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ra_status` varchar(20) DEFAULT NULL,
  `ra_notes` text,
  `method_status` varchar(20) DEFAULT NULL,
  `method_notes` text,
  `fpp_status` varchar(20) DEFAULT NULL,
  `fpp_notes` text,
  `resource_status` varchar(20) DEFAULT NULL,
  `resource_notes` text,
  `resource_vehicles` bigint(20) DEFAULT NULL,
  `resource_height_working` bigint(20) DEFAULT NULL,
  `resource_hot_working` bigint(20) DEFAULT NULL,
  `resource_vessel_under_pressure` bigint(20) DEFAULT NULL,
  `resource_electrical` bigint(20) DEFAULT NULL,
  `team_status` varchar(20) DEFAULT NULL,
  `team_notes` text,
  `team_total` int(11) DEFAULT NULL,
  `team_certificate_valid` tinyint(1) DEFAULT NULL,
  `induction_status` varchar(20) DEFAULT NULL,
  `induction_notes` text,
  `induction_file` text,
  `signed_off_by` bigint(20) DEFAULT NULL,
  `signed_off_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `branch_id` bigint(20) DEFAULT NULL,
  `company_id` bigint(20) DEFAULT NULL,
  `is_active` int(11) DEFAULT '1',
  `created_by` bigint(20) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_contractor_control_details`
--

CREATE TABLE `tbl_contractor_control_details` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `contractor_control_id` bigint(20) DEFAULT NULL,
  `contractor_name` varchar(250) DEFAULT NULL,
  `contractor_position` varchar(250) DEFAULT NULL,
  `contractor_num` varchar(250) DEFAULT NULL,
  `safety_company_name` varchar(250) DEFAULT NULL,
  `safety_fullname` varchar(250) DEFAULT NULL,
  `safety_contact` varchar(50) DEFAULT NULL,
  `safety_email` varchar(250) DEFAULT NULL,
  `office_number` varchar(20) DEFAULT NULL,
  `office_email` varchar(250) DEFAULT NULL,
  `notes` text,
  `branch_id` bigint(20) DEFAULT NULL,
  `company_id` bigint(20) DEFAULT NULL,
  `is_active` int(11) DEFAULT '1',
  `created_by` bigint(20) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_contract_control_type`
--

CREATE TABLE `tbl_contract_control_type` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type_name` varchar(250) DEFAULT NULL,
  `category` varchar(250) DEFAULT NULL,
  `branch_id` bigint(20) DEFAULT NULL,
  `company_id` bigint(20) DEFAULT NULL,
  `is_active` int(11) DEFAULT '1',
  `created_by` bigint(20) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_corrective_actions`
--

CREATE TABLE `tbl_corrective_actions` (
  `id` bigint(20) NOT NULL,
  `department_id` bigint(20) DEFAULT NULL,
  `location_id` bigint(20) NOT NULL,
  `branch_id` bigint(20) NOT NULL,
  `company_id` bigint(20) NOT NULL,
  `type_field` int(11) DEFAULT NULL,
  `priority` varchar(20) DEFAULT NULL,
  `system_element` varchar(50) DEFAULT NULL,
  `non_description` text NOT NULL,
  `recom_description` text NOT NULL,
  `source_id` bigint(20) UNSIGNED DEFAULT NULL,
  `why_id` bigint(20) NOT NULL,
  `source_desc` varchar(250) NOT NULL,
  `description` text,
  `person_responsibile` bigint(20) DEFAULT NULL,
  `notify_duty_manager` bigint(20) DEFAULT NULL,
  `notify_person` bigint(20) DEFAULT NULL,
  `master_sign_off` bigint(20) DEFAULT NULL,
  `target_date` timestamp NULL DEFAULT NULL,
  `upload_photo` text,
  `permit_photo` text,
  `procedure_link` bigint(20) DEFAULT NULL,
  `form_link` bigint(20) DEFAULT NULL,
  `method_statement_link` bigint(20) DEFAULT NULL,
  `resource_id` bigint(20) DEFAULT NULL,
  `approval_required` tinyint(1) DEFAULT NULL,
  `committe` bigint(20) DEFAULT NULL,
  `branch_level` bigint(20) DEFAULT NULL,
  `group_level` bigint(20) DEFAULT NULL,
  `motivation` text,
  `estimate_cost` varchar(250) DEFAULT NULL,
  `longevity_correction_action` text,
  `upload_documents` text,
  `status` varchar(20) DEFAULT NULL,
  `is_active` int(11) NOT NULL,
  `in_active_reason` text NOT NULL,
  `foreignkey_id` bigint(20) NOT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `attended` int(11) NOT NULL,
  `signedOff` int(11) NOT NULL,
  `SignedOffDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `signedOffBy` bigint(20) NOT NULL,
  `signedOffAction` text,
  `person_involved` text NOT NULL,
  `supplier_id` bigint(20) NOT NULL,
  `customerID` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_corrective_action_types`
--

CREATE TABLE `tbl_corrective_action_types` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `group_id` bigint(20) NOT NULL,
  `type_name` varchar(250) DEFAULT NULL,
  `is_default` int(11) DEFAULT NULL,
  `branch_id` bigint(20) DEFAULT NULL,
  `company_id` bigint(20) DEFAULT NULL,
  `is_active` int(11) DEFAULT '1',
  `created_by` bigint(20) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_countries`
--

CREATE TABLE `tbl_countries` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `iso` char(6) DEFAULT NULL,
  `name` varchar(240) DEFAULT NULL,
  `nicename` varchar(240) DEFAULT NULL,
  `iso3` char(9) DEFAULT NULL,
  `numcode` smallint(6) DEFAULT NULL,
  `phonecode` int(5) DEFAULT NULL,
  `nationality` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_countries`
--

INSERT INTO `tbl_countries` (`id`, `iso`, `name`, `nicename`, `iso3`, `numcode`, `phonecode`, `nationality`) VALUES
(1, 'AF', 'AFGHANISTAN', 'Afghanistan', 'AFG', 4, 93, 'Afghan'),
(2, 'AL', 'ALBANIA', 'Albania', 'ALB', 8, 355, 'Albanian'),
(3, 'DZ', 'ALGERIA', 'Algeria', 'DZA', 12, 213, 'Algerian'),
(4, 'AS', 'AMERICAN SAMOA', 'American Samoa', 'ASM', 16, 1684, 'American Samoan'),
(5, 'AD', 'ANDORRA', 'Andorra', 'AND', 20, 376, 'Andorran'),
(6, 'AO', 'ANGOLA', 'Angola', 'AGO', 24, 244, 'Angolan'),
(7, 'AI', 'ANGUILLA', 'Anguilla', 'AIA', 660, 1264, 'Anguillan'),
(8, 'AQ', 'ANTARCTICA', 'Antarctica', NULL, NULL, 0, 'Antarctic'),
(9, 'AG', 'ANTIGUA AND BARBUDA', 'Antigua and Barbuda', 'ATG', 28, 1268, 'Antiguan or Barbudan'),
(10, 'AR', 'ARGENTINA', 'Argentina', 'ARG', 32, 54, 'Argentine'),
(11, 'AM', 'ARMENIA', 'Armenia', 'ARM', 51, 374, 'Armenian'),
(12, 'AW', 'ARUBA', 'Aruba', 'ABW', 533, 297, 'Aruban'),
(13, 'AU', 'AUSTRALIA', 'Australia', 'AUS', 36, 61, 'Australian'),
(14, 'AT', 'AUSTRIA', 'Austria', 'AUT', 40, 43, 'Austrian'),
(15, 'AZ', 'AZERBAIJAN', 'Azerbaijan', 'AZE', 31, 994, 'Azerbaijani, Azeri'),
(16, 'BS', 'BAHAMAS', 'Bahamas', 'BHS', 44, 1242, 'Bahamian'),
(17, 'BH', 'BAHRAIN', 'Bahrain', 'BHR', 48, 973, 'Bahraini'),
(18, 'BD', 'BANGLADESH', 'Bangladesh', 'BGD', 50, 880, 'Bangladeshi'),
(19, 'BB', 'BARBADOS', 'Barbados', 'BRB', 52, 1246, 'Barbadian'),
(20, 'BY', 'BELARUS', 'Belarus', 'BLR', 112, 375, 'Belarusian'),
(21, 'BE', 'BELGIUM', 'Belgium', 'BEL', 56, 32, 'Belgian'),
(22, 'BZ', 'BELIZE', 'Belize', 'BLZ', 84, 501, 'Belizean'),
(23, 'BJ', 'BENIN', 'Benin', 'BEN', 204, 229, 'Beninese, Beninois'),
(24, 'BM', 'BERMUDA', 'Bermuda', 'BMU', 60, 1441, 'Bermudian, Bermudan'),
(25, 'BT', 'BHUTAN', 'Bhutan', 'BTN', 64, 975, 'Bhutanese'),
(26, 'BO', 'BOLIVIA', 'Bolivia', 'BOL', 68, 591, 'Bolivian'),
(27, 'BA', 'BOSNIA AND HERZEGOVINA', 'Bosnia and Herzegovina', 'BIH', 70, 387, 'Bosnian or Herzegovinian'),
(28, 'BW', 'BOTSWANA', 'Botswana', 'BWA', 72, 267, 'Motswana, Botswanan'),
(29, 'BV', 'BOUVET ISLAND', 'Bouvet Island', NULL, NULL, 0, 'Bouvet Island'),
(30, 'BR', 'BRAZIL', 'Brazil', 'BRA', 76, 55, 'Brazilian'),
(31, 'IO', 'BRITISH INDIAN OCEAN TERRITORY', 'British Indian Ocean Territory', NULL, NULL, 246, 'BIOT'),
(32, 'BN', 'BRUNEI DARUSSALAM', 'Brunei Darussalam', 'BRN', 96, 673, 'Bruneian'),
(33, 'BG', 'BULGARIA', 'Bulgaria', 'BGR', 100, 359, 'Bulgarian'),
(34, 'BF', 'BURKINA FASO', 'Burkina Faso', 'BFA', 854, 226, 'Burkinabé'),
(35, 'BI', 'BURUNDI', 'Burundi', 'BDI', 108, 257, 'Burundian'),
(36, 'KH', 'CAMBODIA', 'Cambodia', 'KHM', 116, 855, 'Cambodian'),
(37, 'CM', 'CAMEROON', 'Cameroon', 'CMR', 120, 237, 'Cameroonian'),
(38, 'CA', 'CANADA', 'Canada', 'CAN', 124, 1, 'Canadian'),
(39, 'CV', 'CAPE VERDE', 'Cape Verde', 'CPV', 132, 238, 'Cabo Verdean'),
(40, 'KY', 'CAYMAN ISLANDS', 'Cayman Islands', 'CYM', 136, 1345, 'Caymanian'),
(41, 'CF', 'CENTRAL AFRICAN REPUBLIC', 'Central African Republic', 'CAF', 140, 236, 'Central African'),
(42, 'TD', 'CHAD', 'Chad', 'TCD', 148, 235, 'Chadian'),
(43, 'CL', 'CHILE', 'Chile', 'CHL', 152, 56, 'Chilean'),
(44, 'CN', 'CHINA', 'China', 'CHN', 156, 86, 'Chinese'),
(45, 'CX', 'CHRISTMAS ISLAND', 'Christmas Island', NULL, NULL, 61, 'Christmas Island'),
(46, 'CC', 'COCOS (KEELING) ISLANDS', 'Cocos (Keeling) Islands', NULL, NULL, 672, 'Cocos Island'),
(47, 'CO', 'COLOMBIA', 'Colombia', 'COL', 170, 57, 'Colombian'),
(48, 'KM', 'COMOROS', 'Comoros', 'COM', 174, 269, 'Comoran, Comorian'),
(49, 'CG', 'CONGO', 'Congo', 'COG', 178, 242, 'Congolese'),
(50, 'CD', 'CONGO, THE DEMOCRATIC REPUBLIC OF THE', 'Congo, the Democratic Republic of the', 'COD', 180, 242, 'Congolese'),
(51, 'CK', 'COOK ISLANDS', 'Cook Islands', 'COK', 184, 682, 'Cook Island'),
(52, 'CR', 'COSTA RICA', 'Costa Rica', 'CRI', 188, 506, 'Costa Rican'),
(53, 'CI', 'COTE D\'IVOIRE', 'Cote D\'Ivoire', 'CIV', 384, 225, 'Ivorian'),
(54, 'HR', 'CROATIA', 'Croatia', 'HRV', 191, 385, 'Croatian'),
(55, 'CU', 'CUBA', 'Cuba', 'CUB', 192, 53, 'Cuban'),
(56, 'CY', 'CYPRUS', 'Cyprus', 'CYP', 196, 357, 'Cypriot'),
(57, 'CZ', 'CZECH REPUBLIC', 'Czech Republic', 'CZE', 203, 420, 'Czech'),
(58, 'DK', 'DENMARK', 'Denmark', 'DNK', 208, 45, 'Danish'),
(59, 'DJ', 'DJIBOUTI', 'Djibouti', 'DJI', 262, 253, 'Djiboutian'),
(60, 'DM', 'DOMINICA', 'Dominica', 'DMA', 212, 1767, 'Dominican'),
(61, 'DO', 'DOMINICAN REPUBLIC', 'Dominican Republic', 'DOM', 214, 1809, 'Dominican'),
(62, 'EC', 'ECUADOR', 'Ecuador', 'ECU', 218, 593, 'Ecuadorian'),
(63, 'EG', 'EGYPT', 'Egypt', 'EGY', 818, 20, 'Egyptian'),
(64, 'SV', 'EL SALVADOR', 'El Salvador', 'SLV', 222, 503, 'Salvadoran'),
(65, 'GQ', 'EQUATORIAL GUINEA', 'Equatorial Guinea', 'GNQ', 226, 240, 'Equatorial Guinean, Equatoguinean'),
(66, 'ER', 'ERITREA', 'Eritrea', 'ERI', 232, 291, 'Eritrean'),
(67, 'EE', 'ESTONIA', 'Estonia', 'EST', 233, 372, 'Estonian'),
(68, 'ET', 'ETHIOPIA', 'Ethiopia', 'ETH', 231, 251, 'Ethiopian'),
(69, 'FK', 'FALKLAND ISLANDS (MALVINAS)', 'Falkland Islands (Malvinas)', 'FLK', 238, 500, 'Falkland Island'),
(70, 'FO', 'FAROE ISLANDS', 'Faroe Islands', 'FRO', 234, 298, 'Faroese'),
(71, 'FJ', 'FIJI', 'Fiji', 'FJI', 242, 679, 'Fijian'),
(72, 'FI', 'FINLAND', 'Finland', 'FIN', 246, 358, 'Finnish'),
(73, 'FR', 'FRANCE', 'France', 'FRA', 250, 33, 'French'),
(74, 'GF', 'FRENCH GUIANA', 'French Guiana', 'GUF', 254, 594, 'French Guianese'),
(75, 'PF', 'FRENCH POLYNESIA', 'French Polynesia', 'PYF', 258, 689, 'French Polynesian'),
(76, 'TF', 'FRENCH SOUTHERN TERRITORIES', 'French Southern Territories', NULL, NULL, 0, 'French Southern Territories'),
(77, 'GA', 'GABON', 'Gabon', 'GAB', 266, 241, 'Gabonese'),
(78, 'GM', 'GAMBIA', 'Gambia', 'GMB', 270, 220, 'Gambian'),
(79, 'GE', 'GEORGIA', 'Georgia', 'GEO', 268, 995, 'Georgian'),
(80, 'DE', 'GERMANY', 'Germany', 'DEU', 276, 49, 'German'),
(81, 'GH', 'GHANA', 'Ghana', 'GHA', 288, 233, 'Ghanaian'),
(82, 'GI', 'GIBRALTAR', 'Gibraltar', 'GIB', 292, 350, 'Gibraltar'),
(83, 'GR', 'GREECE', 'Greece', 'GRC', 300, 30, 'Greek, Hellenic'),
(84, 'GL', 'GREENLAND', 'Greenland', 'GRL', 304, 299, 'Greenlandic'),
(85, 'GD', 'GRENADA', 'Grenada', 'GRD', 308, 1473, 'Grenadian'),
(86, 'GP', 'GUADELOUPE', 'Guadeloupe', 'GLP', 312, 590, 'Guadeloupe'),
(87, 'GU', 'GUAM', 'Guam', 'GUM', 316, 1671, 'Guamanian, Guambat'),
(88, 'GT', 'GUATEMALA', 'Guatemala', 'GTM', 320, 502, 'Guatemalan'),
(89, 'GN', 'GUINEA', 'Guinea', 'GIN', 324, 224, 'Guinean'),
(90, 'GW', 'GUINEA-BISSAU', 'Guinea-Bissau', 'GNB', 624, 245, 'Bissau-Guinean'),
(91, 'GY', 'GUYANA', 'Guyana', 'GUY', 328, 592, 'Guyanese'),
(92, 'HT', 'HAITI', 'Haiti', 'HTI', 332, 509, 'Haitian'),
(93, 'HM', 'HEARD ISLAND AND MCDONALD ISLANDS', 'Heard Island and Mcdonald Islands', NULL, NULL, 0, 'Heard Island or McDonald Islands'),
(94, 'VA', 'HOLY SEE (VATICAN CITY STATE)', 'Holy See (Vatican City State)', 'VAT', 336, 39, 'Vatican'),
(95, 'HN', 'HONDURAS', 'Honduras', 'HND', 340, 504, 'Honduran'),
(96, 'HK', 'HONG KONG', 'Hong Kong', 'HKG', 344, 852, 'Hong Kong, Hong Kongese'),
(97, 'HU', 'HUNGARY', 'Hungary', 'HUN', 348, 36, 'Hungarian, Magyar'),
(98, 'IS', 'ICELAND', 'Iceland', 'ISL', 352, 354, 'Icelandic'),
(99, 'IN', 'INDIA', 'India', 'IND', 356, 91, 'Indian'),
(100, 'ID', 'INDONESIA', 'Indonesia', 'IDN', 360, 62, 'Indonesian'),
(101, 'IR', 'IRAN, ISLAMIC REPUBLIC OF', 'Iran, Islamic Republic of', 'IRN', 364, 98, 'Iranian, Persian'),
(102, 'IQ', 'IRAQ', 'Iraq', 'IRQ', 368, 964, 'Iraqi'),
(103, 'IE', 'IRELAND', 'Ireland', 'IRL', 372, 353, 'Irish'),
(104, 'IL', 'ISRAEL', 'Israel', 'ISR', 376, 972, 'Israeli'),
(105, 'IT', 'ITALY', 'Italy', 'ITA', 380, 39, 'Italian'),
(106, 'JM', 'JAMAICA', 'Jamaica', 'JAM', 388, 1876, 'Jamaican'),
(107, 'JP', 'JAPAN', 'Japan', 'JPN', 392, 81, 'Japanese'),
(108, 'JO', 'JORDAN', 'Jordan', 'JOR', 400, 962, 'Jordanian'),
(109, 'KZ', 'KAZAKHSTAN', 'Kazakhstan', 'KAZ', 398, 7, 'Kazakhstani, Kazakh'),
(110, 'KE', 'KENYA', 'Kenya', 'KEN', 404, 254, 'Kenyan'),
(111, 'KI', 'KIRIBATI', 'Kiribati', 'KIR', 296, 686, 'I-Kiribati'),
(112, 'KP', 'KOREA, DEMOCRATIC PEOPLE\'S REPUBLIC OF', 'Korea, Democratic People\'s Republic of', 'PRK', 408, 850, 'North Korean'),
(113, 'KR', 'KOREA, REPUBLIC OF', 'Korea, Republic of', 'KOR', 410, 82, 'South Korean'),
(114, 'KW', 'KUWAIT', 'Kuwait', 'KWT', 414, 965, 'Kuwaiti'),
(115, 'KG', 'KYRGYZSTAN', 'Kyrgyzstan', 'KGZ', 417, 996, 'Kyrgyzstani, Kyrgyz, Kirgiz, Kirghiz'),
(116, 'LA', 'LAO PEOPLE\'S DEMOCRATIC REPUBLIC', 'Lao People\'s Democratic Republic', 'LAO', 418, 856, 'Lao, Laotian'),
(117, 'LV', 'LATVIA', 'Latvia', 'LVA', 428, 371, 'Latvian'),
(118, 'LB', 'LEBANON', 'Lebanon', 'LBN', 422, 961, 'Lebanese'),
(119, 'LS', 'LESOTHO', 'Lesotho', 'LSO', 426, 266, 'Basotho'),
(120, 'LR', 'LIBERIA', 'Liberia', 'LBR', 430, 231, 'Liberian'),
(121, 'LY', 'LIBYAN ARAB JAMAHIRIYA', 'Libyan Arab Jamahiriya', 'LBY', 434, 218, 'Libyan'),
(122, 'LI', 'LIECHTENSTEIN', 'Liechtenstein', 'LIE', 438, 423, 'Liechtenstein'),
(123, 'LT', 'LITHUANIA', 'Lithuania', 'LTU', 440, 370, 'Lithuanian'),
(124, 'LU', 'LUXEMBOURG', 'Luxembourg', 'LUX', 442, 352, 'Luxembourg, Luxembourgish'),
(125, 'MO', 'MACAO', 'Macao', 'MAC', 446, 853, 'Macanese, Chinese'),
(126, 'MK', 'MACEDONIA, THE FORMER YUGOSLAV REPUBLIC OF', 'Macedonia, the Former Yugoslav Republic of', 'MKD', 807, 389, 'Macedonian'),
(127, 'MG', 'MADAGASCAR', 'Madagascar', 'MDG', 450, 261, 'Malagasy'),
(128, 'MW', 'MALAWI', 'Malawi', 'MWI', 454, 265, 'Malawian'),
(129, 'MY', 'MALAYSIA', 'Malaysia', 'MYS', 458, 60, 'Malaysian'),
(130, 'MV', 'MALDIVES', 'Maldives', 'MDV', 462, 960, 'Maldivian'),
(131, 'ML', 'MALI', 'Mali', 'MLI', 466, 223, 'Malian, Malinese'),
(132, 'MT', 'MALTA', 'Malta', 'MLT', 470, 356, 'Maltese'),
(133, 'MH', 'MARSHALL ISLANDS', 'Marshall Islands', 'MHL', 584, 692, 'Marshallese'),
(134, 'MQ', 'MARTINIQUE', 'Martinique', 'MTQ', 474, 596, 'Martiniquais, Martinican'),
(135, 'MR', 'MAURITANIA', 'Mauritania', 'MRT', 478, 222, 'Mauritanian'),
(136, 'MU', 'MAURITIUS', 'Mauritius', 'MUS', 480, 230, 'Mauritian'),
(137, 'YT', 'MAYOTTE', 'Mayotte', NULL, NULL, 269, 'Mahoran'),
(138, 'MX', 'MEXICO', 'Mexico', 'MEX', 484, 52, 'Mexican'),
(139, 'FM', 'MICRONESIA, FEDERATED STATES OF', 'Micronesia, Federated States of', 'FSM', 583, 691, 'Micronesian'),
(140, 'MD', 'MOLDOVA, REPUBLIC OF', 'Moldova, Republic of', 'MDA', 498, 373, 'Moldovan'),
(141, 'MC', 'MONACO', 'Monaco', 'MCO', 492, 377, 'Monégasque, Monacan'),
(142, 'MN', 'MONGOLIA', 'Mongolia', 'MNG', 496, 976, 'Mongolian'),
(143, 'MS', 'MONTSERRAT', 'Montserrat', 'MSR', 500, 1664, 'Montserratian'),
(144, 'MA', 'MOROCCO', 'Morocco', 'MAR', 504, 212, 'Moroccan'),
(145, 'MZ', 'MOZAMBIQUE', 'Mozambique', 'MOZ', 508, 258, 'Mozambican'),
(146, 'MM', 'MYANMAR', 'Myanmar', 'MMR', 104, 95, 'Burmese'),
(147, 'NA', 'NAMIBIA', 'Namibia', 'NAM', 516, 264, 'Namibian'),
(148, 'NR', 'NAURU', 'Nauru', 'NRU', 520, 674, 'Nauruan'),
(149, 'NP', 'NEPAL', 'Nepal', 'NPL', 524, 977, 'Nepali, Nepalese'),
(150, 'NL', 'NETHERLANDS', 'Netherlands', 'NLD', 528, 31, 'Dutch, Netherlandic'),
(151, 'AN', 'NETHERLANDS ANTILLES', 'Netherlands Antilles', 'ANT', 530, 599, ''),
(152, 'NC', 'NEW CALEDONIA', 'New Caledonia', 'NCL', 540, 687, 'New Caledonian'),
(153, 'NZ', 'NEW ZEALAND', 'New Zealand', 'NZL', 554, 64, 'New Zealand, NZ'),
(154, 'NI', 'NICARAGUA', 'Nicaragua', 'NIC', 558, 505, 'Nicaraguan'),
(155, 'NE', 'NIGER', 'Niger', 'NER', 562, 227, 'Nigerien'),
(156, 'NG', 'NIGERIA', 'Nigeria', 'NGA', 566, 234, 'Nigerian'),
(157, 'NU', 'NIUE', 'Niue', 'NIU', 570, 683, 'Niuean'),
(158, 'NF', 'NORFOLK ISLAND', 'Norfolk Island', 'NFK', 574, 672, 'Norfolk Island'),
(159, 'MP', 'NORTHERN MARIANA ISLANDS', 'Northern Mariana Islands', 'MNP', 580, 1670, 'Northern Marianan'),
(160, 'NO', 'NORWAY', 'Norway', 'NOR', 578, 47, 'Norwegian'),
(161, 'OM', 'OMAN', 'Oman', 'OMN', 512, 968, 'Omani'),
(162, 'PK', 'PAKISTAN', 'Pakistan', 'PAK', 586, 92, 'Pakistani'),
(163, 'PW', 'PALAU', 'Palau', 'PLW', 585, 680, 'Palauan'),
(164, 'PS', 'PALESTINIAN TERRITORY, OCCUPIED', 'Palestinian Territory, Occupied', NULL, NULL, 970, 'Palestinian'),
(165, 'PA', 'PANAMA', 'Panama', 'PAN', 591, 507, 'Panamanian'),
(166, 'PG', 'PAPUA NEW GUINEA', 'Papua New Guinea', 'PNG', 598, 675, 'Papua New Guinean, Papuan'),
(167, 'PY', 'PARAGUAY', 'Paraguay', 'PRY', 600, 595, 'Paraguayan'),
(168, 'PE', 'PERU', 'Peru', 'PER', 604, 51, 'Peruvian'),
(169, 'PH', 'PHILIPPINES', 'Philippines', 'PHL', 608, 63, 'Philippine, Filipino'),
(170, 'PN', 'PITCAIRN', 'Pitcairn', 'PCN', 612, 0, 'Pitcairn Island'),
(171, 'PL', 'POLAND', 'Poland', 'POL', 616, 48, 'Polish'),
(172, 'PT', 'PORTUGAL', 'Portugal', 'PRT', 620, 351, 'Portuguese'),
(173, 'PR', 'PUERTO RICO', 'Puerto Rico', 'PRI', 630, 1787, 'Puerto Rican'),
(174, 'QA', 'QATAR', 'Qatar', 'QAT', 634, 974, 'Qatari'),
(175, 'RE', 'REUNION', 'Reunion', 'REU', 638, 262, 'Réunionese, Réunionnais'),
(176, 'RO', 'ROMANIA', 'Romania', 'ROM', 642, 40, 'Romanian'),
(177, 'RU', 'RUSSIAN FEDERATION', 'Russian Federation', 'RUS', 643, 70, 'Russian'),
(178, 'RW', 'RWANDA', 'Rwanda', 'RWA', 646, 250, 'Rwandan'),
(179, 'SH', 'SAINT HELENA', 'Saint Helena', 'SHN', 654, 290, 'Saint Helenian'),
(180, 'KN', 'SAINT KITTS AND NEVIS', 'Saint Kitts and Nevis', 'KNA', 659, 1869, 'Kittitian or Nevisian'),
(181, 'LC', 'SAINT LUCIA', 'Saint Lucia', 'LCA', 662, 1758, 'Saint Lucian'),
(182, 'PM', 'SAINT PIERRE AND MIQUELON', 'Saint Pierre and Miquelon', 'SPM', 666, 508, 'Saint-Pierrais or Miquelonnais'),
(183, 'VC', 'SAINT VINCENT AND THE GRENADINES', 'Saint Vincent and the Grenadines', 'VCT', 670, 1784, 'Saint Vincentian, Vincentian'),
(184, 'WS', 'SAMOA', 'Samoa', 'WSM', 882, 684, 'Samoan'),
(185, 'SM', 'SAN MARINO', 'San Marino', 'SMR', 674, 378, 'Sammarinese'),
(186, 'ST', 'SAO TOME AND PRINCIPE', 'Sao Tome and Principe', 'STP', 678, 239, 'São Toméan'),
(187, 'SA', 'SAUDI ARABIA', 'Saudi Arabia', 'SAU', 682, 966, 'Saudi, Saudi Arabian'),
(188, 'SN', 'SENEGAL', 'Senegal', 'SEN', 686, 221, 'Senegalese'),
(189, 'CS', 'SERBIA AND MONTENEGRO', 'Serbia and Montenegro', NULL, NULL, 381, ''),
(190, 'SC', 'SEYCHELLES', 'Seychelles', 'SYC', 690, 248, 'Seychellois'),
(191, 'SL', 'SIERRA LEONE', 'Sierra Leone', 'SLE', 694, 232, 'Sierra Leonean'),
(192, 'SG', 'SINGAPORE', 'Singapore', 'SGP', 702, 65, 'Singaporean'),
(193, 'SK', 'SLOVAKIA', 'Slovakia', 'SVK', 703, 421, 'Slovak'),
(194, 'SI', 'SLOVENIA', 'Slovenia', 'SVN', 705, 386, 'Slovenian, Slovene'),
(195, 'SB', 'SOLOMON ISLANDS', 'Solomon Islands', 'SLB', 90, 677, 'Solomon Island'),
(196, 'SO', 'SOMALIA', 'Somalia', 'SOM', 706, 252, 'Somali, Somalian'),
(197, 'ZA', 'SOUTH AFRICA', 'South Africa', 'ZAF', 710, 27, 'South African'),
(198, 'GS', 'SOUTH GEORGIA AND THE SOUTH SANDWICH ISLANDS', 'South Georgia and the South Sandwich Islands', NULL, NULL, 0, 'South Georgia or South Sandwich Islands'),
(199, 'ES', 'SPAIN', 'Spain', 'ESP', 724, 34, 'Spanish'),
(200, 'LK', 'SRI LANKA', 'Sri Lanka', 'LKA', 144, 94, 'Sri Lankan'),
(201, 'SD', 'SUDAN', 'Sudan', 'SDN', 736, 249, 'Sudanese'),
(202, 'SR', 'SURINAME', 'Suriname', 'SUR', 740, 597, 'Surinamese'),
(203, 'SJ', 'SVALBARD AND JAN MAYEN', 'Svalbard and Jan Mayen', 'SJM', 744, 47, 'Svalbard'),
(204, 'SZ', 'SWAZILAND', 'Swaziland', 'SWZ', 748, 268, 'Swazi'),
(205, 'SE', 'SWEDEN', 'Sweden', 'SWE', 752, 46, 'Swedish'),
(206, 'CH', 'SWITZERLAND', 'Switzerland', 'CHE', 756, 41, 'Swiss'),
(207, 'SY', 'SYRIAN ARAB REPUBLIC', 'Syrian Arab Republic', 'SYR', 760, 963, 'Syrian'),
(208, 'TW', 'TAIWAN, PROVINCE OF CHINA', 'Taiwan, Province of China', 'TWN', 158, 886, 'Chinese, Taiwanese'),
(209, 'TJ', 'TAJIKISTAN', 'Tajikistan', 'TJK', 762, 992, 'Tajikistani'),
(210, 'TZ', 'TANZANIA, UNITED REPUBLIC OF', 'Tanzania, United Republic of', 'TZA', 834, 255, 'Tanzanian'),
(211, 'TH', 'THAILAND', 'Thailand', 'THA', 764, 66, 'Thai'),
(212, 'TL', 'TIMOR-LESTE', 'Timor-Leste', NULL, NULL, 670, 'Timorese'),
(213, 'TG', 'TOGO', 'Togo', 'TGO', 768, 228, 'Togolese'),
(214, 'TK', 'TOKELAU', 'Tokelau', 'TKL', 772, 690, 'Tokelauan'),
(215, 'TO', 'TONGA', 'Tonga', 'TON', 776, 676, 'Tongan'),
(216, 'TT', 'TRINIDAD AND TOBAGO', 'Trinidad and Tobago', 'TTO', 780, 1868, 'Trinidadian or Tobagonian'),
(217, 'TN', 'TUNISIA', 'Tunisia', 'TUN', 788, 216, 'Tunisian'),
(218, 'TR', 'TURKEY', 'Turkey', 'TUR', 792, 90, 'Turkish'),
(219, 'TM', 'TURKMENISTAN', 'Turkmenistan', 'TKM', 795, 7370, 'Turkmen'),
(220, 'TC', 'TURKS AND CAICOS ISLANDS', 'Turks and Caicos Islands', 'TCA', 796, 1649, 'Turks and Caicos Island'),
(221, 'TV', 'TUVALU', 'Tuvalu', 'TUV', 798, 688, 'Tuvaluan'),
(222, 'UG', 'UGANDA', 'Uganda', 'UGA', 800, 256, 'Ugandan'),
(223, 'UA', 'UKRAINE', 'Ukraine', 'UKR', 804, 380, 'Ukrainian'),
(224, 'AE', 'UNITED ARAB EMIRATES', 'United Arab Emirates', 'ARE', 784, 971, 'Emirati, Emirian, Emiri'),
(225, 'GB', 'UNITED KINGDOM', 'United Kingdom', 'GBR', 826, 44, 'British, UK'),
(226, 'US', 'UNITED STATES', 'United States', 'USA', 840, 1, 'American'),
(227, 'UM', 'UNITED STATES MINOR OUTLYING ISLANDS', 'United States Minor Outlying Islands', NULL, NULL, 1, 'American'),
(228, 'UY', 'URUGUAY', 'Uruguay', 'URY', 858, 598, 'Uruguayan'),
(229, 'UZ', 'UZBEKISTAN', 'Uzbekistan', 'UZB', 860, 998, 'Uzbekistani, Uzbek'),
(230, 'VU', 'VANUATU', 'Vanuatu', 'VUT', 548, 678, 'Ni-Vanuatu, Vanuatuan'),
(231, 'VE', 'VENEZUELA', 'Venezuela', 'VEN', 862, 58, 'Venezuelan'),
(232, 'VN', 'VIET NAM', 'Viet Nam', 'VNM', 704, 84, 'Vietnamese'),
(233, 'VG', 'VIRGIN ISLANDS, BRITISH', 'Virgin Islands, British', 'VGB', 92, 1284, 'British Virgin Island'),
(234, 'VI', 'VIRGIN ISLANDS, U.S.', 'Virgin Islands, U.s.', 'VIR', 850, 1340, 'U.S. Virgin Island'),
(235, 'WF', 'WALLIS AND FUTUNA', 'Wallis and Futuna', 'WLF', 876, 681, 'Wallis and Futuna, Wallisian or Futunan'),
(236, 'EH', 'WESTERN SAHARA', 'Western Sahara', 'ESH', 732, 212, 'Sahrawi, Sahrawian, Sahraouian'),
(237, 'YE', 'YEMEN', 'Yemen', 'YEM', 887, 967, 'Yemeni'),
(238, 'ZM', 'ZAMBIA', 'Zambia', 'ZMB', 894, 260, 'Zambian'),
(239, 'ZW', 'ZIMBABWE', 'Zimbabwe', 'ZWE', 716, 263, 'Zimbabwean');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_country_languages`
--

CREATE TABLE `tbl_country_languages` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` char(49) CHARACTER SET utf8 DEFAULT NULL,
  `iso_639-1` char(2) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `tbl_country_languages`
--

INSERT INTO `tbl_country_languages` (`id`, `name`, `iso_639-1`) VALUES
(1, 'English', 'en'),
(2, 'Afar', 'aa'),
(3, 'Abkhazian', 'ab'),
(4, 'Afrikaans', 'af'),
(5, 'Amharic', 'am'),
(6, 'Arabic', 'ar'),
(7, 'Assamese', 'as'),
(8, 'Aymara', 'ay'),
(9, 'Azerbaijani', 'az'),
(10, 'Bashkir', 'ba'),
(11, 'Belarusian', 'be'),
(12, 'Bulgarian', 'bg'),
(13, 'Bihari', 'bh'),
(14, 'Bislama', 'bi'),
(15, 'Bengali/Bangla', 'bn'),
(16, 'Tibetan', 'bo'),
(17, 'Breton', 'br'),
(18, 'Catalan', 'ca'),
(19, 'Corsican', 'co'),
(20, 'Czech', 'cs'),
(21, 'Welsh', 'cy'),
(22, 'Danish', 'da'),
(23, 'German', 'de'),
(24, 'Bhutani', 'dz'),
(25, 'Greek', 'el'),
(26, 'Esperanto', 'eo'),
(27, 'Spanish', 'es'),
(28, 'Estonian', 'et'),
(29, 'Basque', 'eu'),
(30, 'Persian', 'fa'),
(31, 'Finnish', 'fi'),
(32, 'Fiji', 'fj'),
(33, 'Faeroese', 'fo'),
(34, 'French', 'fr'),
(35, 'Frisian', 'fy'),
(36, 'Irish', 'ga'),
(37, 'Scots/Gaelic', 'gd'),
(38, 'Galician', 'gl'),
(39, 'Guarani', 'gn'),
(40, 'Gujarati', 'gu'),
(41, 'Hausa', 'ha'),
(42, 'Hindi', 'hi'),
(43, 'Croatian', 'hr'),
(44, 'Hungarian', 'hu'),
(45, 'Armenian', 'hy'),
(46, 'Interlingua', 'ia'),
(47, 'Interlingue', 'ie'),
(48, 'Inupiak', 'ik'),
(49, 'Indonesian', 'in'),
(50, 'Icelandic', 'is'),
(51, 'Italian', 'it'),
(52, 'Hebrew', 'iw'),
(53, 'Japanese', 'ja'),
(54, 'Yiddish', 'ji'),
(55, 'Javanese', 'jw'),
(56, 'Georgian', 'ka'),
(57, 'Kazakh', 'kk'),
(58, 'Greenlandic', 'kl'),
(59, 'Cambodian', 'km'),
(60, 'Kannada', 'kn'),
(61, 'Korean', 'ko'),
(62, 'Kashmiri', 'ks'),
(63, 'Kurdish', 'ku'),
(64, 'Kirghiz', 'ky'),
(65, 'Latin', 'la'),
(66, 'Lingala', 'ln'),
(67, 'Laothian', 'lo'),
(68, 'Lithuanian', 'lt'),
(69, 'Latvian/Lettish', 'lv'),
(70, 'Malagasy', 'mg'),
(71, 'Maori', 'mi'),
(72, 'Macedonian', 'mk'),
(73, 'Malayalam', 'ml'),
(74, 'Mongolian', 'mn'),
(75, 'Moldavian', 'mo'),
(76, 'Marathi', 'mr'),
(77, 'Malay', 'ms'),
(78, 'Maltese', 'mt'),
(79, 'Burmese', 'my'),
(80, 'Nauru', 'na'),
(81, 'Nepali', 'ne'),
(82, 'Dutch', 'nl'),
(83, 'Norwegian', 'no'),
(84, 'Occitan', 'oc'),
(85, '(Afan)/Oromoor/Oriya', 'om'),
(86, 'Punjabi', 'pa'),
(87, 'Polish', 'pl'),
(88, 'Pashto/Pushto', 'ps'),
(89, 'Portuguese', 'pt'),
(90, 'Quechua', 'qu'),
(91, 'Rhaeto-Romance', 'rm'),
(92, 'Kirundi', 'rn'),
(93, 'Romanian', 'ro'),
(94, 'Russian', 'ru'),
(95, 'Kinyarwanda', 'rw'),
(96, 'Sanskrit', 'sa'),
(97, 'Sindhi', 'sd'),
(98, 'Sangro', 'sg'),
(99, 'Serbo-Croatian', 'sh'),
(100, 'Singhalese', 'si'),
(101, 'Slovak', 'sk'),
(102, 'Slovenian', 'sl'),
(103, 'Samoan', 'sm'),
(104, 'Shona', 'sn'),
(105, 'Somali', 'so'),
(106, 'Albanian', 'sq'),
(107, 'Serbian', 'sr'),
(108, 'Siswati', 'ss'),
(109, 'Sesotho', 'st'),
(110, 'Sundanese', 'su'),
(111, 'Swedish', 'sv'),
(112, 'Swahili', 'sw'),
(113, 'Tamil', 'ta'),
(114, 'Telugu', 'te'),
(115, 'Tajik', 'tg'),
(116, 'Thai', 'th'),
(117, 'Tigrinya', 'ti'),
(118, 'Turkmen', 'tk'),
(119, 'Tagalog', 'tl'),
(120, 'Setswana', 'tn'),
(121, 'Tonga', 'to'),
(122, 'Turkish', 'tr'),
(123, 'Tsonga', 'ts'),
(124, 'Tatar', 'tt'),
(125, 'Twi', 'tw'),
(126, 'Ukrainian', 'uk'),
(127, 'Urdu', 'ur'),
(128, 'Uzbek', 'uz'),
(129, 'Vietnamese', 'vi'),
(130, 'Volapuk', 'vo'),
(131, 'Wolof', 'wo'),
(132, 'Xhosa', 'xh'),
(133, 'Yoruba', 'yo'),
(134, 'Chinese', 'zh'),
(135, 'Zulu', 'zu');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_courses`
--

CREATE TABLE `tbl_courses` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `group_id` bigint(19) UNSIGNED NOT NULL,
  `type_id` bigint(20) UNSIGNED NOT NULL,
  `branch_id` bigint(20) UNSIGNED NOT NULL,
  `course_name` varchar(145) DEFAULT NULL,
  `course_duration` decimal(10,0) DEFAULT NULL,
  `certificate_duration` decimal(10,0) DEFAULT NULL,
  `course_estimate_cost` decimal(10,0) DEFAULT NULL,
  `seta_id` bigint(20) UNSIGNED DEFAULT NULL,
  `unit_standard` varchar(145) DEFAULT NULL,
  `nqf_level` varchar(145) DEFAULT NULL,
  `cpd_point` varchar(145) DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `modified_by` bigint(20) DEFAULT NULL,
  `date_created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `date_modified` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_course_matrix`
--

CREATE TABLE `tbl_course_matrix` (
  `courses_id` bigint(20) UNSIGNED NOT NULL,
  `type_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `module_id` bigint(20) UNSIGNED NOT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `modified_by` bigint(20) DEFAULT NULL,
  `date_created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `date_modified` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_course_seta`
--

CREATE TABLE `tbl_course_seta` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `company_id` bigint(19) UNSIGNED NOT NULL,
  `seta_name` varchar(145) DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `modified_by` bigint(20) DEFAULT NULL,
  `date_created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `date_modified` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_course_status`
--

CREATE TABLE `tbl_course_status` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `courses_id` bigint(20) UNSIGNED NOT NULL,
  `course_due_date` timestamp NULL DEFAULT NULL,
  `course_start_date` timestamp NULL DEFAULT NULL,
  `notes` text,
  `is_filed` tinyint(4) DEFAULT '0',
  `date_created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `date_modified` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `created_by` bigint(20) DEFAULT NULL,
  `modified_by` bigint(20) DEFAULT NULL,
  `suppliers_id` bigint(20) UNSIGNED DEFAULT NULL,
  `frequency` bigint(20) DEFAULT NULL,
  `responisble_person` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_course_training`
--

CREATE TABLE `tbl_course_training` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `course_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `is_assigned` tinyint(1) NOT NULL DEFAULT '0',
  `file_id` bigint(20) UNSIGNED DEFAULT NULL,
  `is_attended` tinyint(1) NOT NULL DEFAULT '0',
  `is_complete` tinyint(1) NOT NULL DEFAULT '0',
  `is_certificate_received` tinyint(1) NOT NULL DEFAULT '0',
  `is_started` tinyint(1) NOT NULL DEFAULT '0',
  `start_date` timestamp NULL DEFAULT NULL,
  `end_date` timestamp NULL DEFAULT NULL,
  `suppliers_id` bigint(20) NOT NULL,
  `responisble_person` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_course_training_history`
--

CREATE TABLE `tbl_course_training_history` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `course_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `is_assigned` tinyint(1) NOT NULL DEFAULT '0',
  `file_id` bigint(20) UNSIGNED DEFAULT NULL,
  `is_attended` tinyint(1) NOT NULL DEFAULT '0',
  `is_complete` tinyint(1) NOT NULL DEFAULT '0',
  `is_certificate_received` tinyint(1) NOT NULL DEFAULT '0',
  `start_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `suppliers_id` bigint(20) NOT NULL,
  `responisble_person` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_course_types`
--

CREATE TABLE `tbl_course_types` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(145) DEFAULT NULL,
  `note` varchar(60) NOT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `modified_by` bigint(20) DEFAULT NULL,
  `date_created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `date_modified` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_course_types`
--

INSERT INTO `tbl_course_types` (`id`, `name`, `note`, `created_by`, `modified_by`, `date_created`, `date_modified`, `description`) VALUES
(1, 'InternalCourse', 'Internal', 1, NULL, '2017-05-26 04:44:05', '2017-05-30 03:50:16', 'Add Internal Course'),
(2, 'ExternalCourse', 'External', 1, NULL, '2017-05-26 04:44:05', '2017-05-30 03:50:24', 'Add External Course'),
(3, 'LicensingTypes', 'Licensing', 1, 1, '2017-05-29 04:34:31', '2017-05-30 03:50:32', 'Add Licensing Types'),
(4, 'ProfessionalBodies', 'Professional Body', 1, 1, '2017-05-29 04:34:31', '2017-05-30 03:50:44', 'Add Professional Bodies Types');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_course_uploads`
--

CREATE TABLE `tbl_course_uploads` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `course_id` bigint(20) UNSIGNED NOT NULL,
  `file_id` bigint(20) UNSIGNED DEFAULT NULL,
  `doc_id` bigint(20) UNSIGNED DEFAULT NULL,
  `date_created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `is_file_upload` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_departments`
--

CREATE TABLE `tbl_departments` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `branch_id` bigint(20) UNSIGNED NOT NULL,
  `company_id` bigint(20) UNSIGNED NOT NULL,
  `department_name` varchar(255) NOT NULL,
  `department_building` varchar(120) NOT NULL,
  `department_floor` int(11) NOT NULL,
  `is_head_office` tinyint(4) DEFAULT '0',
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_created` timestamp NULL DEFAULT NULL,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_department_assets`
--

CREATE TABLE `tbl_department_assets` (
  `department_id` bigint(20) UNSIGNED NOT NULL,
  `assets_id` bigint(20) UNSIGNED NOT NULL,
  `branch_id` bigint(20) UNSIGNED NOT NULL,
  `company_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_department_incident`
--

CREATE TABLE `tbl_department_incident` (
  `department_id` bigint(20) UNSIGNED NOT NULL,
  `incident_id` bigint(20) UNSIGNED NOT NULL,
  `branch_id` bigint(20) UNSIGNED NOT NULL,
  `company_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_department_locations`
--

CREATE TABLE `tbl_department_locations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `departments_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_department_manager`
--

CREATE TABLE `tbl_department_manager` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `department_id` bigint(20) UNSIGNED NOT NULL,
  `department_manager_id` bigint(20) UNSIGNED NOT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT '1',
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NULL DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_department_manager_roles`
--

CREATE TABLE `tbl_department_manager_roles` (
  `id` bigint(20) NOT NULL,
  `manager_id` bigint(20) UNSIGNED NOT NULL,
  `role` text,
  `responsibility` text,
  `accountability` text,
  `authority` text,
  `created_by` bigint(20) DEFAULT NULL,
  `modified_by` bigint(20) DEFAULT NULL,
  `date_modified` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `date_created` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_department_material`
--

CREATE TABLE `tbl_department_material` (
  `department_id` varchar(120) DEFAULT NULL,
  `material_id` bigint(20) UNSIGNED NOT NULL,
  `branch_id` bigint(20) UNSIGNED NOT NULL,
  `company_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_department_occupations`
--

CREATE TABLE `tbl_department_occupations` (
  `employee_id` bigint(20) UNSIGNED NOT NULL,
  `occupation_id` bigint(20) UNSIGNED NOT NULL,
  `department_id` bigint(20) UNSIGNED NOT NULL,
  `branch_id` bigint(20) UNSIGNED NOT NULL,
  `company_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_documents`
--

CREATE TABLE `tbl_documents` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `group_id` bigint(20) UNSIGNED NOT NULL,
  `folder_id` bigint(20) UNSIGNED NOT NULL,
  `doc_type_id` bigint(20) UNSIGNED NOT NULL,
  `originator_id` bigint(20) UNSIGNED NOT NULL,
  `is_signed` tinyint(1) NOT NULL,
  `branches` int(11) NOT NULL,
  `document_number` varchar(255) DEFAULT NULL,
  `system_number` varchar(255) DEFAULT NULL,
  `original_filename` varchar(255) NOT NULL,
  `extension` varchar(150) NOT NULL,
  `filename` varchar(255) DEFAULT NULL,
  `description` text,
  `current_revision_number` decimal(19,2) NOT NULL DEFAULT '0.01',
  `revision_frequency` varchar(50) DEFAULT NULL,
  `next_revision_date` timestamp NULL DEFAULT NULL,
  `is_all_approved` tinyint(4) NOT NULL DEFAULT '0',
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_document_approvals`
--

CREATE TABLE `tbl_document_approvals` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `doc_id` bigint(20) UNSIGNED NOT NULL,
  `employee_id` bigint(20) UNSIGNED NOT NULL,
  `due_date` timestamp NULL DEFAULT NULL,
  `document_version` decimal(19,2) NOT NULL DEFAULT '0.01',
  `is_approved` tinyint(3) UNSIGNED NOT NULL DEFAULT '0',
  `approval_order` int(11) NOT NULL,
  `is_amendments` tinyint(3) UNSIGNED NOT NULL DEFAULT '0',
  `approval_comments` text,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_document_folders`
--

CREATE TABLE `tbl_document_folders` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `company_id` bigint(20) UNSIGNED NOT NULL,
  `parent_id` bigint(20) UNSIGNED DEFAULT NULL,
  `folder_path` varchar(255) DEFAULT NULL,
  `branch_id` bigint(20) NOT NULL,
  `folder_name` varchar(120) DEFAULT NULL,
  `is_hidden` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_document_links`
--

CREATE TABLE `tbl_document_links` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `doc_id` bigint(20) UNSIGNED NOT NULL,
  `links_types_id` bigint(20) UNSIGNED NOT NULL,
  `description` text,
  `notes` text,
  `link_no` int(11) DEFAULT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_document_links_types`
--

CREATE TABLE `tbl_document_links_types` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `company_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(145) DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `modified_by` bigint(20) DEFAULT NULL,
  `date_created` timestamp NULL DEFAULT NULL,
  `date_modified` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_document_type`
--

CREATE TABLE `tbl_document_type` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `company_id` bigint(20) UNSIGNED NOT NULL,
  `type_name` varchar(255) CHARACTER SET latin1 NOT NULL,
  `type_description` text CHARACTER SET latin1,
  `order` int(11) NOT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_document_version`
--

CREATE TABLE `tbl_document_version` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `doc_id` bigint(20) UNSIGNED NOT NULL,
  `version_number` decimal(19,2) NOT NULL DEFAULT '0.01',
  `document_link` varchar(255) DEFAULT NULL,
  `original_filename` varchar(255) NOT NULL,
  `extension` varchar(150) NOT NULL,
  `filename` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_emergency`
--

CREATE TABLE `tbl_emergency` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1',
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `date_created` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_emergency_assemblyarea_department`
--

CREATE TABLE `tbl_emergency_assemblyarea_department` (
  `assemblyarea_id` bigint(20) UNSIGNED NOT NULL,
  `department_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_emergency_authority`
--

CREATE TABLE `tbl_emergency_authority` (
  `id` bigint(20) NOT NULL,
  `type` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `contact_number1` varchar(255) DEFAULT NULL,
  `contact_number2` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `fax` varchar(255) DEFAULT NULL,
  `contact_person` varchar(255) DEFAULT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1',
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `date_created` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_emergency_department`
--

CREATE TABLE `tbl_emergency_department` (
  `emergency_id` bigint(20) UNSIGNED NOT NULL,
  `department_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_emergency_evacuation`
--

CREATE TABLE `tbl_emergency_evacuation` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` varchar(255) DEFAULT NULL,
  `scenario` varchar(255) DEFAULT NULL,
  `start_date` timestamp NULL DEFAULT NULL,
  `head_coordinator` bigint(20) UNSIGNED NOT NULL,
  `notes` varchar(255) DEFAULT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1',
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `date_created` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_emergency_evacuation_assemblyarea`
--

CREATE TABLE `tbl_emergency_evacuation_assemblyarea` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `team_leader` bigint(20) UNSIGNED NOT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1',
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `date_created` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_emergency_evacuation_department`
--

CREATE TABLE `tbl_emergency_evacuation_department` (
  `evacuation_id` bigint(20) UNSIGNED NOT NULL,
  `department_id` bigint(20) UNSIGNED NOT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1',
  `date_modified` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `date_created` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_emergency_evacuation_plan`
--

CREATE TABLE `tbl_emergency_evacuation_plan` (
  `id` bigint(20) NOT NULL,
  `department` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1',
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `date_created` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_emergency_evacuation_signoff`
--

CREATE TABLE `tbl_emergency_evacuation_signoff` (
  `id` bigint(20) NOT NULL,
  `evacuation_id` bigint(20) UNSIGNED NOT NULL,
  `time_evacuation` timestamp NULL DEFAULT NULL,
  `rollcall_time` timestamp NULL DEFAULT NULL,
  `is_signoff` int(11) NOT NULL DEFAULT '1',
  `is_active` int(11) NOT NULL DEFAULT '1',
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `date_created` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_emergency_setup`
--

CREATE TABLE `tbl_emergency_setup` (
  `id` bigint(20) NOT NULL,
  `emergency_id` bigint(20) UNSIGNED NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `responsible_person` bigint(20) UNSIGNED NOT NULL,
  `appointment` bigint(20) UNSIGNED DEFAULT NULL,
  `action_description` varchar(255) DEFAULT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1',
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `date_created` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_employee`
--

CREATE TABLE `tbl_employee` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `employee_type_id` bigint(11) DEFAULT '1',
  `employee_number` varchar(255) DEFAULT NULL,
  `employee_fullname` varchar(255) DEFAULT NULL,
  `employee_name` varchar(255) DEFAULT NULL,
  `employee_surname` varchar(255) DEFAULT NULL,
  `employee_nationality` varchar(145) DEFAULT NULL,
  `employee_idnumber` varchar(145) DEFAULT NULL,
  `employee_race` int(11) DEFAULT NULL,
  `employee_gender` int(11) DEFAULT NULL,
  `employee_cellphone` varchar(45) DEFAULT NULL,
  `employee_email` varchar(255) DEFAULT NULL,
  `employee_language` varchar(45) DEFAULT NULL,
  `employee_second_language` varchar(45) DEFAULT NULL,
  `employee_marital_status` varchar(145) DEFAULT NULL,
  `employee_image` bigint(20) DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT NULL,
  `company_id` bigint(20) UNSIGNED NOT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `modified_by` bigint(20) DEFAULT NULL,
  `date_created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `date_modified` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_employee_types`
--

CREATE TABLE `tbl_employee_types` (
  `id` bigint(20) NOT NULL,
  `type_name` varchar(255) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  `created_by` bigint(20) DEFAULT NULL,
  `modified_by` bigint(20) DEFAULT NULL,
  `date_created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `date_modified` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_employee_types`
--

INSERT INTO `tbl_employee_types` (`id`, `type_name`, `is_deleted`, `created_by`, `modified_by`, `date_created`, `date_modified`) VALUES
(1, 'Permanent', 0, NULL, NULL, '2017-10-12 03:40:04', NULL),
(2, 'Temporary', 0, NULL, NULL, '2017-10-12 03:40:04', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_environmental_electricity_measurement`
--

CREATE TABLE `tbl_environmental_electricity_measurement` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `electricity_measurement_name` varchar(255) NOT NULL,
  `electricity_measurement_description` varchar(50) NOT NULL,
  `is_active` tinyint(1) DEFAULT '1',
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NULL DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_environmental_electricity_utility`
--

CREATE TABLE `tbl_environmental_electricity_utility` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `date` varchar(255) NOT NULL,
  `measurement` varchar(255) NOT NULL,
  `measurement_unit` varchar(50) NOT NULL,
  `account` varchar(50) NOT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1',
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NULL DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_environmental_utility_bill`
--

CREATE TABLE `tbl_environmental_utility_bill` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `utility_bill_id` bigint(20) UNSIGNED NOT NULL,
  `file_location` varchar(50) NOT NULL,
  `original_filename` varchar(255) NOT NULL,
  `filename` varchar(50) NOT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1',
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NULL DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_environmental_water_measurement`
--

CREATE TABLE `tbl_environmental_water_measurement` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `water_measurement_name` varchar(255) NOT NULL,
  `water_measurement_description` varchar(50) NOT NULL,
  `is_active` tinyint(1) DEFAULT '1',
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NULL DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_environmental_water_utility`
--

CREATE TABLE `tbl_environmental_water_utility` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `date` varchar(255) NOT NULL,
  `measurement` varchar(255) NOT NULL,
  `measurement_unit` varchar(50) NOT NULL,
  `account` varchar(50) NOT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1',
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NULL DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_ethnic`
--

CREATE TABLE `tbl_ethnic` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(120) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_ethnic`
--

INSERT INTO `tbl_ethnic` (`id`, `name`) VALUES
(1, 'Black'),
(2, 'White'),
(3, 'Indian'),
(4, 'Colored ');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_events`
--

CREATE TABLE `tbl_events` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type_id` bigint(20) UNSIGNED NOT NULL,
  `company_id` bigint(20) UNSIGNED NOT NULL,
  `supplier_id` bigint(20) UNSIGNED DEFAULT NULL,
  `event_name` varchar(145) DEFAULT NULL,
  `start_date` timestamp NULL DEFAULT NULL,
  `end_date` timestamp NULL DEFAULT NULL,
  `location` varchar(160) DEFAULT NULL,
  `responsible_person_id` bigint(20) UNSIGNED NOT NULL,
  `notes` text,
  `is_signed` tinyint(1) NOT NULL DEFAULT '0',
  `is_complete` tinyint(1) NOT NULL DEFAULT '0',
  `is_active` int(11) NOT NULL DEFAULT '1',
  `date_created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `date_modified` timestamp NULL DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `modified_by` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_events_attachment`
--

CREATE TABLE `tbl_events_attachment` (
  `id` bigint(20) NOT NULL,
  `event_id` bigint(20) UNSIGNED NOT NULL,
  `file_id` bigint(20) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_events_notifications`
--

CREATE TABLE `tbl_events_notifications` (
  `id` bigint(20) NOT NULL,
  `event_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `notification_date` timestamp NULL DEFAULT NULL,
  `date_created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `date_modified` timestamp NULL DEFAULT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_events_signoff`
--

CREATE TABLE `tbl_events_signoff` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `description` varchar(145) DEFAULT NULL,
  `cost` text,
  `rating` varchar(145) DEFAULT NULL,
  `comment` text,
  `created_by` bigint(20) DEFAULT NULL,
  `date_created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` bigint(20) DEFAULT NULL,
  `date_modified` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_events_type`
--

CREATE TABLE `tbl_events_type` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type_name` varchar(145) DEFAULT NULL,
  `type_description` varchar(145) DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `date_created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` bigint(20) DEFAULT NULL,
  `date_modified` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_exclusion_from_work`
--

CREATE TABLE `tbl_exclusion_from_work` (
  `id` int(11) UNSIGNED NOT NULL,
  `occupation_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '0',
  `created_by` bigint(20) DEFAULT NULL,
  `modified_by` bigint(20) DEFAULT NULL,
  `date_created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `date_modified` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_exposed_material`
--

CREATE TABLE `tbl_exposed_material` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `working_instruction_id` bigint(20) UNSIGNED NOT NULL,
  `asset_id` bigint(20) UNSIGNED NOT NULL,
  `date_created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` timestamp NULL DEFAULT NULL,
  `date_modified` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_exposed_to_asset`
--

CREATE TABLE `tbl_exposed_to_asset` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `working_instruction_id` bigint(20) UNSIGNED NOT NULL,
  `asset_id` bigint(20) UNSIGNED NOT NULL,
  `date_created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` timestamp NULL DEFAULT NULL,
  `date_modified` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_fall_protection_plan`
--

CREATE TABLE `tbl_fall_protection_plan` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type_id` bigint(20) DEFAULT NULL,
  `description` varchar(250) DEFAULT NULL,
  `details` text,
  `branch_id` bigint(20) DEFAULT NULL,
  `company_id` bigint(20) DEFAULT NULL,
  `is_active` int(11) DEFAULT '1',
  `created_by` bigint(20) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_fall_protection_plan_types`
--

CREATE TABLE `tbl_fall_protection_plan_types` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type_name` varchar(250) DEFAULT NULL,
  `branch_id` bigint(20) DEFAULT NULL,
  `company_id` bigint(20) DEFAULT NULL,
  `is_active` int(11) DEFAULT '1',
  `created_by` bigint(20) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_file_documents`
--

CREATE TABLE `tbl_file_documents` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `file_name` varchar(255) DEFAULT NULL,
  `original_filename` varchar(255) NOT NULL,
  `file_type` varchar(140) DEFAULT NULL,
  `file_extension` varchar(45) DEFAULT NULL,
  `file_size` bigint(15) DEFAULT NULL,
  `local_file_path` varchar(255) DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `date_created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` bigint(20) DEFAULT NULL,
  `date_modified` timestamp NULL DEFAULT NULL,
  `folders_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_finance_approval`
--

CREATE TABLE `tbl_finance_approval` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `priority` varchar(50) DEFAULT NULL,
  `due_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `approved_by` bigint(20) DEFAULT NULL,
  `category_id` bigint(20) DEFAULT NULL,
  `type_id` bigint(20) DEFAULT NULL,
  `description` varchar(250) DEFAULT NULL,
  `note` text,
  `document` text,
  `materials_id` text,
  `status` varchar(250) DEFAULT NULL,
  `hasQuote` int(11) NOT NULL,
  `po_status` varchar(50) NOT NULL,
  `po_notes` text NOT NULL,
  `po_num` varchar(250) NOT NULL,
  `signedOff` int(11) NOT NULL DEFAULT '0',
  `signedOffBy` bigint(20) NOT NULL,
  `signedOffDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `branch_id` bigint(20) DEFAULT NULL,
  `company_id` bigint(20) DEFAULT NULL,
  `is_active` int(11) DEFAULT '1',
  `created_by` bigint(20) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_finance_approval_quotes`
--

CREATE TABLE `tbl_finance_approval_quotes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `request_id` bigint(20) DEFAULT NULL,
  `supplier_id` varchar(50) DEFAULT NULL,
  `notes` text,
  `amount` decimal(10,0) DEFAULT NULL,
  `inv_file` text NOT NULL,
  `quote` text,
  `quote_po` varchar(255) NOT NULL,
  `quote_description` varchar(255) NOT NULL,
  `description` varchar(250) NOT NULL,
  `inv_amount` varchar(250) NOT NULL,
  `payment_ref` varchar(250) NOT NULL,
  `payment_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `approvedStatus` varchar(50) NOT NULL,
  `approvedBy` bigint(20) NOT NULL,
  `approvedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `branch_id` bigint(20) DEFAULT NULL,
  `company_id` bigint(20) DEFAULT NULL,
  `is_active` int(11) DEFAULT '1',
  `created_by` bigint(20) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_finance_categories`
--

CREATE TABLE `tbl_finance_categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(250) DEFAULT NULL,
  `branch_id` bigint(20) DEFAULT NULL,
  `company_id` bigint(20) DEFAULT NULL,
  `is_active` int(11) DEFAULT '1',
  `created_by` bigint(20) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_finance_service_providers`
--

CREATE TABLE `tbl_finance_service_providers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `supplier_id` bigint(20) DEFAULT NULL,
  `approved` varchar(50) DEFAULT NULL,
  `bbeee` int(11) DEFAULT NULL,
  `service_description` varchar(250) DEFAULT NULL,
  `purpose` varchar(250) DEFAULT NULL,
  `notes` text,
  `contract` text,
  `start_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `start_end` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `cancellation_terms` varchar(250) DEFAULT NULL,
  `month_notice` varchar(250) DEFAULT NULL,
  `cost` varchar(250) DEFAULT NULL,
  `cost_frequency` varchar(250) DEFAULT NULL,
  `quality_start_date` timestamp NULL DEFAULT NULL,
  `quality_days` varchar(250) DEFAULT NULL,
  `quality_responsible_person` bigint(20) DEFAULT NULL,
  `status` varchar(20) DEFAULT NULL,
  `rating` int(11) DEFAULT NULL,
  `branch_id` bigint(20) DEFAULT NULL,
  `company_id` bigint(20) DEFAULT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1',
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_finance_types`
--

CREATE TABLE `tbl_finance_types` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(250) DEFAULT NULL,
  `branch_id` bigint(20) DEFAULT NULL,
  `company_id` bigint(20) DEFAULT NULL,
  `is_active` int(11) DEFAULT '1',
  `created_by` bigint(20) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_financial_loss`
--

CREATE TABLE `tbl_financial_loss` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `source` varchar(250) DEFAULT NULL,
  `source_id` bigint(20) NOT NULL,
  `investigation_id` bigint(20) NOT NULL,
  `loss_type` varchar(250) DEFAULT NULL,
  `hasFinance` int(11) NOT NULL DEFAULT '0',
  `total_cost` float NOT NULL,
  `branch_id` bigint(20) DEFAULT NULL,
  `company_id` bigint(20) DEFAULT NULL,
  `is_active` int(11) DEFAULT '1',
  `created_by` bigint(20) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_financial_loss_asset`
--

CREATE TABLE `tbl_financial_loss_asset` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `source` bigint(20) DEFAULT NULL,
  `asset_id` bigint(20) DEFAULT NULL,
  `repair_replace` varchar(50) DEFAULT NULL,
  `total` decimal(10,0) DEFAULT NULL,
  `notes` text,
  `branch_id` bigint(20) DEFAULT NULL,
  `company_id` bigint(20) DEFAULT NULL,
  `is_active` int(11) DEFAULT '1',
  `created_by` bigint(20) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_financial_loss_client`
--

CREATE TABLE `tbl_financial_loss_client` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `source` bigint(20) DEFAULT NULL,
  `client` bigint(20) DEFAULT NULL,
  `total` decimal(10,0) DEFAULT NULL,
  `per` varchar(50) DEFAULT NULL,
  `notes` text NOT NULL,
  `branch_id` bigint(20) DEFAULT NULL,
  `company_id` bigint(20) DEFAULT NULL,
  `is_active` int(11) DEFAULT '1',
  `created_by` bigint(20) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_financial_loss_employees`
--

CREATE TABLE `tbl_financial_loss_employees` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `source` bigint(20) DEFAULT NULL,
  `employee_id` bigint(20) DEFAULT NULL,
  `cost_per_hour` bigint(20) DEFAULT NULL,
  `total` decimal(10,0) DEFAULT NULL,
  `notes` text,
  `branch_id` bigint(20) DEFAULT NULL,
  `company_id` bigint(20) DEFAULT NULL,
  `is_active` int(11) DEFAULT '1',
  `created_by` bigint(20) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_financial_loss_insurance`
--

CREATE TABLE `tbl_financial_loss_insurance` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `source` bigint(20) DEFAULT NULL,
  `description` varchar(250) DEFAULT NULL,
  `total` decimal(10,0) DEFAULT NULL,
  `notes` text NOT NULL,
  `branch_id` bigint(20) DEFAULT NULL,
  `company_id` bigint(20) DEFAULT NULL,
  `is_active` int(11) DEFAULT '1',
  `created_by` bigint(20) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_financial_loss_material`
--

CREATE TABLE `tbl_financial_loss_material` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `source` bigint(20) DEFAULT NULL,
  `material_id` bigint(20) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `cost_per_unit` decimal(10,0) DEFAULT NULL,
  `total` decimal(10,0) DEFAULT NULL,
  `notes` text,
  `branch_id` bigint(20) DEFAULT NULL,
  `company_id` bigint(20) DEFAULT NULL,
  `is_active` int(11) DEFAULT '1',
  `created_by` bigint(20) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_financial_loss_medical`
--

CREATE TABLE `tbl_financial_loss_medical` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `source` bigint(20) DEFAULT NULL,
  `employee_id` bigint(20) DEFAULT NULL,
  `total` decimal(10,0) DEFAULT NULL,
  `notes` text NOT NULL,
  `branch_id` bigint(20) DEFAULT NULL,
  `company_id` bigint(20) DEFAULT NULL,
  `is_active` int(11) DEFAULT '1',
  `created_by` bigint(20) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_financial_loss_other`
--

CREATE TABLE `tbl_financial_loss_other` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `source` bigint(20) DEFAULT NULL,
  `description` varchar(250) DEFAULT NULL,
  `cost_per_hour` bigint(20) DEFAULT NULL,
  `total` decimal(10,0) DEFAULT NULL,
  `notes` text,
  `branch_id` bigint(20) DEFAULT NULL,
  `company_id` bigint(20) DEFAULT NULL,
  `is_active` int(11) DEFAULT '1',
  `created_by` bigint(20) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_financial_loss_production`
--

CREATE TABLE `tbl_financial_loss_production` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `source` bigint(20) DEFAULT NULL,
  `department` bigint(20) DEFAULT NULL,
  `cost_per_hour` bigint(20) DEFAULT NULL,
  `total` decimal(10,0) DEFAULT NULL,
  `notes` text,
  `branch_id` bigint(20) DEFAULT NULL,
  `company_id` bigint(20) DEFAULT NULL,
  `is_active` int(11) DEFAULT '1',
  `created_by` bigint(20) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_financial_loss_settings`
--

CREATE TABLE `tbl_financial_loss_settings` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `department_id` bigint(20) DEFAULT NULL,
  `cost_to_operate` decimal(10,0) DEFAULT NULL,
  `branch_id` bigint(20) DEFAULT NULL,
  `company_id` bigint(20) DEFAULT NULL,
  `is_active` int(11) DEFAULT '1',
  `created_by` bigint(20) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_frequency`
--

CREATE TABLE `tbl_frequency` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `frequency_type_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `order` int(11) DEFAULT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1',
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NULL DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_frequency`
--

INSERT INTO `tbl_frequency` (`id`, `frequency_type_id`, `category_id`, `order`, `is_active`, `created_by`, `modified_by`, `date_modified`, `date_created`) VALUES
(1, 3, 1, 1, 1, 1, NULL, NULL, '2017-06-23 06:13:56'),
(2, 4, 1, 2, 1, 1, NULL, NULL, '2017-06-23 06:13:56'),
(3, 5, 1, 3, 1, 1, NULL, NULL, '2017-06-23 06:13:56'),
(4, 6, 1, 4, 1, 1, NULL, NULL, '2017-06-23 06:13:56'),
(5, 3, 2, 1, 1, 1, NULL, NULL, '2017-06-23 06:13:56'),
(6, 4, 2, 2, 1, 1, NULL, NULL, '2017-06-23 06:13:56'),
(7, 5, 2, 3, 1, 1, NULL, NULL, '2017-06-23 06:13:56'),
(8, 6, 2, 4, 1, 1, NULL, NULL, '2017-06-23 06:13:56'),
(9, 1, 3, 1, 1, 1, NULL, NULL, '2017-06-23 06:13:56'),
(10, 2, 3, 2, 1, 1, NULL, NULL, '2017-06-23 06:13:56'),
(11, 3, 3, 3, 1, 1, NULL, NULL, '2017-06-23 06:13:56'),
(12, 4, 3, 4, 1, 1, NULL, NULL, '2017-06-23 06:13:56'),
(13, 5, 3, 5, 1, 1, NULL, NULL, '2017-06-23 06:13:56'),
(14, 6, 3, 6, 1, 1, NULL, NULL, '2017-06-23 06:13:56');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_frequency_type`
--

CREATE TABLE `tbl_frequency_type` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `frequency_name` varchar(255) DEFAULT NULL,
  `frequency_description` varchar(255) DEFAULT NULL,
  `frequency_calculate` varchar(255) NOT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1',
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NULL DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_frequency_type`
--

INSERT INTO `tbl_frequency_type` (`id`, `frequency_name`, `frequency_description`, `frequency_calculate`, `is_active`, `created_by`, `modified_by`, `date_modified`, `date_created`) VALUES
(1, 'Daily', 'Daily', '+1 day', 1, 1, NULL, NULL, '2017-06-23 06:14:15'),
(2, 'Weekly', 'Weekly', '+1 week', 1, 1, NULL, NULL, '2017-06-23 06:14:15'),
(3, 'Monthly', 'Monthly', '+1 month', 1, 1, NULL, NULL, '2017-06-23 06:14:15'),
(4, 'Every 6 Months', 'Every 6 Months', '+6 month', 1, 1, NULL, NULL, '2017-06-23 06:14:15'),
(5, 'Yearly', 'Yearly', '+1 year', 1, 1, NULL, NULL, '2017-06-23 06:14:15'),
(6, 'Every 2 Years', 'Every 2 Years', '+2 years', 1, 1, NULL, NULL, '2017-06-23 06:14:15');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_gender`
--

CREATE TABLE `tbl_gender` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(120) DEFAULT NULL,
  `description` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_gender`
--

INSERT INTO `tbl_gender` (`id`, `name`, `description`) VALUES
(1, 'Male', 'Male'),
(2, 'Female', 'Female');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_global_status`
--

CREATE TABLE `tbl_global_status` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `source_id` bigint(20) DEFAULT NULL,
  `topic_id` bigint(20) NOT NULL,
  `source_type` varchar(250) DEFAULT NULL,
  `status` varchar(250) DEFAULT NULL,
  `notes` text,
  `performed_by` bigint(20) NOT NULL,
  `supplier_id` bigint(20) NOT NULL,
  `schedule_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `rating` int(11) NOT NULL,
  `branch_id` bigint(20) DEFAULT NULL,
  `company_id` bigint(20) DEFAULT NULL,
  `is_active` int(11) DEFAULT '1',
  `created_by` bigint(20) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_health_baseline`
--

CREATE TABLE `tbl_health_baseline` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `baseline_type_id` bigint(20) UNSIGNED NOT NULL,
  `baseline_id` bigint(20) UNSIGNED NOT NULL,
  `department_id` bigint(20) UNSIGNED NOT NULL,
  `occupations_id` bigint(20) UNSIGNED NOT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1',
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NULL DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_health_biological`
--

CREATE TABLE `tbl_health_biological` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `cost` double(20,2) NOT NULL,
  `medical_types_id` varchar(255) DEFAULT NULL,
  `medical_purpose` varchar(255) DEFAULT NULL,
  `frequency_id` bigint(20) UNSIGNED NOT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1',
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NULL DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_health_entry`
--

CREATE TABLE `tbl_health_entry` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `description` varchar(255) NOT NULL,
  `cost` double(20,2) NOT NULL,
  `medical_types_id` varchar(255) DEFAULT NULL,
  `medical_purpose` varchar(255) DEFAULT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1',
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NULL DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_health_medical_practitioner`
--

CREATE TABLE `tbl_health_medical_practitioner` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `practitioner_type_id` bigint(20) UNSIGNED DEFAULT NULL,
  `practitioner_name` varchar(255) DEFAULT NULL,
  `company_name` varchar(255) DEFAULT NULL,
  `facility_name` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `contact_number` varchar(30) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1',
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NULL DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_health_medical_practitioner_type`
--

CREATE TABLE `tbl_health_medical_practitioner_type` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `practitioner_type` varchar(255) DEFAULT NULL,
  `practitioner_type_description` varchar(255) DEFAULT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1',
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NULL DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_health_medical_status`
--

CREATE TABLE `tbl_health_medical_status` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `baseline_id` bigint(20) UNSIGNED NOT NULL,
  `baseline_type` bigint(20) UNSIGNED NOT NULL,
  `date_completed` timestamp NULL DEFAULT NULL,
  `medical_practitioner` bigint(20) UNSIGNED NOT NULL,
  `occupational_limitation` varchar(255) DEFAULT NULL,
  `is_safe_accessible` int(11) NOT NULL DEFAULT '1',
  `is_active` int(11) NOT NULL DEFAULT '1',
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NULL DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_health_medical_types`
--

CREATE TABLE `tbl_health_medical_types` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `medical_type` varchar(255) DEFAULT NULL,
  `medical_type_description` varchar(255) DEFAULT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1',
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NULL DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_health_periodic`
--

CREATE TABLE `tbl_health_periodic` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `cost` double(20,2) NOT NULL,
  `medical_types_id` varchar(255) DEFAULT NULL,
  `medical_purpose` varchar(255) DEFAULT NULL,
  `frequency_id` bigint(20) UNSIGNED NOT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1',
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NULL DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_health_survey`
--

CREATE TABLE `tbl_health_survey` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `company_id` varchar(255) DEFAULT NULL,
  `department_id` bigint(20) UNSIGNED NOT NULL,
  `date_completed` timestamp NULL DEFAULT NULL,
  `next_date` timestamp NULL DEFAULT NULL,
  `noise_level` decimal(20,2) UNSIGNED DEFAULT NULL,
  `dust_parts` decimal(20,2) UNSIGNED DEFAULT NULL,
  `volatile` decimal(20,2) UNSIGNED DEFAULT NULL,
  `light` decimal(20,2) UNSIGNED DEFAULT NULL,
  `notes` varchar(255) DEFAULT NULL,
  `evidence` int(11) NOT NULL DEFAULT '0',
  `is_active` int(11) NOT NULL DEFAULT '1',
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NULL DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_health_survey_document`
--

CREATE TABLE `tbl_health_survey_document` (
  `id` bigint(20) NOT NULL,
  `survey_id` bigint(20) UNSIGNED NOT NULL,
  `file_location` varchar(50) NOT NULL,
  `original_filename` varchar(255) NOT NULL,
  `filename` varchar(50) NOT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1',
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NULL DEFAULT NULL,
  `date_created` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_improvement_log`
--

CREATE TABLE `tbl_improvement_log` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `group_id` bigint(20) NOT NULL,
  `improvement_type` varchar(255) DEFAULT NULL,
  `improvement_description` varchar(255) DEFAULT NULL,
  `frequency` varchar(50) DEFAULT NULL,
  `benefit_details` varchar(255) DEFAULT NULL,
  `document_link` varchar(255) DEFAULT NULL,
  `non_conformances_link` varchar(255) DEFAULT NULL,
  `corrective_action_link` varchar(255) DEFAULT NULL,
  `investigation_link` varchar(255) DEFAULT NULL,
  `incident_link` varchar(255) DEFAULT NULL,
  `risk_assessment_link` varchar(255) DEFAULT NULL,
  `check_frequency` varchar(50) DEFAULT NULL,
  `responsible_person` bigint(20) DEFAULT NULL,
  `branch_id` bigint(20) DEFAULT NULL,
  `company_id` bigint(20) DEFAULT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1',
  `in_active_reason` text NOT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_incident`
--

CREATE TABLE `tbl_incident` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `incident_category_id` bigint(20) UNSIGNED NOT NULL,
  `incident_type_id` bigint(20) UNSIGNED NOT NULL,
  `department_manager_id` bigint(20) UNSIGNED NOT NULL,
  `department_id` bigint(20) UNSIGNED NOT NULL,
  `incident_number` varchar(255) DEFAULT NULL,
  `incident_date` timestamp NULL DEFAULT NULL,
  `shift` varchar(255) NOT NULL,
  `reported_person_id` int(11) NOT NULL,
  `incident_task` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `report_authorities` varchar(255) NOT NULL,
  `investigator_id` int(11) NOT NULL,
  `due_date` timestamp NULL DEFAULT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1',
  `modified_by` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `date_modified` timestamp NULL DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_incident_assets`
--

CREATE TABLE `tbl_incident_assets` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `asset_id` bigint(20) UNSIGNED NOT NULL,
  `incident_id` bigint(20) UNSIGNED NOT NULL,
  `involvement_type` int(11) NOT NULL,
  `effect_on_asset` int(11) NOT NULL,
  `incident_cause` varchar(250) NOT NULL,
  `downtime_expected` int(11) DEFAULT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1',
  `created_by` int(11) NOT NULL,
  `modified_by` int(11) NOT NULL,
  `date_modified` timestamp NULL DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_incident_assets_cause`
--

CREATE TABLE `tbl_incident_assets_cause` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cause_name` varchar(100) NOT NULL,
  `cause_description` varchar(100) NOT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT '1',
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NULL DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_incident_authorities`
--

CREATE TABLE `tbl_incident_authorities` (
  `id` int(11) NOT NULL,
  `fields_name` varchar(25) NOT NULL,
  `label_name` varchar(100) NOT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1',
  `created_by` int(11) DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `date_modified` timestamp NULL DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_incident_category`
--

CREATE TABLE `tbl_incident_category` (
  `id` int(11) NOT NULL,
  `category_name` varchar(100) NOT NULL,
  `category_description` varchar(255) NOT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1',
  `created_by` int(11) DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `date_modified` timestamp NULL DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_incident_documents`
--

CREATE TABLE `tbl_incident_documents` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `incident_id` bigint(20) UNSIGNED NOT NULL,
  `description` varchar(50) NOT NULL,
  `file_location` varchar(50) NOT NULL,
  `original_filename` varchar(255) NOT NULL,
  `filename` varchar(50) NOT NULL,
  `is_active` tinyint(1) DEFAULT '1',
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NULL DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_incident_final_medical`
--

CREATE TABLE `tbl_incident_final_medical` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `incident_id` bigint(20) UNSIGNED NOT NULL,
  `person_involved_id` bigint(20) UNSIGNED NOT NULL,
  `report_date` timestamp NULL DEFAULT NULL,
  `notes` text,
  `is_still_employed` int(11) NOT NULL DEFAULT '0',
  `is_still_occupation` int(11) NOT NULL DEFAULT '0',
  `department_id` bigint(20) UNSIGNED DEFAULT NULL,
  `occupation_id` bigint(20) UNSIGNED DEFAULT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1',
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NULL DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_incident_medical`
--

CREATE TABLE `tbl_incident_medical` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `incident_id` bigint(20) UNSIGNED NOT NULL,
  `person_involved_id` bigint(20) UNSIGNED NOT NULL,
  `report_order` int(11) NOT NULL DEFAULT '1',
  `report_date` timestamp NULL DEFAULT NULL,
  `doctor_name` varchar(255) DEFAULT NULL,
  `medical_type` varchar(255) DEFAULT NULL,
  `contact_number` varchar(255) DEFAULT NULL,
  `doctor_feedback` varchar(255) DEFAULT NULL,
  `medical_bill` bigint(20) UNSIGNED NOT NULL,
  `trasport_cost` bigint(20) UNSIGNED NOT NULL,
  `employee_rate` bigint(20) UNSIGNED NOT NULL,
  `days_booked_off` bigint(20) UNSIGNED NOT NULL,
  `next_appointment` int(11) NOT NULL DEFAULT '0',
  `is_active` int(11) NOT NULL DEFAULT '1',
  `created_by` bigint(20) DEFAULT NULL,
  `modified_by` bigint(20) DEFAULT NULL,
  `date_modified` timestamp NULL DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_incident_medical_appointment`
--

CREATE TABLE `tbl_incident_medical_appointment` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `medical_id` bigint(20) UNSIGNED NOT NULL,
  `report_date` timestamp NULL DEFAULT NULL,
  `doctor_name` varchar(255) DEFAULT NULL,
  `medical_type_name` varchar(255) DEFAULT NULL,
  `contact_number` varchar(255) DEFAULT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1',
  `created_by` bigint(20) UNSIGNED NOT NULL,
  `modified_by` bigint(20) UNSIGNED NOT NULL,
  `date_modified` timestamp NULL DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_incident_medical_documents`
--

CREATE TABLE `tbl_incident_medical_documents` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `medical_id` bigint(20) UNSIGNED NOT NULL,
  `file_location` text NOT NULL,
  `origin_name` varchar(255) NOT NULL,
  `filename` text NOT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1',
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_by` bigint(20) UNSIGNED NOT NULL,
  `date_modified` timestamp NULL DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_incident_person_involved`
--

CREATE TABLE `tbl_incident_person_involved` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `incident_id` bigint(20) UNSIGNED NOT NULL,
  `person_involved_id` bigint(20) UNSIGNED NOT NULL,
  `person_involved_type` bigint(20) UNSIGNED NOT NULL,
  `injured_or_ill` tinyint(4) NOT NULL,
  `part_affected` varchar(20) DEFAULT NULL,
  `effect_person` varchar(20) DEFAULT NULL,
  `disease_description` varchar(255) DEFAULT NULL,
  `offsite_treatment` varchar(255) DEFAULT NULL,
  `cold_reference` varchar(255) DEFAULT NULL,
  `hours_expected` varchar(255) DEFAULT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1',
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NULL DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_incident_photo`
--

CREATE TABLE `tbl_incident_photo` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `incident_id` bigint(20) UNSIGNED NOT NULL,
  `description` varchar(50) NOT NULL,
  `file_location` varchar(50) NOT NULL,
  `original_filename` varchar(255) NOT NULL,
  `filename` varchar(50) NOT NULL,
  `is_active` tinyint(1) DEFAULT '1',
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NULL DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_incident_publicperson`
--

CREATE TABLE `tbl_incident_publicperson` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `incident_id` bigint(20) UNSIGNED NOT NULL,
  `first_name` varchar(100) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `id_number` varchar(50) NOT NULL,
  `contact_number` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `company_name` varchar(100) NOT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1',
  `modified_by` bigint(20) UNSIGNED NOT NULL,
  `created_by` bigint(20) UNSIGNED NOT NULL,
  `date_modified` timestamp NULL DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_incident_type`
--

CREATE TABLE `tbl_incident_type` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `category_id` bigint(20) UNSIGNED NOT NULL,
  `type_name` varchar(100) NOT NULL,
  `type_description` varchar(255) NOT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1',
  `created_by` int(11) DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `date_modified` timestamp NULL DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_incident_visitors`
--

CREATE TABLE `tbl_incident_visitors` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `incident_id` bigint(20) UNSIGNED NOT NULL,
  `first_name` varchar(100) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `id_number` varchar(50) NOT NULL,
  `contact_number` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `company_name` varchar(100) NOT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1',
  `modified_by` bigint(20) UNSIGNED NOT NULL,
  `created_by` bigint(20) UNSIGNED NOT NULL,
  `date_modified` timestamp NULL DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_incident_witness`
--

CREATE TABLE `tbl_incident_witness` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `incident_id` bigint(20) UNSIGNED NOT NULL,
  `person_type` bigint(20) NOT NULL,
  `witness_id` bigint(20) UNSIGNED NOT NULL,
  `witness_type` bigint(20) UNSIGNED NOT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_inspections`
--

CREATE TABLE `tbl_inspections` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `inspectiontype_id` bigint(20) UNSIGNED NOT NULL,
  `department_id` varchar(250) DEFAULT NULL,
  `due_date` timestamp NULL DEFAULT NULL,
  `inspector` bigint(20) UNSIGNED NOT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT '1',
  `created_by` bigint(20) UNSIGNED NOT NULL,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NULL DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_inspections_active`
--

CREATE TABLE `tbl_inspections_active` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `inspection_id` bigint(20) UNSIGNED NOT NULL,
  `start_date` timestamp NULL DEFAULT NULL,
  `end_date` timestamp NULL DEFAULT NULL,
  `inspected_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `signedoff_by` bigint(20) UNSIGNED DEFAULT NULL,
  `is_signed_off` tinyint(4) DEFAULT '0',
  `signedoff_date` timestamp NULL DEFAULT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT '1',
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NULL DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_inspections_active_document`
--

CREATE TABLE `tbl_inspections_active_document` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `active_inspection_id` bigint(20) UNSIGNED NOT NULL,
  `file_location` varchar(255) NOT NULL,
  `original_filename` varchar(255) NOT NULL,
  `filename` varchar(50) NOT NULL,
  `is_active` tinyint(1) DEFAULT '1',
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NULL DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_inspections_conformance`
--

CREATE TABLE `tbl_inspections_conformance` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `inspection_id` bigint(20) UNSIGNED NOT NULL,
  `department_id` bigint(20) UNSIGNED NOT NULL,
  `asset_type_id` bigint(20) UNSIGNED NOT NULL,
  `comformance_description` varchar(255) NOT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT '1',
  `is_corrective_action` tinyint(4) NOT NULL DEFAULT '0',
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NULL DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_inspections_document`
--

CREATE TABLE `tbl_inspections_document` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `inspection_id` bigint(20) UNSIGNED NOT NULL,
  `file_location` text NOT NULL,
  `original_filename` varchar(255) NOT NULL,
  `filename` varchar(50) NOT NULL,
  `is_active` tinyint(1) DEFAULT '1',
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NULL DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_inspections_frequency`
--

CREATE TABLE `tbl_inspections_frequency` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `frequency_name` varchar(255) NOT NULL,
  `frequency_description` varchar(255) NOT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT '1',
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NULL DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_inspections_frequency`
--

INSERT INTO `tbl_inspections_frequency` (`id`, `frequency_name`, `frequency_description`, `is_active`, `created_by`, `modified_by`, `date_modified`, `date_created`) VALUES
(1, 'Daily', 'Daily', 1, NULL, NULL, NULL, '2017-03-09 07:29:41'),
(2, 'Weekly', 'Weekly', 1, NULL, NULL, NULL, '2017-03-09 07:29:41'),
(3, 'Monthly', 'Monthly', 1, NULL, NULL, NULL, '2017-03-09 07:31:27'),
(4, 'Every 6 Months', 'Every 6 Months', 1, NULL, NULL, NULL, '2017-03-09 07:31:27'),
(5, 'Yearly', 'Yearly', 1, NULL, NULL, NULL, '2017-03-09 07:32:45'),
(6, 'Every 2 Years', 'Every 2 Years', 1, NULL, NULL, NULL, '2017-03-09 07:32:45');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_inspections_signoff`
--

CREATE TABLE `tbl_inspections_signoff` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `is_document_in_file` tinyint(4) NOT NULL DEFAULT '1',
  `document_id` bigint(20) UNSIGNED NOT NULL,
  `conformance_id` bigint(20) UNSIGNED NOT NULL,
  `is_signed` tinyint(4) NOT NULL DEFAULT '1',
  `is_active` tinyint(4) NOT NULL DEFAULT '1',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_modified` timestamp NULL DEFAULT NULL,
  `created_by` bigint(20) UNSIGNED NOT NULL,
  `modified_by` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_inspections_type`
--

CREATE TABLE `tbl_inspections_type` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `group_id` bigint(20) UNSIGNED NOT NULL,
  `description` varchar(255) NOT NULL,
  `frequency_id` bigint(20) UNSIGNED NOT NULL,
  `linkform_id` bigint(20) UNSIGNED NOT NULL,
  `appointment_id` bigint(20) UNSIGNED NOT NULL,
  `assettype_id` varchar(250) NOT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT '1',
  `created_by` bigint(20) UNSIGNED NOT NULL,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NULL DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_investigations`
--

CREATE TABLE `tbl_investigations` (
  `id` bigint(20) NOT NULL,
  `investigate_link_id` bigint(20) DEFAULT NULL,
  `branch_id` bigint(20) NOT NULL,
  `company_id` bigint(20) NOT NULL,
  `investigation_type` bigint(20) DEFAULT NULL,
  `investigationOptions` varchar(250) NOT NULL,
  `investigator` bigint(20) DEFAULT NULL,
  `investigator_due_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `investigator_status` varchar(20) DEFAULT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `is_active` int(11) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `attended` int(11) NOT NULL DEFAULT '0',
  `signedOff` int(11) DEFAULT NULL,
  `SignedOffDate` timestamp NULL DEFAULT NULL,
  `signedOffBy` bigint(20) DEFAULT NULL,
  `signedOffAction` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_investigation_assessment`
--

CREATE TABLE `tbl_investigation_assessment` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `investigate_id` bigint(20) DEFAULT NULL,
  `topic` varchar(250) DEFAULT NULL,
  `cause` text,
  `recommended_improvement` text,
  `action_taken` int(11) DEFAULT NULL,
  `branch_id` bigint(20) DEFAULT NULL,
  `company_id` bigint(20) DEFAULT NULL,
  `is_active` int(11) DEFAULT '1',
  `created_by` bigint(20) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_investigation_evidence`
--

CREATE TABLE `tbl_investigation_evidence` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `invest_id` bigint(20) DEFAULT NULL,
  `file_name` text,
  `file_type` varchar(140) DEFAULT NULL,
  `file_extension` varchar(45) DEFAULT NULL,
  `file_size` bigint(15) DEFAULT NULL,
  `is_active` int(11) DEFAULT '1',
  `created_by` bigint(20) DEFAULT NULL,
  `date_created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` bigint(20) DEFAULT NULL,
  `date_modified` timestamp NULL DEFAULT NULL,
  `folders_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_investigators`
--

CREATE TABLE `tbl_investigators` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `department_id` bigint(20) DEFAULT NULL,
  `occupation_id` bigint(20) DEFAULT NULL,
  `emp_id` bigint(20) DEFAULT NULL,
  `branch_id` bigint(20) DEFAULT NULL,
  `company_id` bigint(20) DEFAULT NULL,
  `investigation_dep` text,
  `investigation_group` text,
  `is_active` int(11) DEFAULT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_jobs`
--

CREATE TABLE `tbl_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `source_id` bigint(20) NOT NULL,
  `source_type` varchar(50) DEFAULT NULL,
  `job_description` varchar(255) DEFAULT NULL,
  `start_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `responsible_persons` text,
  `department_involved` text,
  `maintenance_type` varchar(20) DEFAULT NULL,
  `job_details` text,
  `risk_level` varchar(10) DEFAULT NULL,
  `hazard_types` text,
  `supplier_id` bigint(20) DEFAULT NULL,
  `resources_used` text,
  `parts_used` text,
  `approval_required` int(11) DEFAULT NULL,
  `signedOff` int(11) DEFAULT NULL,
  `signedOffBy` bigint(20) DEFAULT NULL,
  `agent_id` bigint(20) NOT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1',
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_jobs_list`
--

CREATE TABLE `tbl_jobs_list` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `source_id` bigint(20) DEFAULT NULL,
  `source_name` varchar(255) DEFAULT NULL,
  `sender` bigint(20) DEFAULT NULL,
  `recipient` bigint(20) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `subject` text,
  `branch_id` bigint(20) DEFAULT NULL,
  `company_id` bigint(20) DEFAULT NULL,
  `is_active` int(11) DEFAULT '1',
  `created_by` bigint(20) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_job_description`
--

CREATE TABLE `tbl_job_description` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `occupation_id` bigint(20) UNSIGNED NOT NULL,
  `task` text,
  `notes` text,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `date_modified` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `parent_occupation_id` bigint(19) UNSIGNED DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_job_tasks_uploads`
--

CREATE TABLE `tbl_job_tasks_uploads` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `task_id` bigint(20) UNSIGNED DEFAULT NULL,
  `file_id` bigint(20) UNSIGNED DEFAULT NULL,
  `doc_id` bigint(20) UNSIGNED DEFAULT NULL,
  `date_created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `is_file_upload` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_languages`
--

CREATE TABLE `tbl_languages` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT '0',
  `created_by` bigint(11) UNSIGNED DEFAULT NULL,
  `date_created` timestamp NULL DEFAULT NULL,
  `modified_by` bigint(11) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_languages`
--

INSERT INTO `tbl_languages` (`id`, `name`, `is_active`, `created_by`, `date_created`, `modified_by`, `date_modified`) VALUES
(1, 'English', 1, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_language_data`
--

CREATE TABLE `tbl_language_data` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `language_id` bigint(20) UNSIGNED NOT NULL,
  `code` varchar(255) NOT NULL,
  `description` text,
  `language_text` longtext NOT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_created` timestamp NULL DEFAULT NULL,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_language_data`
--

INSERT INTO `tbl_language_data` (`id`, `language_id`, `code`, `description`, `language_text`, `created_by`, `date_created`, `modified_by`, `date_modified`) VALUES
(1, 1, 'COMPANY_DETAILS', 'Company details', ' Company Details\n', NULL, NULL, 0, NULL),
(2, 1, 'SITE_NAME', 'The name of the system or website', ' SHEQ Intel\n', NULL, NULL, 0, NULL),
(3, 1, 'WORD_LOGIN', ' The word login', 'Login\n', NULL, NULL, 0, NULL),
(4, 1, 'COMPANY_INFO', 'Company information', 'Company Information\n', NULL, NULL, 0, NULL),
(5, 1, 'COMPANY_NAME', 'Name of the company', 'Company Name\n', NULL, NULL, 0, NULL),
(6, 1, 'COMPANY_VAT_NUMBER', 'company vat number', 'Vat Number\n', NULL, NULL, 0, NULL),
(7, 1, 'COMPANY_LOGO', 'Company logo', 'Logo\n', NULL, NULL, 0, NULL),
(8, 1, 'COMPANY_PTY_NUMBER', 'PTY number ', 'PTY Number\n', NULL, NULL, 0, NULL),
(9, 1, 'HOW_MANY_BRANCHES', 'How many branches a company have', 'How many Branches do your company Have?\n', NULL, NULL, 0, NULL),
(10, 1, 'HEAD_OFFICE_DETAILS', 'Head Office Details', 'Head Office Details\n', NULL, NULL, 0, NULL),
(11, 1, 'OFFICE_ADDRESS', 'Office address', 'Office Address\n', NULL, NULL, 0, NULL),
(12, 1, 'OFFICE_NUMBER', 'Office Number', 'Office Number\n', NULL, NULL, 0, NULL),
(13, 1, 'CELLPHONE_NUMBER', 'Cellphone number', 'Cellphone Number\n', NULL, NULL, 0, NULL),
(14, 1, 'FAX_NUMBER', 'Fax number', 'Fax Number\n', NULL, NULL, 0, NULL),
(15, 1, 'OFFICE_EMAIL', 'Office e-mail address', 'Office E-mail Address\n', NULL, NULL, 0, NULL),
(16, 1, 'SUBMIT', 'Submit button', 'Submit\n', NULL, NULL, 0, NULL),
(17, 1, 'RESET', 'Reset button ', 'Reset\n', NULL, NULL, 0, NULL),
(18, 1, 'EMPLOYEE_NUMBER', 'Employee number ', 'Employee Number\n', NULL, NULL, 0, NULL),
(19, 1, 'EMPLOYEE_FULLNAME', 'Employee fullnames ', 'Employee Fullname\n', NULL, NULL, 0, NULL),
(20, 1, 'EMPLOYEE_NAME', 'Employee fullnames ', 'Employee Name\n', NULL, NULL, 0, NULL),
(21, 1, 'EMPLOYEE_SURNAME', 'Employee lastname', 'Employee Surname\n', NULL, NULL, 0, NULL),
(27, 1, 'UPDATE', 'Update button', 'Update\n', NULL, NULL, 0, NULL),
(28, 1, 'EMPLOYEE_NATIONALITY', 'Employee Nationality', ' Employee Nationality\n', NULL, NULL, 0, NULL),
(29, 1, 'EMPLOYEE_ID', 'Employee Id', 'Employee ID\n', NULL, NULL, 0, NULL),
(30, 1, 'EMPLOYEE_GENDER', 'Employee Gender', 'Employee Gender\n', NULL, NULL, 0, NULL),
(31, 1, 'EMPLOYEE_RACE', 'Employee Race', 'Employee Race\n', NULL, NULL, 0, NULL),
(32, 1, 'EMPLOYEE_CELLPHONE', 'Employee Cellphone', 'Employee Cellphone\n', NULL, NULL, 0, NULL),
(33, 1, 'EMPLOYEE_EMAIL', 'Employee email', 'Employee Email\n', NULL, NULL, 0, NULL),
(34, 1, 'EMPLOYEE_ALLERGIES', 'Employee allergies', 'Employee Allergies\n', NULL, NULL, 0, NULL),
(35, 1, 'EMPLOYEE_BLOOD_TYPE', 'Employee blood Type', ' Employee Blood Type\n', NULL, NULL, 0, NULL),
(36, 1, 'DEPARTMENT_NAME', 'Department Name', 'Department Name\n', NULL, NULL, 0, NULL),
(37, 1, 'BUILDING_NAME', 'Building Name', 'Building Name\n', NULL, NULL, 0, NULL),
(38, 1, 'DELETE', 'Delete', 'Delete\n', NULL, NULL, 0, NULL),
(39, 1, 'EDIT', 'Edit', 'Edit\n', NULL, NULL, 0, NULL),
(40, 1, 'EMPLOYEE_LANGUAGE', 'Employee language', ' Home Language\n', NULL, NULL, 0, NULL),
(41, 1, 'SUPPLIER_DETAILS', 'Supplier Details', 'Supplier Details\n', NULL, NULL, 0, NULL),
(42, 1, 'REQUEST_QUOTATION', 'Request Quotaion', 'Request Quote\n', NULL, NULL, 0, NULL),
(43, 1, 'SHEQ_COMPLIANCE', 'Sheq Compliance', 'SHEQ Compliance\n', NULL, NULL, 0, NULL),
(44, 1, 'AUDIT', 'audit', 'Audit\n', NULL, NULL, 0, NULL),
(45, 1, 'NON_CONFORMANCE', 'Non conformance', 'Non-Conformance\n', NULL, NULL, 0, NULL),
(46, 1, 'HISTORY', 'History', 'Histroy\n', NULL, NULL, 0, NULL),
(47, 1, 'CONTACT_PERSON', 'Primary Contact', 'Contact person\n', NULL, NULL, 0, NULL),
(48, 1, 'CONTACT_NUMBER', 'Primary contact number', 'Contact Number\n', NULL, NULL, 0, NULL),
(49, 1, 'EMAIL', 'Email', 'E-mail\n', NULL, NULL, 0, NULL),
(50, 1, 'WEBSITE', 'Website', 'Website\n', NULL, NULL, 0, NULL),
(51, 1, 'BEE_CERTIFICATE', 'BEE certificate', 'BEE Certificate\n', NULL, NULL, 0, NULL),
(52, 1, 'RSA_HEAD_OFFICE_DETAILS', 'RSA Head Office Details', 'RSA Head Office Details\n', NULL, NULL, 0, NULL),
(53, 1, 'SERVICE_DETAILS', 'Service Details', 'Service Details\n', NULL, NULL, 0, NULL),
(54, 1, 'SUPPLY_TYPE', 'Supply Type', 'Supply Type\n', NULL, NULL, 0, NULL),
(55, 1, 'CORE_BUSINESS_DESCRIPTION', 'Core Business Description', 'Core Business Description\n', NULL, NULL, 0, NULL),
(56, 1, 'SERVICE_FREQUENCY', 'Service Frequency', 'Service Frequency\n', NULL, NULL, 0, NULL),
(57, 1, 'UPLOAD_SUPPORTING_DOCUMENTS', 'Upload Supporting Documents', 'Upload Supporting Documents\n', NULL, NULL, 0, NULL),
(58, 1, 'COMPLIANCE', 'Compliance', 'Compliance\n', NULL, NULL, 0, NULL),
(59, 1, 'CONSTRUCTION', 'Construction', 'Construction\n', NULL, NULL, 0, NULL),
(60, 1, 'HIGH_RISK', 'High Risk', 'High Risk\n', NULL, NULL, 0, NULL),
(61, 1, 'DESCRIPTION', 'Description', 'Description\n', NULL, NULL, 0, NULL),
(62, 1, 'SUPPLIER_AUDIT', 'Supplier Audit', 'Supplier Audit\n', NULL, NULL, 0, NULL),
(63, 1, 'WILL_APPLICABLE_SUPPLIER_BE_AUDITED', 'Will Applicable supplier be Audited', 'Will Applicable supplier be Audited\n', NULL, NULL, 0, NULL),
(64, 1, 'AUDIT_TYPE', 'Audit Type', 'Audit Type\n', NULL, NULL, 0, NULL),
(65, 1, 'AUDIT_FREQUENCY', 'Audit Frequency', 'Audit Frequency\n', NULL, NULL, 0, NULL),
(66, 1, 'NEXT_AUDIT_MONTH', 'Next Audit Month', 'Next Audit Month\n', NULL, NULL, 0, NULL),
(67, 1, 'NEW_SUPPLIER', 'New Supplier', 'New Supplier\n', NULL, NULL, 0, NULL),
(68, 1, 'ADD_SUPPLIER', 'Add Supplier', 'Add Supplier\n', NULL, NULL, 0, NULL),
(69, 1, 'SERVICE_PROVIDER_DETAILS', 'Service Provider Details', 'Service Provider Details\n', NULL, NULL, 0, NULL),
(70, 1, 'SCHEDULE', 'Schedule', 'Schedule\n', NULL, NULL, 0, NULL),
(71, 1, 'SERVICE_TYPE', 'Service Type', 'Service Type\n', NULL, NULL, 0, NULL),
(72, 1, 'SERVICE_PROVIDER_AUDIT', 'Service Provider Audit', 'Service Provider Audit\n', NULL, NULL, 0, NULL),
(73, 1, 'WILL_APPLICABLE_SERVICE_PROVIDER_BE_AUDITED', 'Will Applicable Service Provider be Audited', 'Will Applicable Service Provider be Audited\n', NULL, NULL, 0, NULL),
(74, 1, 'ADD_SERVICE_PROVIDER', 'Add Service Provider', 'Add Service Provider\n', NULL, NULL, 0, NULL),
(75, 1, 'NEW_SERVICE_PROVIDER', 'New Service Provider', 'New Service Provider\n', NULL, NULL, 0, NULL),
(76, 1, 'CUSTOMER_DETAILS', 'Customer details', 'Customer Details\n', NULL, NULL, 0, NULL),
(77, 1, 'CUSTOMER_COMPLAINTS', 'Customer complaints', 'Customer Complaints\n', NULL, NULL, 0, NULL),
(78, 1, 'SETTINGS', 'Settings', 'Settings\n', NULL, NULL, 0, NULL),
(79, 1, 'CUSTOMER_TYPE', 'Customer Type', 'Customer Type\n', NULL, NULL, 0, NULL),
(80, 1, 'REASON_FOR_LISTING', 'Reason For Listing', 'Reason For Listing\n', NULL, NULL, 0, NULL),
(81, 1, 'ADDRESS', 'Address', 'Address\n', NULL, NULL, 0, NULL),
(82, 1, 'GENDER', 'Gender', 'Gender\n', NULL, NULL, 0, NULL),
(83, 1, 'NAME', 'Name', 'Name\n', NULL, NULL, 0, NULL),
(84, 1, 'SURNAME', 'Surname', 'Surname\n', NULL, NULL, 0, NULL),
(85, 1, 'EMPLOYEE_DETAILS', 'Employee details', 'Employee Details\n', NULL, NULL, 0, NULL),
(86, 1, 'SHEQ_TEAM', 'Sheq Team', 'SHEQ Team\n', NULL, NULL, 0, NULL),
(87, 1, 'EMERGENCY', 'Emergency', 'Emergency\n', NULL, NULL, 0, NULL),
(88, 1, 'OCCUPATION', 'Occupation', 'Occupation\n', NULL, NULL, 0, NULL),
(89, 1, 'TRAINING_COMPETENCE', 'Training and competence', 'Training & Competence\n', NULL, NULL, 0, NULL),
(90, 1, 'HEALTH', 'Health', 'Health\n', NULL, NULL, 0, NULL),
(91, 1, 'PPE', 'Personal protective Equipment', 'PPE\n', NULL, NULL, 0, NULL),
(92, 1, 'PERFORMANCE', 'Performance', 'Performance\n', NULL, NULL, 0, NULL),
(93, 1, 'PERSONAL_DETAILS', 'Personal details', 'Personal Details\n', NULL, NULL, 0, NULL),
(94, 1, 'BRANCH_NAME', 'Branch Name', 'Branch Name\n', NULL, NULL, 0, NULL),
(95, 1, 'DIRECT_MANAGER', 'Direct Manager', 'Direct Manager', NULL, NULL, 0, NULL),
(96, 1, 'EMPLOYEE_MARITAL_STATUS', 'Marital Status', 'Marital Status\n', NULL, NULL, 0, NULL),
(97, 1, 'SECOND_LAMGUAGE', 'Second language', 'Second language\r\n', NULL, NULL, 0, NULL),
(98, 1, 'SECOND_LANGUAGE', 'Second language', 'Second language\n', NULL, NULL, 0, NULL),
(99, 1, 'HEAD_OFFICE_DESCRIPTION', 'Head Office Description', 'Head Office Description\n', NULL, NULL, 0, NULL),
(100, 1, '', '', '', NULL, NULL, 0, NULL),
(101, 1, 'APPROVAL', 'Approval', 'Approval\r\n', NULL, NULL, 0, NULL),
(102, 1, 'SUPPLIER_OVERVIEW', 'Supplier Overview', 'Supplier Overview', NULL, NULL, 0, NULL),
(103, 1, 'EMPLOYEE_OVERVIEW', 'Employee overview', 'Employee Overview\n', NULL, NULL, 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_material`
--

CREATE TABLE `tbl_material` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `category_id` bigint(20) UNSIGNED NOT NULL,
  `type_id` bigint(20) UNSIGNED NOT NULL,
  `description` text,
  `manufacture_name` varchar(255) NOT NULL,
  `companyassetnumber` varchar(255) DEFAULT NULL,
  `supplier_id` bigint(20) UNSIGNED DEFAULT NULL,
  `days_of_delivery` varchar(255) NOT NULL,
  `cost` decimal(20,4) DEFAULT NULL,
  `cost_unit` bigint(20) UNSIGNED DEFAULT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT '1',
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NULL DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_material_category`
--

CREATE TABLE `tbl_material_category` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `branch_id` bigint(20) DEFAULT NULL,
  `company_id` bigint(20) NOT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT '1',
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_material_department_store`
--

CREATE TABLE `tbl_material_department_store` (
  `material_id` bigint(20) UNSIGNED NOT NULL,
  `department_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_material_department_use`
--

CREATE TABLE `tbl_material_department_use` (
  `material_id` bigint(20) UNSIGNED NOT NULL,
  `department_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_material_documents`
--

CREATE TABLE `tbl_material_documents` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `description` text,
  `source_id` bigint(20) NOT NULL,
  `document` text,
  `is_active` int(11) DEFAULT '1',
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_material_properties_emergency`
--

CREATE TABLE `tbl_material_properties_emergency` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `file_msds` text,
  `storage_department` bigint(20) DEFAULT NULL,
  `storage_condition` varchar(50) DEFAULT NULL,
  `storage_facility` varchar(250) DEFAULT NULL,
  `chemical_table` varchar(10) DEFAULT NULL,
  `chemical_class` varchar(250) DEFAULT NULL,
  `emergency_eyes` varchar(250) DEFAULT NULL,
  `emergency_skin` varchar(250) DEFAULT NULL,
  `emergency_inhalation` varchar(250) DEFAULT NULL,
  `emergency_injection` varchar(250) DEFAULT NULL,
  `emergency_consumption` varchar(250) DEFAULT NULL,
  `emergency_fire_equipment` varchar(250) DEFAULT NULL,
  `emergency_fire_fighting_ppe` varchar(250) DEFAULT NULL,
  `emergency_method` varchar(250) DEFAULT NULL,
  `emergency_is_biological_monitoring` varchar(10) DEFAULT NULL,
  `emergency_biological_type` varchar(250) DEFAULT NULL,
  `emergency_biological_frequency` varchar(250) DEFAULT NULL,
  `emergency_by_product` varchar(250) DEFAULT NULL,
  `emergency_effect_on_environment` varchar(250) DEFAULT NULL,
  `emergency_removal_method` varchar(250) DEFAULT NULL,
  `is_active` int(11) DEFAULT '1',
  `material_id` bigint(20) NOT NULL,
  `branch_id` bigint(20) NOT NULL,
  `company_id` bigint(20) NOT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_material_som`
--

CREATE TABLE `tbl_material_som` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `description` text,
  `source_id` bigint(20) NOT NULL,
  `is_active` int(11) DEFAULT '1',
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_material_type`
--

CREATE TABLE `tbl_material_type` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `category_id` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  `branch_id` bigint(20) DEFAULT NULL,
  `company_id` bigint(20) NOT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT '1',
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_material_types`
--

CREATE TABLE `tbl_material_types` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `category_id` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  `branch_id` bigint(20) DEFAULT NULL,
  `company_id` bigint(20) NOT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT '1',
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_material_unit`
--

CREATE TABLE `tbl_material_unit` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `unit_name` varchar(45) DEFAULT NULL,
  `unit_description` varchar(255) DEFAULT NULL,
  `group_id` int(11) DEFAULT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT '1',
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NULL DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_material_usage`
--

CREATE TABLE `tbl_material_usage` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `material_id` bigint(20) NOT NULL,
  `suppliers` varchar(255) DEFAULT NULL,
  `purchase_price` varchar(250) NOT NULL,
  `unit` varchar(250) NOT NULL,
  `supplier_delivery_days` bigint(20) DEFAULT NULL,
  `description` varchar(250) NOT NULL,
  `units_per_frequency` varchar(50) NOT NULL,
  `cost_per_unit` decimal(20,0) NOT NULL,
  `cost_days` int(11) NOT NULL,
  `responsible_person` bigint(20) NOT NULL,
  `branch_id` bigint(20) DEFAULT NULL,
  `company_id` bigint(20) NOT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT '1',
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_matrix_type`
--

CREATE TABLE `tbl_matrix_type` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type_name` varchar(60) DEFAULT NULL,
  `description` varchar(145) DEFAULT NULL,
  `date_created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `date_modified` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_matrix_type`
--

INSERT INTO `tbl_matrix_type` (`id`, `type_name`, `description`, `date_created`, `date_modified`) VALUES
(1, 'occupation', 'set course by occupation', '2017-06-02 05:31:28', NULL),
(2, 'appointment', 'set course by appointment', '2017-06-02 05:31:28', NULL),
(3, 'individual', 'set course by individual', '2017-06-02 05:31:51', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_method_statements`
--

CREATE TABLE `tbl_method_statements` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type_id` bigint(20) DEFAULT NULL,
  `description` varchar(250) DEFAULT NULL,
  `details` text,
  `branch_id` bigint(20) DEFAULT NULL,
  `company_id` bigint(20) DEFAULT NULL,
  `is_active` int(11) DEFAULT '1',
  `created_by` bigint(20) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_method_statements_types`
--

CREATE TABLE `tbl_method_statements_types` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type_name` varchar(250) DEFAULT NULL,
  `branch_id` bigint(20) DEFAULT NULL,
  `company_id` bigint(20) DEFAULT NULL,
  `is_active` int(11) DEFAULT '1',
  `created_by` bigint(20) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_modules`
--

CREATE TABLE `tbl_modules` (
  `id` bigint(20) NOT NULL,
  `module_id` varchar(255) NOT NULL,
  `module_parent_id` int(11) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `sort` int(10) DEFAULT NULL,
  `icon` varchar(30) DEFAULT NULL,
  `name_lang_key` varchar(255) DEFAULT NULL,
  `desc_lang_key` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_module_item`
--

CREATE TABLE `tbl_module_item` (
  `id` bigint(20) NOT NULL,
  `module_id` varchar(255) NOT NULL,
  `module_item_name` int(11) DEFAULT NULL,
  `sort` int(10) DEFAULT NULL,
  `icon` varchar(30) DEFAULT NULL,
  `name_lang_key` varchar(255) DEFAULT NULL,
  `desc_lang_key` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_near_miss`
--

CREATE TABLE `tbl_near_miss` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `branch_id` bigint(20) NOT NULL,
  `company_id` int(11) NOT NULL,
  `due_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `group_field` bigint(20) NOT NULL,
  `department_field` bigint(20) NOT NULL,
  `non_conformance_details` text,
  `type_field` varchar(100) DEFAULT NULL,
  `sourceID` bigint(20) NOT NULL,
  `sourceType` varchar(50) NOT NULL,
  `action_field` text NOT NULL,
  `recommended_improvement_field` text,
  `resource_involved` text,
  `material_involved` bigint(20) NOT NULL,
  `person_involved` text,
  `responsible_person` bigint(20) NOT NULL,
  `supplier_id` bigint(20) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `photo_name` text NOT NULL,
  `projectID` bigint(20) NOT NULL,
  `customerID` bigint(20) NOT NULL,
  `Investigate` int(11) NOT NULL,
  `inspection_id` bigint(20) NOT NULL,
  `question_id` bigint(20) NOT NULL,
  `InvestigateDueDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `correctiveActionAdded` tinyint(1) NOT NULL,
  `correctiveActionDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `correctiveActionAddedBy` bigint(20) NOT NULL,
  `signedOff` int(11) NOT NULL,
  `signedOffDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `signedOffBy` bigint(20) NOT NULL,
  `signedOffAction` text NOT NULL,
  `attended` int(11) NOT NULL,
  `is_active` int(11) NOT NULL,
  `foreignkey_id` bigint(20) NOT NULL,
  `first_field` varchar(255) DEFAULT NULL,
  `second_field` tinyint(4) DEFAULT '0',
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `in_active_reason` text NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_near_miss_types`
--

CREATE TABLE `tbl_near_miss_types` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `group_id` bigint(20) NOT NULL,
  `type_name` varchar(250) DEFAULT NULL,
  `is_default` int(11) DEFAULT NULL,
  `branch_id` bigint(20) DEFAULT NULL,
  `company_id` bigint(20) DEFAULT NULL,
  `is_active` int(11) DEFAULT '1',
  `created_by` bigint(20) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_non_conformance`
--

CREATE TABLE `tbl_non_conformance` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `branch_id` bigint(20) NOT NULL,
  `company_id` int(11) NOT NULL,
  `due_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `priority` varchar(50) NOT NULL,
  `group_field` bigint(20) NOT NULL,
  `department_field` bigint(20) NOT NULL,
  `location_field` bigint(20) NOT NULL,
  `non_conformance_details` text,
  `type_field` varchar(100) DEFAULT NULL,
  `sourceID` bigint(20) NOT NULL,
  `sourceType` varchar(50) NOT NULL,
  `recommended_improvement_field` text,
  `resource_involved` text,
  `material_involved` bigint(20) NOT NULL,
  `person_involved` text,
  `responsible_person` bigint(20) NOT NULL,
  `supplier_id` bigint(20) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `photo_name` text NOT NULL,
  `projectID` bigint(20) NOT NULL,
  `customerID` bigint(20) NOT NULL,
  `Investigate` int(11) NOT NULL,
  `inspection_id` bigint(20) NOT NULL,
  `question_id` bigint(20) NOT NULL,
  `InvestigateDueDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `correctiveActionAdded` tinyint(1) NOT NULL,
  `correctiveActionDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `correctiveActionAddedBy` bigint(20) NOT NULL,
  `signedOff` int(11) NOT NULL,
  `signedOffDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `signedOffBy` bigint(20) NOT NULL,
  `signedOffAction` text NOT NULL,
  `attended` int(11) NOT NULL,
  `is_active` int(11) NOT NULL,
  `foreignkey_id` bigint(20) NOT NULL,
  `first_field` varchar(255) DEFAULT NULL,
  `second_field` tinyint(4) DEFAULT '0',
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `in_active_reason` text NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_non_conformance_types`
--

CREATE TABLE `tbl_non_conformance_types` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `group_id` bigint(20) NOT NULL,
  `type_name` varchar(250) DEFAULT NULL,
  `is_default` int(11) DEFAULT NULL,
  `branch_id` bigint(20) DEFAULT NULL,
  `company_id` bigint(20) DEFAULT NULL,
  `is_active` int(11) DEFAULT '1',
  `created_by` bigint(20) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_notes`
--

CREATE TABLE `tbl_notes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `company_id` bigint(20) UNSIGNED DEFAULT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `note` longtext,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_created` timestamp NULL DEFAULT NULL,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_objectives`
--

CREATE TABLE `tbl_objectives` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `start_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `group_id` bigint(20) UNSIGNED NOT NULL,
  `type_id` bigint(20) UNSIGNED NOT NULL,
  `increase_decrease` int(11) NOT NULL DEFAULT '1',
  `description` varchar(255) DEFAULT NULL,
  `measurement` bigint(20) UNSIGNED DEFAULT NULL,
  `measurement_unit` varchar(250) DEFAULT NULL,
  `percentage` bigint(20) UNSIGNED DEFAULT NULL,
  `percentage_frequency` bigint(20) UNSIGNED NOT NULL,
  `target_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `responsible_person` bigint(20) UNSIGNED NOT NULL,
  `frequency_id` bigint(20) UNSIGNED NOT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1',
  `is_requested` int(11) NOT NULL DEFAULT '0',
  `is_approved` int(11) NOT NULL DEFAULT '0',
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NULL DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_objectives_frequency`
--

CREATE TABLE `tbl_objectives_frequency` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `frequency_name` varchar(255) DEFAULT NULL,
  `frequency_description` varchar(255) DEFAULT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1',
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NULL DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_objectives_frequency`
--

INSERT INTO `tbl_objectives_frequency` (`id`, `frequency_name`, `frequency_description`, `is_active`, `created_by`, `modified_by`, `date_modified`, `date_created`) VALUES
(1, 'Day', 'Day', 1, 1, NULL, '2017-01-23 03:30:58', '2017-01-18 04:30:17'),
(2, 'Week', 'Week', 1, 1, NULL, '2017-01-23 03:31:07', '2017-01-18 04:30:17'),
(3, 'Month', 'Month', 1, 1, NULL, '2017-01-23 03:31:17', '2017-01-18 04:33:27'),
(4, 'Year', 'Year', 1, 1, NULL, '2017-01-23 03:36:19', '2017-01-18 04:33:50');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_objectives_performance_frequency`
--

CREATE TABLE `tbl_objectives_performance_frequency` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `frequency_name` varchar(255) DEFAULT NULL,
  `frequency_description` varchar(255) DEFAULT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1',
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NULL DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_objectives_performance_frequency`
--

INSERT INTO `tbl_objectives_performance_frequency` (`id`, `frequency_name`, `frequency_description`, `is_active`, `created_by`, `modified_by`, `date_modified`, `date_created`) VALUES
(1, 'Daily', 'Daily', 1, 1, NULL, '2017-01-23 03:30:58', '2017-01-18 04:30:17'),
(2, 'Weekly', 'Weekly', 1, 1, NULL, '2017-01-23 03:31:07', '2017-01-18 04:30:17'),
(3, 'Month', 'Monthly', 1, 1, NULL, '2017-01-23 03:31:17', '2017-01-18 04:33:27'),
(4, 'Year', 'Yearly', 1, 1, NULL, '2017-01-23 03:36:19', '2017-01-18 04:33:50');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_objectives_status`
--

CREATE TABLE `tbl_objectives_status` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `objectives_id` bigint(20) UNSIGNED NOT NULL,
  `date` timestamp NULL DEFAULT NULL,
  `measurement` varchar(255) DEFAULT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1',
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NULL DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_objectives_strategy`
--

CREATE TABLE `tbl_objectives_strategy` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `objectives_id` bigint(20) UNSIGNED NOT NULL,
  `responsible_person` bigint(20) UNSIGNED NOT NULL,
  `due_date` timestamp NULL DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `date_completed` timestamp NULL DEFAULT NULL,
  `action_taken` varchar(255) DEFAULT NULL,
  `is_signed_off` int(11) NOT NULL DEFAULT '0',
  `is_active` int(11) NOT NULL DEFAULT '1',
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NULL DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_objectives_types`
--

CREATE TABLE `tbl_objectives_types` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name_type` varchar(255) DEFAULT NULL,
  `description_type` varchar(255) DEFAULT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1',
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NULL DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_occupations`
--

CREATE TABLE `tbl_occupations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `department_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_created` timestamp NULL DEFAULT NULL,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NULL DEFAULT NULL,
  `is_deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_online_inspections`
--

CREATE TABLE `tbl_online_inspections` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `group_id` bigint(20) NOT NULL,
  `department_id` varchar(250) NOT NULL,
  `branch_id` bigint(20) DEFAULT NULL,
  `company_id` bigint(20) NOT NULL,
  `description_type` varchar(255) DEFAULT NULL,
  `specific_type` varchar(255) DEFAULT NULL,
  `inspection_description` varchar(255) NOT NULL,
  `inspection_status` varchar(10) NOT NULL DEFAULT 'Draft',
  `details` text NOT NULL,
  `frequency` varchar(255) NOT NULL,
  `hasInspected` int(11) NOT NULL DEFAULT '0',
  `is_active` int(11) NOT NULL DEFAULT '1',
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_online_inspection_assets`
--

CREATE TABLE `tbl_online_inspection_assets` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `inspection_id` varchar(250) DEFAULT NULL,
  `asset_id` bigint(20) DEFAULT NULL,
  `present` int(11) DEFAULT NULL,
  `branch_id` bigint(20) DEFAULT NULL,
  `company_id` bigint(20) DEFAULT NULL,
  `is_active` int(11) DEFAULT '1',
  `created_by` bigint(20) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_online_inspection_assets_assessment`
--

CREATE TABLE `tbl_online_inspection_assets_assessment` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `inspection_id` varchar(250) DEFAULT NULL,
  `asset_id` bigint(20) DEFAULT NULL,
  `present` int(11) DEFAULT NULL,
  `branch_id` bigint(20) DEFAULT NULL,
  `company_id` bigint(20) DEFAULT NULL,
  `is_active` int(11) DEFAULT '1',
  `created_by` bigint(20) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_online_inspection_category`
--

CREATE TABLE `tbl_online_inspection_category` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `inspection_id` bigint(20) DEFAULT NULL,
  `category` varchar(255) DEFAULT NULL,
  `branch_id` bigint(20) DEFAULT NULL,
  `company_id` bigint(20) DEFAULT NULL,
  `is_active` bigint(20) DEFAULT '1',
  `created_by` bigint(20) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_online_inspection_details`
--

CREATE TABLE `tbl_online_inspection_details` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `template_id` bigint(20) DEFAULT NULL,
  `inspector_id` bigint(20) DEFAULT NULL,
  `departments` varchar(500) DEFAULT NULL,
  `frequency` varchar(250) DEFAULT NULL,
  `next_inspection_date` timestamp NULL DEFAULT NULL,
  `setupStatus` varchar(10) DEFAULT NULL,
  `hasInspected` int(11) DEFAULT NULL,
  `IsScheduled` int(11) DEFAULT NULL,
  `ScheduleDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `score` int(11) DEFAULT NULL,
  `branch_id` bigint(20) DEFAULT NULL,
  `company_id` bigint(20) DEFAULT NULL,
  `is_active` int(11) DEFAULT '1',
  `in_active_reason` text NOT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_online_inspection_leg_iso`
--

CREATE TABLE `tbl_online_inspection_leg_iso` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `inspection_id` bigint(20) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `branch_id` bigint(20) DEFAULT NULL,
  `company_id` bigint(20) NOT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT '1',
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_online_inspection_materials`
--

CREATE TABLE `tbl_online_inspection_materials` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `inspection_id` varchar(250) DEFAULT NULL,
  `material_id` bigint(20) DEFAULT NULL,
  `present` int(11) DEFAULT NULL,
  `branch_id` bigint(20) DEFAULT NULL,
  `company_id` bigint(20) DEFAULT NULL,
  `is_active` int(11) DEFAULT '1',
  `created_by` bigint(20) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_online_inspection_materials_assessment`
--

CREATE TABLE `tbl_online_inspection_materials_assessment` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `inspection_id` varchar(250) DEFAULT NULL,
  `material_id` bigint(20) DEFAULT NULL,
  `present` int(11) DEFAULT NULL,
  `branch_id` bigint(20) DEFAULT NULL,
  `company_id` bigint(20) DEFAULT NULL,
  `is_active` int(11) DEFAULT '1',
  `created_by` bigint(20) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_online_inspection_plan`
--

CREATE TABLE `tbl_online_inspection_plan` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `group_id` bigint(20) NOT NULL,
  `branch_id` bigint(20) DEFAULT NULL,
  `company_id` bigint(20) NOT NULL,
  `department` varchar(250) NOT NULL,
  `inspection_description` bigint(20) NOT NULL,
  `frequency` varchar(50) DEFAULT NULL,
  `next_inspection_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `inspectors_id` bigint(20) DEFAULT NULL,
  `setupStatus` varchar(10) NOT NULL,
  `hasInspected` int(11) NOT NULL DEFAULT '0',
  `hasScheduled` int(11) NOT NULL,
  `ScheduleDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `score` int(11) NOT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1',
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_online_inspection_questions`
--

CREATE TABLE `tbl_online_inspection_questions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `inspection_id` varchar(250) DEFAULT NULL,
  `category_id` bigint(20) DEFAULT NULL,
  `question` text,
  `complaint` varchar(10) DEFAULT NULL,
  `resource_involved` bigint(20) DEFAULT NULL,
  `comments` text,
  `status` varchar(20) DEFAULT NULL,
  `photo` text,
  `branch_id` bigint(20) DEFAULT NULL,
  `company_id` bigint(20) DEFAULT NULL,
  `is_active` int(11) DEFAULT '1',
  `created_by` bigint(20) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_online_inspection_questions_assessment`
--

CREATE TABLE `tbl_online_inspection_questions_assessment` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `inspection_id` varchar(250) DEFAULT NULL,
  `category_id` varchar(250) DEFAULT NULL,
  `question` text,
  `complaint` varchar(10) DEFAULT NULL,
  `resource_involved` bigint(20) DEFAULT NULL,
  `comments` text,
  `status` varchar(20) DEFAULT NULL,
  `photo` text,
  `branch_id` bigint(20) DEFAULT NULL,
  `company_id` bigint(20) DEFAULT NULL,
  `is_active` int(11) DEFAULT '1',
  `created_by` bigint(20) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_person_involved_affected`
--

CREATE TABLE `tbl_person_involved_affected` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `affected_name` varchar(255) NOT NULL,
  `affected_description` varchar(255) NOT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1',
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NULL DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_person_involved_effects`
--

CREATE TABLE `tbl_person_involved_effects` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `effects_name` varchar(255) NOT NULL,
  `effects_description` varchar(255) NOT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1',
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NULL DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_physical_requirements`
--

CREATE TABLE `tbl_physical_requirements` (
  `id` int(11) UNSIGNED NOT NULL,
  `occupation_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '0',
  `created_by` bigint(20) DEFAULT NULL,
  `modified_by` bigint(20) DEFAULT NULL,
  `date_created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `date_modified` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_ppe`
--

CREATE TABLE `tbl_ppe` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `ppe_type_id` bigint(20) UNSIGNED DEFAULT NULL,
  `supplier_id` bigint(20) UNSIGNED DEFAULT NULL,
  `description` varchar(145) DEFAULT NULL,
  `brand` varchar(180) DEFAULT NULL,
  `frequency_id` bigint(20) UNSIGNED DEFAULT NULL,
  `unit_cost` decimal(10,0) DEFAULT NULL,
  `quantity` double NOT NULL DEFAULT '0',
  `noise_reduction_rate` bigint(20) DEFAULT NULL,
  `dust_reduction` bigint(20) DEFAULT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `date_modified` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_ppe_issue_frequency`
--

CREATE TABLE `tbl_ppe_issue_frequency` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `requirement_id` bigint(20) UNSIGNED NOT NULL,
  `issue_date` timestamp NULL DEFAULT NULL,
  `due_date` timestamp NULL DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `modified_by` bigint(20) DEFAULT NULL,
  `date_created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `date_modified` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_ppe_requirements`
--

CREATE TABLE `tbl_ppe_requirements` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `occupation_id` bigint(20) UNSIGNED NOT NULL,
  `ppe_id` bigint(20) UNSIGNED NOT NULL,
  `frequency_id` bigint(20) UNSIGNED NOT NULL,
  `date_created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `date_modified` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `created_by` bigint(20) DEFAULT NULL,
  `modified_by` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_ppe_types`
--

CREATE TABLE `tbl_ppe_types` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(145) DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `modified_by` bigint(20) DEFAULT NULL,
  `date_created` timestamp NULL DEFAULT NULL,
  `date_modified` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_ppe_uploads`
--

CREATE TABLE `tbl_ppe_uploads` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `ppe_id` bigint(20) UNSIGNED NOT NULL,
  `file_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_preventative_asset_list`
--

CREATE TABLE `tbl_preventative_asset_list` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `prev_id` bigint(20) NOT NULL,
  `asset_id` bigint(20) NOT NULL,
  `branch_id` bigint(20) DEFAULT NULL,
  `company_id` bigint(20) DEFAULT NULL,
  `is_active` int(11) DEFAULT '1',
  `created_by` bigint(20) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_preventative_asset_status`
--

CREATE TABLE `tbl_preventative_asset_status` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `asset_list_id` bigint(20) DEFAULT NULL,
  `prev_id` bigint(20) NOT NULL,
  `frequency` varchar(50) DEFAULT NULL,
  `due_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `responsible_person` bigint(20) DEFAULT NULL,
  `hidden` tinyint(1) NOT NULL DEFAULT '0',
  `status` varchar(100) DEFAULT NULL,
  `supplier_id` text NOT NULL,
  `estimated_cost` varchar(250) NOT NULL,
  `action_id` bigint(20) DEFAULT NULL,
  `evidence` text,
  `branch_id` bigint(20) DEFAULT NULL,
  `company_id` bigint(20) DEFAULT NULL,
  `is_active` int(11) DEFAULT '1',
  `created_by` bigint(20) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_preventative_jobs`
--

CREATE TABLE `tbl_preventative_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `prev_id` bigint(20) NOT NULL,
  `description` text,
  `branch_id` bigint(20) DEFAULT NULL,
  `company_id` bigint(20) DEFAULT NULL,
  `is_active` int(11) DEFAULT '1',
  `created_by` bigint(20) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_preventative_maintenance`
--

CREATE TABLE `tbl_preventative_maintenance` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `maintenance` varchar(250) NOT NULL,
  `int_ext` varchar(250) NOT NULL,
  `type_service` varchar(50) DEFAULT NULL,
  `description` text,
  `notes` text,
  `frequency` varchar(100) NOT NULL,
  `due_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `responsible_person` bigint(20) NOT NULL,
  `supplier_id` text NOT NULL,
  `estimated_cost` varchar(250) NOT NULL,
  `status` varchar(250) NOT NULL,
  `branch_id` bigint(20) DEFAULT NULL,
  `company_id` bigint(20) DEFAULT NULL,
  `is_active` int(11) DEFAULT '1',
  `created_by` bigint(20) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_preventative_maintenance_asset`
--

CREATE TABLE `tbl_preventative_maintenance_asset` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `asset_id` bigint(20) NOT NULL,
  `branch_id` bigint(20) DEFAULT NULL,
  `company_id` bigint(20) NOT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT '1',
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_preventative_parts`
--

CREATE TABLE `tbl_preventative_parts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `material_id` bigint(20) NOT NULL,
  `prev_id` bigint(20) NOT NULL,
  `description` text,
  `cpu` varchar(250) DEFAULT NULL,
  `quantity` bigint(20) DEFAULT NULL,
  `branch_id` bigint(20) DEFAULT NULL,
  `company_id` bigint(20) DEFAULT NULL,
  `is_active` int(11) DEFAULT '1',
  `created_by` bigint(20) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_preventative_types`
--

CREATE TABLE `tbl_preventative_types` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type_name` varchar(250) DEFAULT NULL,
  `branch_id` bigint(20) DEFAULT NULL,
  `company_id` bigint(20) DEFAULT NULL,
  `is_active` int(11) DEFAULT '1',
  `created_by` bigint(20) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_recommendations`
--

CREATE TABLE `tbl_recommendations` (
  `id` int(11) NOT NULL,
  `recommendation_no` varchar(20) NOT NULL,
  `message` varchar(45) DEFAULT NULL,
  `risk_data_id` int(11) NOT NULL,
  `type_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_recommendation_types`
--

CREATE TABLE `tbl_recommendation_types` (
  `id` int(11) NOT NULL,
  `type_name` varchar(45) DEFAULT NULL,
  `percentage` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_regions`
--

CREATE TABLE `tbl_regions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `country` char(6) DEFAULT NULL,
  `code` char(9) DEFAULT NULL,
  `name` varchar(150) DEFAULT NULL,
  `latitude` double DEFAULT NULL,
  `longitude` double DEFAULT NULL,
  `cities` bigint(20) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_regions`
--

INSERT INTO `tbl_regions` (`id`, `country`, `code`, `name`, `latitude`, `longitude`, `cities`) VALUES
(1, 'AD', '02', 'Canillo', 0, 0, 1),
(2, 'AD', '03', 'Encamp', 0, 0, 1),
(3, 'AD', '04', 'La Massana', 0, 0, 1),
(4, 'AD', '05', 'Ordino', 0, 0, 1),
(5, 'AD', '06', 'Sant Julia de Loria', 42.465031, 1.491064, 0),
(6, 'AD', '07', 'Andorra la Vella', 0, 0, 1),
(7, 'AD', '08', 'Escaldes-Engordany', 0, 0, 1),
(8, 'AE', '01', 'Abu Dhabi', 0, 0, 5),
(9, 'AE', '02', 'Ajman', 25.41222, 55.434738, 0),
(10, 'AE', '03', 'Dubai', 0, 0, 2),
(11, 'AE', '04', 'Fujairah', 0, 0, 1),
(12, 'AE', '05', 'Ras Al Khaimah', 0, 0, 4),
(13, 'AE', '06', 'Sharjah', 0, 0, 2),
(14, 'AE', '07', 'Umm Al Quwain', 0, 0, 1),
(15, 'AF', '01', 'Badakhshan', 36.962134, 72.48409, 0),
(16, 'AF', '02', 'Badghis', 35.267697, 63.88498, 0),
(17, 'AF', '03', 'Baghlan', 36.1276, 68.6891, 0),
(18, 'AF', '05', 'Bamian', 0, 0, 1),
(19, 'AF', '06', 'Farah', 32.3754, 62.1123, 0),
(20, 'AF', '07', 'Faryab', 36.186405, 64.849624, 0),
(21, 'AF', '08', 'Ghazni', 33.5533, 68.426, 0),
(22, 'AF', '09', 'Ghowr', 34.154854, 64.905668, 0),
(23, 'AF', '10', 'Helmand', 31.791489, 64.565208, 0),
(24, 'AF', '11', 'Herat', 0, 0, 1),
(25, 'AF', '13', 'Kabol', 0, 0, 1),
(26, 'AF', '14', 'Kapisa', 34.871027, 69.729433, 0),
(27, 'AF', '15', 'Konar', 35.025774, 71.170784, 0),
(28, 'AF', '16', 'Laghman', 34.796748, 70.311782, 0),
(29, 'AF', '17', 'Lowgar', 33.943638, 69.204366, 0),
(30, 'AF', '18', 'Nangarhar', 0, 0, 1),
(31, 'AF', '19', 'Nimruz', 30.801132, 62.212044, 0),
(32, 'AF', '20', 'Oruzgan', 32.93021, 66.636429, 0),
(33, 'AF', '21', 'Paktia', 33.682466, 69.481928, 0),
(34, 'AF', '22', 'Parvan', 0, 0, 2),
(35, 'AF', '23', 'Kandahar', 0, 0, 1),
(36, 'AF', '24', 'Kondoz', 0, 0, 1),
(37, 'AF', '26', 'Takhar', 0, 0, 1),
(38, 'AF', '27', 'Vardak', 34.19443, 68.270509, 0),
(39, 'AF', '28', 'Zabol', 32.248099, 67.183598, 0),
(40, 'AF', '29', 'Paktika', 32.493147, 68.764845, 0),
(41, 'AF', '30', 'Balkh', 36.7597, 66.8897, 0),
(42, 'AF', '31', 'Jowzjan', 36.701094, 65.9608, 0),
(43, 'AF', '32', 'Samangan', 35.935035, 67.898998, 0),
(44, 'AF', '33', 'Sar-e Pol', 0, 0, 1),
(45, 'AF', '37', 'Khowst', 33.3394, 69.9148, 0),
(46, 'AF', '38', 'Nurestan', 34.93742, 70.375687, 0),
(47, 'AG', '01', 'Barbuda', 17.637099, -61.809593, 0),
(48, 'AG', '03', 'Saint George', 0, 0, 1),
(49, 'AG', '04', 'Saint John', 0, 0, 5),
(50, 'AG', '05', 'Saint Mary', 0, 0, 2),
(51, 'AG', '06', 'Saint Paul', 0, 0, 1),
(52, 'AG', '07', 'Saint Peter', 0, 0, 1),
(53, 'AG', '08', 'Saint Philip', 0, 0, 0),
(54, 'AL', '40', 'Berat', 40.707495, 19.972282, 0),
(55, 'AL', '41', 'Diber', 0, 0, 1),
(56, 'AL', '42', 'Durres', 41.330831, 19.442902, 0),
(57, 'AL', '43', 'Elbasan', 0, 0, 1),
(58, 'AL', '44', 'Fier', 0, 0, 1),
(59, 'AL', '45', 'Gjirokaster', 40.075965, 20.150654, 0),
(60, 'AL', '46', 'Korce', 40.623001, 20.785239, 0),
(61, 'AL', '47', 'Kukes', 42.065326, 20.440384, 0),
(62, 'AL', '48', 'Lezhe', 0, 0, 1),
(63, 'AL', '49', 'Shkoder', 42.066251, 19.522997, 0),
(64, 'AL', '50', 'Tirane', 0, 0, 1),
(65, 'AL', '51', 'Vlore', 40.473662, 19.488411, 0),
(66, 'AM', '01', 'Aragatsotn', 0, 0, 0),
(67, 'AM', '02', 'Ararat', 0, 0, 3),
(68, 'AM', '03', 'Armavir', 0, 0, 2),
(69, 'AM', '04', 'Geghark\'unik\'', 0, 0, 0),
(70, 'AM', '05', 'Kotayk\'', 0, 0, 6),
(71, 'AM', '06', 'Lorri', 0, 0, 3),
(72, 'AM', '07', 'Shirak', 0, 0, 1),
(73, 'AM', '08', 'Syunik\'', 0, 0, 1),
(74, 'AM', '09', 'Tavush', 0, 0, 0),
(75, 'AM', '10', 'Vayots\' Dzor', 0, 0, 0),
(76, 'AM', '11', 'Yerevan', 0, 0, 1),
(77, 'AO', '01', 'Benguela', 0, 0, 1),
(78, 'AO', '02', 'Bie', -12.4, 17.4319, 0),
(79, 'AO', '03', 'Cabinda', -5.5616, 12.185, 0),
(80, 'AO', '04', 'Cuando Cubango', -15.95284, 19.759604, 0),
(81, 'AO', '05', 'Cuanza Norte', -8.85, 14.925, 0),
(82, 'AO', '06', 'Cuanza Sul', 0, 0, 2),
(83, 'AO', '07', 'Cunene', -16.316481, 15.28353, 0),
(84, 'AO', '08', 'Huambo', -12.7677, 15.738, 0),
(85, 'AO', '09', 'Huila', 0, 0, 1),
(86, 'AO', '12', 'Malanje', 0, 0, 1),
(87, 'AO', '14', 'Moxico', -11.85, 20.033333, 0),
(88, 'AO', '15', 'Uige', -7.602778, 15.02775, 0),
(89, 'AO', '16', 'Zaire', 0, 0, 1),
(90, 'AO', '17', 'Lunda Norte', 0, 0, 1),
(91, 'AO', '18', 'Lunda Sul', -9.958333, 20.389265, 0),
(92, 'AO', '19', 'Bengo', -9.016666, 13.920003, 0),
(93, 'AO', '20', 'Luanda', -8.8135, 13.2319, 0),
(94, 'AR', '01', 'Buenos Aires', 0, 0, 205),
(95, 'AR', '02', 'Catamarca', 0, 0, 1),
(96, 'AR', '03', 'Chaco', 0, 0, 1),
(97, 'AR', '04', 'Chubut', 0, 0, 4),
(98, 'AR', '05', 'Cordoba', 0, 0, 32),
(99, 'AR', '06', 'Corrientes', 0, 0, 3),
(100, 'AR', '07', 'Distrito Federal', 0, 0, 19),
(101, 'AR', '08', 'Entre Rios', 0, 0, 8),
(102, 'AR', '09', 'Formosa', 0, 0, 1),
(103, 'AR', '10', 'Jujuy', 0, 0, 2),
(104, 'AR', '11', 'La Pampa', 0, 0, 4),
(105, 'AR', '12', 'La Rioja', 0, 0, 1),
(106, 'AR', '13', 'Mendoza', 0, 0, 5),
(107, 'AR', '14', 'Misiones', 0, 0, 3),
(108, 'AR', '15', 'Neuquen', 0, 0, 4),
(109, 'AR', '16', 'Rio Negro', 0, 0, 11),
(110, 'AR', '17', 'Salta', 0, 0, 2),
(111, 'AR', '18', 'San Juan', 0, 0, 3),
(112, 'AR', '19', 'San Luis', 0, 0, 3),
(113, 'AR', '20', 'Santa Cruz', 0, 0, 4),
(114, 'AR', '21', 'Santa Fe', 0, 0, 34),
(115, 'AR', '22', 'Santiago del Estero', 0, 0, 15),
(116, 'AR', '23', 'Tierra del Fuego', 0, 0, 2),
(117, 'AR', '24', 'Tucuman', 0, 0, 4),
(118, 'AT', '01', 'Burgenland', 0, 0, 211),
(119, 'AT', '02', 'Karnten', 0, 0, 486),
(120, 'AT', '03', 'Niederosterreich', 0, 0, 1218),
(121, 'AT', '04', 'Oberosterreich', 0, 0, 905),
(122, 'AT', '05', 'Salzburg', 0, 0, 351),
(123, 'AT', '06', 'Steiermark', 0, 0, 821),
(124, 'AT', '07', 'Tirol', 0, 0, 485),
(125, 'AT', '08', 'Vorarlberg', 0, 0, 113),
(126, 'AT', '09', 'Wien', 0, 0, 15),
(127, 'AU', '01', 'Australian Capital Territory', 0, 0, 9),
(128, 'AU', '02', 'New South Wales', 0, 0, 770),
(129, 'AU', '03', 'Northern Territory', 0, 0, 12),
(130, 'AU', '04', 'Queensland', 0, 0, 488),
(131, 'AU', '05', 'South Australia', 0, 0, 226),
(132, 'AU', '06', 'Tasmania', 0, 0, 127),
(133, 'AU', '07', 'Victoria', 0, 0, 657),
(134, 'AU', '08', 'Western Australia', 0, 0, 203),
(135, 'AZ', '01', 'Abseron', 0, 0, 1),
(136, 'AZ', '02', 'Agcabadi', 0, 0, 0),
(137, 'AZ', '03', 'Agdam', 39.9837, 46.928612, 0),
(138, 'AZ', '04', 'Agdas', 0, 0, 0),
(139, 'AZ', '05', 'Agstafa', 0, 0, 0),
(140, 'AZ', '06', 'Agsu', 0, 0, 0),
(141, 'AZ', '07', 'Ali Bayramli', 0, 0, 0),
(142, 'AZ', '08', 'Astara', 38.44, 48.8752, 0),
(143, 'AZ', '09', 'Baki', 0, 0, 4),
(144, 'AZ', '10', 'Balakan', 0, 0, 0),
(145, 'AZ', '11', 'Barda', 40.38316, 47.118622, 0),
(146, 'AZ', '12', 'Beylaqan', 0, 0, 1),
(147, 'AZ', '13', 'Bilasuvar', 0, 0, 0),
(148, 'AZ', '14', 'Cabrayil', 0, 0, 0),
(149, 'AZ', '15', 'Calilabad', 0, 0, 0),
(150, 'AZ', '16', 'Daskasan', 0, 0, 0),
(151, 'AZ', '17', 'Davaci', 0, 0, 0),
(152, 'AZ', '18', 'Fuzuli', 0, 0, 1),
(153, 'AZ', '19', 'Gadabay', 0, 0, 0),
(154, 'AZ', '20', 'Ganca', 0, 0, 0),
(155, 'AZ', '21', 'Goranboy', 0, 0, 1),
(156, 'AZ', '22', 'Goycay', 0, 0, 0),
(157, 'AZ', '23', 'Haciqabul', 0, 0, 0),
(158, 'AZ', '24', 'Imisli', 0, 0, 0),
(159, 'AZ', '25', 'Ismayilli', 0, 0, 1),
(160, 'AZ', '26', 'Kalbacar', 0, 0, 0),
(161, 'AZ', '27', 'Kurdamir', 0, 0, 0),
(162, 'AZ', '28', 'Lacin', 0, 0, 0),
(163, 'AZ', '29', 'Lankaran', 0, 0, 0),
(164, 'AZ', '31', 'Lerik', 38.76685, 48.405731, 0),
(165, 'AZ', '32', 'Masalli', 0, 0, 0),
(166, 'AZ', '33', 'Mingacevir', 0, 0, 0),
(167, 'AZ', '34', 'Naftalan', 0, 0, 0),
(168, 'AZ', '35', 'Naxcivan', 0, 0, 1),
(169, 'AZ', '36', 'Neftcala', 0, 0, 0),
(170, 'AZ', '37', 'Oguz', 0, 0, 0),
(171, 'AZ', '38', 'Qabala', 0, 0, 0),
(172, 'AZ', '39', 'Qax', 0, 0, 0),
(173, 'AZ', '40', 'Qazax', 0, 0, 0),
(174, 'AZ', '41', 'Qobustan', 0, 0, 0),
(175, 'AZ', '42', 'Quba', 0, 0, 0),
(176, 'AZ', '43', 'Qubadli', 0, 0, 0),
(177, 'AZ', '44', 'Qusar', 0, 0, 0),
(178, 'AZ', '45', 'Saatli', 0, 0, 0),
(179, 'AZ', '46', 'Sabirabad', 40.005211, 48.471931, 0),
(180, 'AZ', '47', 'Saki', 0, 0, 0),
(181, 'AZ', '49', 'Salyan', 0, 0, 1),
(182, 'AZ', '50', 'Samaxi', 0, 0, 0),
(183, 'AZ', '51', 'Samkir', 0, 0, 0),
(184, 'AZ', '52', 'Samux', 0, 0, 0),
(185, 'AZ', '53', 'Siyazan', 0, 0, 0),
(186, 'AZ', '54', 'Sumqayit', 0, 0, 1),
(187, 'AZ', '55', 'Susa', 0, 0, 0),
(188, 'AZ', '57', 'Tartar', 0, 0, 0),
(189, 'AZ', '58', 'Tovuz', 0, 0, 0),
(190, 'AZ', '59', 'Ucar', 0, 0, 0),
(191, 'AZ', '60', 'Xacmaz', 0, 0, 0),
(192, 'AZ', '61', 'Xankandi', 39.812351, 46.751671, 0),
(193, 'AZ', '62', 'Xanlar', 0, 0, 0),
(194, 'AZ', '63', 'Xizi', 0, 0, 0),
(195, 'AZ', '64', 'Xocali', 0, 0, 0),
(196, 'AZ', '65', 'Xocavand', 0, 0, 0),
(197, 'AZ', '66', 'Yardimli', 0, 0, 0),
(198, 'AZ', '67', 'Yevlax', 0, 0, 0),
(199, 'AZ', '69', 'Zangilan', 0, 0, 0),
(200, 'AZ', '70', 'Zaqatala', 0, 0, 0),
(201, 'AZ', '71', 'Zardab', 0, 0, 0),
(202, 'BA', '01', 'Federation of Bosnia and Herzegovina', 0, 0, 62),
(203, 'BA', '02', 'Republika Srpska', 0, 0, 28),
(204, 'BB', '01', 'Christ Church', 0, 0, 3),
(205, 'BB', '02', 'Saint Andrew', 0, 0, 0),
(206, 'BB', '03', 'Saint George', 0, 0, 2),
(207, 'BB', '04', 'Saint James', 0, 0, 4),
(208, 'BB', '05', 'Saint John', 0, 0, 1),
(209, 'BB', '06', 'Saint Joseph', 13.20232, -59.52676, 0),
(210, 'BB', '07', 'Saint Lucy', 0, 0, 3),
(211, 'BB', '08', 'Saint Michael', 0, 0, 6),
(212, 'BB', '09', 'Saint Peter', 0, 0, 1),
(213, 'BB', '10', 'Saint Philip', 0, 0, 1),
(214, 'BB', '11', 'Saint Thomas', 0, 0, 1),
(215, 'BD', '01', 'Barisal', 22.400712, 90.42992, 0),
(216, 'BD', '04', 'Bandarban', 22.1619, 92.1803, 0),
(217, 'BD', '05', 'Comilla', 23.4358, 91.189, 0),
(218, 'BD', '12', 'Mymensingh', 24.7557, 90.3981, 0),
(219, 'BD', '13', 'Noakhali', 22.81008, 91.099747, 0),
(220, 'BD', '15', 'Patuakhali', 22.355881, 90.242104, 0),
(221, 'BD', '22', 'Bagerhat', 22.65707, 89.80368, 0),
(222, 'BD', '23', 'Bhola', 22.679489, 90.643913, 0),
(223, 'BD', '24', 'Bogra', 24.8444, 89.3784, 0),
(224, 'BD', '25', 'Barguna', 0, 0, 0),
(225, 'BD', '26', 'Brahmanbaria', 0, 0, 0),
(226, 'BD', '27', 'Chandpur', 23.222309, 90.650513, 0),
(227, 'BD', '28', 'Chapai Nawabganj', 0, 0, 0),
(228, 'BD', '29', 'Chattagram', 0, 0, 0),
(229, 'BD', '30', 'Chuadanga', 23.640779, 88.685478, 0),
(230, 'BD', '31', 'Cox\'s Bazar', 0, 0, 0),
(231, 'BD', '32', 'Dhaka', 23.70548, 90.411295, 0),
(232, 'BD', '33', 'Dinajpur', 25.6255, 88.6404, 0),
(233, 'BD', '34', 'Faridpur', 23.440359, 89.512733, 0),
(234, 'BD', '35', 'Feni', 22.991859, 91.389702, 0),
(235, 'BD', '36', 'Gaibandha', 0, 0, 0),
(236, 'BD', '37', 'Gazipur', 22.43, 89.18, 0),
(237, 'BD', '38', 'Gopalganj', 0, 0, 0),
(238, 'BD', '39', 'Habiganj', 24.379789, 91.409088, 0),
(239, 'BD', '40', 'Jaipurhat', 0, 0, 0),
(240, 'BD', '41', 'Jamalpur', 24.9162, 89.9426, 0),
(241, 'BD', '42', 'Jessore', 23.1833, 89.1666, 0),
(242, 'BD', '43', 'Jhalakati', 0, 0, 0),
(243, 'BD', '44', 'Jhenaidah', 0, 0, 0),
(244, 'BD', '45', 'Khagrachari', 0, 0, 0),
(245, 'BD', '46', 'Khulna', 22.912098, 89.279507, 0),
(246, 'BD', '47', 'Kishorganj', 24.4258, 90.7846, 0),
(247, 'BD', '48', 'Kurigram', 0, 0, 0),
(248, 'BD', '49', 'Kushtia', 23.9345, 88.9936, 0),
(249, 'BD', '50', 'Laksmipur', 0, 0, 0),
(250, 'BD', '51', 'Lalmonirhat', 0, 0, 0),
(251, 'BD', '52', 'Madaripur', 23.1654, 90.1986, 0),
(252, 'BD', '53', 'Magura', 26.28, 88.51, 0),
(253, 'BD', '54', 'Manikganj', 0, 0, 0),
(254, 'BD', '55', 'Meherpur', 23.7712, 88.6425, 0),
(255, 'BD', '56', 'Moulavibazar', 0, 0, 0),
(256, 'BD', '57', 'Munshiganj', 23.549971, 90.525223, 0),
(257, 'BD', '58', 'Naogaon', 24.80452, 88.935257, 0),
(258, 'BD', '59', 'Narail', 23.16, 89.5, 0),
(259, 'BD', '60', 'Narayanganj', 23.619, 90.4979, 0),
(260, 'BD', '61', 'Narsingdi', 0, 0, 0),
(261, 'BD', '62', 'Nator', 24.4145, 88.9915, 0),
(262, 'BD', '63', 'Netrakona', 24.886101, 90.729134, 0),
(263, 'BD', '64', 'Nilphamari', 25.93454, 88.849564, 0),
(264, 'BD', '65', 'Pabna', 24.0048, 89.2579, 0),
(265, 'BD', '66', 'Panchagar', 0, 0, 0),
(266, 'BD', '67', 'Parbattya Chattagram', 0, 0, 0),
(267, 'BD', '68', 'Pirojpur', 22.573521, 89.987373, 0),
(268, 'BD', '69', 'Rajbari', 23.754089, 89.65197, 0),
(269, 'BD', '70', 'Rajshahi', 25.228028, 88.954334, 0),
(270, 'BD', '71', 'Rangpur', 25.75, 89.2333, 0),
(271, 'BD', '72', 'Satkhira', 22.704399, 89.082939, 0),
(272, 'BD', '73', 'Shariyatpur', 0, 0, 0),
(273, 'BD', '74', 'Sherpur', 25.0164, 90.0143, 0),
(274, 'BD', '75', 'Sirajganj', 24.455391, 89.699951, 0),
(275, 'BD', '76', 'Sunamganj', 25.04562, 91.387154, 0),
(276, 'BD', '77', 'Sylhet', 24.589978, 91.726587, 0),
(277, 'BD', '78', 'Tangail', 24.2447, 89.9243, 0),
(278, 'BD', '79', 'Thakurgaon', 26.016667, 88.466667, 0),
(279, 'BE', '01', 'Antwerpen', 0, 0, 137),
(280, 'BE', '02', 'Brabant', 0, 0, 190),
(281, 'BE', '03', 'Hainaut', 0, 0, 124),
(282, 'BE', '04', 'Liege', 0, 0, 118),
(283, 'BE', '05', 'Limburg', 0, 0, 72),
(284, 'BE', '06', 'Luxembourg', 0, 0, 51),
(285, 'BE', '07', 'Namur', 0, 0, 67),
(286, 'BE', '08', 'Oost-Vlaanderen', 0, 0, 148),
(287, 'BE', '09', 'West-Vlaanderen', 0, 0, 122),
(288, 'BE', '10', 'Brabant Wallon', 50.666348, 4.555613, 0),
(289, 'BE', '11', 'Brussels Hoofdstedelijk Gewest', 0, 0, 2),
(290, 'BE', '12', 'Vlaams-Brabant', 0, 0, 3),
(291, 'BF', '15', 'Bam', 0, 0, 0),
(292, 'BF', '19', 'Boulkiemde', 0, 0, 0),
(293, 'BF', '20', 'Ganzourgou', 0, 0, 0),
(294, 'BF', '21', 'Gnagna', 0, 0, 0),
(295, 'BF', '28', 'Kouritenga', 0, 0, 0),
(296, 'BF', '33', 'Oudalan', 0, 0, 0),
(297, 'BF', '34', 'Passore', 0, 0, 0),
(298, 'BF', '36', 'Sanguie', 0, 0, 0),
(299, 'BF', '40', 'Soum', 0, 0, 0),
(300, 'BF', '42', 'Tapoa', 12.484337, 2.399365, 0),
(301, 'BF', '44', 'Zoundweogo', 0, 0, 0),
(302, 'BF', '45', 'Bale', 13.7, -0.616667, 0),
(303, 'BF', '46', 'Banwa', 0, 0, 0),
(304, 'BF', '47', 'Bazega', 0, 0, 0),
(305, 'BF', '48', 'Bougouriba', 0, 0, 0),
(306, 'BF', '49', 'Boulgou', 12.51166, 0.832415, 0),
(307, 'BF', '50', 'Gourma', 12.933333, 0.733333, 0),
(308, 'BF', '51', 'Houet', 0, 0, 0),
(309, 'BF', '52', 'Ioba', 0, 0, 0),
(310, 'BF', '53', 'Kadiogo', 0, 0, 1),
(311, 'BF', '54', 'Kenedougou', 0, 0, 0),
(312, 'BF', '55', 'Komoe', 0, 0, 0),
(313, 'BF', '56', 'Komondjari', 0, 0, 0),
(314, 'BF', '57', 'Kompienga', 0, 0, 0),
(315, 'BF', '58', 'Kossi', 0, 0, 0),
(316, 'BF', '59', 'Koulpelogo', 0, 0, 0),
(317, 'BF', '60', 'Kourweogo', 0, 0, 0),
(318, 'BF', '61', 'Leraba', 10.06782, -4.799133, 0),
(319, 'BF', '62', 'Loroum', 0, 0, 0),
(320, 'BF', '63', 'Mouhoun', 0, 0, 0),
(321, 'BF', '64', 'Namentenga', 0, 0, 0),
(322, 'BF', '65', 'Naouri', 12.133333, 0.583333, 0),
(323, 'BF', '66', 'Nayala', 0, 0, 0),
(324, 'BF', '67', 'Noumbiel', 0, 0, 0),
(325, 'BF', '68', 'Oubritenga', 0, 0, 0),
(326, 'BF', '69', 'Poni', 0, 0, 0),
(327, 'BF', '70', 'Sanmatenga', 0, 0, 0),
(328, 'BF', '71', 'Seno', 14.366667, -0.566667, 0),
(329, 'BF', '72', 'Sissili', 0, 0, 0),
(330, 'BF', '73', 'Sourou', 11.416667, -1.866667, 0),
(331, 'BF', '74', 'Tuy', 0, 0, 0),
(332, 'BF', '75', 'Yagha', 10.466667, -2.783333, 0),
(333, 'BF', '76', 'Yatenga', 0, 0, 0),
(334, 'BF', '77', 'Ziro', 0, 0, 0),
(335, 'BF', '78', 'Zondoma', 0, 0, 0),
(336, 'BG', '33', 'Mikhaylovgrad', 0, 0, 2),
(337, 'BG', '38', 'Blagoevgrad', 0, 0, 11),
(338, 'BG', '39', 'Burgas', 0, 0, 31),
(339, 'BG', '40', 'Dobrich', 0, 0, 6),
(340, 'BG', '41', 'Gabrovo', 0, 0, 3),
(341, 'BG', '42', 'Grad Sofiya', 0, 0, 9),
(342, 'BG', '43', 'Khaskovo', 0, 0, 15),
(343, 'BG', '44', 'Kurdzhali', 0, 0, 4),
(344, 'BG', '45', 'Kyustendil', 0, 0, 6),
(345, 'BG', '46', 'Lovech', 0, 0, 18),
(346, 'BG', '47', 'Montana', 0, 0, 4),
(347, 'BG', '48', 'Pazardzhik', 0, 0, 13),
(348, 'BG', '49', 'Pernik', 0, 0, 3),
(349, 'BG', '50', 'Pleven', 0, 0, 5),
(350, 'BG', '51', 'Plovdiv', 0, 0, 23),
(351, 'BG', '52', 'Razgrad', 0, 0, 13),
(352, 'BG', '53', 'Ruse', 0, 0, 5),
(353, 'BG', '54', 'Shumen', 0, 0, 5),
(354, 'BG', '55', 'Silistra', 0, 0, 3),
(355, 'BG', '56', 'Sliven', 0, 0, 2),
(356, 'BG', '57', 'Smolyan', 0, 0, 7),
(357, 'BG', '58', 'Sofiya', 0, 0, 23),
(358, 'BG', '59', 'Stara Zagora', 0, 0, 6),
(359, 'BG', '60', 'Turgovishte', 0, 0, 3),
(360, 'BG', '61', 'Varna', 0, 0, 26),
(361, 'BG', '62', 'Veliko Turnovo', 0, 0, 8),
(362, 'BG', '63', 'Vidin', 0, 0, 1),
(363, 'BG', '64', 'Vratsa', 0, 0, 5),
(364, 'BG', '65', 'Yambol', 0, 0, 3),
(365, 'BH', '01', 'Al Hadd', 0, 0, 0),
(366, 'BH', '02', 'Al Manamah', 0, 0, 1),
(367, 'BH', '03', 'Al Muharraq', 0, 0, 1),
(368, 'BH', '05', 'Jidd Hafs', 0, 0, 1),
(369, 'BH', '06', 'Sitrah', 26.161012, 50.625151, 0),
(370, 'BH', '08', 'Al Mintaqah al Gharbiyah', 26.04178, 50.497529, 0),
(371, 'BH', '09', 'Mintaqat Juzur Hawar', 0, 0, 0),
(372, 'BH', '10', 'Al Mintaqah ash Shamaliyah', 0, 0, 1),
(373, 'BH', '11', 'Al Mintaqah al Wusta', 26.178315, 50.532508, 0),
(374, 'BH', '12', 'Madinat', 0, 0, 1),
(375, 'BH', '13', 'Ar Rifa', 26.1298, 50.537, 0),
(376, 'BH', '14', 'Madinat Hamad', 26.100456, 50.512521, 0),
(377, 'BI', '02', 'Bujumbura', 0, 0, 1),
(378, 'BI', '09', 'Bubanza', -3.133502, 29.386686, 0),
(379, 'BI', '10', 'Bururi', -3.882269, 29.641667, 0),
(380, 'BI', '11', 'Cankuzo', -3.15, 30.603676, 0),
(381, 'BI', '12', 'Cibitoke', -2.833333, 29.225, 0),
(382, 'BI', '13', 'Gitega', -3.441667, 29.916666, 0),
(383, 'BI', '14', 'Karuzi', -3.125, 30.125, 0),
(384, 'BI', '15', 'Kayanza', -3.025, 29.641667, 0),
(385, 'BI', '16', 'Kirundo', -2.55, 30.158333, 0),
(386, 'BI', '17', 'Makamba', -4.191667, 29.825, 0),
(387, 'BI', '18', 'Muyinga', -2.708333, 30.3, 0),
(388, 'BI', '19', 'Ngozi', -2.9, 29.875, 0),
(389, 'BI', '20', 'Rutana', -3.825, 30.166666, 0),
(390, 'BI', '21', 'Ruyigi', -3.455418, 30.358333, 0),
(391, 'BI', '22', 'Muramvya', -3.383333, 29.691666, 0),
(392, 'BI', '23', 'Mwaro', 0, 0, 0),
(393, 'BJ', '01', 'Atakora', 0, 0, 0),
(394, 'BJ', '02', 'Atlantique', 0, 0, 0),
(395, 'BJ', '03', 'Borgou', 0, 0, 0),
(396, 'BJ', '04', 'Mono', 0, 0, 0),
(397, 'BJ', '05', 'Oueme', 0, 0, 1),
(398, 'BJ', '06', 'Zou', 0, 0, 0),
(399, 'BM', '01', 'Devonshire', 33.73958, -116.309267, 0),
(400, 'BM', '02', 'Hamilton', 32.329337, -64.706, 0),
(401, 'BM', '04', 'Paget', 0, 0, 0),
(402, 'BM', '05', 'Pembroke', 37.289949, -77.432914, 0),
(403, 'BM', '06', 'Saint George', 0, 0, 1),
(404, 'BM', '07', 'Saint George\'s', 0, 0, 0),
(405, 'BM', '08', 'Sandys', 0, 0, 0),
(406, 'BM', '09', 'Smiths', 0, 0, 0),
(407, 'BM', '10', 'Southampton', 37.237342, -77.376659, 0),
(408, 'BM', '11', 'Warwick', 37.45889, -77.42306, 0),
(409, 'BN', '07', 'Alibori', 0, 0, 0),
(410, 'BN', '08', 'Belait', 4.449758, 114.318703, 0),
(411, 'BN', '09', 'Brunei and Muara', 0, 0, 0),
(412, 'BN', '10', 'Temburong', 0, 0, 0),
(413, 'BN', '11', 'Collines', 0, 0, 0),
(414, 'BN', '12', 'Kouffo', 0, 0, 0),
(415, 'BN', '13', 'Donga', 0, 0, 0),
(416, 'BN', '14', 'Littoral', 0, 0, 0),
(417, 'BN', '15', 'Tutong', 4.805784, 114.646698, 0),
(418, 'BN', '16', 'Oueme', 0, 0, 0),
(419, 'BN', '17', 'Plateau', 0, 0, 0),
(420, 'BN', '18', 'Zou', 0, 0, 0),
(421, 'BO', '01', 'Chuquisaca', 0, 0, 1),
(422, 'BO', '02', 'Cochabamba', 0, 0, 2),
(423, 'BO', '03', 'El Beni', 0, 0, 3),
(424, 'BO', '04', 'La Paz', 0, 0, 1),
(425, 'BO', '05', 'Oruro', 0, 0, 1),
(426, 'BO', '06', 'Pando', -11.088623, -67.423198, 0),
(427, 'BO', '07', 'Potosi', 0, 0, 2),
(428, 'BO', '08', 'Santa Cruz', 0, 0, 2),
(429, 'BO', '09', 'Tarija', 0, 0, 2),
(430, 'BR', '01', 'Acre', 0, 0, 4),
(431, 'BR', '02', 'Alagoas', 0, 0, 18),
(432, 'BR', '03', 'Amapa', 0, 0, 1),
(433, 'BR', '04', 'Amazonas', 0, 0, 9),
(434, 'BR', '05', 'Bahia', 0, 0, 65),
(435, 'BR', '06', 'Ceara', 0, 0, 23),
(436, 'BR', '07', 'Distrito Federal', 0, 0, 2),
(437, 'BR', '08', 'Espirito Santo', 0, 0, 25),
(438, 'BR', '11', 'Mato Grosso do Sul', 0, 0, 23),
(439, 'BR', '13', 'Maranhao', 0, 0, 11),
(440, 'BR', '14', 'Mato Grosso', 0, 0, 4),
(441, 'BR', '15', 'Minas Gerais', 0, 0, 121),
(442, 'BR', '16', 'Para', 0, 0, 14),
(443, 'BR', '17', 'Paraiba', 0, 0, 19),
(444, 'BR', '18', 'Parana', 0, 0, 63),
(445, 'BR', '20', 'Piaui', 0, 0, 9),
(446, 'BR', '21', 'Rio de Janeiro', 0, 0, 51),
(447, 'BR', '22', 'Rio Grande do Norte', 0, 0, 19),
(448, 'BR', '23', 'Rio Grande do Sul', 0, 0, 75),
(449, 'BR', '24', 'Rondonia', 0, 0, 8),
(450, 'BR', '25', 'Roraima', 0, 0, 4),
(451, 'BR', '26', 'Santa Catarina', 0, 0, 49),
(452, 'BR', '27', 'Sao Paulo', 0, 0, 231),
(453, 'BR', '28', 'Sergipe', 0, 0, 9),
(454, 'BR', '29', 'Goias', 0, 0, 16),
(455, 'BR', '30', 'Pernambuco', 0, 0, 36),
(456, 'BR', '31', 'Tocantins', 0, 0, 6),
(457, 'BS', '05', 'Bimini', 25.700378, -79.26474, 0),
(458, 'BS', '06', 'Cat Island', 24.403209, -75.525948, 0),
(459, 'BS', '10', 'Exuma', 23.56913, -75.845447, 0),
(460, 'BS', '13', 'Inagua', 20.977222, -73.665276, 0),
(461, 'BS', '15', 'Long Island', 23.271651, -75.085708, 0),
(462, 'BS', '16', 'Mayaguana', 22.379528, -73.0135, 0),
(463, 'BS', '18', 'Ragged Island', 22.22731, -75.730793, 0),
(464, 'BS', '22', 'Harbour Island', 0, 0, 1),
(465, 'BS', '23', 'New Providence', 0, 0, 1),
(466, 'BS', '24', 'Acklins and Crooked Islands', 0, 0, 0),
(467, 'BS', '25', 'Freeport', 0, 0, 1),
(468, 'BS', '26', 'Fresh Creek', 0, 0, 0),
(469, 'BS', '27', 'Governor\'s Harbour', 0, 0, 1),
(470, 'BS', '28', 'Green Turtle Cay', 0, 0, 0),
(471, 'BS', '29', 'High Rock', 0, 0, 0),
(472, 'BS', '30', 'Kemps Bay', 23.866667, -77.516667, 0),
(473, 'BS', '31', 'Marsh Harbour', 0, 0, 1),
(474, 'BS', '32', 'Nichollstown and Berry Islands', 0, 0, 0),
(475, 'BS', '33', 'Rock Sound', 24.96731, -76.183823, 0),
(476, 'BS', '34', 'Sandy Point', 26.004639, -77.395483, 0),
(477, 'BS', '35', 'San Salvador and Rum Cay', 0, 0, 0),
(478, 'BT', '05', 'Bumthang', 27.682259, 90.630643, 0),
(479, 'BT', '06', 'Chhukha', 0, 0, 0),
(480, 'BT', '07', 'Chirang', 0, 0, 0),
(481, 'BT', '08', 'Daga', 0, 0, 0),
(482, 'BT', '09', 'Geylegphug', 0, 0, 0),
(483, 'BT', '10', 'Ha', 0, 0, 0),
(484, 'BT', '11', 'Lhuntshi', 0, 0, 0),
(485, 'BT', '12', 'Mongar', 27.129152, 91.167156, 0),
(486, 'BT', '13', 'Paro', 27.513568, 89.357058, 0),
(487, 'BT', '14', 'Pemagatsel', 0, 0, 0),
(488, 'BT', '15', 'Punakha', 27.680425, 89.895182, 0),
(489, 'BT', '16', 'Samchi', 0, 0, 0),
(490, 'BT', '17', 'Samdrup', 26.81, 91.56, 0),
(491, 'BT', '18', 'Shemgang', 0, 0, 0),
(492, 'BT', '19', 'Tashigang', 27.379431, 91.879189, 0),
(493, 'BT', '20', 'Thimphu', 0, 0, 1),
(494, 'BT', '21', 'Tongsa', 27.499969, 90.503357, 0),
(495, 'BT', '22', 'Wangdi Phodrang', 27.53841, 89.892951, 0),
(496, 'BW', '01', 'Central', -21.483334, 26.422628, 0),
(497, 'BW', '02', 'Chobe', -18.308334, 24.883333, 0),
(498, 'BW', '03', 'Ghanzi', -21.703975, 21.64618, 0),
(499, 'BW', '04', 'Kgalagadi', -25.075, 22.075, 0),
(500, 'BW', '05', 'Kgatleng', -24.216667, 26.35, 0),
(501, 'BW', '06', 'Kweneng', -24.030063, 24.5, 0),
(502, 'BW', '07', 'Ngamiland', -19.244866, 23.1, 0),
(503, 'BW', '08', 'North-East', 0, 0, 2),
(504, 'BW', '09', 'South-East', 0, 0, 2),
(505, 'BW', '10', 'Southern', -25.008333, 24.396088, 0),
(506, 'BY', '01', 'Brestskaya Voblasts\'', 0, 0, 7),
(507, 'BY', '02', 'Homyel\'skaya Voblasts\'', 0, 0, 9),
(508, 'BY', '03', 'Hrodzyenskaya Voblasts\'', 0, 0, 10),
(509, 'BY', '04', 'Minsk', 0, 0, 10),
(510, 'BY', '05', 'Minskaya Voblasts\'', 0, 0, 0),
(511, 'BY', '06', 'Mahilyowskaya Voblasts\'', 0, 0, 12),
(512, 'BY', '07', 'Vitsyebskaya Voblasts\'', 0, 0, 15),
(513, 'BZ', '01', 'Belize', 0, 0, 6),
(514, 'BZ', '02', 'Cayo', 0, 0, 6),
(515, 'BZ', '03', 'Corozal', 0, 0, 2),
(516, 'BZ', '04', 'Orange Walk', 17.779726, -88.718411, 0),
(517, 'BZ', '05', 'Stann Creek', 0, 0, 1),
(518, 'BZ', '06', 'Toledo', 0, 0, 1),
(519, 'CA', '01', 'Alberta', 54.500981, -114.99992, 0),
(520, 'CA', '02', 'British Columbia', 54.115749, -126.55071, 0),
(521, 'CA', '03', 'Manitoba', 54.503629, -95.494255, 0),
(522, 'CA', '04', 'New Brunswick', 46.285875, -66.256015, 0),
(523, 'CA', '05', 'Newfoundland and Labrador', 53.337125, -60.21835, 0),
(524, 'CA', '07', 'Nova Scotia', 45.543425, -61.63183, 0),
(525, 'CA', '08', 'Ontario', 49.26788, -84.74493, 0),
(526, 'CA', '09', 'Prince Edward Island', 46.634645, -63.05213, 0),
(527, 'CA', '10', 'Quebec', 46.81274, -71.21935, 0),
(528, 'CA', '11', 'Saskatchewan', 54.50071, -105.68423, 0),
(529, 'CA', '12', 'Yukon Territory', 64.8369, -132.39536, 0),
(530, 'CA', '13', 'Northwest Territories', 69.36212, -119.240445, 0),
(531, 'CA', '14', 'Nunavut', 67.136865, -91.092025, 0),
(532, 'CD', '01', 'Bandundu', -3.307778, 17.383333, 0),
(533, 'CD', '02', 'Equateur', 1.358334, 20.290649, 0),
(534, 'CD', '03', 'Kasai-Occidental', -5.149982, 21.736541, 0),
(535, 'CD', '04', 'Kasai-Oriental', -4.856722, 24.058333, 0),
(536, 'CD', '05', 'Katanga', -9.179045, 26.191997, 0),
(537, 'CD', '06', 'Kinshasa', -4.491666, 15.828001, 0),
(538, 'CD', '07', 'Kivu', 0, 0, 0),
(539, 'CD', '08', 'Bas-Congo', -5.183388, 14.258333, 0),
(540, 'CD', '09', 'Orientale', 1.505395, 26.888719, 0),
(541, 'CD', '10', 'Maniema', -1.167485, 28.61903, 0),
(542, 'CD', '11', 'Nord-Kivu', 0, 0, 0),
(543, 'CD', '12', 'Sud-Kivu', 0, 0, 0),
(544, 'CF', '01', 'Bamingui-Bangoran', 8.202459, 20.274489, 0),
(545, 'CF', '02', 'Basse-Kotto', 4.924944, 21.35, 0),
(546, 'CF', '03', 'Haute-Kotto', 7.03974, 22.515856, 0),
(547, 'CF', '04', 'Mambere-Kadei', 4.522075, 15.908333, 0),
(548, 'CF', '05', 'Haut-Mbomou', 6.072541, 26.041666, 0),
(549, 'CF', '06', 'Kemo', 5.711471, 19.475, 0),
(550, 'CF', '07', 'Lobaye', 4.251303, 17.727095, 0),
(551, 'CF', '08', 'Mbomou', 5.247212, 23.4, 0),
(552, 'CF', '09', 'Nana-Mambere', 5.773557, 15.54668, 0),
(553, 'CF', '11', 'Ouaka', 6.143173, 20.76937, 0),
(554, 'CF', '12', 'Ouham', 7.067532, 17.861185, 0),
(555, 'CF', '13', 'Ouham-Pende', 6.683333, 16.108816, 0),
(556, 'CF', '14', 'Vakaga', 9.643518, 22.147255, 0),
(557, 'CF', '15', 'Nana-Grebizi', 6.973257, 19.425, 0),
(558, 'CF', '16', 'Sangha-Mbaere', 3.300875, 16.3, 0),
(559, 'CF', '17', 'Ombella-Mpoko', 4.916667, 17.833333, 0),
(560, 'CF', '18', 'Bangui', 0, 0, 1),
(561, 'CG', '01', 'Bouenza', -4.15, 13.516667, 0),
(562, 'CG', '03', 'Cuvette', -0.466667, 15.682525, 0),
(563, 'CG', '04', 'Kouilou', 0, 0, 1),
(564, 'CG', '05', 'Lekoumou', -3.171705, 13.475, 0),
(565, 'CG', '06', 'Likouala', 1.401412, 17.74006, 0),
(566, 'CG', '07', 'Niari', -3.384698, 12.568701, 0),
(567, 'CG', '08', 'Plateaux', -1.941667, 15.441667, 0),
(568, 'CG', '10', 'Sangha', 0, 0, 1),
(569, 'CG', '11', 'Pool', -3.907402, 15.082881, 0),
(570, 'CG', '12', 'Brazzaville', 0, 0, 1),
(571, 'CH', '01', 'Aargau', 0, 0, 168),
(572, 'CH', '02', 'Ausser-Rhoden', 0, 0, 20),
(573, 'CH', '03', 'Basel-Landschaft', 0, 0, 15),
(574, 'CH', '04', 'Basel-Stadt', 0, 0, 12),
(575, 'CH', '05', 'Bern', 0, 0, 103),
(576, 'CH', '06', 'Fribourg', 0, 0, 33),
(577, 'CH', '07', 'Geneve', 0, 0, 46),
(578, 'CH', '08', 'Glarus', 0, 0, 14),
(579, 'CH', '09', 'Graubunden', 0, 0, 71),
(580, 'CH', '10', 'Inner-Rhoden', 0, 0, 14),
(581, 'CH', '11', 'Luzern', 0, 0, 32),
(582, 'CH', '12', 'Neuchatel', 0, 0, 26),
(583, 'CH', '13', 'Nidwalden', 0, 0, 7),
(584, 'CH', '14', 'Obwalden', 0, 0, 21),
(585, 'CH', '15', 'Sankt Gallen', 0, 0, 39),
(586, 'CH', '16', 'Schaffhausen', 0, 0, 21),
(587, 'CH', '17', 'Schwyz', 0, 0, 11),
(588, 'CH', '18', 'Solothurn', 0, 0, 22),
(589, 'CH', '19', 'Thurgau', 0, 0, 24),
(590, 'CH', '20', 'Ticino', 0, 0, 76),
(591, 'CH', '21', 'Uri', 0, 0, 11),
(592, 'CH', '22', 'Valais', 0, 0, 48),
(593, 'CH', '23', 'Vaud', 0, 0, 77),
(594, 'CH', '24', 'Zug', 0, 0, 18),
(595, 'CH', '25', 'Zurich', 0, 0, 86),
(596, 'CH', '26', 'Jura', 0, 0, 12),
(597, 'CI', '01', 'Abengourou', 6.72909, -3.491421, 0),
(598, 'CI', '03', 'Dabakala', 8.3611, -4.424, 0),
(599, 'CI', '05', 'Adzope', 6.096143, -3.868163, 0),
(600, 'CI', '06', 'Agboville', 5.927773, -4.222393, 0),
(601, 'CI', '07', 'Biankouma', 7.7344, -7.6128, 0),
(602, 'CI', '11', 'Bouna', 9.269945, -2.994693, 0),
(603, 'CI', '12', 'Boundiali', 9.52211, -6.488623, 0),
(604, 'CI', '14', 'Danane', 7.260198, -8.150842, 0),
(605, 'CI', '16', 'Divo', 5.843325, -5.36144, 0),
(606, 'CI', '17', 'Ferkessedougou', 9.594077, -5.202254, 0),
(607, 'CI', '18', 'Gagnoa', 6.137461, -5.908112, 0),
(608, 'CI', '20', 'Katiola', 9.65, -7.616667, 0),
(609, 'CI', '21', 'Korhogo', 9.449102, -5.642024, 0),
(610, 'CI', '23', 'Odienne', 9.509565, -7.574298, 0),
(611, 'CI', '25', 'Seguela', 7.966667, -6.666667, 0),
(612, 'CI', '26', 'Touba', 9.723194, -7.395185, 0),
(613, 'CI', '27', 'Bongouanou', 6.65, -4.2, 0),
(614, 'CI', '28', 'Issia', 6.4801, -6.5866, 0),
(615, 'CI', '29', 'Lakota', 5.85, -5.683333, 0),
(616, 'CI', '30', 'Mankono', 8.45, -6.866667, 0),
(617, 'CI', '31', 'Oume', 6.3779, -5.4256, 0),
(618, 'CI', '32', 'Soubre', 5.781, -6.6032, 0),
(619, 'CI', '33', 'Tingrela', 10.4696, -6.4084, 0),
(620, 'CI', '34', 'Zuenoula', 7.433333, -6.05, 0),
(621, 'CI', '36', 'Bangolo', 7.033248, -7.144255, 0),
(622, 'CI', '37', 'Beoumi', 7.775733, -5.391067, 0),
(623, 'CI', '38', 'Bondoukou', 7.083333, -5.05, 0),
(624, 'CI', '39', 'Bouafle', 6.98166, -5.750575, 0),
(625, 'CI', '40', 'Bouake', 9.616667, -7.416667, 0),
(626, 'CI', '41', 'Daloa', 6.87775, -6.4472, 0),
(627, 'CI', '42', 'Daoukro', 7.0591, -3.9631, 0),
(628, 'CI', '44', 'Duekoue', 6.7426, -7.3372, 0),
(629, 'CI', '45', 'Grand-Lahou', 5.182858, -4.999068, 0),
(630, 'CI', '47', 'Man', 7.404119, -7.550929, 0),
(631, 'CI', '48', 'Mbahiakro', 7.4557, -4.3469, 0),
(632, 'CI', '49', 'Sakassou', 6.542578, -4.827003, 0),
(633, 'CI', '50', 'San Pedro', 4.745555, -6.661667, 0),
(634, 'CI', '51', 'Sassandra', 0, 0, 1),
(635, 'CI', '52', 'Sinfra', 6.620751, -5.914704, 0),
(636, 'CI', '53', 'Tabou', 4.4173, -7.3593, 0),
(637, 'CI', '54', 'Tanda', 7.8018, -3.1678, 0),
(638, 'CI', '55', 'Tiassale', 5.9327, -4.9867, 0),
(639, 'CI', '56', 'Toumodi', 7.433333, -5.616667, 0),
(640, 'CI', '57', 'Vavoua', 7.3713, -6.4837, 0),
(641, 'CI', '61', 'Abidjan', 0, 0, 3),
(642, 'CI', '62', 'Aboisso', 5.466667, -3.2, 0),
(643, 'CI', '63', 'Adiake', 5.2908, -3.3267, 0),
(644, 'CI', '64', 'Alepe', 0, 0, 0),
(645, 'CI', '65', 'Bocanda', 7.0481, -4.5284, 0),
(646, 'CI', '66', 'Dabou', 5.313541, -4.383539, 0),
(647, 'CI', '67', 'Dimbokro', 6.645828, -4.706601, 0),
(648, 'CI', '68', 'Grand-Bassam', 5.196354, -3.745698, 0),
(649, 'CI', '69', 'Guiglo', 6.55, -7.483333, 0),
(650, 'CI', '70', 'Jacqueville', 5.2, -4.416667, 0),
(651, 'CI', '71', 'Tiebissou', 7.716667, -4.666667, 0),
(652, 'CI', '72', 'Toulepleu', 6.576575, -8.420091, 0),
(653, 'CI', '73', 'Yamoussoukro', 6.84025, -5.3239, 0),
(654, 'CL', '01', 'Valparaiso', 0, 0, 12),
(655, 'CL', '02', 'Aisen del General Carlos Ibanez del Campo', 0, 0, 1),
(656, 'CL', '03', 'Antofagasta', 0, 0, 5),
(657, 'CL', '04', 'Araucania', 0, 0, 7),
(658, 'CL', '05', 'Atacama', 0, 0, 7),
(659, 'CL', '06', 'Bio-Bio', 0, 0, 7),
(660, 'CL', '07', 'Coquimbo', 0, 0, 9),
(661, 'CL', '08', 'Libertador General Bernardo O\'Higgins', 0, 0, 7),
(662, 'CL', '09', 'Los Lagos', 0, 0, 10),
(663, 'CL', '10', 'Magallanes y de la Antartica Chilena', 0, 0, 2),
(664, 'CL', '11', 'Maule', 0, 0, 5),
(665, 'CL', '12', 'Region Metropolitana', 0, 0, 16),
(666, 'CL', '13', 'Tarapaca', 0, 0, 2),
(667, 'CM', '04', 'Est', 4.008334, 14.11337, 0),
(668, 'CM', '05', 'Littoral', 0, 0, 2),
(669, 'CM', '07', 'Nord-Ouest', 0, 0, 1),
(670, 'CM', '08', 'Ouest', 0, 0, 2),
(671, 'CM', '09', 'Sud-Ouest', 0, 0, 3),
(672, 'CM', '10', 'Adamaoua', 0, 0, 0),
(673, 'CM', '11', 'Centre', 0, 0, 1),
(674, 'CM', '12', 'Extreme-Nord', 0, 0, 1),
(675, 'CM', '13', 'Nord', 8.666667, 13.915753, 0),
(676, 'CM', '14', 'Sud', 2.916667, 11.615155, 0),
(677, 'CN', '01', 'Anhui', 0, 0, 162),
(678, 'CN', '02', 'Zhejiang', 0, 0, 259),
(679, 'CN', '03', 'Jiangxi', 0, 0, 198),
(680, 'CN', '04', 'Jiangsu', 0, 0, 135),
(681, 'CN', '05', 'Jilin', 0, 0, 73),
(682, 'CN', '06', 'Qinghai', 0, 0, 16),
(683, 'CN', '07', 'Fujian', 0, 0, 481),
(684, 'CN', '08', 'Heilongjiang', 0, 0, 105),
(685, 'CN', '09', 'Henan', 0, 0, 127),
(686, 'CN', '10', 'Hebei', 39.344369, 116.662459, 145),
(687, 'CN', '11', 'Hunan', 0, 0, 401),
(688, 'CN', '12', 'Hubei', 0, 0, 84),
(689, 'CN', '13', 'Xinjiang', 0, 0, 20),
(690, 'CN', '14', 'Xizang', 0, 0, 8),
(691, 'CN', '15', 'Gansu', 0, 0, 51),
(692, 'CN', '16', 'Guangxi', 0, 0, 104),
(693, 'CN', '18', 'Guizhou', 0, 0, 159),
(694, 'CN', '19', 'Liaoning', 0, 0, 79),
(695, 'CN', '20', 'Nei Mongol', 0, 0, 23),
(696, 'CN', '21', 'Ningxia', 0, 0, 18),
(697, 'CN', '22', 'Beijing', 0, 0, 43),
(698, 'CN', '23', 'Shanghai', 0, 0, 22),
(699, 'CN', '24', 'Shanxi', 0, 0, 169),
(700, 'CN', '25', 'Shandong', 0, 0, 252),
(701, 'CN', '26', 'Shaanxi', 0, 0, 134),
(702, 'CN', '28', 'Tianjin', 0, 0, 26),
(703, 'CN', '29', 'Yunnan', 0, 0, 76),
(704, 'CN', '30', 'Guangdong', 22.878949, 113.447452, 296),
(705, 'CN', '31', 'Hainan', 0, 0, 126),
(706, 'CN', '32', 'Sichuan', 0, 0, 142),
(707, 'CN', '33', 'Chongqing', 0, 0, 63),
(708, 'CO', '01', 'Amazonas', -2.035092, -71.879177, 0),
(709, 'CO', '02', 'Antioquia', 0, 0, 11),
(710, 'CO', '03', 'Arauca', 6.540525, -70.929477, 0),
(711, 'CO', '04', 'Atlantico', 0, 0, 6),
(712, 'CO', '08', 'Caqueta', 1.107, -73.894897, 1),
(713, 'CO', '09', 'Cauca', 0, 0, 1),
(714, 'CO', '10', 'Cesar', 0, 0, 1),
(715, 'CO', '11', 'Choco', 6.339892, -76.952061, 0),
(716, 'CO', '12', 'Cordoba', 0, 0, 1),
(717, 'CO', '14', 'Guaviare', 1.752, -71.829002, 0),
(718, 'CO', '15', 'Guainia', 0, 0, 0),
(719, 'CO', '16', 'Huila', 0, 0, 5),
(720, 'CO', '17', 'La Guajira', 11.422254, -72.398506, 0),
(721, 'CO', '19', 'Meta', 0, 0, 4),
(722, 'CO', '20', 'Narino', 0, 0, 5),
(723, 'CO', '21', 'Norte de Santander', 0, 0, 4),
(724, 'CO', '22', 'Putumayo', 0, 0, 2),
(725, 'CO', '23', 'Quindio', 0, 0, 1),
(726, 'CO', '24', 'Risaralda', 0, 0, 4),
(727, 'CO', '25', 'San Andres y Providencia', 12.938668, -81.53886, 0),
(728, 'CO', '26', 'Santander', 0, 0, 11),
(729, 'CO', '27', 'Sucre', 0, 0, 3),
(730, 'CO', '28', 'Tolima', 0, 0, 9),
(731, 'CO', '29', 'Valle del Cauca', 0, 0, 14),
(732, 'CO', '30', 'Vaupes', 0.3853, -70.577064, 0),
(733, 'CO', '31', 'Vichada', 4.505044, -69.259456, 0),
(734, 'CO', '32', 'Casanare', 0, 0, 2),
(735, 'CO', '33', 'Cundinamarca', 0, 0, 22),
(736, 'CO', '34', 'Distrito Especial', 0, 0, 0),
(737, 'CO', '35', 'Bolivar', 8.89113, -74.785027, 0),
(738, 'CO', '36', 'Boyaca', 5.83475, -73.328693, 0),
(739, 'CO', '37', 'Caldas', 5.28265, -75.301853, 0),
(740, 'CO', '38', 'Magdalena', 10.153941, -74.279499, 0),
(741, 'CR', '01', 'Alajuela', 0, 0, 14),
(742, 'CR', '02', 'Cartago', 0, 0, 6),
(743, 'CR', '03', 'Guanacaste', 0, 0, 12),
(744, 'CR', '04', 'Heredia', 0, 0, 6),
(745, 'CR', '06', 'Limon', 0, 0, 1),
(746, 'CR', '07', 'Puntarenas', 0, 0, 10),
(747, 'CR', '08', 'San Jose', 0, 0, 19),
(748, 'CU', '01', 'Pinar del Rio', 0, 0, 1),
(749, 'CU', '02', 'Ciudad de la Habana', 0, 0, 1),
(750, 'CU', '03', 'Matanzas', 0, 0, 3),
(751, 'CU', '04', 'Isla de la Juventud', 21.668465, -82.876911, 0),
(752, 'CU', '05', 'Camaguey', 0, 0, 1),
(753, 'CU', '07', 'Ciego de Avila', 0, 0, 1),
(754, 'CU', '08', 'Cienfuegos', 0, 0, 1),
(755, 'CU', '09', 'Granma', 0, 0, 2),
(756, 'CU', '10', 'Guantanamo', 20.14303, -75.209007, 0),
(757, 'CU', '11', 'La Habana', 0, 0, 1),
(758, 'CU', '12', 'Holguin', 20.808333, -75.72327, 0),
(759, 'CU', '13', 'Las Tunas', 0, 0, 2),
(760, 'CU', '14', 'Sancti Spiritus', 21.954473, -79.518886, 0),
(761, 'CU', '15', 'Santiago de Cuba', 0, 0, 1),
(762, 'CU', '16', 'Villa Clara', 0, 0, 1),
(763, 'CV', '01', 'Boa Vista', 16.096086, -22.817226, 0),
(764, 'CV', '02', 'Brava', 14.850144, -24.719832, 0),
(765, 'CV', '04', 'Maio', 15.13626, -23.22175, 0),
(766, 'CV', '05', 'Paul', 0, 0, 0),
(767, 'CV', '07', 'Ribeira Grande', 0, 0, 0),
(768, 'CV', '08', 'Sal', 16.717597, -22.932311, 0),
(769, 'CV', '10', 'Sao Nicolau', 0, 0, 0),
(770, 'CV', '11', 'Sao Vicente', 0, 0, 0),
(771, 'CV', '13', 'Mosteiros', 15, -24.433333, 0),
(772, 'CV', '14', 'Praia', 14.9218, -23.508695, 0),
(773, 'CV', '15', 'Santa Catarina', 0, 0, 0),
(774, 'CV', '16', 'Santa Cruz', 15.133333, -23.566667, 0),
(775, 'CV', '17', 'Sao Domingos', 0, 0, 1),
(776, 'CV', '18', 'Sao Filipe', 14.958304, -23.532255, 0),
(777, 'CV', '19', 'Sao Miguel', 0, 0, 0),
(778, 'CV', '20', 'Tarrafal', 15.2732, -23.76123, 0),
(779, 'CY', '01', 'Famagusta', 0, 0, 15),
(780, 'CY', '02', 'Kyrenia', 0, 0, 3),
(781, 'CY', '03', 'Larnaca', 0, 0, 12),
(782, 'CY', '04', 'Nicosia', 0, 0, 23),
(783, 'CY', '05', 'Limassol', 0, 0, 17),
(784, 'CY', '06', 'Paphos', 0, 0, 12),
(785, 'CZ', '03', 'Blansko', 49.450699, 16.606538, 0),
(786, 'CZ', '04', 'Breclav', 48.838985, 16.756269, 0),
(787, 'CZ', '20', 'Hradec Kralove', 0, 0, 6),
(788, 'CZ', '21', 'Jablonec nad Nisou', 0, 0, 9),
(789, 'CZ', '23', 'Jiein', 0, 0, 6),
(790, 'CZ', '24', 'Jihlava', 0, 0, 1),
(791, 'CZ', '30', 'Kolin', 0, 0, 26),
(792, 'CZ', '33', 'Liberec', 0, 0, 28),
(793, 'CZ', '36', 'Melnik', 0, 0, 1),
(794, 'CZ', '37', 'Mlada Boleslav', 0, 0, 3),
(795, 'CZ', '39', 'Nachod', 0, 0, 10),
(796, 'CZ', '41', 'Nymburk', 0, 0, 3),
(797, 'CZ', '45', 'Pardubice', 0, 0, 22),
(798, 'CZ', '52', 'Hlavni Mesto Praha', 0, 0, 111),
(799, 'CZ', '61', 'Semily', 0, 0, 6),
(800, 'CZ', '70', 'Trutnov', 0, 0, 9),
(801, 'CZ', '78', 'Jihomoravsky Kraj', 0, 0, 93),
(802, 'CZ', '79', 'Jihocesky Kraj', 0, 0, 66),
(803, 'CZ', '80', 'Vysocina', 0, 0, 52),
(804, 'CZ', '81', 'Karlovarsky Kraj', 0, 0, 62),
(805, 'CZ', '82', 'Kralovehradecky Kraj', 0, 0, 17),
(806, 'CZ', '83', 'Liberecky Kraj', 0, 0, 13),
(807, 'CZ', '84', 'Olomoucky Kraj', 0, 0, 86),
(808, 'CZ', '85', 'Moravskoslezsky Kraj', 0, 0, 86),
(809, 'CZ', '86', 'Pardubicky Kraj', 0, 0, 32),
(810, 'CZ', '87', 'Plzensky Kraj', 0, 0, 94),
(811, 'CZ', '88', 'Stredocesky Kraj', 0, 0, 118),
(812, 'CZ', '89', 'Ustecky Kraj', 0, 0, 72),
(813, 'CZ', '90', 'Zlinsky Kraj', 0, 0, 54),
(814, 'DE', '01', 'Baden-Württemberg', 0, 0, 1018),
(815, 'DE', '02', 'Bayern', 0, 0, 1488),
(816, 'DE', '03', 'Bremen', 0, 0, 8),
(817, 'DE', '04', 'Hamburg', 0, 0, 22),
(818, 'DE', '05', 'Hessen', 0, 0, 369),
(819, 'DE', '06', 'Niedersachsen', 0, 0, 698),
(820, 'DE', '07', 'Nordrhein-Westfalen', 0, 0, 995),
(821, 'DE', '08', 'Rheinland-Pfalz', 0, 0, 701),
(822, 'DE', '09', 'Saarland', 0, 0, 67),
(823, 'DE', '10', 'Schleswig-Holstein', 0, 0, 403),
(824, 'DE', '11', 'Brandenburg', 0, 0, 418),
(825, 'DE', '12', 'Mecklenburg-Vorpommern', 0, 0, 276),
(826, 'DE', '13', 'Sachsen', 0, 0, 384),
(827, 'DE', '14', 'Sachsen-Anhalt', 0, 0, 244),
(828, 'DE', '15', 'Thuringen', 0, 0, 249),
(829, 'DE', '16', 'Berlin', 0, 0, 25),
(830, 'DJ', '02', 'Dikhil', 11.099622, 42.348721, 0),
(831, 'DJ', '03', 'Djibouti', 11.5806, 43.1425, 0),
(832, 'DJ', '04', 'Obock', 11.9642, 43.2725, 0),
(833, 'DJ', '05', 'Tadjoura', 11.7766, 42.8221, 0),
(834, 'DK', '01', 'Arhus', 0, 0, 92),
(835, 'DK', '02', 'Bornholm', 0, 0, 6),
(836, 'DK', '03', 'Frederiksborg', 0, 0, 25),
(837, 'DK', '04', 'Fyn', 0, 0, 58),
(838, 'DK', '05', 'Kobenhavn', 0, 0, 25),
(839, 'DK', '06', 'Staden Kobenhavn', 0, 0, 7),
(840, 'DK', '07', 'Nordjylland', 0, 0, 80),
(841, 'DK', '08', 'Ribe', 0, 0, 35),
(842, 'DK', '09', 'Ringkobing', 0, 0, 46),
(843, 'DK', '10', 'Roskilde', 0, 0, 26),
(844, 'DK', '11', 'Sonderjylland', 0, 0, 55),
(845, 'DK', '12', 'Storstrom', 0, 0, 48),
(846, 'DK', '13', 'Vejle', 0, 0, 44),
(847, 'DK', '14', 'Vestsjalland', 0, 0, 26),
(848, 'DK', '15', 'Viborg', 0, 0, 34),
(849, 'DM', '02', 'Saint Andrew', 15.5571, -61.314098, 0),
(850, 'DM', '03', 'Saint David', 15.433333, -61.266667, 0),
(851, 'DM', '04', 'Saint George', 0, 0, 1),
(852, 'DM', '05', 'Saint John', 15.56953, -61.455132, 0),
(853, 'DM', '06', 'Saint Joseph', 15.433333, -61.45, 0),
(854, 'DM', '07', 'Saint Luke', 0, 0, 0),
(855, 'DM', '08', 'Saint Mark', 0, 0, 0),
(856, 'DM', '09', 'Saint Patrick', 15.284935, -61.286041, 0),
(857, 'DM', '10', 'Saint Paul', 0, 0, 1),
(858, 'DM', '11', 'Saint Peter', 0, 0, 0),
(859, 'DO', '01', 'Azua', 18.624858, -70.763495, 0),
(860, 'DO', '02', 'Baoruco', 18.494061, -71.311864, 0),
(861, 'DO', '03', 'Barahona', 0, 0, 1),
(862, 'DO', '04', 'Dajabon', 19.44998, -71.628043, 0),
(863, 'DO', '05', 'Distrito Nacional', 0, 0, 2),
(864, 'DO', '06', 'Duarte', 19.260834, -70.00056, 0),
(865, 'DO', '08', 'Espaillat', 0, 0, 1),
(866, 'DO', '09', 'Independencia', 18.432037, -71.605446, 0),
(867, 'DO', '10', 'La Altagracia', 18.536392, -68.617096, 0),
(868, 'DO', '11', 'Elias Pina', 18.87628, -71.706543, 0),
(869, 'DO', '12', 'La Romana', 0, 0, 1),
(870, 'DO', '14', 'Maria Trinidad Sanchez', 0, 0, 2),
(871, 'DO', '15', 'Monte Cristi', 19.850609, -71.645294, 0),
(872, 'DO', '16', 'Pedernales', 17.905631, -71.51482, 0),
(873, 'DO', '17', 'Peravia', 18.350542, -70.368385, 0),
(874, 'DO', '18', 'Puerto Plata', 0, 0, 2),
(875, 'DO', '19', 'Salcedo', 0, 0, 2),
(876, 'DO', '20', 'Samana', 0, 0, 1),
(877, 'DO', '21', 'Sanchez Ramirez', 0, 0, 1),
(878, 'DO', '23', 'San Juan', 0, 0, 1),
(879, 'DO', '24', 'San Pedro De Macoris', 0, 0, 2),
(880, 'DO', '25', 'Santiago', 0, 0, 2),
(881, 'DO', '26', 'Santiago Rodriguez', 19.396625, -71.316328, 0),
(882, 'DO', '27', 'Valverde', 0, 0, 1),
(883, 'DO', '28', 'El Seibo', 0, 0, 1),
(884, 'DO', '29', 'Hato Mayor', 0, 0, 1),
(885, 'DO', '30', 'La Vega', 0, 0, 2),
(886, 'DO', '31', 'Monsenor Nouel', 0, 0, 1),
(887, 'DO', '32', 'Monte Plata', 0, 0, 2),
(888, 'DO', '33', 'San Cristobal', 0, 0, 4),
(889, 'DZ', '01', 'Alger', 0, 0, 4),
(890, 'DZ', '03', 'Batna', 0, 0, 1),
(891, 'DZ', '04', 'Constantine', 0, 0, 2),
(892, 'DZ', '06', 'Medea', 36.262874, 2.760296, 0),
(893, 'DZ', '07', 'Mostaganem', 0, 0, 1),
(894, 'DZ', '09', 'Oran', 0, 0, 2),
(895, 'DZ', '10', 'Saida', 34.830576, 0.1518, 0),
(896, 'DZ', '12', 'Setif', 36.18795, 5.4091, 0),
(897, 'DZ', '13', 'Tiaret', 0, 0, 1),
(898, 'DZ', '14', 'Tizi Ouzou', 0, 0, 1),
(899, 'DZ', '15', 'Tlemcen', 0, 0, 1),
(900, 'DZ', '18', 'Bejaia', 0, 0, 1),
(901, 'DZ', '19', 'Biskra', 0, 0, 2),
(902, 'DZ', '20', 'Blida', 0, 0, 1),
(903, 'DZ', '21', 'Bouira', 0, 0, 1),
(904, 'DZ', '22', 'Djelfa', 0, 0, 1),
(905, 'DZ', '23', 'Guelma', 36.4658, 7.4339, 0),
(906, 'DZ', '24', 'Jijel', 0, 0, 1),
(907, 'DZ', '25', 'Laghouat', 0, 0, 1),
(908, 'DZ', '26', 'Mascara', 0, 0, 3),
(909, 'DZ', '27', 'M\'sila', 35.702, 4.5471, 0),
(910, 'DZ', '29', 'Oum el Bouaghi', 35.790001, 7.055849, 0),
(911, 'DZ', '30', 'Sidi Bel Abbes', 35.199926, -0.635242, 0),
(912, 'DZ', '31', 'Skikda', 0, 0, 1),
(913, 'DZ', '33', 'Tebessa', 35.410219, 8.1215, 0),
(914, 'DZ', '34', 'Adrar', 0, 0, 1),
(915, 'DZ', '35', 'Ain Defla', 36.067322, 4.555303, 0),
(916, 'DZ', '36', 'Ain Temouchent', 35.2891, -1.1425, 0),
(917, 'DZ', '37', 'Annaba', 36.89875, 7.75635, 0),
(918, 'DZ', '38', 'Bechar', 31.60875, -2.2168, 0),
(919, 'DZ', '39', 'Bordj Bou Arreridj', 36.05912, 4.630318, 0),
(920, 'DZ', '40', 'Boumerdes', 36.726484, 3.709856, 0),
(921, 'DZ', '41', 'Chlef', 0, 0, 2),
(922, 'DZ', '42', 'El Bayadh', 32.572275, 0.950011, 0),
(923, 'DZ', '43', 'El Oued', 33.368309, 6.867369, 0),
(924, 'DZ', '44', 'El Tarf', 36.662735, 8.203498, 0),
(925, 'DZ', '45', 'Ghardaia', 32.4933, 3.675419, 0),
(926, 'DZ', '46', 'Illizi', 26.56481, 8.922689, 0),
(927, 'DZ', '47', 'Khenchela', 35.43525, 7.1403, 0),
(928, 'DZ', '48', 'Mila', 36.217729, 6.200075, 0),
(929, 'DZ', '49', 'Naama', 33.199561, -0.800004, 0),
(930, 'DZ', '50', 'Ouargla', 0, 0, 1),
(931, 'DZ', '51', 'Relizane', 0, 0, 2),
(932, 'DZ', '52', 'Souk Ahras', 36.28376, 7.955379, 0),
(933, 'DZ', '53', 'Tamanghasset', 0, 0, 0),
(934, 'DZ', '54', 'Tindouf', 0, 0, 1),
(935, 'DZ', '55', 'Tipaza', 0, 0, 2),
(936, 'DZ', '56', 'Tissemsilt', 35.75276, 1.86724, 0),
(937, 'EC', '01', 'Galapagos', 0, 0, 1),
(938, 'EC', '02', 'Azuay', 0, 0, 2),
(939, 'EC', '03', 'Bolivar', 0, 0, 1),
(940, 'EC', '04', 'Canar', 0, 0, 2),
(941, 'EC', '05', 'Carchi', 0, 0, 1),
(942, 'EC', '06', 'Chimborazo', 0, 0, 1),
(943, 'EC', '07', 'Cotopaxi', 0, 0, 1),
(944, 'EC', '08', 'El Oro', 0, 0, 1),
(945, 'EC', '09', 'Esmeraldas', 0, 0, 1),
(946, 'EC', '10', 'Guayas', 0, 0, 8),
(947, 'EC', '11', 'Imbabura', 0, 0, 2),
(948, 'EC', '12', 'Loja', 0, 0, 1),
(949, 'EC', '13', 'Los Rios', 0, 0, 3),
(950, 'EC', '14', 'Manabi', 0, 0, 3),
(951, 'EC', '15', 'Morona-Santiago', -2.518864, -77.854122, 0),
(952, 'EC', '17', 'Pastaza', 0, 0, 1),
(953, 'EC', '18', 'Pichincha', 0, 0, 4),
(954, 'EC', '19', 'Tungurahua', 0, 0, 1),
(955, 'EC', '20', 'Zamora-Chinchipe', -4.167306, -78.887458, 0),
(956, 'EC', '22', 'Sucumbios', 0.00139, -76.604355, 0),
(957, 'EC', '23', 'Napo', -0.754009, -76.797336, 0),
(958, 'EC', '24', 'Orellana', -0.794787, -76.426547, 0),
(959, 'EE', '01', 'Harjumaa', 0, 0, 53),
(960, 'EE', '02', 'Hiiumaa', 0, 0, 2),
(961, 'EE', '03', 'Ida-Virumaa', 0, 0, 7),
(962, 'EE', '04', 'Jarvamaa', 0, 0, 9),
(963, 'EE', '05', 'Jogevamaa', 0, 0, 6),
(964, 'EE', '06', 'Kohtla-Jarve', 59.398711, 27.250498, 0),
(965, 'EE', '07', 'Laanemaa', 0, 0, 19),
(966, 'EE', '08', 'Laane-Virumaa', 0, 0, 19),
(967, 'EE', '09', 'Narva', 59.374251, 28.182294, 0),
(968, 'EE', '10', 'Parnu', 58.386015, 24.498637, 0),
(969, 'EE', '11', 'Parnumaa', 0, 0, 17),
(970, 'EE', '12', 'Polvamaa', 0, 0, 6),
(971, 'EE', '13', 'Raplamaa', 0, 0, 15),
(972, 'EE', '14', 'Saaremaa', 0, 0, 6),
(973, 'EE', '15', 'Sillamae', 59.400758, 27.745486, 0),
(974, 'EE', '16', 'Tallinn', 59.439232, 24.758619, 0),
(975, 'EE', '17', 'Tartu', 58.370846, 26.714761, 0),
(976, 'EE', '18', 'Tartumaa', 0, 0, 19),
(977, 'EE', '19', 'Valgamaa', 0, 0, 6),
(978, 'EE', '20', 'Viljandimaa', 0, 0, 8),
(979, 'EE', '21', 'Vorumaa', 0, 0, 7),
(980, 'EG', '01', 'Ad Daqahliyah', 0, 0, 5),
(981, 'EG', '02', 'Al Bahr al Ahmar', 0, 0, 1),
(982, 'EG', '03', 'Al Buhayrah', 0, 0, 6),
(983, 'EG', '04', 'Al Fayyum', 0, 0, 2),
(984, 'EG', '05', 'Al Gharbiyah', 0, 0, 2),
(985, 'EG', '06', 'Al Iskandariyah', 0, 0, 1),
(986, 'EG', '07', 'Al Isma\'iliyah', 0, 0, 1),
(987, 'EG', '08', 'Al Jizah', 0, 0, 3),
(988, 'EG', '09', 'Al Minufiyah', 0, 0, 2),
(989, 'EG', '10', 'Al Minya', 0, 0, 1),
(990, 'EG', '11', 'Al Qahirah', 0, 0, 9),
(991, 'EG', '12', 'Al Qalyubiyah', 0, 0, 4),
(992, 'EG', '13', 'Al Wadi al Jadid', 25.782845, 30.401712, 0),
(993, 'EG', '14', 'Ash Sharqiyah', 0, 0, 9),
(994, 'EG', '15', 'As Suways', 0, 0, 1),
(995, 'EG', '16', 'Aswan', 0, 0, 1),
(996, 'EG', '17', 'Asyut', 0, 0, 2),
(997, 'EG', '18', 'Bani Suwayf', 29.048205, 30.628919, 0),
(998, 'EG', '19', 'Bur Sa\'id', 0, 0, 1),
(999, 'EG', '20', 'Dumyat', 0, 0, 1),
(1000, 'EG', '21', 'Kafr ash Shaykh', 0, 0, 3),
(1001, 'EG', '22', 'Matruh', 0, 0, 1),
(1002, 'EG', '23', 'Qina', 0, 0, 2),
(1003, 'EG', '24', 'Suhaj', 0, 0, 1),
(1004, 'EG', '26', 'Janub Sina\'', 28.727204, 33.688162, 0),
(1005, 'EG', '27', 'Shamal Sina\'', 30.3877, 33.815547, 0),
(1006, 'ES', '07', 'Islas Baleares', 0, 0, 36),
(1007, 'ES', '27', 'La Rioja', 0, 0, 21),
(1008, 'ES', '29', 'Madrid', 0, 0, 59),
(1009, 'ES', '31', 'Murcia', 0, 0, 36),
(1010, 'ES', '32', 'Navarra', 0, 0, 48),
(1011, 'ES', '34', 'Asturias', 0, 0, 80),
(1012, 'ES', '39', 'Cantabria', 0, 0, 65),
(1013, 'ES', '51', 'Andalucia', 0, 0, 190),
(1014, 'ES', '52', 'Aragon', 0, 0, 89),
(1015, 'ES', '53', 'Canarias', 0, 0, 77),
(1016, 'ES', '54', 'Castilla-La Mancha', 0, 0, 104),
(1017, 'ES', '55', 'Castilla y Leon', 0, 0, 110),
(1018, 'ES', '56', 'Catalonia', 0, 0, 224),
(1019, 'ES', '57', 'Extremadura', 0, 0, 39),
(1020, 'ES', '58', 'Galicia', 0, 0, 131),
(1021, 'ES', '59', 'Pais Vasco', 0, 0, 107),
(1022, 'ES', '60', 'Comunidad Valenciana', 0, 0, 175),
(1023, 'ET', '02', 'Amhara', 0, 0, 0),
(1024, 'ET', '07', 'Somali', 0, 0, 0),
(1025, 'ET', '08', 'Gambella', 8.128333, 34.562778, 0),
(1026, 'ET', '10', 'Addis Abeba', 0, 0, 2),
(1027, 'ET', '11', 'Southern', 0, 0, 0),
(1028, 'ET', '12', 'Tigray', 13.666667, 39.683333, 0),
(1029, 'ET', '13', 'Benishangul', 0, 0, 0),
(1030, 'ET', '14', 'Afar', 0, 0, 0),
(1031, 'ET', '44', 'Adis Abeba', 9.022736, 38.746799, 0),
(1032, 'ET', '46', 'Amara', 8.8, 34.733333, 0),
(1033, 'ET', '47', 'Binshangul Gumuz', 0, 0, 0),
(1034, 'ET', '48', 'Dire Dawa', 9.589229, 41.87019, 0),
(1035, 'ET', '49', 'Gambela Hizboch', 0, 0, 0),
(1036, 'ET', '50', 'Hareri Hizb', 0, 0, 0),
(1037, 'ET', '51', 'Oromiya', 0, 0, 0),
(1038, 'ET', '52', 'Sumale', 0, 0, 0),
(1039, 'ET', '54', 'YeDebub Biheroch Bihereseboch na Hizboch', 0, 0, 0),
(1040, 'FI', '01', '', 0, 0, 17),
(1041, 'FI', '06', 'Lapland', 0, 0, 61),
(1042, 'FI', '08', 'Oulu', 0, 0, 186),
(1043, 'FI', '13', 'Southern Finland', 0, 0, 296),
(1044, 'FI', '14', 'Eastern Finland', 0, 0, 135),
(1045, 'FI', '15', 'Western Finland', 0, 0, 428),
(1046, 'FJ', '01', 'Central', 0, 0, 10),
(1047, 'FJ', '02', 'Eastern', 0, 0, 0),
(1048, 'FJ', '03', 'Northern', 0, 0, 0),
(1049, 'FJ', '04', 'Rotuma', -12.5, 177.083333, 0),
(1050, 'FJ', '05', 'Western', 0, 0, 6),
(1051, 'FM', '01', 'Kosrae', 0, 0, 0),
(1052, 'FM', '02', 'Pohnpei', 0, 0, 0),
(1053, 'FM', '03', 'Chuuk', 0, 0, 0),
(1054, 'FM', '04', 'Yap', 0, 0, 0),
(1055, 'FR', '97', 'Aquitaine', 0, 0, 466),
(1056, 'FR', '98', 'Auvergne', 0, 0, 443),
(1057, 'FR', '99', 'Basse-Normandie', 0, 0, 285),
(1058, 'FR', 'A1', 'Bourgogne', 0, 0, 522),
(1059, 'FR', 'A2', 'Bretagne', 0, 0, 467),
(1060, 'FR', 'A3', 'Centre', 0, 0, 769),
(1061, 'FR', 'A4', 'Champagne-Ardenne', 0, 0, 210),
(1062, 'FR', 'A5', 'Corse', 0, 0, 101),
(1063, 'FR', 'A6', 'Franche-Comte', 0, 0, 247),
(1064, 'FR', 'A7', 'Haute-Normandie', 0, 0, 232),
(1065, 'FR', 'A8', 'Ile-de-France', 0, 0, 381),
(1066, 'FR', 'A9', 'Languedoc-Roussillon', 0, 0, 267),
(1067, 'FR', 'B1', 'Limousin', 0, 0, 264),
(1068, 'FR', 'B2', 'Lorraine', 0, 0, 350),
(1069, 'FR', 'B3', 'Midi-Pyrenees', 0, 0, 461),
(1070, 'FR', 'B4', 'Nord-Pas-de-Calais', 0, 0, 381),
(1071, 'FR', 'B5', 'Pays de la Loire', 0, 0, 250),
(1072, 'FR', 'B6', 'Picardie', 0, 0, 322),
(1073, 'FR', 'B7', 'Poitou-Charentes', 0, 0, 422),
(1074, 'FR', 'B8', 'Provence-Alpes-Cote d\'Azur', 0, 0, 369),
(1075, 'FR', 'B9', 'Rhone-Alpes', 0, 0, 1004),
(1076, 'FR', 'C1', 'Alsace', 0, 0, 292),
(1077, 'GA', '01', 'Estuaire', 0, 0, 1),
(1078, 'GA', '02', 'Haut-Ogooue', -1.158333, 13.625, 0),
(1079, 'GA', '03', 'Moyen-Ogooue', -0.460345, 10.518148, 0),
(1080, 'GA', '04', 'Ngounie', -1.801056, 11.246085, 0),
(1081, 'GA', '05', 'Nyanga', -3.083334, 10.889559, 0),
(1082, 'GA', '06', 'Ogooue-Ivindo', 0.341953, 12.857685, 0),
(1083, 'GA', '07', 'Ogooue-Lolo', -0.937319, 12.60437, 0),
(1084, 'GA', '08', 'Ogooue-Maritime', 0, 0, 1),
(1085, 'GA', '09', 'Woleu-Ntem', 1.258931, 11.825, 0),
(1086, 'GB', '01', 'Avon', 51.414792, -2.601525, 0),
(1087, 'GB', '03', 'Berkshire', 0, 0, 29),
(1088, 'GB', '07', 'Cleveland', 54.525579, -1.189651, 0),
(1089, 'GB', '17', 'Greater London', 0, 0, 1),
(1090, 'GB', '18', 'Greater Manchester', 53.442362, -2.23348, 0),
(1091, 'GB', '20', 'Hereford and Worcester', 52.134277, -2.450174, 0),
(1092, 'GB', '22', 'Humberside', 53.583093, -0.349013, 0),
(1093, 'GB', '28', 'Merseyside', 0, 0, 2),
(1094, 'GB', '37', 'South Yorkshire', 0, 0, 4),
(1095, 'GB', '41', 'Tyne and Wear', 55.053928, -2.017697, 0),
(1096, 'GB', '43', 'West Midlands', 0, 0, 8),
(1097, 'GB', '45', 'West Yorkshire', 0, 0, 4),
(1098, 'GB', '79', 'Central', 54.96932, -1.615655, 0),
(1099, 'GB', '82', 'Grampian', 52.87985, -1.495505, 0),
(1100, 'GB', '84', 'Lothian', 55.886686, -3.608939, 0),
(1101, 'GB', '87', 'Strathclyde', 0, 0, 6),
(1102, 'GB', '88', 'Tayside', 55.87392, -3.989278, 0),
(1103, 'GB', '90', 'Clwyd', 0, 0, 5),
(1104, 'GB', '91', 'Dyfed', 52.129642, -4.357746, 0),
(1105, 'GB', '92', 'Gwent', 51.774147, -3.006797, 0),
(1106, 'GB', '94', 'Mid Glamorgan', 51.65536, -3.400905, 0),
(1107, 'GB', '96', 'South Glamorgan', 51.548506, -3.32428, 0),
(1108, 'GB', '97', 'West Glamorgan', 51.801831, -3.810818, 0),
(1109, 'GB', 'A1', 'Barking and Dagenham', 0, 0, 1),
(1110, 'GB', 'A2', 'Barnet', 0, 0, 1),
(1111, 'GB', 'A3', 'Barnsley', 0, 0, 7),
(1112, 'GB', 'A4', 'Bath and North East Somerset', 0, 0, 15),
(1113, 'GB', 'A5', 'Bedfordshire', 0, 0, 39),
(1114, 'GB', 'A6', 'Bexley', 0, 0, 1),
(1115, 'GB', 'A7', 'Birmingham', 0, 0, 7),
(1116, 'GB', 'A8', 'Blackburn with Darwen', 0, 0, 2),
(1117, 'GB', 'A9', 'Blackpool', 0, 0, 3),
(1118, 'GB', 'B1', 'Bolton', 0, 0, 7),
(1119, 'GB', 'B2', 'Bournemouth', 0, 0, 1),
(1120, 'GB', 'B3', 'Bracknell Forest', 0, 0, 5),
(1121, 'GB', 'B4', 'Bradford', 0, 0, 2),
(1122, 'GB', 'B5', 'Brent', 0, 0, 1),
(1123, 'GB', 'B6', 'Brighton and Hove', 0, 0, 1),
(1124, 'GB', 'B7', 'Bristol, City of', 0, 0, 7),
(1125, 'GB', 'B8', 'Bromley', 0, 0, 9),
(1126, 'GB', 'B9', 'Buckinghamshire', 0, 0, 88),
(1127, 'GB', 'C1', 'Bury', 0, 0, 7),
(1128, 'GB', 'C2', 'Calderdale', 0, 0, 3),
(1129, 'GB', 'C3', 'Cambridgeshire', 0, 0, 51),
(1130, 'GB', 'C4', 'Camden', 0, 0, 1);
INSERT INTO `tbl_regions` (`id`, `country`, `code`, `name`, `latitude`, `longitude`, `cities`) VALUES
(1131, 'GB', 'C5', 'Cheshire', 0, 0, 103),
(1132, 'GB', 'C6', 'Cornwall', 0, 0, 91),
(1133, 'GB', 'C7', 'Coventry', 0, 0, 7),
(1134, 'GB', 'C8', 'Croydon', 0, 0, 1),
(1135, 'GB', 'C9', 'Cumbria', 0, 0, 142),
(1136, 'GB', 'D1', 'Darlington', 0, 0, 4),
(1137, 'GB', 'D2', 'Derby', 0, 0, 8),
(1138, 'GB', 'D3', 'Derbyshire', 0, 0, 73),
(1139, 'GB', 'D4', 'Devon', 0, 0, 117),
(1140, 'GB', 'D5', 'Doncaster', 0, 0, 9),
(1141, 'GB', 'D6', 'Dorset', 0, 0, 86),
(1142, 'GB', 'D7', 'Dudley', 0, 0, 1),
(1143, 'GB', 'D8', 'Durham', 0, 0, 79),
(1144, 'GB', 'D9', 'Ealing', 0, 0, 1),
(1145, 'GB', 'E1', 'East Riding of Yorkshire', 0, 0, 1),
(1146, 'GB', 'E2', 'East Sussex', 0, 0, 58),
(1147, 'GB', 'E3', 'Enfield', 0, 0, 1),
(1148, 'GB', 'E4', 'Essex', 0, 0, 238),
(1149, 'GB', 'E5', 'Gateshead', 0, 0, 2),
(1150, 'GB', 'E6', 'Gloucestershire', 0, 0, 115),
(1151, 'GB', 'E7', 'Greenwich', 0, 0, 1),
(1152, 'GB', 'E8', 'Hackney', 0, 0, 1),
(1153, 'GB', 'E9', 'Halton', 0, 0, 1),
(1154, 'GB', 'F1', 'Hammersmith and Fulham', 0, 0, 1),
(1155, 'GB', 'F2', 'Hampshire', 0, 0, 137),
(1156, 'GB', 'F3', 'Haringey', 0, 0, 1),
(1157, 'GB', 'F4', 'Harrow', 0, 0, 1),
(1158, 'GB', 'F5', 'Hartlepool', 0, 0, 2),
(1159, 'GB', 'F6', 'Havering', 0, 0, 5),
(1160, 'GB', 'F7', 'Herefordshire', 0, 0, 27),
(1161, 'GB', 'F8', 'Hertford', 0, 0, 58),
(1162, 'GB', 'F9', 'Hillingdon', 0, 0, 1),
(1163, 'GB', 'G1', 'Hounslow', 0, 0, 1),
(1164, 'GB', 'G2', 'Isle of Wight', 0, 0, 26),
(1165, 'GB', 'G3', 'Islington', 0, 0, 1),
(1166, 'GB', 'G4', 'Kensington and Chelsea', 0, 0, 1),
(1167, 'GB', 'G5', 'Kent', 0, 0, 232),
(1168, 'GB', 'G6', 'Kingston upon Hull, City of', 0, 0, 2),
(1169, 'GB', 'G7', 'Kingston upon Thames', 0, 0, 2),
(1170, 'GB', 'G8', 'Kirklees', 0, 0, 5),
(1171, 'GB', 'G9', 'Knowsley', 0, 0, 2),
(1172, 'GB', 'H1', 'Lambeth', 0, 0, 1),
(1173, 'GB', 'H2', 'Lancashire', 0, 0, 193),
(1174, 'GB', 'H3', 'Leeds', 0, 0, 6),
(1175, 'GB', 'H4', 'Leicester', 0, 0, 11),
(1176, 'GB', 'H5', 'Leicestershire', 0, 0, 60),
(1177, 'GB', 'H6', 'Lewisham', 0, 0, 1),
(1178, 'GB', 'H7', 'Lincolnshire', 0, 0, 56),
(1179, 'GB', 'H8', 'Liverpool', 0, 0, 3),
(1180, 'GB', 'H9', 'London, City of', 0, 0, 1),
(1181, 'GB', 'I1', 'Luton', 0, 0, 1),
(1182, 'GB', 'I2', 'Manchester', 0, 0, 2),
(1183, 'GB', 'I3', 'Medway', 0, 0, 1),
(1184, 'GB', 'I4', 'Merton', 0, 0, 1),
(1185, 'GB', 'I5', 'Middlesbrough', 0, 0, 3),
(1186, 'GB', 'I6', 'Milton Keynes', 0, 0, 4),
(1187, 'GB', 'I7', 'Newcastle upon Tyne', 0, 0, 9),
(1188, 'GB', 'I8', 'Newham', 0, 0, 13),
(1189, 'GB', 'I9', 'Norfolk', 0, 0, 175),
(1190, 'GB', 'J1', 'Northamptonshire', 0, 0, 79),
(1191, 'GB', 'J2', 'North East Lincolnshire', 0, 0, 1),
(1192, 'GB', 'J3', 'North Lincolnshire', 0, 0, 11),
(1193, 'GB', 'J4', 'North Somerset', 0, 0, 15),
(1194, 'GB', 'J5', 'North Tyneside', 0, 0, 2),
(1195, 'GB', 'J6', 'Northumberland', 0, 0, 76),
(1196, 'GB', 'J7', 'North Yorkshire', 0, 0, 5),
(1197, 'GB', 'J8', 'Nottingham', 0, 0, 16),
(1198, 'GB', 'J9', 'Nottinghamshire', 0, 0, 61),
(1199, 'GB', 'K1', 'Oldham', 0, 0, 5),
(1200, 'GB', 'K2', 'Oxfordshire', 0, 0, 143),
(1201, 'GB', 'K3', 'Peterborough', 0, 0, 3),
(1202, 'GB', 'K4', 'Plymouth', 0, 0, 5),
(1203, 'GB', 'K5', 'Poole', 0, 0, 1),
(1204, 'GB', 'K6', 'Portsmouth', 0, 0, 2),
(1205, 'GB', 'K7', 'Reading', 0, 0, 10),
(1206, 'GB', 'K8', 'Redbridge', 0, 0, 12),
(1207, 'GB', 'K9', 'Redcar and Cleveland', 0, 0, 4),
(1208, 'GB', 'L1', 'Richmond upon Thames', 0, 0, 1),
(1209, 'GB', 'L2', 'Rochdale', 0, 0, 2),
(1210, 'GB', 'L3', 'Rotherham', 0, 0, 11),
(1211, 'GB', 'L4', 'Rutland', 0, 0, 3),
(1212, 'GB', 'L5', 'Salford', 0, 0, 4),
(1213, 'GB', 'L6', 'Shropshire', 0, 0, 96),
(1214, 'GB', 'L7', 'Sandwell', 0, 0, 1),
(1215, 'GB', 'L8', 'Sefton', 0, 0, 5),
(1216, 'GB', 'L9', 'Sheffield', 0, 0, 4),
(1217, 'GB', 'M1', 'Slough', 0, 0, 25),
(1218, 'GB', 'M2', 'Solihull', 0, 0, 11),
(1219, 'GB', 'M3', 'Somerset', 0, 0, 124),
(1220, 'GB', 'M4', 'Southampton', 0, 0, 5),
(1221, 'GB', 'M5', 'Southend-on-Sea', 0, 0, 3),
(1222, 'GB', 'M6', 'South Gloucestershire', 0, 0, 27),
(1223, 'GB', 'M7', 'South Tyneside', 0, 0, 2),
(1224, 'GB', 'M8', 'Southwark', 0, 0, 1),
(1225, 'GB', 'M9', 'Staffordshire', 0, 0, 84),
(1226, 'GB', 'N1', 'St. Helens', 0, 0, 10),
(1227, 'GB', 'N2', 'Stockport', 0, 0, 8),
(1228, 'GB', 'N3', 'Stockton-on-Tees', 0, 0, 1),
(1229, 'GB', 'N4', 'Stoke-on-Trent', 0, 0, 12),
(1230, 'GB', 'N5', 'Suffolk', 0, 0, 74),
(1231, 'GB', 'N6', 'Sunderland', 0, 0, 3),
(1232, 'GB', 'N7', 'Surrey', 0, 0, 186),
(1233, 'GB', 'N8', 'Sutton', 0, 0, 1),
(1234, 'GB', 'N9', 'Swindon', 0, 0, 6),
(1235, 'GB', 'O1', 'Tameside', 0, 0, 7),
(1236, 'GB', 'O2', 'Telford and Wrekin', 0, 0, 3),
(1237, 'GB', 'O3', 'Thurrock', 0, 0, 4),
(1238, 'GB', 'O4', 'Torbay', 0, 0, 1),
(1239, 'GB', 'O5', 'Tower Hamlets', 0, 0, 1),
(1240, 'GB', 'O6', 'Trafford', 0, 0, 1),
(1241, 'GB', 'O7', 'Wakefield', 0, 0, 1),
(1242, 'GB', 'O8', 'Walsall', 0, 0, 6),
(1243, 'GB', 'O9', 'Waltham Forest', 0, 0, 1),
(1244, 'GB', 'P1', 'Wandsworth', 0, 0, 1),
(1245, 'GB', 'P2', 'Warrington', 0, 0, 6),
(1246, 'GB', 'P3', 'Warwickshire', 0, 0, 63),
(1247, 'GB', 'P4', 'West Berkshire', 0, 0, 5),
(1248, 'GB', 'P5', 'Westminster', 0, 0, 1),
(1249, 'GB', 'P6', 'West Sussex', 0, 0, 31),
(1250, 'GB', 'P7', 'Wigan', 0, 0, 7),
(1251, 'GB', 'P8', 'Wiltshire', 0, 0, 77),
(1252, 'GB', 'P9', 'Windsor and Maidenhead', 0, 0, 4),
(1253, 'GB', 'Q1', 'Wirral', 0, 0, 2),
(1254, 'GB', 'Q2', 'Wokingham', 0, 0, 10),
(1255, 'GB', 'Q3', 'Wolverhampton', 0, 0, 3),
(1256, 'GB', 'Q4', 'Worcestershire', 0, 0, 45),
(1257, 'GB', 'Q5', 'York', 0, 0, 326),
(1258, 'GB', 'Q6', 'Antrim', 0, 0, 11),
(1259, 'GB', 'Q7', 'Ards', 0, 0, 2),
(1260, 'GB', 'Q8', 'Armagh', 0, 0, 1),
(1261, 'GB', 'Q9', 'Ballymena', 0, 0, 5),
(1262, 'GB', 'R1', 'Ballymoney', 0, 0, 2),
(1263, 'GB', 'R2', 'Banbridge', 0, 0, 3),
(1264, 'GB', 'R3', 'Belfast', 0, 0, 5),
(1265, 'GB', 'R4', 'Carrickfergus', 0, 0, 5),
(1266, 'GB', 'R5', 'Castlereagh', 0, 0, 3),
(1267, 'GB', 'R6', 'Coleraine', 0, 0, 2),
(1268, 'GB', 'R7', 'Cookstown', 0, 0, 8),
(1269, 'GB', 'R8', 'Craigavon', 0, 0, 5),
(1270, 'GB', 'R9', 'Down', 0, 0, 21),
(1271, 'GB', 'S1', 'Dungannon', 0, 0, 9),
(1272, 'GB', 'S2', 'Fermanagh', 0, 0, 10),
(1273, 'GB', 'S3', 'Larne', 0, 0, 4),
(1274, 'GB', 'S4', 'Limavady', 0, 0, 3),
(1275, 'GB', 'S5', 'Lisburn', 0, 0, 4),
(1276, 'GB', 'S6', 'Derry', 0, 0, 13),
(1277, 'GB', 'S7', 'Magherafelt', 0, 0, 2),
(1278, 'GB', 'S8', 'Moyle', 0, 0, 3),
(1279, 'GB', 'S9', 'Newry and Mourne', 0, 0, 4),
(1280, 'GB', 'T1', 'Newtownabbey', 0, 0, 2),
(1281, 'GB', 'T2', 'North Down', 0, 0, 1),
(1282, 'GB', 'T3', 'Omagh', 0, 0, 7),
(1283, 'GB', 'T4', 'Strabane', 0, 0, 5),
(1284, 'GB', 'T5', 'Aberdeen City', 0, 0, 8),
(1285, 'GB', 'T6', 'Aberdeenshire', 0, 0, 57),
(1286, 'GB', 'T7', 'Angus', 0, 0, 18),
(1287, 'GB', 'T8', 'Argyll and Bute', 0, 0, 22),
(1288, 'GB', 'T9', 'Scottish Borders, The', 0, 0, 63),
(1289, 'GB', 'U1', 'Clackmannanshire', 0, 0, 7),
(1290, 'GB', 'U2', 'Dumfries and Galloway', 0, 0, 72),
(1291, 'GB', 'U3', 'Dundee City', 0, 0, 1),
(1292, 'GB', 'U4', 'East Ayrshire', 0, 0, 10),
(1293, 'GB', 'U5', 'East Dunbartonshire', 0, 0, 10),
(1294, 'GB', 'U6', 'East Lothian', 0, 0, 18),
(1295, 'GB', 'U7', 'East Renfrewshire', 0, 0, 8),
(1296, 'GB', 'U8', 'Edinburgh, City of', 0, 0, 4),
(1297, 'GB', 'U9', 'Falkirk', 0, 0, 14),
(1298, 'GB', 'V1', 'Fife', 0, 0, 51),
(1299, 'GB', 'V2', 'Glasgow City', 0, 0, 5),
(1300, 'GB', 'V3', 'Highland', 0, 0, 70),
(1301, 'GB', 'V4', 'Inverclyde', 0, 0, 7),
(1302, 'GB', 'V5', 'Midlothian', 0, 0, 21),
(1303, 'GB', 'V6', 'Moray', 0, 0, 22),
(1304, 'GB', 'V7', 'North Ayrshire', 0, 0, 10),
(1305, 'GB', 'V8', 'North Lanarkshire', 0, 0, 12),
(1306, 'GB', 'V9', 'Orkney', 0, 0, 5),
(1307, 'GB', 'W1', 'Perth and Kinross', 0, 0, 37),
(1308, 'GB', 'W2', 'Renfrewshire', 0, 0, 25),
(1309, 'GB', 'W3', 'Shetland Islands', 0, 0, 6),
(1310, 'GB', 'W4', 'South Ayrshire', 0, 0, 15),
(1311, 'GB', 'W5', 'South Lanarkshire', 0, 0, 7),
(1312, 'GB', 'W6', 'Stirling', 0, 0, 21),
(1313, 'GB', 'W7', 'West Dunbartonshire', 0, 0, 6),
(1314, 'GB', 'W8', 'Eilean Siar', 0, 0, 19),
(1315, 'GB', 'W9', 'West Lothian', 0, 0, 14),
(1316, 'GB', 'X1', 'Isle of Anglesey', 0, 0, 11),
(1317, 'GB', 'X2', 'Blaenau Gwent', 0, 0, 10),
(1318, 'GB', 'X3', 'Bridgend', 0, 0, 11),
(1319, 'GB', 'X4', 'Caerphilly', 0, 0, 25),
(1320, 'GB', 'X5', 'Cardiff', 0, 0, 11),
(1321, 'GB', 'X6', 'Ceredigion', 0, 0, 13),
(1322, 'GB', 'X7', 'Carmarthenshire', 0, 0, 30),
(1323, 'GB', 'X8', 'Conwy', 0, 0, 11),
(1324, 'GB', 'X9', 'Denbighshire', 0, 0, 9),
(1325, 'GB', 'Y1', 'Flintshire', 0, 0, 13),
(1326, 'GB', 'Y2', 'Gwynedd', 0, 0, 19),
(1327, 'GB', 'Y3', 'Merthyr Tydfil', 0, 0, 4),
(1328, 'GB', 'Y4', 'Monmouthshire', 0, 0, 18),
(1329, 'GB', 'Y5', 'Neath Port Talbot', 0, 0, 18),
(1330, 'GB', 'Y6', 'Newport', 0, 0, 8),
(1331, 'GB', 'Y7', 'Pembrokeshire', 0, 0, 45),
(1332, 'GB', 'Y8', 'Powys', 0, 0, 38),
(1333, 'GB', 'Y9', 'Rhondda Cynon Taff', 0, 0, 16),
(1334, 'GB', 'Z1', 'Swansea', 0, 0, 14),
(1335, 'GB', 'Z2', 'Torfaen', 0, 0, 5),
(1336, 'GB', 'Z3', 'Vale of Glamorgan, The', 0, 0, 12),
(1337, 'GB', 'Z4', 'Wrexham', 0, 0, 7),
(1338, 'GD', '01', 'Saint Andrew', 0, 0, 1),
(1339, 'GD', '02', 'Saint David', 12.04876, -61.664089, 0),
(1340, 'GD', '03', 'Saint George', 0, 0, 5),
(1341, 'GD', '04', 'Saint John', 0, 0, 1),
(1342, 'GD', '05', 'Saint Mark', 12.19327, -61.696461, 0),
(1343, 'GD', '06', 'Saint Patrick', 0, 0, 0),
(1344, 'GE', '01', 'Abashis Raioni', 0, 0, 0),
(1345, 'GE', '02', 'Abkhazia', 0, 0, 0),
(1346, 'GE', '03', 'Adigenis Raioni', 0, 0, 0),
(1347, 'GE', '04', 'Ajaria', 0, 0, 0),
(1348, 'GE', '05', 'Akhalgoris Raioni', 0, 0, 0),
(1349, 'GE', '06', 'Akhalk\'alak\'is Raioni', 0, 0, 0),
(1350, 'GE', '07', 'Akhalts\'ikhis Raioni', 0, 0, 0),
(1351, 'GE', '08', 'Akhmetis Raioni', 0, 0, 0),
(1352, 'GE', '09', 'Ambrolauris Raioni', 0, 0, 0),
(1353, 'GE', '10', 'Aspindzis Raioni', 0, 0, 0),
(1354, 'GE', '11', 'Baghdat\'is Raioni', 0, 0, 0),
(1355, 'GE', '12', 'Bolnisis Raioni', 0, 0, 0),
(1356, 'GE', '13', 'Borjomis Raioni', 0, 0, 0),
(1357, 'GE', '14', 'Chiat\'ura', 0, 0, 0),
(1358, 'GE', '15', 'Ch\'khorotsqus Raioni', 0, 0, 0),
(1359, 'GE', '16', 'Ch\'okhatauris Raioni', 0, 0, 0),
(1360, 'GE', '17', 'Dedop\'listsqaros Raioni', 0, 0, 0),
(1361, 'GE', '18', 'Dmanisis Raioni', 0, 0, 0),
(1362, 'GE', '19', 'Dushet\'is Raioni', 0, 0, 3),
(1363, 'GE', '20', 'Gardabanis Raioni', 0, 0, 0),
(1364, 'GE', '21', 'Gori', 41.981361, 44.109539, 0),
(1365, 'GE', '22', 'Goris Raioni', 0, 0, 3),
(1366, 'GE', '23', 'Gurjaanis Raioni', 0, 0, 0),
(1367, 'GE', '24', 'Javis Raioni', 0, 0, 0),
(1368, 'GE', '25', 'K\'arelis Raioni', 0, 0, 1),
(1369, 'GE', '26', 'Kaspis Raioni', 0, 0, 1),
(1370, 'GE', '27', 'Kharagaulis Raioni', 0, 0, 0),
(1371, 'GE', '28', 'Khashuris Raioni', 0, 0, 1),
(1372, 'GE', '29', 'Khobis Raioni', 0, 0, 0),
(1373, 'GE', '30', 'Khonis Raioni', 0, 0, 0),
(1374, 'GE', '31', 'K\'ut\'aisi', 0, 0, 1),
(1375, 'GE', '32', 'Lagodekhis Raioni', 0, 0, 0),
(1376, 'GE', '33', 'Lanch\'khut\'is Raioni', 0, 0, 0),
(1377, 'GE', '34', 'Lentekhis Raioni', 0, 0, 0),
(1378, 'GE', '35', 'Marneulis Raioni', 0, 0, 0),
(1379, 'GE', '36', 'Martvilis Raioni', 0, 0, 0),
(1380, 'GE', '37', 'Mestiis Raioni', 0, 0, 0),
(1381, 'GE', '38', 'Mts\'khet\'is Raioni', 0, 0, 1),
(1382, 'GE', '39', 'Ninotsmindis Raioni', 0, 0, 0),
(1383, 'GE', '40', 'Onis Raioni', 0, 0, 0),
(1384, 'GE', '41', 'Ozurget\'is Raioni', 0, 0, 0),
(1385, 'GE', '42', 'P\'ot\'i', 0, 0, 0),
(1386, 'GE', '43', 'Qazbegis Raioni', 0, 0, 0),
(1387, 'GE', '44', 'Qvarlis Raioni', 0, 0, 0),
(1388, 'GE', '45', 'Rust\'avi', 0, 0, 0),
(1389, 'GE', '46', 'Sach\'kheris Raioni', 0, 0, 0),
(1390, 'GE', '47', 'Sagarejos Raioni', 0, 0, 1),
(1391, 'GE', '48', 'Samtrediis Raioni', 0, 0, 0),
(1392, 'GE', '49', 'Senakis Raioni', 0, 0, 0),
(1393, 'GE', '50', 'Sighnaghis Raioni', 0, 0, 0),
(1394, 'GE', '51', 'T\'bilisi', 41.709981, 44.792998, 0),
(1395, 'GE', '52', 'T\'elavis Raioni', 0, 0, 0),
(1396, 'GE', '53', 'T\'erjolis Raioni', 0, 0, 0),
(1397, 'GE', '54', 'T\'et\'ritsqaros Raioni', 0, 0, 0),
(1398, 'GE', '55', 'T\'ianet\'is Raioni', 0, 0, 0),
(1399, 'GE', '56', 'Tqibuli', 0, 0, 0),
(1400, 'GE', '57', 'Ts\'ageris Raioni', 0, 0, 0),
(1401, 'GE', '58', 'Tsalenjikhis Raioni', 0, 0, 0),
(1402, 'GE', '59', 'Tsalkis Raioni', 0, 0, 0),
(1403, 'GE', '60', 'Tsqaltubo', 0, 0, 0),
(1404, 'GE', '61', 'Vanis Raioni', 0, 0, 0),
(1405, 'GE', '62', 'Zestap\'onis Raioni', 0, 0, 1),
(1406, 'GE', '63', 'Zugdidi', 42.502129, 41.86095, 0),
(1407, 'GE', '64', 'Zugdidis Raioni', 0, 0, 0),
(1408, 'GH', '01', 'Greater Accra', 0, 0, 1),
(1409, 'GH', '02', 'Ashanti', 0, 0, 3),
(1410, 'GH', '03', 'Brong-Ahafo', 0, 0, 1),
(1411, 'GH', '04', 'Central', 0, 0, 2),
(1412, 'GH', '05', 'Eastern', 6.443033, -0.458333, 0),
(1413, 'GH', '06', 'Northern', 0, 0, 1),
(1414, 'GH', '08', 'Volta', 0, 0, 12),
(1415, 'GH', '09', 'Western', 0, 0, 4),
(1416, 'GH', '10', 'Upper East', 10.733333, -0.753362, 0),
(1417, 'GH', '11', 'Upper West', 0, 0, 1),
(1418, 'GL', '01', 'Nordgronland', 0, 0, 4),
(1419, 'GL', '02', 'Ostgronland', 0, 0, 3),
(1420, 'GL', '03', 'Vestgronland', 0, 0, 19),
(1421, 'GM', '01', 'Banjul', 0, 0, 1),
(1422, 'GM', '02', 'Lower River', 13.4, -15.691666, 0),
(1423, 'GM', '03', 'MacCarthy Island', 0, 0, 0),
(1424, 'GM', '04', 'Upper River', 13.384087, -14.166666, 0),
(1425, 'GM', '05', 'Western', 0, 0, 2),
(1426, 'GM', '07', 'North Bank', 13.463586, -15.933333, 0),
(1427, 'GN', '01', 'Beyla', 8.708333, -8.325438, 0),
(1428, 'GN', '02', 'Boffa', 10.297138, -14.117445, 0),
(1429, 'GN', '03', 'Boke', 11.057385, -14.408333, 0),
(1430, 'GN', '04', 'Conakry', 0, 0, 1),
(1431, 'GN', '05', 'Dabola', 10.620505, -10.9208, 0),
(1432, 'GN', '06', 'Dalaba', 11.82565, -12.549234, 0),
(1433, 'GN', '07', 'Dinguiraye', 11.627974, -10.691666, 0),
(1434, 'GN', '09', 'Faranah', 9.85, -10.79884, 0),
(1435, 'GN', '10', 'Forecariah', 9.393325, -13.041667, 0),
(1436, 'GN', '11', 'Fria', 11.911633, -12.663922, 0),
(1437, 'GN', '12', 'Gaoual', 11.741666, -13.358333, 0),
(1438, 'GN', '13', 'Gueckedou', 8.691579, -10.200025, 0),
(1439, 'GN', '15', 'Kerouane', 11.426489, -9.596666, 0),
(1440, 'GN', '16', 'Kindia', 10.35795, -13.60195, 0),
(1441, 'GN', '17', 'Kissidougou', 9.266667, -9.858333, 0),
(1442, 'GN', '18', 'Koundara', 12.358333, -13.159111, 0),
(1443, 'GN', '19', 'Kouroussa', 10.533333, -10.068091, 0),
(1444, 'GN', '21', 'Macenta', 8.378457, -9.383333, 0),
(1445, 'GN', '22', 'Mali', 12.071596, -12.100296, 0),
(1446, 'GN', '23', 'Mamou', 10.149622, -11.955389, 0),
(1447, 'GN', '25', 'Pita', 11.398633, -10.8042, 0),
(1448, 'GN', '27', 'Telimele', 10.8538, -13.482125, 0),
(1449, 'GN', '28', 'Tougue', 11.508334, -11.6, 0),
(1450, 'GN', '29', 'Yomou', 10.6723, -12.273633, 0),
(1451, 'GN', '30', 'Coyah', 9.991666, -13.509133, 0),
(1452, 'GN', '31', 'Dubreka', 9.77487, -13.48532, 0),
(1453, 'GN', '32', 'Kankan', 10.6375, -9.78145, 0),
(1454, 'GN', '33', 'Koubia', 12.175571, -12.286126, 0),
(1455, 'GN', '34', 'Labe', 11.497561, -12.825444, 0),
(1456, 'GN', '35', 'Lelouma', 11.458334, -12.741666, 0),
(1457, 'GN', '36', 'Lola', 7.908334, -8.325, 0),
(1458, 'GN', '37', 'Mandiana', 10.686393, -8.658333, 0),
(1459, 'GN', '38', 'Nzerekore', 8.4547, -9.18, 0),
(1460, 'GN', '39', 'Siguiri', 11.662141, -9.314431, 0),
(1461, 'GQ', '03', 'Annobon', 0, 0, 0),
(1462, 'GQ', '04', 'Bioko Norte', 0, 0, 1),
(1463, 'GQ', '05', 'Bioko Sur', 3.441717, 8.696038, 0),
(1464, 'GQ', '06', 'Centro Sur', 1.591667, 10.475, 0),
(1465, 'GQ', '07', 'Kie-Ntem', 1.958334, 10.875, 0),
(1466, 'GQ', '08', 'Litoral', 0, 0, 1),
(1467, 'GQ', '09', 'Wele-Nzas', 1.478656, 10.95, 0),
(1468, 'GR', '01', 'Evros', 41.030616, 26.05834, 0),
(1469, 'GR', '02', 'Rodhopi', 0, 0, 0),
(1470, 'GR', '03', 'Xanthi', 41.1484, 24.850591, 0),
(1471, 'GR', '04', 'Drama', 41.271417, 24.158333, 0),
(1472, 'GR', '05', 'Serrai', 41.093391, 23.56204, 0),
(1473, 'GR', '06', 'Kilkis', 41.033333, 22.763585, 0),
(1474, 'GR', '07', 'Pella', 40.89761, 22.189165, 0),
(1475, 'GR', '08', 'Florina', 40.740591, 21.424662, 0),
(1476, 'GR', '09', 'Kastoria', 40.466667, 21.139247, 0),
(1477, 'GR', '10', 'Grevena', 40.05029, 21.412603, 0),
(1478, 'GR', '11', 'Kozani', 0, 0, 2),
(1479, 'GR', '12', 'Imathia', 0, 0, 1),
(1480, 'GR', '13', 'Thessaloniki', 0, 0, 2),
(1481, 'GR', '14', 'Kavala', 40.895889, 24.360599, 0),
(1482, 'GR', '15', 'Khalkidhiki', 40.508111, 23.664907, 0),
(1483, 'GR', '16', 'Pieria', 0, 0, 1),
(1484, 'GR', '17', 'Ioannina', 39.655955, 20.834226, 0),
(1485, 'GR', '18', 'Thesprotia', 0, 0, 1),
(1486, 'GR', '19', 'Preveza', 39.153955, 20.69682, 0),
(1487, 'GR', '20', 'Arta', 39.257078, 21.131045, 0),
(1488, 'GR', '21', 'Larisa', 39.645269, 22.350711, 0),
(1489, 'GR', '22', 'Trikala', 39.63895, 21.708649, 0),
(1490, 'GR', '23', 'Kardhitsa', 0, 0, 1),
(1491, 'GR', '24', 'Magnisia', 39.298579, 23.134485, 0),
(1492, 'GR', '25', 'Kerkira', 0, 0, 1),
(1493, 'GR', '26', 'Levkas', 38.910519, 21.998131, 0),
(1494, 'GR', '27', 'Kefallinia', 38.277131, 20.580682, 0),
(1495, 'GR', '28', 'Zakinthos', 37.782879, 20.897779, 0),
(1496, 'GR', '29', 'Fthiotis', 38.956575, 22.956264, 0),
(1497, 'GR', '30', 'Evritania', 0, 0, 0),
(1498, 'GR', '31', 'Aitolia kai Akarnania', 0, 0, 0),
(1499, 'GR', '32', 'Fokis', 0, 0, 0),
(1500, 'GR', '33', 'Voiotia', 38.375765, 23.079725, 0),
(1501, 'GR', '34', 'Evvoia', 38.512075, 23.713319, 0),
(1502, 'GR', '35', 'Attiki', 0, 0, 13),
(1503, 'GR', '36', 'Argolis', 0, 0, 0),
(1504, 'GR', '37', 'Korinthia', 0, 0, 1),
(1505, 'GR', '38', 'Akhaia', 0, 0, 1),
(1506, 'GR', '39', 'Ilia', 0, 0, 0),
(1507, 'GR', '40', 'Messinia', 0, 0, 2),
(1508, 'GR', '41', 'Arkadhia', 0, 0, 0),
(1509, 'GR', '42', 'Lakonia', 36.902534, 22.688189, 0),
(1510, 'GR', '43', 'Khania', 0, 0, 0),
(1511, 'GR', '44', 'Rethimni', 0, 0, 0),
(1512, 'GR', '45', 'Iraklion', 0, 0, 1),
(1513, 'GR', '46', 'Lasithi', 35.185535, 25.88944, 0),
(1514, 'GR', '47', 'Dhodhekanisos', 0, 0, 2),
(1515, 'GR', '48', 'Samos', 37.661529, 26.502899, 0),
(1516, 'GR', '49', 'Kikladhes', 0, 0, 2),
(1517, 'GR', '50', 'Khios', 38.373409, 26.139219, 0),
(1518, 'GR', '51', 'Lesvos', 39.510497, 25.810257, 0),
(1519, 'GT', '01', 'Alta Verapaz', 15.596932, -90.114359, 0),
(1520, 'GT', '02', 'Baja Verapaz', 0, 0, 3),
(1521, 'GT', '03', 'Chimaltenango', 0, 0, 4),
(1522, 'GT', '04', 'Chiquimula', 0, 0, 2),
(1523, 'GT', '05', 'El Progreso', 0, 0, 2),
(1524, 'GT', '06', 'Escuintla', 14.191499, -91.059039, 0),
(1525, 'GT', '07', 'Guatemala', 0, 0, 9),
(1526, 'GT', '08', 'Huehuetenango', 0, 0, 2),
(1527, 'GT', '09', 'Izabal', 0, 0, 1),
(1528, 'GT', '10', 'Jalapa', 0, 0, 3),
(1529, 'GT', '11', 'Jutiapa', 14.149869, -89.89818, 0),
(1530, 'GT', '12', 'Peten', 0, 0, 2),
(1531, 'GT', '13', 'Quetzaltenango', 0, 0, 1),
(1532, 'GT', '14', 'Quiche', 0, 0, 1),
(1533, 'GT', '15', 'Retalhuleu', 14.453467, -91.841876, 0),
(1534, 'GT', '16', 'Sacatepequez', 0, 0, 1),
(1535, 'GT', '17', 'San Marcos', 0, 0, 1),
(1536, 'GT', '18', 'Santa Rosa', 0, 0, 3),
(1537, 'GT', '19', 'Solola', 14.709968, -91.289426, 0),
(1538, 'GT', '20', 'Suchitepequez', 14.376171, -91.440007, 0),
(1539, 'GT', '21', 'Totonicapan', 15.038409, -91.365361, 0),
(1540, 'GT', '22', 'Zacapa', 0, 0, 3),
(1541, 'GW', '01', 'Bafata', 12.166445, -14.684685, 0),
(1542, 'GW', '02', 'Quinara', 11.5737, -15.3887, 0),
(1543, 'GW', '04', 'Oio', 12.28655, -15.2986, 0),
(1544, 'GW', '05', 'Bolama', 11.588865, -15.468925, 0),
(1545, 'GW', '06', 'Cacheu', 12.26849, -16.165205, 0),
(1546, 'GW', '07', 'Tombali', 11.300838, -15.409939, 0),
(1547, 'GW', '10', 'Gabu', 12.12055, -14.0982, 0),
(1548, 'GW', '11', 'Bissau', 0, 0, 1),
(1549, 'GW', '12', 'Biombo', 11.88055, -15.79065, 0),
(1550, 'GY', '10', 'Barima-Waini', 7.769285, -59.748726, 0),
(1551, 'GY', '11', 'Cuyuni-Mazaruni', 6.196833, -59.979461, 0),
(1552, 'GY', '12', 'Demerara-Mahaica', 0, 0, 2),
(1553, 'GY', '13', 'East Berbice-Corentyne', 3.888603, -57.499504, 0),
(1554, 'GY', '14', 'Essequibo Islands-West Demerara', 6.614159, -58.500341, 0),
(1555, 'GY', '15', 'Mahaica-Berbice', 6.278418, -57.828646, 0),
(1556, 'GY', '16', 'Pomeroon-Supenaam', 0, 0, 0),
(1557, 'GY', '17', 'Potaro-Siparuni', 4.861596, -59.218071, 0),
(1558, 'GY', '18', 'Upper Demerara-Berbice', 0, 0, 1),
(1559, 'GY', '19', 'Upper Takutu-Upper Essequibo', 0, 0, 0),
(1560, 'HN', '01', 'Atlantida', 0, 0, 7),
(1561, 'HN', '02', 'Choluteca', 0, 0, 2),
(1562, 'HN', '03', 'Colon', 15.548755, -85.728193, 0),
(1563, 'HN', '04', 'Comayagua', 14.551193, -87.648525, 0),
(1564, 'HN', '05', 'Copan', 0, 0, 1),
(1565, 'HN', '06', 'Cortes', 0, 0, 7),
(1566, 'HN', '07', 'El Paraiso', 13.961111, -86.397265, 0),
(1567, 'HN', '08', 'Francisco Morazan', 0, 0, 5),
(1568, 'HN', '09', 'Gracias a Dios', 15.297685, -84.074761, 0),
(1569, 'HN', '10', 'Intibuca', 14.254961, -88.179536, 0),
(1570, 'HN', '11', 'Islas de la Bahia', 0, 0, 1),
(1571, 'HN', '12', 'La Paz', 14.127596, -87.913945, 0),
(1572, 'HN', '13', 'Lempira', 14.454659, -88.648798, 0),
(1573, 'HN', '14', 'Ocotepeque', 14.475017, -89.035606, 0),
(1574, 'HN', '15', 'Olancho', 14.820052, -85.995711, 0),
(1575, 'HN', '16', 'Santa Barbara', 0, 0, 3),
(1576, 'HN', '17', 'Valle', 13.549931, -87.580012, 0),
(1577, 'HN', '18', 'Yoro', 0, 0, 2),
(1578, 'HR', '01', 'Bjelovarsko-Bilogorska', 0, 0, 3),
(1579, 'HR', '02', 'Brodsko-Posavska', 0, 0, 3),
(1580, 'HR', '03', 'Dubrovacko-Neretvanska', 0, 0, 15),
(1581, 'HR', '04', 'Istarska', 0, 0, 36),
(1582, 'HR', '05', 'Karlovacka', 0, 0, 6),
(1583, 'HR', '06', 'Koprivnicko-Krizevacka', 0, 0, 3),
(1584, 'HR', '07', 'Krapinsko-Zagorska', 0, 0, 13),
(1585, 'HR', '08', 'Licko-Senjska', 0, 0, 4),
(1586, 'HR', '09', 'Medimurska', 0, 0, 1),
(1587, 'HR', '10', 'Osjecko-Baranjska', 0, 0, 5),
(1588, 'HR', '11', 'Pozesko-Slavonska', 0, 0, 4),
(1589, 'HR', '12', 'Primorsko-Goranska', 0, 0, 27),
(1590, 'HR', '13', 'Sibensko-Kninska', 0, 0, 3),
(1591, 'HR', '14', 'Sisacko-Moslavacka', 0, 0, 9),
(1592, 'HR', '15', 'Splitsko-Dalmatinska', 0, 0, 15),
(1593, 'HR', '16', 'Varazdinska', 0, 0, 5),
(1594, 'HR', '17', 'Viroviticko-Podravska', 0, 0, 4),
(1595, 'HR', '18', 'Vukovarsko-Srijemska', 0, 0, 5),
(1596, 'HR', '19', 'Zadarska', 0, 0, 0),
(1597, 'HR', '20', 'Zagrebacka', 0, 0, 18),
(1598, 'HR', '21', 'Grad Zagreb', 0, 0, 22),
(1599, 'HT', '03', 'Nord-Ouest', 19.852331, -73.046462, 0),
(1600, 'HT', '06', 'Artibonite', 19.338183, -72.631967, 0),
(1601, 'HT', '07', 'Centre', 19.008885, -72.025, 0),
(1602, 'HT', '08', 'Grand\' Anse', 18.512105, -73.769989, 0),
(1603, 'HT', '09', 'Nord', 19.5753, -72.333333, 0),
(1604, 'HT', '10', 'Nord-Est', 19.508756, -71.895558, 0),
(1605, 'HT', '11', 'Ouest', 0, 0, 5),
(1606, 'HT', '12', 'Sud', 18.227232, -73.696853, 0),
(1607, 'HT', '13', 'Sud-Est', 18.258334, -72.436863, 0),
(1608, 'HU', '01', 'Bacs-Kiskun', 0, 0, 25),
(1609, 'HU', '02', 'Baranya', 0, 0, 15),
(1610, 'HU', '03', 'Bekes', 0, 0, 19),
(1611, 'HU', '04', 'Borsod-Abauj-Zemplen', 0, 0, 38),
(1612, 'HU', '05', 'Budapest', 0, 0, 4),
(1613, 'HU', '06', 'Csongrad', 0, 0, 10),
(1614, 'HU', '07', 'Debrecen', 0, 0, 1),
(1615, 'HU', '08', 'Fejer', 0, 0, 26),
(1616, 'HU', '09', 'Gyor-Moson-Sopron', 0, 0, 23),
(1617, 'HU', '10', 'Hajdu-Bihar', 0, 0, 14),
(1618, 'HU', '11', 'Heves', 0, 0, 23),
(1619, 'HU', '12', 'Komarom-Esztergom', 0, 0, 15),
(1620, 'HU', '13', 'Miskolc', 0, 0, 1),
(1621, 'HU', '14', 'Nograd', 0, 0, 10),
(1622, 'HU', '15', 'Pecs', 0, 0, 1),
(1623, 'HU', '16', 'Pest', 0, 0, 53),
(1624, 'HU', '17', 'Somogy', 0, 0, 23),
(1625, 'HU', '18', 'Szabolcs-Szatmar-Bereg', 0, 0, 39),
(1626, 'HU', '19', 'Szeged', 0, 0, 1),
(1627, 'HU', '20', 'Jasz-Nagykun-Szolnok', 0, 0, 14),
(1628, 'HU', '21', 'Tolna', 0, 0, 16),
(1629, 'HU', '22', 'Vas', 0, 0, 20),
(1630, 'HU', '23', 'Veszprem', 0, 0, 26),
(1631, 'HU', '24', 'Zala', 0, 0, 21),
(1632, 'HU', '25', 'Gyor', 0, 0, 1),
(1633, 'HU', '26', 'Bekescsaba', 46.684216, 21.086753, 0),
(1634, 'HU', '27', 'Dunaujvaros', 46.980668, 18.912706, 0),
(1635, 'HU', '28', 'Eger', 47.903198, 20.373107, 0),
(1636, 'HU', '29', 'Hodmezovasarhely', 46.430411, 20.318829, 0),
(1637, 'HU', '30', 'Kaposvar', 46.36653, 17.782483, 0),
(1638, 'HU', '31', 'Kecskemet', 46.9062, 19.689988, 0),
(1639, 'HU', '32', 'Nagykanizsa', 46.455237, 16.995341, 0),
(1640, 'HU', '33', 'Nyiregyhaza', 47.953161, 21.726792, 0),
(1641, 'HU', '34', 'Sopron', 47.684898, 16.583053, 0),
(1642, 'HU', '35', 'Szekesfehervar', 47.188715, 18.413808, 0),
(1643, 'HU', '36', 'Szolnok', 47.174662, 20.17628, 0),
(1644, 'HU', '37', 'Szombathely', 47.23514, 16.621917, 0),
(1645, 'HU', '38', 'Tatabanya', 47.586186, 18.394766, 0),
(1646, 'HU', '40', 'Zalaegerszeg', 46.845389, 16.847235, 0),
(1647, 'ID', '01', 'Aceh', 0, 0, 15),
(1648, 'ID', '02', 'Bali', 0, 0, 11),
(1649, 'ID', '03', 'Bengkulu', 0, 0, 1),
(1650, 'ID', '04', 'Jakarta Raya', 0, 0, 1),
(1651, 'ID', '05', 'Jambi', 0, 0, 4),
(1652, 'ID', '07', 'Jawa Tengah', 0, 0, 60),
(1653, 'ID', '08', 'Jawa Timur', 0, 0, 43),
(1654, 'ID', '09', 'Papua', 0, 0, 15),
(1655, 'ID', '10', 'Yogyakarta', 0, 0, 6),
(1656, 'ID', '11', 'Kalimantan Barat', 0, 0, 11),
(1657, 'ID', '12', 'Kalimantan Selatan', 0, 0, 6),
(1658, 'ID', '13', 'Kalimantan Tengah', 0, 0, 4),
(1659, 'ID', '14', 'Kalimantan Timur', 0, 0, 8),
(1660, 'ID', '15', 'Lampung', 0, 0, 4),
(1661, 'ID', '17', 'Nusa Tenggara Barat', 0, 0, 6),
(1662, 'ID', '18', 'Nusa Tenggara Timur', 0, 0, 14),
(1663, 'ID', '19', 'Riau', 0, 0, 11),
(1664, 'ID', '20', 'Sulawesi Selatan', 0, 0, 26),
(1665, 'ID', '21', 'Sulawesi Tengah', 0, 0, 16),
(1666, 'ID', '22', 'Sulawesi Tenggara', 0, 0, 13),
(1667, 'ID', '24', 'Sumatera Barat', 0, 0, 7),
(1668, 'ID', '26', 'Sumatera Utara', 0, 0, 11),
(1669, 'ID', '28', 'Maluku', -3.636802, 130.307541, 0),
(1670, 'ID', '29', 'Maluku Utara', 0.079817, 126.894764, 0),
(1671, 'ID', '30', 'Jawa Barat', 0, 0, 51),
(1672, 'ID', '31', 'Sulawesi Utara', 0, 0, 10),
(1673, 'ID', '32', 'Sumatera Selatan', -3.22161, 106.214485, 0),
(1674, 'ID', '33', 'Banten', -6.412115, 105.961223, 0),
(1675, 'ID', '34', 'Gorontalo', 0.699133, 122.161024, 0),
(1676, 'ID', '35', 'Kepulauan Bangka Belitung', -2.402084, 106.923316, 0),
(1677, 'IE', '01', 'Carlow', 0, 0, 6),
(1678, 'IE', '02', 'Cavan', 0, 0, 7),
(1679, 'IE', '03', 'Clare', 0, 0, 27),
(1680, 'IE', '04', 'Cork', 0, 0, 31),
(1681, 'IE', '06', 'Donegal', 0, 0, 15),
(1682, 'IE', '07', 'Dublin', 0, 0, 65),
(1683, 'IE', '10', 'Galway', 0, 0, 21),
(1684, 'IE', '11', 'Kerry', 0, 0, 18),
(1685, 'IE', '12', 'Kildare', 0, 0, 20),
(1686, 'IE', '13', 'Kilkenny', 0, 0, 7),
(1687, 'IE', '14', 'Leitrim', 0, 0, 1),
(1688, 'IE', '15', 'Laois', 0, 0, 8),
(1689, 'IE', '16', 'Limerick', 0, 0, 12),
(1690, 'IE', '18', 'Longford', 0, 0, 4),
(1691, 'IE', '19', 'Louth', 0, 0, 10),
(1692, 'IE', '20', 'Mayo', 0, 0, 16),
(1693, 'IE', '21', 'Meath', 0, 0, 21),
(1694, 'IE', '22', 'Monaghan', 0, 0, 6),
(1695, 'IE', '23', 'Offaly', 0, 0, 9),
(1696, 'IE', '24', 'Roscommon', 0, 0, 12),
(1697, 'IE', '25', 'Sligo', 0, 0, 7),
(1698, 'IE', '26', 'Tipperary', 0, 0, 15),
(1699, 'IE', '27', 'Waterford', 0, 0, 10),
(1700, 'IE', '29', 'Westmeath', 0, 0, 4),
(1701, 'IE', '30', 'Wexford', 0, 0, 11),
(1702, 'IE', '31', 'Wicklow', 0, 0, 11),
(1703, 'IL', '01', 'HaDarom', 0, 0, 54),
(1704, 'IL', '02', 'HaMerkaz', 0, 0, 71),
(1705, 'IL', '03', 'HaZafon', 0, 0, 96),
(1706, 'IL', '04', 'Hefa', 0, 0, 36),
(1707, 'IL', '05', 'Tel Aviv', 0, 0, 20),
(1708, 'IL', '06', 'Yerushalayim', 0, 0, 12),
(1709, 'IN', '01', 'Andaman and Nicobar Islands', 0, 0, 2),
(1710, 'IN', '02', 'Andhra Pradesh', 0, 0, 66),
(1711, 'IN', '03', 'Assam', 0, 0, 15),
(1712, 'IN', '05', 'Chandigarh', 0, 0, 1),
(1713, 'IN', '06', 'Dadra and Nagar Haveli', 0, 0, 1),
(1714, 'IN', '07', 'Delhi', 0, 0, 8),
(1715, 'IN', '09', 'Gujarat', 0, 0, 67),
(1716, 'IN', '10', 'Haryana', 0, 0, 35),
(1717, 'IN', '11', 'Himachal Pradesh', 0, 0, 22),
(1718, 'IN', '12', 'Jammu and Kashmir', 0, 0, 26),
(1719, 'IN', '13', 'Kerala', 0, 0, 47),
(1720, 'IN', '14', 'Lakshadweep', 10.332617, 72.74059, 0),
(1721, 'IN', '16', 'Maharashtra', 0, 0, 107),
(1722, 'IN', '17', 'Manipur', 0, 0, 2),
(1723, 'IN', '18', 'Meghalaya', 0, 0, 3),
(1724, 'IN', '19', 'Karnataka', 0, 0, 53),
(1725, 'IN', '20', 'Nagaland', 0, 0, 1),
(1726, 'IN', '21', 'Orissa', 0, 0, 21),
(1727, 'IN', '22', 'Pondicherry', 0, 0, 1),
(1728, 'IN', '23', 'Punjab', 0, 0, 38),
(1729, 'IN', '24', 'Rajasthan', 0, 0, 48),
(1730, 'IN', '25', 'Tamil Nadu', 0, 0, 110),
(1731, 'IN', '26', 'Tripura', 0, 0, 1),
(1732, 'IN', '28', 'West Bengal', 0, 0, 55),
(1733, 'IN', '29', 'Sikkim', 0, 0, 2),
(1734, 'IN', '30', 'Arunachal Pradesh', 0, 0, 2),
(1735, 'IN', '31', 'Mizoram', 0, 0, 1),
(1736, 'IN', '32', 'Daman and Diu', 0, 0, 1),
(1737, 'IN', '33', 'Goa', 0, 0, 25),
(1738, 'IN', '34', 'Bihar', 0, 0, 12),
(1739, 'IN', '35', 'Madhya Pradesh', 0, 0, 43),
(1740, 'IN', '36', 'Uttar Pradesh', 0, 0, 50),
(1741, 'IN', '37', 'Chhattisgarh', 0, 0, 9),
(1742, 'IN', '38', 'Jharkhand', 0, 0, 12),
(1743, 'IN', '39', 'Uttaranchal', 0, 0, 28),
(1744, 'IQ', '01', 'Al Anbar', 0, 0, 3),
(1745, 'IQ', '02', 'Al Basrah', 0, 0, 1),
(1746, 'IQ', '03', 'Al Muthanna', 0, 0, 1),
(1747, 'IQ', '04', 'Al Qadisiyah', 31.837274, 45.101553, 0),
(1748, 'IQ', '05', 'As Sulaymaniyah', 0, 0, 4),
(1749, 'IQ', '06', 'Babil', 32.656051, 44.531958, 0),
(1750, 'IQ', '07', 'Baghdad', 0, 0, 3),
(1751, 'IQ', '08', 'Dahuk', 0, 0, 2),
(1752, 'IQ', '09', 'Dhi Qar', 0, 0, 1),
(1753, 'IQ', '10', 'Diyala', 0, 0, 1),
(1754, 'IQ', '11', 'Arbil', 0, 0, 5),
(1755, 'IQ', '12', 'Karbala\'', 0, 0, 1),
(1756, 'IQ', '13', 'At Ta\'mim', 0, 0, 1),
(1757, 'IQ', '14', 'Maysan', 31.99154, 47.083365, 0),
(1758, 'IQ', '15', 'Ninawa', 0, 0, 2),
(1759, 'IQ', '16', 'Wasit', 32.694176, 45.558189, 0),
(1760, 'IQ', '17', 'An Najaf', 0, 0, 2),
(1761, 'IQ', '18', 'Salah ad Din', 34.544936, 43.702881, 0),
(1762, 'IR', '01', 'Azarbayjan-e Bakhtari', 0, 0, 20),
(1763, 'IR', '02', 'Azarbayjan-e Khavari', 0, 0, 14),
(1764, 'IR', '03', 'Chahar Mahall va Bakhtiari', 0, 0, 8),
(1765, 'IR', '04', 'Sistan va Baluchestan', 0, 0, 14),
(1766, 'IR', '05', 'Kohkiluyeh va Buyer Ahmadi', 0, 0, 6),
(1767, 'IR', '07', 'Fars', 0, 0, 49),
(1768, 'IR', '08', 'Gilan', 0, 0, 12),
(1769, 'IR', '09', 'Hamadan', 0, 0, 9),
(1770, 'IR', '10', 'Ilam', 0, 0, 9),
(1771, 'IR', '11', 'Hormozgan', 0, 0, 17),
(1772, 'IR', '13', 'Bakhtaran', 0, 0, 11),
(1773, 'IR', '15', 'Khuzestan', 0, 0, 30),
(1774, 'IR', '16', 'Kordestan', 0, 0, 10),
(1775, 'IR', '22', 'Bushehr', 0, 0, 13),
(1776, 'IR', '23', 'Lorestan', 0, 0, 14),
(1777, 'IR', '25', 'Semnan', 0, 0, 8),
(1778, 'IR', '26', 'Tehran', 0, 0, 15),
(1779, 'IR', '28', 'Esfahan', 0, 0, 31),
(1780, 'IR', '29', 'Kerman', 0, 0, 19),
(1781, 'IR', '30', 'Khorasan', 0, 0, 62),
(1782, 'IR', '31', 'Yazd', 0, 0, 11),
(1783, 'IR', '32', 'Ardabil', 0, 0, 8),
(1784, 'IR', '34', 'Markazi', 0, 0, 1),
(1785, 'IR', '35', 'Mazandaran', 0, 0, 4),
(1786, 'IR', '36', 'Zanjan', 0, 0, 0),
(1787, 'IR', '37', 'Golestan', 0, 0, 3),
(1788, 'IR', '38', 'Qazvin', 0, 0, 0),
(1789, 'IR', '39', 'Qom', 0, 0, 0),
(1790, 'IS', '01', 'Akranes', 64.322357, -22.07443, 0),
(1791, 'IS', '02', 'Akureyri', 65.683868, -18.11046, 0),
(1792, 'IS', '03', 'Arnessysla', 0, 0, 5),
(1793, 'IS', '04', 'Austur-Bardastrandarsysla', 0, 0, 0),
(1794, 'IS', '05', 'Austur-Hunavatnssysla', 0, 0, 0),
(1795, 'IS', '06', 'Austur-Skaftafellssysla', 0, 0, 1),
(1796, 'IS', '07', 'Borgarfjardarsysla', 0, 0, 2),
(1797, 'IS', '08', 'Dalasysla', 0, 0, 0),
(1798, 'IS', '09', 'Eyjafjardarsysla', 0, 0, 1),
(1799, 'IS', '10', 'Gullbringusysla', 0, 0, 2),
(1800, 'IS', '11', 'Hafnarfjordur', 0, 0, 0),
(1801, 'IS', '12', 'Husavik', 66.04763, -17.343599, 0),
(1802, 'IS', '13', 'Isafjordur', 66.070558, -23.12886, 0),
(1803, 'IS', '14', 'Keflavik', 64.006538, -22.5721, 0),
(1804, 'IS', '15', 'Kjosarsysla', 0, 0, 1),
(1805, 'IS', '16', 'Kopavogur', 64.11103, -21.905692, 0),
(1806, 'IS', '17', 'Myrasysla', 0, 0, 2),
(1807, 'IS', '18', 'Neskaupstadur', 0, 0, 0),
(1808, 'IS', '19', 'Nordur-Isafjardarsysla', 0, 0, 0),
(1809, 'IS', '20', 'Nordur-Mulasysla', 0, 0, 0),
(1810, 'IS', '21', 'Nordur-Tingeyjarsysla', 0, 0, 0),
(1811, 'IS', '22', 'Olafsfjordur', 66.06517, -18.657633, 0),
(1812, 'IS', '23', 'Rangarvallasysla', 0, 0, 2),
(1813, 'IS', '24', 'Reykjavik', 64.135338, -21.89521, 0),
(1814, 'IS', '25', 'Saudarkrokur', 0, 0, 0),
(1815, 'IS', '26', 'Seydisfjordur', 65.266667, -14, 0),
(1816, 'IS', '27', 'Siglufjordur', 0, 0, 0),
(1817, 'IS', '28', 'Skagafjardarsysla', 0, 0, 0),
(1818, 'IS', '29', 'Snafellsnes- og Hnappadalssysla', 0, 0, 2),
(1819, 'IS', '30', 'Strandasysla', 0, 0, 0),
(1820, 'IS', '31', 'Sudur-Mulasysla', 0, 0, 0),
(1821, 'IS', '32', 'Sudur-Tingeyjarsysla', 0, 0, 0),
(1822, 'IS', '33', 'Vestmannaeyjar', 63.437679, -20.267321, 0),
(1823, 'IS', '34', 'Vestur-Bardastrandarsysla', 0, 0, 0),
(1824, 'IS', '35', 'Vestur-Hunavatnssysla', 0, 0, 0),
(1825, 'IS', '36', 'Vestur-Isafjardarsysla', 0, 0, 1),
(1826, 'IS', '37', 'Vestur-Skaftafellssysla', 0, 0, 0),
(1827, 'IT', '01', 'Abruzzi', 0, 0, 302),
(1828, 'IT', '02', 'Basilicata', 0, 0, 70),
(1829, 'IT', '03', 'Calabria', 0, 0, 354),
(1830, 'IT', '04', 'Campania', 0, 0, 333),
(1831, 'IT', '05', 'Emilia-Romagna', 0, 0, 823),
(1832, 'IT', '06', 'Friuli-Venezia Giulia', 0, 0, 249),
(1833, 'IT', '07', 'Lazio', 0, 0, 394),
(1834, 'IT', '08', 'Liguria', 0, 0, 431),
(1835, 'IT', '09', 'Lombardia', 0, 0, 729),
(1836, 'IT', '10', 'Marche', 0, 0, 150),
(1837, 'IT', '11', 'Molise', 0, 0, 58),
(1838, 'IT', '12', 'Piemonte', 0, 0, 434),
(1839, 'IT', '13', 'Puglia', 0, 0, 247),
(1840, 'IT', '14', 'Sardegna', 0, 0, 218),
(1841, 'IT', '15', 'Sicilia', 0, 0, 430),
(1842, 'IT', '16', 'Toscana', 0, 0, 400),
(1843, 'IT', '17', 'Trentino-Alto Adige', 0, 0, 245),
(1844, 'IT', '18', 'Umbria', 0, 0, 203),
(1845, 'IT', '19', 'Valle d\'Aosta', 0, 0, 34),
(1846, 'IT', '20', 'Veneto', 0, 0, 753),
(1847, 'JM', '01', 'Clarendon', 0, 0, 2),
(1848, 'JM', '02', 'Hanover', 0, 0, 1),
(1849, 'JM', '04', 'Manchester', 0, 0, 1),
(1850, 'JM', '07', 'Portland', 18.124961, -76.508211, 0),
(1851, 'JM', '08', 'Saint Andrew', 0, 0, 1),
(1852, 'JM', '09', 'Saint Ann', 0, 0, 3),
(1853, 'JM', '10', 'Saint Catherine', 0, 0, 5),
(1854, 'JM', '11', 'Saint Elizabeth', 0, 0, 1),
(1855, 'JM', '12', 'Saint James', 0, 0, 3),
(1856, 'JM', '13', 'Saint Mary', 0, 0, 1),
(1857, 'JM', '14', 'Saint Thomas', 17.967124, -76.422687, 0),
(1858, 'JM', '15', 'Trelawny', 0, 0, 1),
(1859, 'JM', '16', 'Westmoreland', 0, 0, 1),
(1860, 'JM', '17', 'Kingston', 17.992731, -76.792009, 0),
(1861, 'JO', '02', 'Al Balqa\'', 0, 0, 2),
(1862, 'JO', '07', 'Ma', 0, 0, 2),
(1863, 'JO', '09', 'Al Karak', 0, 0, 2),
(1864, 'JO', '10', 'Al Mafraq', 0, 0, 3),
(1865, 'JO', '12', 'At Tafilah', 0, 0, 0),
(1866, 'JO', '13', 'Az Zarqa', 32.041149, 36.08147, 0),
(1867, 'JO', '14', 'Irbid', 0, 0, 4),
(1868, 'JO', '16', 'Amman', 31.949685, 35.92635, 0),
(1869, 'JP', '01', 'Aichi', 0, 0, 73),
(1870, 'JP', '02', 'Akita', 0, 0, 24),
(1871, 'JP', '03', 'Aomori', 0, 0, 45),
(1872, 'JP', '04', 'Chiba', 0, 0, 65),
(1873, 'JP', '05', 'Ehime', 0, 0, 39),
(1874, 'JP', '06', 'Fukui', 0, 0, 21),
(1875, 'JP', '07', 'Fukuoka', 0, 0, 68),
(1876, 'JP', '08', 'Fukushima', 0, 0, 63),
(1877, 'JP', '09', 'Gifu', 0, 0, 35),
(1878, 'JP', '10', 'Gumma', 0, 0, 29),
(1879, 'JP', '11', 'Hiroshima', 0, 0, 91),
(1880, 'JP', '12', 'Hokkaido', 0, 0, 194),
(1881, 'JP', '13', 'Hyogo', 0, 0, 72),
(1882, 'JP', '14', 'Ibaraki', 0, 0, 49),
(1883, 'JP', '15', 'Ishikawa', 0, 0, 39),
(1884, 'JP', '16', 'Iwate', 0, 0, 43),
(1885, 'JP', '17', 'Kagawa', 0, 0, 24),
(1886, 'JP', '18', 'Kagoshima', 0, 0, 102),
(1887, 'JP', '19', 'Kanagawa', 0, 0, 62),
(1888, 'JP', '20', 'Kochi', 0, 0, 18),
(1889, 'JP', '21', 'Kumamoto', 0, 0, 45),
(1890, 'JP', '22', 'Kyoto', 0, 0, 50),
(1891, 'JP', '23', 'Mie', 0, 0, 42),
(1892, 'JP', '24', 'Miyagi', 0, 0, 55),
(1893, 'JP', '25', 'Miyazaki', 0, 0, 49),
(1894, 'JP', '26', 'Nagano', 0, 0, 68),
(1895, 'JP', '27', 'Nagasaki', 0, 0, 102),
(1896, 'JP', '28', 'Nara', 0, 0, 27),
(1897, 'JP', '29', 'Niigata', 0, 0, 76),
(1898, 'JP', '30', 'Oita', 0, 0, 32),
(1899, 'JP', '31', 'Okayama', 0, 0, 45),
(1900, 'JP', '32', 'Osaka', 0, 0, 47),
(1901, 'JP', '33', 'Saga', 0, 0, 33),
(1902, 'JP', '34', 'Saitama', 0, 0, 77),
(1903, 'JP', '35', 'Shiga', 0, 0, 41),
(1904, 'JP', '36', 'Shimane', 0, 0, 47),
(1905, 'JP', '37', 'Shizuoka', 0, 0, 52),
(1906, 'JP', '38', 'Tochigi', 0, 0, 43),
(1907, 'JP', '39', 'Tokushima', 0, 0, 42),
(1908, 'JP', '40', 'Tokyo', 0, 0, 97),
(1909, 'JP', '41', 'Tottori', 0, 0, 16),
(1910, 'JP', '42', 'Toyama', 0, 0, 47),
(1911, 'JP', '43', 'Wakayama', 0, 0, 51),
(1912, 'JP', '44', 'Yamagata', 0, 0, 49),
(1913, 'JP', '45', 'Yamaguchi', 0, 0, 54),
(1914, 'JP', '46', 'Yamanashi', 0, 0, 20),
(1915, 'JP', '47', 'Okinawa', 0, 0, 99),
(1916, 'KE', '01', 'Central', 0, 0, 9),
(1917, 'KE', '02', 'Coast', 0, 0, 3),
(1918, 'KE', '03', 'Eastern', 0, 0, 2),
(1919, 'KE', '05', 'Nairobi Area', 0, 0, 2),
(1920, 'KE', '06', 'North-Eastern', 1.123543, 40.293748, 0),
(1921, 'KE', '07', 'Nyanza', 0, 0, 4),
(1922, 'KE', '08', 'Rift Valley', 0, 0, 6),
(1923, 'KE', '09', 'Western', 0, 0, 2),
(1924, 'KG', '01', 'Bishkek', 0, 0, 8),
(1925, 'KG', '02', 'Chuy', 0, 0, 7),
(1926, 'KG', '03', 'Jalal-Abad', 0, 0, 2),
(1927, 'KG', '04', 'Naryn', 0, 0, 2),
(1928, 'KG', '06', 'Talas', 0, 0, 1),
(1929, 'KG', '07', 'Ysyk-Kol', 0, 0, 6),
(1930, 'KG', '08', 'Osh', 39.977753, 73.09539, 0),
(1931, 'KG', '09', 'Batken', 0, 0, 1),
(1932, 'KH', '02', 'Kampong Cham', 12.061499, 105.642132, 0),
(1933, 'KH', '03', 'Kampong Chhnang', 12.174812, 104.544854, 0),
(1934, 'KH', '04', 'Kampong Spoe', 11.4582, 104.5088, 0),
(1935, 'KH', '05', 'Kampong Thum', 12.7206, 104.8882, 0),
(1936, 'KH', '06', 'Kampot', 10.785451, 104.32603, 0),
(1937, 'KH', '07', 'Kandal', 11.39354, 104.978661, 0),
(1938, 'KH', '08', 'Kaoh Kong', 11.42569, 103.018997, 0),
(1939, 'KH', '09', 'Kracheh', 12.4924, 106.0252, 0),
(1940, 'KH', '10', 'Mondol Kiri', 12.741562, 106.979027, 0),
(1941, 'KH', '11', 'Phnum Penh', 0, 0, 1),
(1942, 'KH', '12', 'Pouthisat', 12.5279, 103.925, 0),
(1943, 'KH', '13', 'Preah Vihear', 13.766696, 105.120397, 0),
(1944, 'KH', '14', 'Prey Veng', 11.357923, 105.470314, 0),
(1945, 'KH', '15', 'Rotanokiri', 0, 0, 0),
(1946, 'KH', '16', 'Siemreab-Otdar Meanchey', 0, 0, 0),
(1947, 'KH', '17', 'Stoeng Treng', 13.5181, 105.9695, 0),
(1948, 'KH', '18', 'Svay Rieng', 11.188251, 105.895714, 0),
(1949, 'KH', '19', 'Takev', 0, 0, 0),
(1950, 'KH', '29', 'Batdambang', 13.1122, 103.2126, 0),
(1951, 'KH', '30', 'Pailin', 12.85, 102.583333, 0),
(1952, 'KI', '01', 'Gilbert Islands', 0.359344, 173.186325, 0),
(1953, 'KI', '02', 'Line Islands', -3.356279, -155.317575, 0),
(1954, 'KI', '03', 'Phoenix Islands', -3.730257, -172.62754, 0),
(1955, 'KM', '01', 'Anjouan', -12.223382, 44.37088, 0),
(1956, 'KM', '02', 'Grande Comore', 0, 0, 1),
(1957, 'KM', '03', 'Moheli', -12.315185, 43.741028, 0),
(1958, 'KN', '01', 'Christ Church Nichola Town', 0, 0, 0),
(1959, 'KN', '02', 'Saint Anne Sandy Point', 0, 0, 1),
(1960, 'KN', '03', 'Saint George Basseterre', 0, 0, 1),
(1961, 'KN', '04', 'Saint George Gingerland', 0, 0, 0),
(1962, 'KN', '05', 'Saint James Windward', 0, 0, 0),
(1963, 'KN', '06', 'Saint John Capisterre', 0, 0, 0),
(1964, 'KN', '07', 'Saint John Figtree', 0, 0, 1),
(1965, 'KN', '08', 'Saint Mary Cayon', 0, 0, 1),
(1966, 'KN', '09', 'Saint Paul Capisterre', 0, 0, 0),
(1967, 'KN', '10', 'Saint Paul Charlestown', 0, 0, 0),
(1968, 'KN', '11', 'Saint Peter Basseterre', 0, 0, 0),
(1969, 'KN', '12', 'Saint Thomas Lowland', 0, 0, 0),
(1970, 'KN', '13', 'Saint Thomas Middle Island', 0, 0, 1),
(1971, 'KN', '15', 'Trinity Palmetto Point', 0, 0, 0),
(1972, 'KP', '01', 'Chagang-do', 0, 0, 0),
(1973, 'KP', '03', 'Hamgyong-namdo', 0, 0, 0),
(1974, 'KP', '06', 'Hwanghae-namdo', 0, 0, 0),
(1975, 'KP', '07', 'Hwanghae-bukto', 0, 0, 0),
(1976, 'KP', '08', 'Kaesong-si', 0, 0, 0),
(1977, 'KP', '09', 'Kangwon-do', 0, 0, 0),
(1978, 'KP', '11', 'P\'yongan-bukto', 0, 0, 0),
(1979, 'KP', '12', 'P\'yongyang-si', 0, 0, 0),
(1980, 'KP', '13', 'Yanggang-do', 0, 0, 0),
(1981, 'KP', '14', 'Namp\'o-si', 0, 0, 0),
(1982, 'KP', '15', 'P\'yongan-namdo', 0, 0, 0),
(1983, 'KP', '17', 'Hamgyong-bukto', 0, 0, 0),
(1984, 'KP', '18', 'Najin Sonbong-si', 0, 0, 0),
(1985, 'KR', '01', 'Cheju-do', 0, 0, 9),
(1986, 'KR', '03', 'Cholla-bukto', 0, 0, 65),
(1987, 'KR', '05', 'Ch\'ungch\'ong-bukto', 0, 0, 49),
(1988, 'KR', '06', 'Kangwon-do', 0, 0, 34),
(1989, 'KR', '10', 'Pusan-jikhalsi', 0, 0, 17),
(1990, 'KR', '11', 'Seoul-t\'ukpyolsi', 0, 0, 33),
(1991, 'KR', '12', 'Inch\'on-jikhalsi', 0, 0, 10),
(1992, 'KR', '13', 'Kyonggi-do', 0, 0, 80),
(1993, 'KR', '14', 'Kyongsang-bukto', 0, 0, 78),
(1994, 'KR', '15', 'Taegu-jikhalsi', 0, 0, 10),
(1995, 'KR', '16', 'Cholla-namdo', 0, 0, 151),
(1996, 'KR', '17', 'Ch\'ungch\'ong-namdo', 0, 0, 44),
(1997, 'KR', '18', 'Kwangju-jikhalsi', 0, 0, 23),
(1998, 'KR', '19', 'Taejon-jikhalsi', 0, 0, 3),
(1999, 'KR', '20', 'Kyongsang-namdo', 0, 0, 80),
(2000, 'KR', '21', 'Ulsan-gwangyoksi', 0, 0, 26),
(2001, 'KW', '01', 'Al Ahmadi', 28.887016, 48.005989, 0),
(2002, 'KW', '02', 'Al Kuwayt', 0, 0, 28),
(2003, 'KW', '03', 'Hawalli', 29.329404, 48.00393, 0),
(2004, 'KW', '05', 'Al Jahra', 29.332001, 47.695549, 0),
(2005, 'KY', '01', 'Creek', 19.75, -79.766667, 0),
(2006, 'KY', '02', 'Eastern', 0, 0, 0),
(2007, 'KY', '03', 'Midland', 0, 0, 0),
(2008, 'KY', '04', 'South Town', 0, 0, 0),
(2009, 'KY', '05', 'Spot Bay', 0, 0, 0),
(2010, 'KY', '06', 'Stake Bay', 0, 0, 0),
(2011, 'KY', '07', 'West End', 0, 0, 0),
(2012, 'KY', '08', 'Western', 0, 0, 0),
(2013, 'KZ', '01', 'Almaty', 0, 0, 40),
(2014, 'KZ', '02', 'Almaty City', 0, 0, 2),
(2015, 'KZ', '03', 'Aqmola', 0, 0, 19),
(2016, 'KZ', '04', '', 0, 0, 7),
(2017, 'KZ', '05', 'Astana', 0, 0, 2),
(2018, 'KZ', '06', 'Atyrau', 0, 0, 6),
(2019, 'KZ', '07', 'West Kazakhstan', 0, 0, 30),
(2020, 'KZ', '08', 'Bayqonyr', 45.865312, 63.325149, 0),
(2021, 'KZ', '09', 'Mangghystau', 0, 0, 9),
(2022, 'KZ', '10', 'South Kazakhstan', 0, 0, 12),
(2023, 'KZ', '11', 'Pavlodar', 0, 0, 18),
(2024, 'KZ', '12', 'Qaraghandy', 0, 0, 14),
(2025, 'KZ', '13', 'Qostanay', 0, 0, 18),
(2026, 'KZ', '14', 'Qyzylorda', 0, 0, 8),
(2027, 'KZ', '15', 'East Kazakhstan', 0, 0, 18),
(2028, 'KZ', '16', 'North Kazakhstan', 0, 0, 14),
(2029, 'KZ', '17', 'Zhambyl', 0, 0, 7),
(2030, 'LA', '01', 'Attapu', 0, 0, 0),
(2031, 'LA', '02', 'Champasak', 0, 0, 0),
(2032, 'LA', '03', 'Houaphan', 0, 0, 0),
(2033, 'LA', '04', 'Khammouan', 0, 0, 0),
(2034, 'LA', '05', 'Louang Namtha', 0, 0, 0),
(2035, 'LA', '07', 'Oudomxai', 0, 0, 0),
(2036, 'LA', '08', 'Phongsali', 0, 0, 0),
(2037, 'LA', '09', 'Saravan', 0, 0, 0),
(2038, 'LA', '10', 'Savannakhet', 0, 0, 0),
(2039, 'LA', '11', 'Vientiane', 0, 0, 1),
(2040, 'LA', '13', 'Xaignabouri', 0, 0, 0),
(2041, 'LA', '14', 'Xiangkhoang', 0, 0, 0),
(2042, 'LA', '17', 'Louangphrabang', 0, 0, 1),
(2043, 'LB', '01', 'Beqaa', 0, 0, 3),
(2044, 'LB', '03', 'Liban-Nord', 0, 0, 2),
(2045, 'LB', '04', 'Beyrouth', 0, 0, 6),
(2046, 'LB', '05', 'Mont-Liban', 0, 0, 8),
(2047, 'LB', '06', 'Liban-Sud', 0, 0, 1),
(2048, 'LB', '07', 'Nabatiye', 0, 0, 0),
(2049, 'LC', '01', 'Anse-la-Raye', 0, 0, 1),
(2050, 'LC', '02', 'Dauphin', -27.48884, 153.015094, 0),
(2051, 'LC', '03', 'Castries', 0, 0, 1),
(2052, 'LC', '04', 'Choiseul', 0, 0, 0),
(2053, 'LC', '05', 'Dennery', 13.91555, -60.89183, 0),
(2054, 'LC', '06', 'Gros-Islet', 14.07784, -60.945, 0),
(2055, 'LC', '07', 'Laborie', 0, 0, 0),
(2056, 'LC', '08', 'Micoud', 13.816667, -60.916667, 0),
(2057, 'LC', '09', 'Soufriere', 13.85323, -61.052071, 0),
(2058, 'LC', '10', 'Vieux-Fort', 13.73037, -60.94738, 0),
(2059, 'LC', '11', 'Praslin', 0, 0, 0),
(2060, 'LI', '01', 'Balzers', 0, 0, 1),
(2061, 'LI', '02', 'Eschen', 0, 0, 1),
(2062, 'LI', '03', 'Gamprin', 47.220298, 9.509629, 0),
(2063, 'LI', '04', 'Mauren', 0, 0, 1),
(2064, 'LI', '05', 'Planken', 0, 0, 1),
(2065, 'LI', '06', 'Ruggell', 0, 0, 1),
(2066, 'LI', '07', 'Schaan', 0, 0, 2),
(2067, 'LI', '08', 'Schellenberg', 47.230886, 9.546729, 0),
(2068, 'LI', '09', 'Triesen', 0, 0, 1),
(2069, 'LI', '10', 'Triesenberg', 0, 0, 1),
(2070, 'LI', '11', 'Vaduz', 0, 0, 1),
(2071, 'LK', '01', 'Amparai', 7.3333, 81.6166, 0),
(2072, 'LK', '02', 'Anuradhapura', 8.301361, 80.426498, 0),
(2073, 'LK', '03', 'Badulla', 6.9935, 81.0473, 0),
(2074, 'LK', '04', 'Batticaloa', 7.704834, 81.677055, 0),
(2075, 'LK', '06', 'Galle', 6.0308, 80.2387, 0),
(2076, 'LK', '07', 'Hambantota', 6.1233, 81.1174, 0),
(2077, 'LK', '09', 'Kalutara', 6.5894, 79.9573, 0),
(2078, 'LK', '10', 'Kandy', 7.3016, 80.6476, 0),
(2079, 'LK', '11', 'Kegalla', 7.2546, 80.3411, 0),
(2080, 'LK', '12', 'Kurunegala', 7.4805, 80.3664, 0),
(2081, 'LK', '14', 'Matale', 7.4437, 80.6114, 0),
(2082, 'LK', '15', 'Matara', 5.976, 80.425, 0),
(2083, 'LK', '16', 'Moneragala', 0, 0, 0),
(2084, 'LK', '17', 'Nuwara Eliya', 6.9658, 80.7578, 0),
(2085, 'LK', '18', 'Polonnaruwa', 7.932857, 81.008087, 0),
(2086, 'LK', '19', 'Puttalam', 8.033396, 79.82579, 0),
(2087, 'LK', '20', 'Ratnapura', 6.6847, 80.4036, 0),
(2088, 'LK', '21', 'Trincomalee', 8.5766, 81.2309, 0),
(2089, 'LK', '23', 'Colombo', 6.921484, 79.852779, 0),
(2090, 'LK', '24', 'Gampaha', 7.089895, 79.999413, 0),
(2091, 'LK', '25', 'Jaffna', 9.6663, 80.0203, 0),
(2092, 'LK', '26', 'Mannar', 8.9725, 79.911, 0),
(2093, 'LK', '27', 'Mullaittivu', 9.223421, 80.790939, 0),
(2094, 'LK', '28', 'Vavuniya', 8.756166, 80.493141, 0),
(2095, 'LR', '01', 'Bong', 6.616856, -9.199711, 0),
(2096, 'LR', '02', 'Grand Jide', 0, 0, 0),
(2097, 'LR', '04', 'Grand Cape Mount', 0, 0, 0),
(2098, 'LR', '05', 'Lofa', 0, 0, 0),
(2099, 'LR', '06', 'Maryland', 38.903555, -76.896904, 0),
(2100, 'LR', '07', 'Monrovia', 6.300387, -10.79573, 0),
(2101, 'LR', '09', 'Nimba', 7.5, -8.583333, 0),
(2102, 'LR', '10', 'Sino', 5.284467, -8.941356, 0),
(2103, 'LR', '11', 'Grand Bassa', 0, 0, 1),
(2104, 'LR', '14', 'Montserrado', 0, 0, 1),
(2105, 'LS', '10', 'Berea', -29.2, 27.808333, 0),
(2106, 'LS', '11', 'Butha-Buthe', -28.830807, 28.455025, 0),
(2107, 'LS', '12', 'Leribe', -28.958333, 28.108333, 0),
(2108, 'LS', '13', 'Mafeteng', -29.82243, 27.23902, 0),
(2109, 'LS', '14', 'Maseru', 0, 0, 2),
(2110, 'LS', '15', 'Mohales Hoek', -30.158529, 27.480089, 0),
(2111, 'LS', '16', 'Mokhotlong', -29.28936, 29.06063, 0),
(2112, 'LS', '17', 'Qachas Nek', -30.125959, 28.702419, 0),
(2113, 'LS', '18', 'Quthing', -30.411449, 27.716221, 0),
(2114, 'LS', '19', 'Thaba-Tseka', -29.625, 28.484013, 0),
(2115, 'LT', '56', 'Alytaus Apskritis', 0, 0, 9),
(2116, 'LT', '57', 'Kauno Apskritis', 0, 0, 19),
(2117, 'LT', '58', 'Klaipedos Apskritis', 0, 0, 9),
(2118, 'LT', '59', 'Marijampoles Apskritis', 0, 0, 9),
(2119, 'LT', '60', 'Panevezio Apskritis', 0, 0, 33),
(2120, 'LT', '61', 'Siauliu Apskritis', 0, 0, 11),
(2121, 'LT', '62', 'Taurages Apskritis', 0, 0, 9),
(2122, 'LT', '63', 'Telsiu Apskritis', 0, 0, 8),
(2123, 'LT', '64', 'Utenos Apskritis', 0, 0, 5),
(2124, 'LT', '65', 'Vilniaus Apskritis', 0, 0, 15),
(2125, 'LU', '01', 'Diekirch', 0, 0, 43),
(2126, 'LU', '02', 'Grevenmacher', 0, 0, 24),
(2127, 'LU', '03', 'Luxembourg', 0, 0, 129),
(2128, 'LV', '01', 'Aizkraukles', 0, 0, 7),
(2129, 'LV', '02', 'Aluksnes', 0, 0, 1),
(2130, 'LV', '03', 'Balvu', 0, 0, 3),
(2131, 'LV', '04', 'Bauskas', 0, 0, 6),
(2132, 'LV', '05', '', 0, 0, 7),
(2133, 'LV', '06', 'Daugavpils', 0, 0, 1),
(2134, 'LV', '08', 'Dobeles', 0, 0, 6),
(2135, 'LV', '09', 'Gulbenes', 0, 0, 4),
(2136, 'LV', '11', 'Jelgava', 56.648416, 23.713941, 0),
(2137, 'LV', '12', 'Jelgavas', 0, 0, 2),
(2138, 'LV', '13', 'Jurmala', 56.968154, 23.770527, 0),
(2139, 'LV', '15', 'Kuldigas', 0, 0, 7),
(2140, 'LV', '18', 'Limbazu', 0, 0, 4),
(2141, 'LV', '19', 'Ludzas', 0, 0, 2),
(2142, 'LV', '20', 'Madonas', 0, 0, 5),
(2143, 'LV', '21', 'Ogres', 0, 0, 7),
(2144, 'LV', '22', 'Preilu', 0, 0, 3),
(2145, 'LV', '25', 'Riga', 0, 0, 8),
(2146, 'LV', '26', 'Rigas', 0, 0, 20),
(2147, 'LV', '27', 'Saldus', 0, 0, 5),
(2148, 'LV', '28', 'Talsu', 0, 0, 7),
(2149, 'LV', '29', 'Tukuma', 0, 0, 13),
(2150, 'LV', '30', 'Valkas', 0, 0, 6),
(2151, 'LV', '31', 'Valmieras', 0, 0, 9),
(2152, 'LV', '32', 'Ventspils', 0, 0, 1),
(2153, 'LY', '03', 'Al', 32.652599, 14.27167, 0),
(2154, 'LY', '05', 'Al Jufrah', 28.491796, 16.677305, 0),
(2155, 'LY', '08', 'Al Kufrah', 24.132549, 23.268591, 0),
(2156, 'LY', '13', 'Ash Shati\'', 27.540315, 13.987545, 0),
(2157, 'LY', '30', 'Murzuq', 24.844416, 15.750545, 0),
(2158, 'LY', '34', 'Sabha', 0, 0, 1),
(2159, 'LY', '41', 'Tarhunah', 32.291528, 13.568472, 0),
(2160, 'LY', '42', 'Tubruq', 32.087, 23.9642, 0),
(2161, 'LY', '45', 'Zlitan', 32.378107, 14.583333, 0),
(2162, 'LY', '47', 'Ajdabiya', 30.105851, 20.836091, 0),
(2163, 'LY', '48', 'Al Fatih', 32.142551, 20.883333, 0),
(2164, 'LY', '49', 'Al Jabal al Akhdar', 32.54438, 21.680775, 0),
(2165, 'LY', '50', 'Al Khums', 32.652599, 14.27167, 0),
(2166, 'LY', '51', 'An Nuqat al Khams', 32.775, 12.066666, 0),
(2167, 'LY', '52', 'Awbari', 26.679131, 12.72949, 0),
(2168, 'LY', '53', 'Az Zawiyah', 31.983333, 24.666667, 0),
(2169, 'LY', '54', 'Banghazi', 0, 0, 2),
(2170, 'LY', '55', 'Darnah', 32.75735, 22.6455, 0),
(2171, 'LY', '56', 'Ghadamis', 30.139495, 9.489738, 0),
(2172, 'LY', '57', 'Gharyan', 30.350834, 13.1, 0),
(2173, 'LY', '58', 'Misratah', 0, 0, 1),
(2174, 'LY', '59', 'Sawfajjin', 31.275, 14.833639, 0),
(2175, 'LY', '60', 'Surt', 31.20528, 16.588369, 0),
(2176, 'LY', '61', 'Tarabulus', 0, 0, 1),
(2177, 'LY', '62', 'Yafran', 31.675, 12.199305, 0),
(2178, 'MA', '01', 'Agadir', 0, 0, 1),
(2179, 'MA', '02', 'Al Hoceima', 35.249299, -3.937112, 0),
(2180, 'MA', '03', 'Azilal', 31.9669, -6.5662, 0),
(2181, 'MA', '04', 'Ben Slimane', 33.62328, -7.120642, 0),
(2182, 'MA', '05', 'Beni Mellal', 32.350201, -6.358787, 0),
(2183, 'MA', '06', 'Boulemane', 0, 0, 1),
(2184, 'MA', '07', 'Casablanca', 0, 0, 4),
(2185, 'MA', '08', 'Chaouen', 0, 0, 2),
(2186, 'MA', '09', 'El Jadida', 33.255909, -8.50783, 0),
(2187, 'MA', '10', 'El Kelaa des Srarhna', 0, 0, 1),
(2188, 'MA', '11', 'Er Rachidia', 31.94474, -4.4006, 0),
(2189, 'MA', '12', 'Essaouira', 0, 0, 3),
(2190, 'MA', '13', 'Fes', 0, 0, 2),
(2191, 'MA', '14', 'Figuig', 0, 0, 1),
(2192, 'MA', '15', 'Kenitra', 0, 0, 2),
(2193, 'MA', '16', 'Khemisset', 0, 0, 2),
(2194, 'MA', '17', 'Khenifra', 32.9382, -5.6674, 0),
(2195, 'MA', '18', 'Khouribga', 32.88475, -6.91615, 0),
(2196, 'MA', '19', 'Marrakech', 0, 0, 13),
(2197, 'MA', '20', 'Meknes', 0, 0, 4),
(2198, 'MA', '21', 'Nador', 0, 0, 1),
(2199, 'MA', '22', 'Ouarzazate', 30.9173, -6.919119, 0),
(2200, 'MA', '23', 'Oujda', 0, 0, 8),
(2201, 'MA', '24', 'Rabat-Sale', 0, 0, 4),
(2202, 'MA', '25', 'Safi', 0, 0, 2),
(2203, 'MA', '26', 'Settat', 0, 0, 1),
(2204, 'MA', '27', 'Tanger', 0, 0, 2),
(2205, 'MA', '29', 'Tata', 29.7428, -7.9726, 0),
(2206, 'MA', '30', 'Taza', 34.2234, -4.0069, 0),
(2207, 'MA', '32', 'Tiznit', 0, 0, 1),
(2208, 'MA', '33', 'Guelmim', 0, 0, 1),
(2209, 'MA', '34', 'Ifrane', 34.05, -3.766667, 0),
(2210, 'MA', '35', 'Laayoune', 26.858333, -13.125, 0),
(2211, 'MA', '36', 'Tan-Tan', 28.432649, -11.10033, 0),
(2212, 'MA', '37', 'Taounate', 0, 0, 1),
(2213, 'MA', '38', 'Sidi Kacem', 0, 0, 2),
(2214, 'MA', '39', 'Taroudannt', 0, 0, 1),
(2215, 'MA', '40', 'Tetouan', 0, 0, 2),
(2216, 'MA', '41', 'Larache', 35.1918, -6.1536, 0),
(2217, 'MC', '01', 'La Condamine', 43.734664, 7.421143, 0),
(2218, 'MC', '02', 'Monaco', 43.732529, 7.418907, 0),
(2219, 'MC', '03', 'Monte-Carlo', 43.740439, 7.425577, 0),
(2220, 'MD', '46', 'Balti', 0, 0, 2),
(2221, 'MD', '47', 'Cahul', 0, 0, 0),
(2222, 'MD', '48', 'Chisinau', 0, 0, 6),
(2223, 'MD', '49', 'Stinga Nistrului', 0, 0, 3),
(2224, 'MD', '50', 'Edinet', 0, 0, 2),
(2225, 'MD', '51', 'Gagauzia', 0, 0, 2),
(2226, 'MD', '52', 'Lapusna', 0, 0, 3),
(2227, 'MD', '53', 'Orhei', 0, 0, 1),
(2228, 'MD', '54', 'Soroca', 0, 0, 2),
(2229, 'MD', '55', 'Tighina', 0, 0, 6),
(2230, 'MD', '56', 'Ungheni', 0, 0, 1),
(2231, 'MG', '01', 'Antsiranana', -13.96909, 49.165152, 0),
(2232, 'MG', '02', 'Fianarantsoa', 0, 0, 1),
(2233, 'MG', '03', 'Mahajanga', 0, 0, 1),
(2234, 'MG', '04', 'Toamasina', 0, 0, 2),
(2235, 'MG', '05', 'Antananarivo', 0, 0, 13),
(2236, 'MG', '06', 'Toliara', -21.930395, 45.291124, 0);
INSERT INTO `tbl_regions` (`id`, `country`, `code`, `name`, `latitude`, `longitude`, `cities`) VALUES
(2237, 'MK', '01', 'Aracinovo', 0, 0, 1),
(2238, 'MK', '02', 'Bac', 0, 0, 0),
(2239, 'MK', '03', 'Belcista', 0, 0, 0),
(2240, 'MK', '04', 'Berovo', 41.711871, 22.864028, 0),
(2241, 'MK', '05', 'Bistrica', 0, 0, 0),
(2242, 'MK', '06', 'Bitola', 0, 0, 1),
(2243, 'MK', '07', 'Blatec', 41.839661, 22.593161, 0),
(2244, 'MK', '08', 'Bogdanci', 0, 0, 1),
(2245, 'MK', '09', 'Bogomila', 41.59623, 21.48348, 0),
(2246, 'MK', '10', 'Bogovinje', 0, 0, 1),
(2247, 'MK', '11', 'Bosilovo', 0, 0, 0),
(2248, 'MK', '12', 'Brvenica', 0, 0, 0),
(2249, 'MK', '13', 'Cair', 0, 0, 0),
(2250, 'MK', '14', 'Capari', 41.05, 21.183333, 0),
(2251, 'MK', '15', 'Caska', 0, 0, 0),
(2252, 'MK', '16', 'Cegrane', 41.844662, 20.99925, 0),
(2253, 'MK', '17', 'Centar', 0, 0, 0),
(2254, 'MK', '18', 'Centar Zupa', 0, 0, 0),
(2255, 'MK', '19', 'Cesinovo', 0, 0, 0),
(2256, 'MK', '20', 'Cucer-Sandevo', 0, 0, 0),
(2257, 'MK', '21', 'Debar', 41.525927, 20.536737, 0),
(2258, 'MK', '22', 'Delcevo', 0, 0, 1),
(2259, 'MK', '23', 'Delogozdi', 0, 0, 1),
(2260, 'MK', '24', 'Demir Hisar', 0, 0, 1),
(2261, 'MK', '25', 'Demir Kapija', 41.424385, 22.267475, 0),
(2262, 'MK', '26', 'Dobrusevo', 0, 0, 0),
(2263, 'MK', '27', 'Dolna Banjica', 0, 0, 0),
(2264, 'MK', '28', 'Dolneni', 41.428208, 21.454711, 0),
(2265, 'MK', '29', 'Dorce Petrov', 0, 0, 0),
(2266, 'MK', '30', 'Drugovo', 41.487342, 20.933831, 0),
(2267, 'MK', '31', 'Dzepciste', 0, 0, 0),
(2268, 'MK', '32', 'Gazi Baba', 0, 0, 0),
(2269, 'MK', '33', 'Gevgelija', 0, 0, 1),
(2270, 'MK', '34', 'Gostivar', 0, 0, 1),
(2271, 'MK', '35', 'Gradsko', 0, 0, 1),
(2272, 'MK', '36', 'Ilinden', 0, 0, 1),
(2273, 'MK', '37', 'Izvor', 41.563822, 21.685935, 0),
(2274, 'MK', '38', 'Jegunovce', 0, 0, 1),
(2275, 'MK', '39', 'Kamenjane', 0, 0, 1),
(2276, 'MK', '40', 'Karbinci', 0, 0, 1),
(2277, 'MK', '41', 'Karpos', 0, 0, 1),
(2278, 'MK', '42', 'Kavadarci', 0, 0, 1),
(2279, 'MK', '43', 'Kicevo', 0, 0, 1),
(2280, 'MK', '44', 'Kisela Voda', 0, 0, 0),
(2281, 'MK', '45', 'Klecevce', 0, 0, 0),
(2282, 'MK', '46', 'Kocani', 0, 0, 1),
(2283, 'MK', '47', 'Konce', 0, 0, 0),
(2284, 'MK', '48', 'Kondovo', 0, 0, 0),
(2285, 'MK', '49', 'Konopiste', 0, 0, 0),
(2286, 'MK', '50', 'Kosel', 0, 0, 0),
(2287, 'MK', '51', 'Kratovo', 42.077669, 22.188454, 0),
(2288, 'MK', '52', 'Kriva Palanka', 42.198016, 22.333701, 0),
(2289, 'MK', '53', 'Krivogastani', 41.3482, 21.34119, 0),
(2290, 'MK', '54', 'Krusevo', 41.378222, 21.249313, 0),
(2291, 'MK', '55', 'Kuklis', 0, 0, 0),
(2292, 'MK', '56', 'Kukurecani', 0, 0, 1),
(2293, 'MK', '57', 'Kumanovo', 0, 0, 1),
(2294, 'MK', '58', 'Labunista', 0, 0, 0),
(2295, 'MK', '59', 'Lipkovo', 42.147407, 21.589199, 0),
(2296, 'MK', '60', 'Lozovo', 0, 0, 0),
(2297, 'MK', '61', 'Lukovo', 0, 0, 0),
(2298, 'MK', '62', 'Makedonska Kamenica', 0, 0, 1),
(2299, 'MK', '63', 'Makedonski Brod', 0, 0, 1),
(2300, 'MK', '64', 'Mavrovi Anovi', 0, 0, 0),
(2301, 'MK', '65', 'Meseista', 41.243191, 20.78484, 0),
(2302, 'MK', '66', 'Miravci', 41.303411, 22.431497, 0),
(2303, 'MK', '67', 'Mogila', 41.111762, 21.393068, 0),
(2304, 'MK', '68', 'Murtino', 41.419395, 22.706132, 0),
(2305, 'MK', '69', 'Negotino', 0, 0, 1),
(2306, 'MK', '70', 'Negotino-Polosko', 0, 0, 0),
(2307, 'MK', '71', 'Novaci', 41.04707, 21.469955, 0),
(2308, 'MK', '72', 'Novo Selo', 41.418178, 22.87933, 0),
(2309, 'MK', '73', 'Oblesevo', 41.89201, 22.334049, 0),
(2310, 'MK', '74', 'Ohrid', 0, 0, 1),
(2311, 'MK', '75', 'Orasac', 0, 0, 1),
(2312, 'MK', '76', 'Orizari', 41.925785, 22.436979, 0),
(2313, 'MK', '77', 'Oslomej', 0, 0, 1),
(2314, 'MK', '78', 'Pehcevo', 41.768509, 22.901895, 0),
(2315, 'MK', '79', 'Petrovec', 0, 0, 1),
(2316, 'MK', '80', 'Plasnica', 41.462608, 21.138651, 0),
(2317, 'MK', '81', 'Podares', 0, 0, 1),
(2318, 'MK', '82', 'Prilep', 0, 0, 1),
(2319, 'MK', '83', 'Probistip', 0, 0, 0),
(2320, 'MK', '84', 'Radovis', 0, 0, 1),
(2321, 'MK', '85', 'Rankovce', 42.181734, 22.120188, 0),
(2322, 'MK', '86', 'Resen', 0, 0, 1),
(2323, 'MK', '87', 'Rosoman', 0, 0, 1),
(2324, 'MK', '88', 'Rostusa', 0, 0, 0),
(2325, 'MK', '89', 'Samokov', 0, 0, 2),
(2326, 'MK', '90', 'Saraj', 0, 0, 1),
(2327, 'MK', '91', 'Sipkovica', 0, 0, 0),
(2328, 'MK', '92', 'Sopiste', 0, 0, 1),
(2329, 'MK', '93', 'Sopotnica', 41.298389, 21.16424, 0),
(2330, 'MK', '94', 'Srbinovo', 0, 0, 0),
(2331, 'MK', '95', 'Staravina', 41.090305, 21.760409, 0),
(2332, 'MK', '96', 'Star Dojran', 0, 0, 1),
(2333, 'MK', '97', 'Staro Nagoricane', 0, 0, 0),
(2334, 'MK', '98', 'Stip', 0, 0, 1),
(2335, 'MK', '99', 'Struga', 0, 0, 1),
(2336, 'MK', 'A1', 'Strumica', 0, 0, 1),
(2337, 'MK', 'A2', 'Studenicani', 0, 0, 1),
(2338, 'MK', 'A3', 'Suto Orizari', 0, 0, 0),
(2339, 'MK', 'A4', 'Sveti Nikole', 0, 0, 1),
(2340, 'MK', 'A5', 'Tearce', 42.108794, 21.046225, 0),
(2341, 'MK', 'A6', 'Tetovo', 0, 0, 1),
(2342, 'MK', 'A7', 'Topolcani', 0, 0, 0),
(2343, 'MK', 'A8', 'Valandovo', 0, 0, 1),
(2344, 'MK', 'A9', 'Vasilevo', 0, 0, 0),
(2345, 'MK', 'B1', 'Veles', 0, 0, 1),
(2346, 'MK', 'B2', 'Velesta', 41.24696, 20.65213, 0),
(2347, 'MK', 'B3', 'Vevcani', 0, 0, 0),
(2348, 'MK', 'B4', 'Vinica', 41.879848, 22.513377, 0),
(2349, 'MK', 'B5', 'Vitoliste', 0, 0, 0),
(2350, 'MK', 'B6', 'Vranestica', 0, 0, 0),
(2351, 'MK', 'B7', 'Vrapciste', 0, 0, 0),
(2352, 'MK', 'B8', 'Vratnica', 0, 0, 1),
(2353, 'MK', 'B9', 'Vrutok', 41.76563, 20.824766, 0),
(2354, 'MK', 'C1', 'Zajas', 41.593868, 20.952858, 0),
(2355, 'MK', 'C2', 'Zelenikovo', 0, 0, 1),
(2356, 'MK', 'C3', 'Zelino', 0, 0, 1),
(2357, 'MK', 'C4', 'Zitose', 0, 0, 0),
(2358, 'MK', 'C5', 'Zletovo', 41.981483, 22.197522, 0),
(2359, 'MK', 'C6', 'Zrnovci', 0, 0, 1),
(2360, 'ML', '01', 'Bamako', 0, 0, 1),
(2361, 'ML', '03', 'Kayes', 0, 0, 1),
(2362, 'ML', '04', 'Mopti', 14.480395, -4.190015, 0),
(2363, 'ML', '05', 'Segou', 13.439598, -6.268461, 0),
(2364, 'ML', '06', 'Sikasso', 11.315983, -5.669683, 0),
(2365, 'ML', '07', 'Koulikoro', 12.863795, -7.569686, 0),
(2366, 'ML', '08', 'Tombouctou', 16.77206, -3.007782, 0),
(2367, 'ML', '09', 'Gao', 16.2668, -0.042694, 0),
(2368, 'ML', '10', 'Kidal', 18.444309, 1.401523, 0),
(2369, 'MM', '01', 'Rakhine State', 0, 0, 0),
(2370, 'MM', '02', 'Chin State', 0, 0, 0),
(2371, 'MM', '03', 'Irrawaddy', 0, 0, 0),
(2372, 'MM', '04', 'Kachin State', 0, 0, 0),
(2373, 'MM', '05', 'Karan State', 0, 0, 0),
(2374, 'MM', '06', 'Kayah State', 0, 0, 0),
(2375, 'MM', '07', 'Magwe', 20.1253, 94.9318, 0),
(2376, 'MM', '08', 'Mandalay', 21.526697, 95.859206, 0),
(2377, 'MM', '09', 'Pegu', 17.3291, 96.4991, 0),
(2378, 'MM', '10', 'Sagaing', 24.473055, 95.52068, 0),
(2379, 'MM', '11', 'Shan State', 0, 0, 0),
(2380, 'MM', '12', 'Tenasserim', 12.569545, 97.854305, 0),
(2381, 'MM', '13', 'Mon State', 0, 0, 0),
(2382, 'MM', '14', 'Rangoon', 16.867, 96.1419, 0),
(2383, 'MM', '17', 'Yangon', 0, 0, 2),
(2384, 'MN', '01', 'Arhangay', 48.081193, 100.954934, 0),
(2385, 'MN', '02', 'Bayanhongor', 45.016666, 99.325, 0),
(2386, 'MN', '03', 'Bayan-Olgiy', 48.202342, 89.95168, 0),
(2387, 'MN', '05', 'Darhan', 49.90258, 106.175003, 0),
(2388, 'MN', '06', 'Dornod', 48.266506, 115.745317, 0),
(2389, 'MN', '07', 'Dornogovi', 44.55355, 109.866666, 0),
(2390, 'MN', '08', 'Dundgovi', 45.4, 106.365114, 0),
(2391, 'MN', '09', 'Dzavhan', 48.283333, 96.492791, 0),
(2392, 'MN', '10', 'Govi-Altay', 46.100966, 95.908333, 0),
(2393, 'MN', '11', 'Hentiy', 47.783333, 110.458065, 0),
(2394, 'MN', '12', 'Hovd', 47.337268, 92.396063, 0),
(2395, 'MN', '13', 'Hovsgol', 49.975, 99.841666, 0),
(2396, 'MN', '14', 'Omnogovi', 43.410993, 103.741667, 0),
(2397, 'MN', '15', 'Ovorhangay', 45.797805, 102.917962, 0),
(2398, 'MN', '16', 'Selenge', 49.489476, 106.425434, 0),
(2399, 'MN', '17', 'Suhbaatar', 46.3, 113.579001, 0),
(2400, 'MN', '18', 'Tov', 47.598993, 106.5, 0),
(2401, 'MN', '19', 'Uvs', 49.718135, 92.766792, 0),
(2402, 'MN', '20', 'Ulaanbaatar', 0, 0, 1),
(2403, 'MN', '21', 'Bulgan', 48.779655, 103.346699, 0),
(2404, 'MN', '22', 'Erdenet', 48.988945, 104.140419, 0),
(2405, 'MN', '23', 'Darhan Uul', 0, 0, 0),
(2406, 'MN', '24', 'Govi-Sumber', 0, 0, 0),
(2407, 'MN', '25', 'Orhon', 0, 0, 1),
(2408, 'MO', '01', 'Ilhas', 0, 0, 3),
(2409, 'MO', '02', 'Macau', 22.201874, 113.544202, 0),
(2410, 'MR', '01', 'Hodh Ech Chargui', 16.491667, -7.092829, 0),
(2411, 'MR', '02', 'Hodh El Gharbi', 16.322221, -9.672034, 0),
(2412, 'MR', '03', 'Assaba', 16.136835, -11.82893, 0),
(2413, 'MR', '04', 'Gorgol', 15.965265, -13.09689, 0),
(2414, 'MR', '05', 'Brakna', 16.642385, -13.833333, 0),
(2415, 'MR', '06', 'Trarza', 0, 0, 1),
(2416, 'MR', '07', 'Adrar', 20.413935, -12.469033, 0),
(2417, 'MR', '08', 'Dakhlet Nouadhibou', 20.88364, -17.041511, 0),
(2418, 'MR', '09', 'Tagant', 18.15149, -10.912906, 0),
(2419, 'MR', '10', 'Guidimaka', 15.383333, -12.133333, 0),
(2420, 'MR', '11', 'Tiris Zemmour', 24.255131, -9.245411, 0),
(2421, 'MR', '12', 'Inchiri', 19.747311, -14.39091, 0),
(2422, 'MS', '01', 'Saint Anthony', 0, 0, 1),
(2423, 'MS', '02', 'Saint Georges', 0, 0, 0),
(2424, 'MS', '03', 'Saint Peter', 0, 0, 2),
(2425, 'MU', '12', 'Black River', 0, 0, 3),
(2426, 'MU', '13', 'Flacq', 0, 0, 2),
(2427, 'MU', '14', 'Grand Port', 0, 0, 3),
(2428, 'MU', '15', 'Moka', 0, 0, 9),
(2429, 'MU', '16', 'Pamplemousses', 0, 0, 7),
(2430, 'MU', '17', 'Plaines Wilhems', 0, 0, 7),
(2431, 'MU', '18', 'Port Louis', 0, 0, 1),
(2432, 'MU', '19', 'Riviere du Rempart', 0, 0, 3),
(2433, 'MU', '20', 'Savanne', 0, 0, 0),
(2434, 'MU', '21', 'Agalega Islands', 0, 0, 0),
(2435, 'MU', '22', 'Cargados Carajos', 0, 0, 0),
(2436, 'MU', '23', 'Rodrigues', 0, 0, 1),
(2437, 'MV', '01', 'Seenu', -0.639219, 73.158331, 0),
(2438, 'MV', '02', 'Aliff', 0, 0, 0),
(2439, 'MV', '03', 'Laviyani', 0, 0, 0),
(2440, 'MV', '04', 'Waavu', 0, 0, 0),
(2441, 'MV', '05', 'Laamu', 1.956545, 73.406176, 0),
(2442, 'MV', '07', 'Haa Aliff', 0, 0, 0),
(2443, 'MV', '08', 'Thaa', 2.362182, 73.137205, 0),
(2444, 'MV', '12', 'Meemu', 2.940104, 73.501436, 0),
(2445, 'MV', '13', 'Raa', 5.671056, 72.925614, 0),
(2446, 'MV', '14', 'Faafu', 3.162773, 72.937028, 0),
(2447, 'MV', '17', 'Daalu', 0, 0, 0),
(2448, 'MV', '20', 'Baa', 5.086287, 73.002712, 0),
(2449, 'MV', '23', 'Haa Daalu', 0, 0, 0),
(2450, 'MV', '24', 'Shaviyani', 6.210959, 73.104339, 0),
(2451, 'MV', '25', 'Noonu', 5.824573, 73.305436, 0),
(2452, 'MV', '26', 'Kaafu', 0, 0, 1),
(2453, 'MV', '27', 'Gaafu Aliff', 0, 0, 0),
(2454, 'MV', '28', 'Gaafu Daalu', 0, 0, 0),
(2455, 'MV', '29', 'Naviyani', 0, 0, 0),
(2456, 'MW', '02', 'Chikwawa', -16.02433, 34.786751, 0),
(2457, 'MW', '03', 'Chiradzulu', -15.68867, 35.11969, 0),
(2458, 'MW', '04', 'Chitipa', -9.700939, 33.27325, 0),
(2459, 'MW', '05', 'Thyolo', -16.0643, 35.1342, 0),
(2460, 'MW', '06', 'Dedza', -14.383815, 34.335994, 0),
(2461, 'MW', '07', 'Dowa', -13.703316, 33.963566, 0),
(2462, 'MW', '08', 'Karonga', -9.930657, 33.937891, 0),
(2463, 'MW', '09', 'Kasungu', -13.033366, 33.484166, 0),
(2464, 'MW', '11', 'Lilongwe', 0, 0, 1),
(2465, 'MW', '12', 'Mangochi', -14.483333, 35.266667, 0),
(2466, 'MW', '13', 'Mchinji', -13.79977, 32.881305, 0),
(2467, 'MW', '15', 'Mzimba', -11.897435, 33.597809, 0),
(2468, 'MW', '16', 'Ntcheu', -14.7968, 34.6251, 0),
(2469, 'MW', '17', 'Nkhata Bay', -11.607, 34.27942, 0),
(2470, 'MW', '18', 'Nkhotakota', -12.9691, 34.2665, 0),
(2471, 'MW', '19', 'Nsanje', -16.92071, 35.261884, 0),
(2472, 'MW', '20', 'Ntchisi', -13.3531, 33.9211, 0),
(2473, 'MW', '21', 'Rumphi', -11.017475, 33.814855, 0),
(2474, 'MW', '22', 'Salima', -13.78197, 34.45899, 0),
(2475, 'MW', '23', 'Zomba', 0, 0, 1),
(2476, 'MW', '24', 'Blantyre', 0, 0, 2),
(2477, 'MW', '25', 'Mwanza', -15.6051, 34.527014, 0),
(2478, 'MW', '26', 'Balaka', -14.982065, 34.95803, 0),
(2479, 'MW', '27', 'Likoma', -12.067111, 34.734153, 0),
(2480, 'MW', '28', 'Machinga', 0, 0, 0),
(2481, 'MW', '29', 'Mulanje', -16.023616, 35.49595, 0),
(2482, 'MW', '30', 'Phalombe', -15.80015, 35.646465, 0),
(2483, 'MX', '01', 'Aguascalientes', 0, 0, 4),
(2484, 'MX', '02', 'Baja California', 0, 0, 12),
(2485, 'MX', '03', 'Baja California Sur', 0, 0, 9),
(2486, 'MX', '04', 'Campeche', 0, 0, 7),
(2487, 'MX', '05', 'Chiapas', 0, 0, 34),
(2488, 'MX', '06', 'Chihuahua', 0, 0, 11),
(2489, 'MX', '07', 'Coahuila de Zaragoza', 0, 0, 22),
(2490, 'MX', '08', 'Colima', 0, 0, 4),
(2491, 'MX', '09', 'Distrito Federal', 0, 0, 31),
(2492, 'MX', '10', 'Durango', 0, 0, 9),
(2493, 'MX', '11', 'Guanajuato', 0, 0, 44),
(2494, 'MX', '12', 'Guerrero', 0, 0, 18),
(2495, 'MX', '13', 'Hidalgo', 0, 0, 33),
(2496, 'MX', '14', 'Jalisco', 0, 0, 52),
(2497, 'MX', '15', 'Mexico', 0, 0, 74),
(2498, 'MX', '16', 'Michoacan de Ocampo', 0, 0, 34),
(2499, 'MX', '17', 'Morelos', 0, 0, 23),
(2500, 'MX', '18', 'Nayarit', 0, 0, 12),
(2501, 'MX', '19', 'Nuevo Leon', 0, 0, 20),
(2502, 'MX', '20', 'Oaxaca', 0, 0, 34),
(2503, 'MX', '21', 'Puebla', 0, 0, 54),
(2504, 'MX', '22', 'Queretaro de Arteaga', 0, 0, 10),
(2505, 'MX', '23', 'Quintana Roo', 0, 0, 7),
(2506, 'MX', '24', 'San Luis Potosi', 0, 0, 11),
(2507, 'MX', '25', 'Sinaloa', 0, 0, 22),
(2508, 'MX', '26', 'Sonora', 0, 0, 25),
(2509, 'MX', '27', 'Tabasco', 0, 0, 20),
(2510, 'MX', '28', 'Tamaulipas', 0, 0, 18),
(2511, 'MX', '29', 'Tlaxcala', 0, 0, 30),
(2512, 'MX', '30', 'Veracruz-Llave', 0, 0, 65),
(2513, 'MX', '31', 'Yucatan', 0, 0, 10),
(2514, 'MX', '32', 'Zacatecas', 0, 0, 18),
(2515, 'MY', '01', 'Johor', 0, 0, 30),
(2516, 'MY', '02', 'Kedah', 0, 0, 13),
(2517, 'MY', '03', 'Kelantan', 0, 0, 5),
(2518, 'MY', '04', 'Melaka', 0, 0, 11),
(2519, 'MY', '05', 'Negeri Sembilan', 0, 0, 27),
(2520, 'MY', '06', 'Pahang', 0, 0, 20),
(2521, 'MY', '07', 'Perak', 0, 0, 32),
(2522, 'MY', '08', 'Perlis', 0, 0, 6),
(2523, 'MY', '09', 'Pulau Pinang', 0, 0, 22),
(2524, 'MY', '11', 'Sarawak', 0, 0, 19),
(2525, 'MY', '12', 'Selangor', 0, 0, 22),
(2526, 'MY', '13', 'Terengganu', 0, 0, 14),
(2527, 'MY', '14', 'Wilayah Persekutuan', 0, 0, 7),
(2528, 'MY', '15', 'Labuan', 0, 0, 1),
(2529, 'MY', '16', 'Sabah', 0, 0, 27),
(2530, 'MZ', '01', 'Cabo Delgado', -12.333612, 39.380555, 0),
(2531, 'MZ', '02', 'Gaza', -23.304747, 32.899948, 0),
(2532, 'MZ', '03', 'Inhambane', -23.87167, 35.38688, 0),
(2533, 'MZ', '04', 'Maputo', 0, 0, 1),
(2534, 'MZ', '05', 'Sofala', 0, 0, 2),
(2535, 'MZ', '06', 'Nampula', 0, 0, 1),
(2536, 'MZ', '07', 'Niassa', 0, 0, 1),
(2537, 'MZ', '08', 'Tete', 0, 0, 2),
(2538, 'MZ', '09', 'Zambezia', 0, 0, 1),
(2539, 'MZ', '10', 'Manica', -20.0749, 34.719261, 0),
(2540, 'NA', '01', 'Bethanien', -26.50238, 17.16074, 0),
(2541, 'NA', '02', 'Caprivi Oos', 0, 0, 0),
(2542, 'NA', '03', 'Boesmanland', 0, 0, 0),
(2543, 'NA', '04', 'Gobabis', -22.505, 18.97028, 0),
(2544, 'NA', '05', 'Grootfontein', -25.016667, 16.75, 0),
(2545, 'NA', '06', 'Kaokoland', 0, 0, 0),
(2546, 'NA', '07', 'Karibib', -21.938294, 15.85231, 0),
(2547, 'NA', '08', 'Keetmanshoop', -26.5762, 18.1466, 0),
(2548, 'NA', '09', 'Luderitz', -26.6762, 15.2468, 0),
(2549, 'NA', '10', 'Maltahohe', -24.84044, 16.979435, 0),
(2550, 'NA', '11', 'Okahandja', -21.977849, 16.907895, 0),
(2551, 'NA', '12', 'Omaruru', -21.42582, 15.95642, 0),
(2552, 'NA', '13', 'Otjiwarongo', -18.083333, 13.083333, 0),
(2553, 'NA', '14', 'Outjo', -20.105135, 16.143895, 0),
(2554, 'NA', '15', 'Owambo', 0, 0, 0),
(2555, 'NA', '16', 'Rehoboth', -23.321605, 17.079619, 0),
(2556, 'NA', '17', 'Swakopmund', -22.6709, 14.5686, 0),
(2557, 'NA', '18', 'Tsumeb', -19.2666, 17.7333, 0),
(2558, 'NA', '20', 'Karasburg', -28, 18.716667, 0),
(2559, 'NA', '21', 'Windhoek', 0, 0, 1),
(2560, 'NA', '22', 'Damaraland', 0, 0, 0),
(2561, 'NA', '23', 'Hereroland Oos', 0, 0, 0),
(2562, 'NA', '24', 'Hereroland Wes', 0, 0, 0),
(2563, 'NA', '25', 'Kavango', 0, 0, 0),
(2564, 'NA', '26', 'Mariental', -24.6247, 17.961, 0),
(2565, 'NA', '27', 'Namaland', 0, 0, 0),
(2566, 'NA', '28', 'Caprivi', 0, 0, 0),
(2567, 'NA', '29', 'Erongo', 0, 0, 2),
(2568, 'NA', '30', 'Hardap', 0, 0, 1),
(2569, 'NA', '31', 'Karas', 0, 0, 0),
(2570, 'NA', '32', 'Kunene', 0, 0, 0),
(2571, 'NA', '33', 'Ohangwena', -17.467409, 15.897137, 0),
(2572, 'NA', '34', 'Okavango', 0, 0, 0),
(2573, 'NA', '35', 'Omaheke', 0, 0, 0),
(2574, 'NA', '36', 'Omusati', 0, 0, 0),
(2575, 'NA', '37', 'Oshana', 0, 0, 1),
(2576, 'NA', '38', 'Oshikoto', 0, 0, 0),
(2577, 'NA', '39', 'Otjozondjupa', 0, 0, 0),
(2578, 'NE', '01', 'Agadez', 16.9918, 7.985669, 0),
(2579, 'NE', '02', 'Diffa', 13.31166, 12.60946, 0),
(2580, 'NE', '03', 'Dosso', 13.04794, 3.201155, 0),
(2581, 'NE', '04', 'Maradi', 0, 0, 1),
(2582, 'NE', '05', 'Niamey', 13.51405, 2.1073, 0),
(2583, 'NE', '06', 'Tahoua', 14.89715, 5.264483, 0),
(2584, 'NE', '07', 'Zinder', 13.7986, 8.9891, 0),
(2585, 'NG', '05', 'Lagos', 0, 0, 9),
(2586, 'NG', '11', 'Abuja Capital Territory', 0, 0, 2),
(2587, 'NG', '16', 'Ogun', 0, 0, 6),
(2588, 'NG', '21', 'Akwa Ibom', 0, 0, 2),
(2589, 'NG', '22', 'Cross River', 0, 0, 1),
(2590, 'NG', '23', 'Kaduna', 0, 0, 2),
(2591, 'NG', '24', 'Katsina', 0, 0, 1),
(2592, 'NG', '25', 'Anambra', 0, 0, 3),
(2593, 'NG', '26', 'Benue', 0, 0, 1),
(2594, 'NG', '27', 'Borno', 0, 0, 1),
(2595, 'NG', '28', 'Imo', 0, 0, 1),
(2596, 'NG', '29', 'Kano', 0, 0, 1),
(2597, 'NG', '30', 'Kwara', 0, 0, 1),
(2598, 'NG', '31', 'Niger', 0, 0, 1),
(2599, 'NG', '32', 'Oyo', 0, 0, 18),
(2600, 'NG', '35', 'Adamawa', 0, 0, 2),
(2601, 'NG', '36', 'Delta', 0, 0, 2),
(2602, 'NG', '37', 'Edo', 0, 0, 4),
(2603, 'NG', '39', 'Jigawa', 0, 0, 1),
(2604, 'NG', '40', 'Kebbi', 12.033333, 4.633333, 0),
(2605, 'NG', '41', 'Kogi', 0, 0, 1),
(2606, 'NG', '42', 'Osun', 0, 0, 2),
(2607, 'NG', '43', 'Taraba', 8.716667, 13.15, 0),
(2608, 'NG', '44', 'Yobe', 0, 0, 0),
(2609, 'NG', '45', 'Abia', 0, 0, 1),
(2610, 'NG', '46', 'Bauchi', 0, 0, 1),
(2611, 'NG', '47', 'Enugu', 0, 0, 3),
(2612, 'NG', '48', 'Ondo', 0, 0, 1),
(2613, 'NG', '49', 'Plateau', 0, 0, 0),
(2614, 'NG', '50', 'Rivers', 0, 0, 2),
(2615, 'NG', '51', 'Sokoto', 0, 0, 2),
(2616, 'NG', '52', 'Bayelsa', 0, 0, 0),
(2617, 'NG', '53', 'Ebonyi', 0, 0, 4),
(2618, 'NG', '54', 'Ekiti', 7.6, 5.1, 0),
(2619, 'NG', '55', 'Gombe', 0, 0, 1),
(2620, 'NG', '56', 'Nassarawa', 0, 0, 2),
(2621, 'NG', '57', 'Zamfara', 0, 0, 2),
(2622, 'NI', '01', 'Boaco', 0, 0, 2),
(2623, 'NI', '02', 'Carazo', 0, 0, 1),
(2624, 'NI', '03', 'Chinandega', 0, 0, 1),
(2625, 'NI', '04', 'Chontales', 12.130165, -85.10038, 0),
(2626, 'NI', '05', 'Esteli', 13.147049, -86.423076, 0),
(2627, 'NI', '06', 'Granada', 0, 0, 3),
(2628, 'NI', '07', 'Jinotega', 13.804153, -85.54496, 0),
(2629, 'NI', '08', 'Leon', 0, 0, 4),
(2630, 'NI', '09', 'Madriz', 13.427183, -86.418978, 0),
(2631, 'NI', '10', 'Managua', 0, 0, 3),
(2632, 'NI', '11', 'Masaya', 0, 0, 3),
(2633, 'NI', '12', 'Matagalpa', 0, 0, 1),
(2634, 'NI', '13', 'Nueva Segovia', 13.771502, -86.269413, 0),
(2635, 'NI', '14', 'Rio San Juan', 11.294, -84.423649, 0),
(2636, 'NI', '15', 'Rivas', 0, 0, 2),
(2637, 'NI', '16', 'Zelaya', 0, 0, 0),
(2638, 'NL', '01', 'Drenthe', 0, 0, 212),
(2639, 'NL', '02', 'Friesland', 0, 0, 337),
(2640, 'NL', '03', 'Gelderland', 0, 0, 413),
(2641, 'NL', '04', 'Groningen', 0, 0, 188),
(2642, 'NL', '05', 'Limburg', 0, 0, 269),
(2643, 'NL', '06', 'Noord-Brabant', 0, 0, 412),
(2644, 'NL', '07', 'Noord-Holland', 0, 0, 283),
(2645, 'NL', '08', 'Overijssel', 52.486132, 6.42529, 0),
(2646, 'NL', '09', 'Utrecht', 0, 0, 106),
(2647, 'NL', '10', 'Zeeland', 0, 0, 135),
(2648, 'NL', '11', 'Zuid-Holland', 0, 0, 294),
(2649, 'NL', '12', 'Dronten', 52.523516, 5.717889, 0),
(2650, 'NL', '13', 'Zuidelijke IJsselmeerpolders', 0, 0, 0),
(2651, 'NL', '14', 'Lelystad', 52.510007, 5.477723, 0),
(2652, 'NL', '16', 'Flevoland', 0, 0, 19),
(2653, 'NO', '01', 'Akershus', 0, 0, 84),
(2654, 'NO', '02', 'Aust-Agder', 0, 0, 37),
(2655, 'NO', '04', 'Buskerud', 0, 0, 55),
(2656, 'NO', '05', 'Finnmark', 0, 0, 40),
(2657, 'NO', '06', 'Hedmark', 0, 0, 67),
(2658, 'NO', '07', 'Hordaland', 0, 0, 232),
(2659, 'NO', '08', 'More og Romsdal', 0, 0, 113),
(2660, 'NO', '09', 'Nordland', 0, 0, 87),
(2661, 'NO', '10', 'Nord-Trondelag', 0, 0, 64),
(2662, 'NO', '11', 'Oppland', 0, 0, 67),
(2663, 'NO', '12', 'Oslo', 0, 0, 13),
(2664, 'NO', '13', 'Ostfold', 0, 0, 42),
(2665, 'NO', '14', 'Rogaland', 0, 0, 94),
(2666, 'NO', '15', 'Sogn og Fjordane', 0, 0, 67),
(2667, 'NO', '16', 'Sor-Trondelag', 0, 0, 85),
(2668, 'NO', '17', 'Telemark', 0, 0, 55),
(2669, 'NO', '18', 'Troms', 0, 0, 104),
(2670, 'NO', '19', 'Vest-Agder', 0, 0, 54),
(2671, 'NO', '20', 'Vestfold', 0, 0, 32),
(2672, 'NP', '01', 'Bagmati', 27.830105, 85.345552, 0),
(2673, 'NP', '02', 'Bheri', 28.497206, 81.721542, 0),
(2674, 'NP', '03', 'Dhawalagiri', 28.65681, 83.533581, 0),
(2675, 'NP', '04', 'Gandaki', 28.328066, 84.316917, 0),
(2676, 'NP', '05', 'Janakpur', 27.369043, 85.949856, 0),
(2677, 'NP', '06', 'Karnali', 29.571586, 82.472812, 0),
(2678, 'NP', '07', 'Kosi', 27.15545, 87.333866, 0),
(2679, 'NP', '08', 'Lumbini', 27.802501, 83.561152, 0),
(2680, 'NP', '09', 'Mahakali', 29.386108, 80.587177, 0),
(2681, 'NP', '10', 'Mechi', 27.139403, 87.83413, 0),
(2682, 'NP', '11', 'Narayani', 27.314321, 84.715374, 0),
(2683, 'NP', '12', 'Rapti', 28.337317, 82.451027, 0),
(2684, 'NP', '13', 'Sagarmatha', 27.267533, 86.653414, 0),
(2685, 'NP', '14', 'Seti', 29.232908, 81.159915, 0),
(2686, 'NR', '01', 'Aiwo', 0, 0, 0),
(2687, 'NR', '02', 'Anabar', 0, 0, 0),
(2688, 'NR', '03', 'Anetan', 0, 0, 0),
(2689, 'NR', '04', 'Anibare', 0, 0, 0),
(2690, 'NR', '05', 'Baiti', 0, 0, 0),
(2691, 'NR', '06', 'Boe', 0, 0, 0),
(2692, 'NR', '07', 'Buada', 0, 0, 0),
(2693, 'NR', '08', 'Denigomodu', 0, 0, 0),
(2694, 'NR', '09', 'Ewa', 0, 0, 0),
(2695, 'NR', '10', 'Ijuw', 0, 0, 0),
(2696, 'NR', '11', 'Meneng', 0, 0, 0),
(2697, 'NR', '12', 'Nibok', 0, 0, 0),
(2698, 'NR', '13', 'Uaboe', 0, 0, 0),
(2699, 'NR', '14', 'Yaren', -0.547189, 166.911617, 0),
(2700, 'NZ', '01', 'Akaroa', -43.806031, 172.966568, 0),
(2701, 'NZ', '03', 'Amuri', -43.537445, 172.518814, 0),
(2702, 'NZ', '04', 'Ashburton', -43.902496, 171.749326, 0),
(2703, 'NZ', '07', 'Bay of Islands', -35.181293, 174.199209, 0),
(2704, 'NZ', '08', 'Bruce', -40.75173, 175.604404, 0),
(2705, 'NZ', '09', 'Buller', -41.78452, 172.12283, 0),
(2706, 'NZ', '10', 'Chatham Islands', -44.004282, -176.38372, 0),
(2707, 'NZ', '11', 'Cheviot', -42.813045, 173.273989, 0),
(2708, 'NZ', '12', 'Clifton', -46.45028, 168.367828, 0),
(2709, 'NZ', '13', 'Clutha', -41.248878, 174.788564, 0),
(2710, 'NZ', '14', 'Cook', -41.301431, 174.77384, 0),
(2711, 'NZ', '16', 'Dannevirke', -40.207508, 176.104084, 0),
(2712, 'NZ', '17', 'Egmont', -39.266982, 174.059744, 0),
(2713, 'NZ', '18', 'Eketahuna', -40.645019, 175.703102, 0),
(2714, 'NZ', '19', 'Ellesmere', -43.663895, 172.356444, 0),
(2715, 'NZ', '20', 'Eltham', -39.429715, 174.299222, 0),
(2716, 'NZ', '21', 'Eyre', -43.379619, 172.369483, 0),
(2717, 'NZ', '22', 'Featherston', -41.117026, 175.322651, 0),
(2718, 'NZ', '24', 'Franklin', -36.851153, 174.749257, 0),
(2719, 'NZ', '26', 'Golden Bay', -40.686377, 172.687033, 0),
(2720, 'NZ', '27', 'Great Barrier Island', -36.199793, 175.428669, 0),
(2721, 'NZ', '28', 'Grey', -36.860581, 174.734626, 0),
(2722, 'NZ', '29', 'Hauraki Plains', -37.337252, 175.454916, 0),
(2723, 'NZ', '30', 'Hawera', -39.593354, 174.278291, 0),
(2724, 'NZ', '31', 'Hawke\'s Bay', -39.505076, 177.030104, 0),
(2725, 'NZ', '32', 'Heathcote', -43.577533, 172.707428, 0),
(2726, 'NZ', '33', 'Hobson', -36.849814, 174.760834, 0),
(2727, 'NZ', '34', 'Hokianga', -35.935355, 173.861988, 0),
(2728, 'NZ', '35', 'Horowhenua', -40.607551, 175.253554, 0),
(2729, 'NZ', '36', 'Hutt', -43.524026, 171.62063, 0),
(2730, 'NZ', '37', 'Inangahua', -41.859783, 171.949586, 0),
(2731, 'NZ', '38', 'Inglewood', -39.155985, 174.20723, 0),
(2732, 'NZ', '39', 'Kaikoura', -42.402306, 173.680563, 0),
(2733, 'NZ', '40', 'Kairanga', -40.348207, 175.531938, 0),
(2734, 'NZ', '41', 'Kiwitea', -40.108114, 175.721985, 0),
(2735, 'NZ', '43', 'Lake', -46.013211, 170.080408, 0),
(2736, 'NZ', '45', 'Mackenzie', -44.257969, 170.094554, 0),
(2737, 'NZ', '46', 'Malvern', -45.846128, 170.506364, 0),
(2738, 'NZ', '47', 'Manawatu', -39.504299, 175.645004, 0),
(2739, 'NZ', '48', 'Mangonui', -34.992104, 173.531193, 0),
(2740, 'NZ', '49', 'Maniototo', -45.1975, 170.028363, 0),
(2741, 'NZ', '50', 'Marlborough', -41.688202, 173.516998, 0),
(2742, 'NZ', '51', 'Masterton', -40.948949, 175.660764, 0),
(2743, 'NZ', '52', 'Matamata', -37.809565, 175.773252, 0),
(2744, 'NZ', '53', 'Mount Herbert', -43.689447, 172.741565, 0),
(2745, 'NZ', '54', 'Ohinemuri', -37.371702, 175.675373, 0),
(2746, 'NZ', '55', 'Opotiki', -38.004206, 177.287115, 0),
(2747, 'NZ', '56', 'Oroua', -40.332362, 175.411409, 0),
(2748, 'NZ', '57', 'Otamatea', -39.906905, 175.021554, 0),
(2749, 'NZ', '58', 'Otorohanga', -38.184058, 175.212606, 0),
(2750, 'NZ', '59', 'Oxford', -43.299089, 172.184525, 0),
(2751, 'NZ', '60', 'Pahiatua', -40.453381, 175.840662, 0),
(2752, 'NZ', '61', 'Paparua', -43.518313, 172.448371, 0),
(2753, 'NZ', '63', 'Patea', -39.757187, 174.47666, 0),
(2754, 'NZ', '65', 'Piako', -37.678821, 175.394655, 0),
(2755, 'NZ', '66', 'Pohangina', -40.172709, 175.793708, 0),
(2756, 'NZ', '67', 'Raglan', -37.802571, 174.872606, 0),
(2757, 'NZ', '68', 'Rangiora', -43.304038, 172.59056, 0),
(2758, 'NZ', '69', 'Rangitikei', -40.345488, 175.602947, 0),
(2759, 'NZ', '70', 'Rodney', -42.527041, 172.703428, 0),
(2760, 'NZ', '71', 'Rotorua', -38.139826, 176.247073, 0),
(2761, 'NZ', '72', 'Southland', -45.465301, 167.925995, 0),
(2762, 'NZ', '73', 'Stewart Island', -47, 167.866667, 0),
(2763, 'NZ', '74', 'Stratford', -39.337746, 174.284045, 0),
(2764, 'NZ', '76', 'Taranaki', -39.300201, 174.371994, 0),
(2765, 'NZ', '77', 'Taumarunui', -38.883033, 175.262876, 0),
(2766, 'NZ', '78', 'Taupo', -38.686594, 176.069694, 0),
(2767, 'NZ', '79', 'Tauranga', 0, 0, 0),
(2768, 'NZ', '81', 'Tuapeka', -45.936123, 169.551691, 0),
(2769, 'NZ', '82', 'Vincent', -36.900084, 174.922168, 0),
(2770, 'NZ', '83', 'Waiapu', -38.00997, 178.272191, 0),
(2771, 'NZ', '84', 'Waihemo', -45.324555, 170.569918, 0),
(2772, 'NZ', '85', 'Waikato', -40.621122, 172.678871, 0),
(2773, 'NZ', '86', 'Waikohu', -38.391125, 177.719561, 0),
(2774, 'NZ', '88', 'Waimairi', -43.480428, 172.719679, 0),
(2775, 'NZ', '89', 'Waimarino', -37.044409, 174.870204, 0),
(2776, 'NZ', '90', 'Waimate', -44.734039, 171.046498, 0),
(2777, 'NZ', '91', 'Waimate West', -35.273059, 173.901139, 0),
(2778, 'NZ', '92', 'Waimea', -45.872306, 168.683312, 0),
(2779, 'NZ', '93', 'Waipa', -38.450698, 175.394441, 0),
(2780, 'NZ', '95', 'Waipawa', -39.942547, 176.588587, 0),
(2781, 'NZ', '96', 'Waipukurau', -39.995673, 176.556677, 0),
(2782, 'NZ', '97', 'Wairarapa South', 0, 0, 0),
(2783, 'NZ', '98', 'Wairewa', -43.769883, 172.795367, 0),
(2784, 'NZ', '99', 'Wairoa', -39.039527, 177.42539, 0),
(2785, 'NZ', 'A1', 'Whangarei', -35.72287, 174.322685, 0),
(2786, 'NZ', 'A2', 'Whangaroa', -35.052229, 173.741948, 0),
(2787, 'NZ', 'A3', 'Woodville', -40.337094, 175.866697, 0),
(2788, 'NZ', 'A4', 'Waitaki', -44.937925, 171.09713, 0),
(2789, 'NZ', 'A6', 'Waitomo', -38.260916, 175.112533, 0),
(2790, 'NZ', 'A8', 'Waitotara', -39.80589, 174.734355, 0),
(2791, 'NZ', 'B2', 'Wanganui', -39.930326, 175.047663, 0),
(2792, 'NZ', 'B3', 'Westland', -45.902388, 170.424167, 0),
(2793, 'NZ', 'B4', 'Whakatane', -37.958434, 176.985889, 0),
(2794, 'NZ', 'D4', 'Hurunui', -42.880287, 172.7509, 0),
(2795, 'NZ', 'D5', 'Silverpeaks', -45.728374, 170.463893, 0),
(2796, 'NZ', 'D6', 'Strathallan', -45.888365, 170.50409, 0),
(2797, 'NZ', 'D8', 'Waiheke', -36.79504, 175.097165, 0),
(2798, 'NZ', 'D9', 'Hikurangi', -35.598386, 174.286615, 0),
(2799, 'NZ', 'E1', 'Manaia', -39.550919, 174.124862, 0),
(2800, 'NZ', 'E2', 'Runanga', -42.400668, 171.251443, 0),
(2801, 'NZ', 'E3', 'Saint Kilda', -45.904729, 170.503053, 0),
(2802, 'NZ', 'E4', 'Thames-Coromandel', -36.959957, 175.613249, 0),
(2803, 'NZ', 'E5', 'Waverley', -39.761033, 174.629861, 0),
(2804, 'NZ', 'E6', 'Wallace', -36.95069, 174.776967, 0),
(2805, 'OM', '01', 'Ad Dakhiliyah', 0, 0, 0),
(2806, 'OM', '02', 'Al Batinah', 0, 0, 4),
(2807, 'OM', '03', 'Al Wusta', 0, 0, 0),
(2808, 'OM', '04', 'Ash Sharqiyah', 0, 0, 2),
(2809, 'OM', '05', 'Az Zahirah', 0, 0, 0),
(2810, 'OM', '06', 'Masqat', 0, 0, 4),
(2811, 'OM', '07', 'Musandam', 0, 0, 0),
(2812, 'OM', '08', 'Zufar', 0, 0, 1),
(2813, 'PA', '01', 'Bocas del Toro', 0, 0, 4),
(2814, 'PA', '02', 'Chiriqui', 0, 0, 7),
(2815, 'PA', '03', 'Cocle', 0, 0, 5),
(2816, 'PA', '04', 'Colon', 0, 0, 6),
(2817, 'PA', '05', 'Darien', 0, 0, 2),
(2818, 'PA', '06', 'Herrera', 7.855381, -80.677398, 0),
(2819, 'PA', '07', 'Los Santos', 0, 0, 6),
(2820, 'PA', '08', 'Panama', 0, 0, 26),
(2821, 'PA', '09', 'San Blas', 0, 0, 1),
(2822, 'PA', '10', 'Veraguas', 0, 0, 4),
(2823, 'PE', '01', 'Amazonas', 0, 0, 3),
(2824, 'PE', '02', 'Ancash', 0, 0, 8),
(2825, 'PE', '03', 'Apurimac', 0, 0, 2),
(2826, 'PE', '04', 'Arequipa', 0, 0, 5),
(2827, 'PE', '05', 'Ayacucho', 0, 0, 3),
(2828, 'PE', '06', 'Cajamarca', 0, 0, 4),
(2829, 'PE', '07', 'Callao', 0, 0, 1),
(2830, 'PE', '08', 'Cusco', 0, 0, 4),
(2831, 'PE', '09', 'Huancavelica', 0, 0, 4),
(2832, 'PE', '10', 'Huanuco', 0, 0, 3),
(2833, 'PE', '11', 'Ica', 0, 0, 7),
(2834, 'PE', '12', 'Junin', 0, 0, 8),
(2835, 'PE', '13', 'La Libertad', 0, 0, 8),
(2836, 'PE', '14', 'Lambayeque', 0, 0, 4),
(2837, 'PE', '15', 'Lima', 0, 0, 21),
(2838, 'PE', '16', 'Loreto', 0, 0, 1),
(2839, 'PE', '17', 'Madre de Dios', 0, 0, 3),
(2840, 'PE', '18', 'Moquegua', 0, 0, 4),
(2841, 'PE', '19', 'Pasco', 0, 0, 3),
(2842, 'PE', '20', 'Piura', 0, 0, 8),
(2843, 'PE', '21', 'Puno', 0, 0, 4),
(2844, 'PE', '22', 'San Martin', 0, 0, 10),
(2845, 'PE', '23', 'Tacna', 0, 0, 7),
(2846, 'PE', '24', 'Tumbes', 0, 0, 2),
(2847, 'PE', '25', 'Ucayali', 0, 0, 3),
(2848, 'PG', '01', 'Central', -9.078217, 148.097753, 0),
(2849, 'PG', '02', 'Gulf', -7.65354, 144.92883, 0),
(2850, 'PG', '03', 'Milne Bay', 0, 0, 2),
(2851, 'PG', '04', 'Northern', -8.951506, 148.271852, 0),
(2852, 'PG', '05', 'Southern Highlands', -5.842113, 143.491669, 0),
(2853, 'PG', '06', 'Western', -7.091743, 142.383453, 0),
(2854, 'PG', '07', 'North Solomons', 0, 0, 0),
(2855, 'PG', '08', 'Chimbu', -6.30067, 144.67245, 0),
(2856, 'PG', '09', 'Eastern Highlands', -6.512004, 145.650747, 0),
(2857, 'PG', '10', 'East New Britain', -5.069804, 151.587379, 0),
(2858, 'PG', '11', 'East Sepik', -4.156437, 143.123997, 0),
(2859, 'PG', '12', 'Madang', -4.991999, 145.673834, 0),
(2860, 'PG', '13', 'Manus', -1.728484, 145.514313, 0),
(2861, 'PG', '14', 'Morobe', 0, 0, 1),
(2862, 'PG', '15', 'New Ireland', -3.086389, 151.620537, 0),
(2863, 'PG', '16', 'Western Highlands', -5.645085, 144.616932, 0),
(2864, 'PG', '17', 'West New Britain', -5.430028, 150.076101, 0),
(2865, 'PG', '18', 'Sandaun', -3.997003, 142.091103, 0),
(2866, 'PG', '19', 'Enga', -5.467761, 143.567908, 0),
(2867, 'PG', '20', 'National Capital', 0, 0, 2),
(2868, 'PH', '01', 'Abra', 0, 0, 7),
(2869, 'PH', '02', 'Agusan del Norte', 0, 0, 7),
(2870, 'PH', '03', 'Agusan del Sur', 0, 0, 9),
(2871, 'PH', '04', 'Aklan', 0, 0, 8),
(2872, 'PH', '05', 'Albay', 0, 0, 13),
(2873, 'PH', '06', 'Antique', 0, 0, 6),
(2874, 'PH', '07', 'Bataan', 0, 0, 10),
(2875, 'PH', '08', 'Batanes', 0, 0, 1),
(2876, 'PH', '09', 'Batangas', 0, 0, 21),
(2877, 'PH', '10', 'Benguet', 0, 0, 5),
(2878, 'PH', '11', 'Bohol', 0, 0, 12),
(2879, 'PH', '12', 'Bukidnon', 0, 0, 7),
(2880, 'PH', '13', 'Bulacan', 0, 0, 41),
(2881, 'PH', '14', 'Cagayan', 0, 0, 13),
(2882, 'PH', '15', 'Camarines Norte', 0, 0, 4),
(2883, 'PH', '16', 'Camarines Sur', 0, 0, 13),
(2884, 'PH', '17', 'Camiguin', 0, 0, 1),
(2885, 'PH', '18', 'Capiz', 0, 0, 11),
(2886, 'PH', '19', 'Catanduanes', 0, 0, 1),
(2887, 'PH', '20', 'Cavite', 0, 0, 17),
(2888, 'PH', '21', 'Cebu', 0, 0, 44),
(2889, 'PH', '22', 'Basilan', 0, 0, 4),
(2890, 'PH', '23', 'Eastern Samar', 0, 0, 8),
(2891, 'PH', '24', 'Davao', 0, 0, 14),
(2892, 'PH', '25', 'Davao del Sur', 0, 0, 19),
(2893, 'PH', '26', 'Davao Oriental', 0, 0, 5),
(2894, 'PH', '27', 'Ifugao', 0, 0, 2),
(2895, 'PH', '28', 'Ilocos Norte', 0, 0, 11),
(2896, 'PH', '29', 'Ilocos Sur', 0, 0, 15),
(2897, 'PH', '30', 'Iloilo', 0, 0, 39),
(2898, 'PH', '31', 'Isabela', 0, 0, 12),
(2899, 'PH', '32', 'Kalinga-Apayao', 0, 0, 2),
(2900, 'PH', '33', 'Laguna', 0, 0, 34),
(2901, 'PH', '34', 'Lanao del Norte', 0, 0, 6),
(2902, 'PH', '35', 'Lanao del Sur', 0, 0, 3),
(2903, 'PH', '36', 'La Union', 0, 0, 15),
(2904, 'PH', '37', 'Leyte', 0, 0, 22),
(2905, 'PH', '38', 'Marinduque', 0, 0, 3),
(2906, 'PH', '39', 'Masbate', 0, 0, 9),
(2907, 'PH', '40', 'Mindoro Occidental', 0, 0, 6),
(2908, 'PH', '41', 'Mindoro Oriental', 0, 0, 7),
(2909, 'PH', '42', 'Misamis Occidental', 0, 0, 6),
(2910, 'PH', '43', 'Misamis Oriental', 0, 0, 8),
(2911, 'PH', '44', 'Mountain', 0, 0, 1),
(2912, 'PH', '46', 'Negros Oriental', 0, 0, 12),
(2913, 'PH', '47', 'Nueva Ecija', 0, 0, 36),
(2914, 'PH', '48', 'Nueva Vizcaya', 0, 0, 4),
(2915, 'PH', '49', 'Palawan', 0, 0, 3),
(2916, 'PH', '50', 'Pampanga', 0, 0, 45),
(2917, 'PH', '51', 'Pangasinan', 0, 0, 39),
(2918, 'PH', '53', 'Rizal', 0, 0, 54),
(2919, 'PH', '54', 'Romblon', 0, 0, 5),
(2920, 'PH', '55', 'Samar', 0, 0, 6),
(2921, 'PH', '56', 'Maguindanao', 0, 0, 13),
(2922, 'PH', '57', 'North Cotabato', 0, 0, 7),
(2923, 'PH', '58', 'Sorsogon', 0, 0, 4),
(2924, 'PH', '59', 'Southern Leyte', 0, 0, 11),
(2925, 'PH', '60', 'Sulu', 0, 0, 7),
(2926, 'PH', '61', 'Surigao del Norte', 0, 0, 9),
(2927, 'PH', '62', 'Surigao del Sur', 0, 0, 10),
(2928, 'PH', '63', 'Tarlac', 0, 0, 30),
(2929, 'PH', '64', 'Zambales', 0, 0, 8),
(2930, 'PH', '65', 'Zamboanga del Norte', 0, 0, 1),
(2931, 'PH', '66', 'Zamboanga del Sur', 0, 0, 19),
(2932, 'PH', '67', 'Northern Samar', 0, 0, 11),
(2933, 'PH', '68', 'Quirino', 0, 0, 0),
(2934, 'PH', '69', 'Siquijor', 0, 0, 1),
(2935, 'PH', '70', 'South Cotabato', 0, 0, 18),
(2936, 'PH', '71', 'Sultan Kudarat', 0, 0, 5),
(2937, 'PH', '72', 'Tawitawi', 0, 0, 0),
(2938, 'PH', 'A1', 'Angeles', 0, 0, 2),
(2939, 'PH', 'A2', 'Bacolod', 0, 0, 1),
(2940, 'PH', 'A3', 'Bago', 0, 0, 1),
(2941, 'PH', 'A4', 'Baguio', 0, 0, 2),
(2942, 'PH', 'A5', 'Bais', 9.595244, 123.122299, 0),
(2943, 'PH', 'A6', 'Basilan City', 0, 0, 0),
(2944, 'PH', 'A7', 'Batangas City', 0, 0, 1),
(2945, 'PH', 'A8', 'Butuan', 0, 0, 2),
(2946, 'PH', 'A9', 'Cabanatuan', 0, 0, 2),
(2947, 'PH', 'B1', 'Cadiz', 0, 0, 1),
(2948, 'PH', 'B2', 'Cagayan de Oro', 0, 0, 4),
(2949, 'PH', 'B3', 'Calbayog', 0, 0, 5),
(2950, 'PH', 'B4', 'Caloocan', 0, 0, 4),
(2951, 'PH', 'B5', 'Canlaon', 0, 0, 1),
(2952, 'PH', 'B6', 'Cavite City', 0, 0, 1),
(2953, 'PH', 'B7', 'Cebu City', 0, 0, 8),
(2954, 'PH', 'B8', 'Cotabato', 0, 0, 1),
(2955, 'PH', 'B9', 'Dagupan', 16.0382, 120.337196, 0),
(2956, 'PH', 'C1', 'Danao', 0, 0, 3),
(2957, 'PH', 'C2', 'Dapitan', 8.657005, 123.432198, 0),
(2958, 'PH', 'C3', 'Davao City', 0, 0, 10),
(2959, 'PH', 'C4', 'Dipolog', 0, 0, 1),
(2960, 'PH', 'C5', 'Dumaguete', 0, 0, 1),
(2961, 'PH', 'C6', 'General Santos', 0, 0, 4),
(2962, 'PH', 'C7', 'Gingoog', 0, 0, 1),
(2963, 'PH', 'C8', 'Iligan', 0, 0, 1),
(2964, 'PH', 'C9', 'Iloilo City', 0, 0, 3),
(2965, 'PH', 'D1', 'Iriga', 0, 0, 1),
(2966, 'PH', 'D2', 'La Carlota', 0, 0, 1),
(2967, 'PH', 'D3', 'Laoag', 0, 0, 1),
(2968, 'PH', 'D4', 'Lapu-Lapu', 0, 0, 2),
(2969, 'PH', 'D5', 'Legaspi', 0, 0, 4),
(2970, 'PH', 'D6', 'Lipa', 0, 0, 2),
(2971, 'PH', 'D7', 'Lucena', 0, 0, 1),
(2972, 'PH', 'D8', 'Mandaue', 0, 0, 1),
(2973, 'PH', 'D9', 'Manila', 0, 0, 6),
(2974, 'PH', 'E1', 'Marawi', 0, 0, 1),
(2975, 'PH', 'E2', 'Naga', 0, 0, 2),
(2976, 'PH', 'E3', 'Olongapo', 0, 0, 1),
(2977, 'PH', 'E4', 'Ormoc', 0, 0, 2),
(2978, 'PH', 'E5', 'Oroquieta', 0, 0, 1),
(2979, 'PH', 'E6', 'Ozamis', 0, 0, 3),
(2980, 'PH', 'E7', 'Pagadian', 7.829801, 123.437103, 0),
(2981, 'PH', 'E8', 'Palayan', 0, 0, 0),
(2982, 'PH', 'E9', 'Pasay', 0, 0, 8),
(2983, 'PH', 'F1', 'Puerto Princesa', 0, 0, 2),
(2984, 'PH', 'F2', 'Quezon City', 0, 0, 12),
(2985, 'PH', 'F3', 'Roxas', 0, 0, 2),
(2986, 'PH', 'F4', 'San Carlos', 0, 0, 11),
(2987, 'PH', 'F6', 'San Jose', 10.75484, 121.944298, 0),
(2988, 'PH', 'F7', 'San Pablo', 0, 0, 3),
(2989, 'PH', 'F8', 'Silay', 0, 0, 2),
(2990, 'PH', 'F9', 'Surigao', 0, 0, 1),
(2991, 'PH', 'G1', 'Tacloban', 0, 0, 2),
(2992, 'PH', 'G2', 'Tagaytay', 0, 0, 1),
(2993, 'PH', 'G3', 'Tagbilaran', 0, 0, 1),
(2994, 'PH', 'G4', 'Tangub', 8.064722, 123.750198, 0),
(2995, 'PH', 'G5', 'Toledo', 0, 0, 1),
(2996, 'PH', 'G6', 'Trece Martires', 0, 0, 0),
(2997, 'PH', 'G7', 'Zamboanga', 0, 0, 2),
(2998, 'PH', 'G8', 'Aurora', 0, 0, 1),
(2999, 'PH', 'H2', 'Quezon', 0, 0, 21),
(3000, 'PH', 'H3', 'Negros Occidental', 0, 0, 22),
(3001, 'PK', '01', 'Federally Administered Tribal Areas', 33.025469, 70.551628, 0),
(3002, 'PK', '02', 'Balochistan', 29.06, 66.32, 4),
(3003, 'PK', '03', 'North-West Frontier', 0, 0, 14),
(3004, 'PK', '04', 'Punjab', 31.26, 72.59, 30),
(3005, 'PK', '05', 'Sindh', 26.06, 68.53, 9),
(3006, 'PK', '06', 'Azad Kashmir', 0, 0, 3),
(3007, 'PK', '07', 'Northern Areas', 35.806421, 75.190543, 0),
(3008, 'PK', '08', 'Islamabad', 33.691, 73.09, 1),
(3009, 'PL', '23', 'Biala Podlaska', 0, 0, 33),
(3010, 'PL', '24', 'Bialystok', 0, 0, 84),
(3011, 'PL', '25', 'Bielsko', 0, 0, 125),
(3012, 'PL', '26', 'Bydgoszcz', 0, 0, 96),
(3013, 'PL', '27', 'Chelm', 0, 0, 59),
(3014, 'PL', '28', 'Ciechanow', 0, 0, 72),
(3015, 'PL', '29', 'Czestochowa', 0, 0, 62),
(3016, 'PL', '30', 'Elblag', 0, 0, 45),
(3017, 'PL', '31', 'Gdansk', 0, 0, 110),
(3018, 'PL', '32', 'Gorzow', 0, 0, 61),
(3019, 'PL', '33', 'Jelenia Gora', 0, 0, 42),
(3020, 'PL', '34', 'Kalisz', 0, 0, 92),
(3021, 'PL', '35', 'Katowice', 0, 0, 287),
(3022, 'PL', '36', 'Kielce', 0, 0, 76),
(3023, 'PL', '37', 'Konin', 0, 0, 50),
(3024, 'PL', '38', 'Koszalin', 54.189998, 16.18, 0),
(3025, 'PL', '39', 'Krakow', 0, 0, 127),
(3026, 'PL', '40', 'Krosno', 0, 0, 65),
(3027, 'PL', '41', 'Legnica', 0, 0, 25),
(3028, 'PL', '42', 'Leszno', 0, 0, 81),
(3029, 'PL', '43', 'Lodz', 0, 0, 37),
(3030, 'PL', '44', 'Lomza', 0, 0, 36),
(3031, 'PL', '45', 'Lublin', 0, 0, 56),
(3032, 'PL', '46', 'Nowy Sacz', 0, 0, 88),
(3033, 'PL', '47', 'Olsztyn', 0, 0, 83),
(3034, 'PL', '48', 'Opole', 0, 0, 81),
(3035, 'PL', '49', 'Ostroleka', 0, 0, 47),
(3036, 'PL', '50', 'Pila', 0, 0, 81),
(3037, 'PL', '51', 'Piotrkow', 0, 0, 51),
(3038, 'PL', '52', 'Plock', 0, 0, 111),
(3039, 'PL', '53', 'Poznan', 0, 0, 164),
(3040, 'PL', '54', 'Przemysl', 0, 0, 40),
(3041, 'PL', '55', 'Radom', 0, 0, 39),
(3042, 'PL', '56', 'Rzeszow', 0, 0, 102),
(3043, 'PL', '57', 'Siedlce', 0, 0, 65),
(3044, 'PL', '58', 'Sieradz', 0, 0, 43),
(3045, 'PL', '59', 'Skierniewice', 0, 0, 96),
(3046, 'PL', '60', 'Slupsk', 0, 0, 64),
(3047, 'PL', '61', 'Suwalki', 0, 0, 71),
(3048, 'PL', '62', 'Szczecin', 53.43, 14.529999, 0),
(3049, 'PL', '63', 'Tarnobrzeg', 0, 0, 74),
(3050, 'PL', '64', 'Tarnow', 0, 0, 79),
(3051, 'PL', '65', 'Torun', 0, 0, 47),
(3052, 'PL', '66', 'Walbrzych', 0, 0, 61),
(3053, 'PL', '67', 'Warszawa', 0, 0, 169),
(3054, 'PL', '68', 'Wloclawek', 0, 0, 58),
(3055, 'PL', '69', 'Wroclaw', 0, 0, 162),
(3056, 'PL', '70', 'Zamosc', 0, 0, 68),
(3057, 'PL', '71', 'Zielona Gora', 0, 0, 88),
(3058, 'PL', '72', 'Dolnoslaskie', 0, 0, 15),
(3059, 'PL', '73', 'Kujawsko-Pomorskie', 0, 0, 1),
(3060, 'PL', '74', 'Lodzkie', 0, 0, 1),
(3061, 'PL', '75', 'Lubelskie', 0, 0, 1),
(3062, 'PL', '76', 'Lubuskie', 52.242405, 15.482204, 0),
(3063, 'PL', '77', 'Malopolskie', 0, 0, 2),
(3064, 'PL', '78', 'Mazowieckie', 52.247862, 21.195099, 0),
(3065, 'PL', '79', 'Opolskie', 0, 0, 1),
(3066, 'PL', '80', 'Podkarpackie', 49.907897, 22.348303, 0),
(3067, 'PL', '81', 'Podlaskie', 0, 0, 1),
(3068, 'PL', '82', 'Pomorskie', 0, 0, 1),
(3069, 'PL', '83', 'Slaskie', 0, 0, 1),
(3070, 'PL', '84', 'Swietokrzyskie', 0, 0, 1),
(3071, 'PL', '85', 'Warminsko-Mazurskie', 0, 0, 1),
(3072, 'PL', '86', 'Wielkopolskie', 0, 0, 1),
(3073, 'PL', '87', 'Zachodniopomorskie', 0, 0, 281),
(3074, 'PT', '02', 'Aveiro', 0, 0, 39),
(3075, 'PT', '03', 'Beja', 0, 0, 13),
(3076, 'PT', '04', 'Braga', 0, 0, 46),
(3077, 'PT', '05', 'Braganca', 0, 0, 6),
(3078, 'PT', '06', 'Castelo Branco', 0, 0, 10),
(3079, 'PT', '07', 'Coimbra', 0, 0, 45),
(3080, 'PT', '08', 'Evora', 0, 0, 16),
(3081, 'PT', '09', 'Faro', 0, 0, 26),
(3082, 'PT', '10', 'Madeira', 0, 0, 4),
(3083, 'PT', '11', 'Guarda', 0, 0, 28),
(3084, 'PT', '13', 'Leiria', 0, 0, 32),
(3085, 'PT', '14', 'Lisboa', 0, 0, 80),
(3086, 'PT', '16', 'Portalegre', 0, 0, 10),
(3087, 'PT', '17', 'Porto', 0, 0, 86),
(3088, 'PT', '18', 'Santarem', 0, 0, 36),
(3089, 'PT', '19', 'Setubal', 0, 0, 33),
(3090, 'PT', '20', 'Viana do Castelo', 0, 0, 14),
(3091, 'PT', '21', 'Vila Real', 0, 0, 16),
(3092, 'PT', '22', 'Viseu', 0, 0, 47),
(3093, 'PT', '23', 'Azores', 0, 0, 19),
(3094, 'PY', '01', 'Alto Parana', 0, 0, 5),
(3095, 'PY', '02', 'Amambay', -22.95641, -56.159425, 0),
(3096, 'PY', '03', 'Boqueron', -21.968387, -60.965015, 0),
(3097, 'PY', '04', 'Caaguazu', -25.46982, -56.01535, 0),
(3098, 'PY', '05', 'Caazapa', -26.158247, -56.047562, 0),
(3099, 'PY', '06', 'Central', 0, 0, 4),
(3100, 'PY', '07', 'Concepcion', -22.793709, -57.057619, 0),
(3101, 'PY', '08', 'Cordillera', -25.25101, -56.990919, 0),
(3102, 'PY', '10', 'Guaira', -24.079426, -54.257415, 0),
(3103, 'PY', '11', 'Itapua', 0, 0, 2),
(3104, 'PY', '12', 'Misiones', -26.985538, -57.190626, 0),
(3105, 'PY', '13', 'Neembucu', 0, 0, 1),
(3106, 'PY', '15', 'Paraguari', -26.000989, -57.144513, 0),
(3107, 'PY', '16', 'Presidente Hayes', -23.662047, -58.967518, 0),
(3108, 'PY', '17', 'San Pedro', -24.158676, -56.636503, 0),
(3109, 'PY', '19', 'Canindeyu', -24.160003, -55.21221, 0),
(3110, 'PY', '20', 'Chaco', -26.046581, -60.895736, 0),
(3111, 'PY', '21', 'Nueva Asuncion', -20.70307, -61.92321, 0),
(3112, 'PY', '23', 'Alto Paraguay', -20.83925, -59.863866, 0),
(3113, 'QA', '01', 'Ad Dawhah', 0, 0, 4),
(3114, 'QA', '02', 'Al Ghuwariyah', 0, 0, 0),
(3115, 'QA', '03', 'Al Jumaliyah', 0, 0, 0),
(3116, 'QA', '04', 'Al Khawr', 0, 0, 2),
(3117, 'QA', '06', 'Ar Rayyan', 25.302711, 51.334335, 0),
(3118, 'QA', '08', 'Madinat ach Shamal', 0, 0, 0),
(3119, 'QA', '09', 'Umm Salal', 25.52039, 51.369194, 0),
(3120, 'QA', '10', 'Al Wakrah', 25.1665, 51.5961, 0),
(3121, 'QA', '11', 'Jariyan al Batnah', 0, 0, 0),
(3122, 'QA', '12', 'Umm Sa\'id', 0, 0, 0),
(3123, 'RO', '01', 'Alba', 0, 0, 21),
(3124, 'RO', '02', 'Arad', 0, 0, 5),
(3125, 'RO', '03', 'Arges', 0, 0, 9),
(3126, 'RO', '04', 'Bacau', 0, 0, 16),
(3127, 'RO', '05', 'Bihor', 0, 0, 14),
(3128, 'RO', '06', 'Bistrita-Nasaud', 0, 0, 22),
(3129, 'RO', '07', 'Botosani', 0, 0, 21),
(3130, 'RO', '08', 'Braila', 0, 0, 12),
(3131, 'RO', '09', 'Brasov', 0, 0, 27),
(3132, 'RO', '10', 'Bucuresti', 0, 0, 15),
(3133, 'RO', '11', 'Buzau', 0, 0, 17),
(3134, 'RO', '12', 'Caras-Severin', 0, 0, 24),
(3135, 'RO', '13', 'Cluj', 0, 0, 40),
(3136, 'RO', '14', 'Constanta', 0, 0, 25),
(3137, 'RO', '15', 'Covasna', 0, 0, 14),
(3138, 'RO', '16', 'Dambovita', 0, 0, 16),
(3139, 'RO', '17', 'Dolj', 0, 0, 7),
(3140, 'RO', '18', 'Galati', 0, 0, 17),
(3141, 'RO', '19', 'Gorj', 0, 0, 8),
(3142, 'RO', '20', 'Harghita', 0, 0, 25),
(3143, 'RO', '21', 'Hunedoara', 0, 0, 15),
(3144, 'RO', '22', 'Ialomita', 0, 0, 8),
(3145, 'RO', '23', 'Iasi', 0, 0, 33),
(3146, 'RO', '25', 'Maramures', 0, 0, 20),
(3147, 'RO', '26', 'Mehedinti', 0, 0, 37),
(3148, 'RO', '27', 'Mures', 0, 0, 38),
(3149, 'RO', '28', 'Neamt', 0, 0, 24),
(3150, 'RO', '29', 'Olt', 0, 0, 10),
(3151, 'RO', '30', 'Prahova', 0, 0, 22),
(3152, 'RO', '31', 'Salaj', 0, 0, 18),
(3153, 'RO', '32', 'Satu Mare', 0, 0, 15),
(3154, 'RO', '33', 'Sibiu', 0, 0, 14),
(3155, 'RO', '34', 'Suceava', 0, 0, 21),
(3156, 'RO', '35', 'Teleorman', 0, 0, 19),
(3157, 'RO', '36', 'Timis', 0, 0, 49),
(3158, 'RO', '37', 'Tulcea', 0, 0, 8),
(3159, 'RO', '38', 'Vaslui', 0, 0, 69),
(3160, 'RO', '39', 'Valcea', 0, 0, 10),
(3161, 'RO', '40', 'Vrancea', 0, 0, 11),
(3162, 'RO', '41', 'Calarasi', 0, 0, 24),
(3163, 'RO', '42', 'Giurgiu', 0, 0, 98),
(3164, 'RO', '43', 'Ilfov', 0, 0, 21),
(3165, 'RU', '01', 'Adygeya, Republic of', 0, 0, 9),
(3166, 'RU', '02', 'Aginsky Buryatsky AO', 0, 0, 0),
(3167, 'RU', '03', 'Gorno-Altay', 0, 0, 1),
(3168, 'RU', '04', 'Altaisky krai', 0, 0, 17),
(3169, 'RU', '05', 'Amur', 0, 0, 17),
(3170, 'RU', '06', 'Arkhangel\'sk', 0, 0, 16),
(3171, 'RU', '07', 'Astrakhan\'', 0, 0, 20),
(3172, 'RU', '08', 'Bashkortostan', 0, 0, 29),
(3173, 'RU', '09', 'Belgorod', 0, 0, 19),
(3174, 'RU', '10', 'Bryansk', 0, 0, 26),
(3175, 'RU', '11', 'Buryat', 0, 0, 13),
(3176, 'RU', '12', 'Chechnya', 0, 0, 0),
(3177, 'RU', '13', 'Chelyabinsk', 0, 0, 30),
(3178, 'RU', '14', 'Chita', 0, 0, 13),
(3179, 'RU', '15', 'Chukot', 0, 0, 6),
(3180, 'RU', '16', 'Chuvashia', 0, 0, 6),
(3181, 'RU', '17', 'Dagestan', 0, 0, 15),
(3182, 'RU', '18', 'Evenk', 0, 0, 4),
(3183, 'RU', '19', 'Ingush', 0, 0, 0),
(3184, 'RU', '20', 'Irkutsk', 0, 0, 26),
(3185, 'RU', '21', 'Ivanovo', 0, 0, 17),
(3186, 'RU', '22', 'Kabardin-Balkar', 0, 0, 7),
(3187, 'RU', '23', 'Kaliningrad', 0, 0, 24),
(3188, 'RU', '24', 'Kalmyk', 0, 0, 10),
(3189, 'RU', '25', 'Kaluga', 0, 0, 55),
(3190, 'RU', '26', 'Kamchatka', 0, 0, 3),
(3191, 'RU', '27', 'Karachay-Cherkess', 0, 0, 9),
(3192, 'RU', '28', 'Karelia', 0, 0, 20),
(3193, 'RU', '29', 'Kemerovo', 0, 0, 20),
(3194, 'RU', '30', 'Khabarovsk', 0, 0, 31),
(3195, 'RU', '31', 'Khakass', 0, 0, 6),
(3196, 'RU', '32', 'Khanty-Mansiy', 0, 0, 18),
(3197, 'RU', '33', 'Kirov', 0, 0, 50),
(3198, 'RU', '34', 'Komi', 0, 0, 24),
(3199, 'RU', '35', 'Komi-Permyak', 0, 0, 41),
(3200, 'RU', '36', 'Koryak', 0, 0, 4),
(3201, 'RU', '37', 'Kostroma', 0, 0, 6),
(3202, 'RU', '38', 'Krasnodar', 0, 0, 135),
(3203, 'RU', '39', 'Krasnoyarsk', 0, 0, 45),
(3204, 'RU', '40', 'Kurgan', 0, 0, 14),
(3205, 'RU', '41', 'Kursk', 0, 0, 14),
(3206, 'RU', '42', 'Leningrad', 0, 0, 65),
(3207, 'RU', '43', 'Lipetsk', 0, 0, 13),
(3208, 'RU', '44', 'Magadan', 0, 0, 10),
(3209, 'RU', '45', 'Mariy-El', 0, 0, 7),
(3210, 'RU', '46', 'Mordovia', 0, 0, 10),
(3211, 'RU', '47', 'Moskva', 0, 0, 126),
(3212, 'RU', '48', 'Moscow City', 0, 0, 15),
(3213, 'RU', '49', 'Murmansk', 0, 0, 16),
(3214, 'RU', '50', 'Nenets', 0, 0, 3),
(3215, 'RU', '51', 'Nizhegorod', 0, 0, 29),
(3216, 'RU', '52', 'Novgorod', 0, 0, 43),
(3217, 'RU', '53', 'Novosibirsk', 0, 0, 69),
(3218, 'RU', '54', 'Omsk', 0, 0, 13),
(3219, 'RU', '55', 'Orenburg', 0, 0, 27),
(3220, 'RU', '56', 'Orel', 0, 0, 18),
(3221, 'RU', '57', 'Penza', 0, 0, 10),
(3222, 'RU', '58', 'Perm\'', 0, 0, 50),
(3223, 'RU', '59', 'Primor\'ye', 0, 0, 68),
(3224, 'RU', '60', 'Pskov', 0, 0, 15),
(3225, 'RU', '61', 'Rostov', 0, 0, 101),
(3226, 'RU', '62', 'Ryazan\'', 0, 0, 16),
(3227, 'RU', '63', 'Sakha', 0, 0, 36),
(3228, 'RU', '64', 'Sakhalin', 0, 0, 39),
(3229, 'RU', '65', 'Samara', 0, 0, 21),
(3230, 'RU', '66', 'Saint Petersburg City', 0, 0, 13),
(3231, 'RU', '67', 'Saratov', 0, 0, 58),
(3232, 'RU', '68', 'North Ossetia', 0, 0, 14),
(3233, 'RU', '69', 'Smolensk', 0, 0, 83),
(3234, 'RU', '70', 'Stavropol\'', 0, 0, 46),
(3235, 'RU', '71', 'Sverdlovsk', 0, 0, 81),
(3236, 'RU', '72', 'Tambovskaya oblast', 0, 0, 13),
(3237, 'RU', '73', 'Tatarstan', 0, 0, 22),
(3238, 'RU', '74', 'Taymyr', 0, 0, 7),
(3239, 'RU', '75', 'Tomsk', 0, 0, 16),
(3240, 'RU', '76', 'Tula', 0, 0, 17),
(3241, 'RU', '77', 'Tver\'', 0, 0, 39),
(3242, 'RU', '78', 'Tyumen\'', 0, 0, 20),
(3243, 'RU', '79', 'Tuva', 0, 0, 3),
(3244, 'RU', '80', 'Udmurt', 0, 0, 15),
(3245, 'RU', '81', 'Ul\'yanovsk', 0, 0, 9),
(3246, 'RU', '82', 'Ust-Orda Buryat', 0, 0, 4),
(3247, 'RU', '83', 'Vladimir', 0, 0, 25),
(3248, 'RU', '84', 'Volgograd', 0, 0, 144),
(3249, 'RU', '85', 'Vologda', 0, 0, 44),
(3250, 'RU', '86', 'Voronezh', 0, 0, 43),
(3251, 'RU', '87', 'Yamal-Nenets', 0, 0, 10),
(3252, 'RU', '88', 'Yaroslavl\'', 0, 0, 17),
(3253, 'RU', '89', 'Yevrey', 0, 0, 5),
(3254, 'RW', '01', 'Butare', 0, 0, 1),
(3255, 'RW', '02', 'Byumba', -1.622023, 30.12122, 0),
(3256, 'RW', '03', 'Cyangugu', -2.4868, 28.903087, 0),
(3257, 'RW', '04', 'Gikongoro', -2.5075, 29.5954, 0),
(3258, 'RW', '05', 'Gisenyi', -1.69535, 29.260534, 0),
(3259, 'RW', '06', 'Gitarama', -2.079396, 29.7552, 0),
(3260, 'RW', '07', 'Kibungo', -2.178961, 30.545739, 0),
(3261, 'RW', '08', 'Kibuye', -2.053539, 29.375439, 0),
(3262, 'RW', '09', 'Kigali', 0, 0, 1),
(3263, 'RW', '10', 'Ruhengeri', -1.503638, 29.628554, 0),
(3264, 'SA', '02', 'Al Bahah', 0, 0, 1),
(3265, 'SA', '03', 'Al Jawf', 29.563376, 39.879017, 0),
(3266, 'SA', '05', 'Al Madinah', 0, 0, 3),
(3267, 'SA', '06', 'Ash Sharqiyah', 0, 0, 17),
(3268, 'SA', '08', 'Al Qasim', 25.974025, 43.117052, 0),
(3269, 'SA', '09', 'Al Qurayyat', 0, 0, 0),
(3270, 'SA', '10', 'Ar Riyad', 0, 0, 6),
(3271, 'SA', '13', 'Ha\'il', 27.091878, 41.895261, 0),
(3272, 'SA', '14', 'Makkah', 0, 0, 11),
(3273, 'SA', '15', 'Al Hudud ash Shamaliyah', 29.809083, 41.890762, 0),
(3274, 'SA', '16', 'Najran', 18.209735, 45.682905, 0),
(3275, 'SA', '17', 'Jizan', 0, 0, 5),
(3276, 'SA', '19', 'Tabuk', 28.150092, 37.33449, 0),
(3277, 'SB', '03', 'Malaita', -8.979573, 160.9702, 0),
(3278, 'SB', '04', 'Western', 0, 0, 0),
(3279, 'SB', '05', 'Central', 0, 0, 0),
(3280, 'SB', '06', 'Guadalcanal', -9.595812, 160.208984, 0),
(3281, 'SB', '07', 'Isabel', 0, 0, 0),
(3282, 'SB', '08', 'Makira', 0, 0, 1),
(3283, 'SB', '09', 'Temotu', 0, 0, 0),
(3284, 'SC', '01', 'Anse aux Pins', 0, 0, 0),
(3285, 'SC', '02', 'Anse Boileau', -4.7137, 55.486, 0),
(3286, 'SC', '03', 'Anse Etoile', 0, 0, 0),
(3287, 'SC', '04', 'Anse Louis', 0, 0, 0),
(3288, 'SC', '05', 'Anse Royale', 0, 0, 0),
(3289, 'SC', '06', 'Baie Lazare', 0, 0, 0),
(3290, 'SC', '07', 'Baie Sainte Anne', 0, 0, 0),
(3291, 'SC', '08', 'Beau Vallon', 0, 0, 1),
(3292, 'SC', '09', 'Bel Air', 0, 0, 0),
(3293, 'SC', '10', 'Bel Ombre', 0, 0, 0),
(3294, 'SC', '11', 'Cascade', 0, 0, 0),
(3295, 'SC', '12', 'Glacis', 0, 0, 0),
(3296, 'SC', '13', 'Grand\' Anse', 0, 0, 0),
(3297, 'SC', '15', 'La Digue', -4.353773, 55.836973, 0),
(3298, 'SC', '16', 'La Riviere Anglaise', 0, 0, 0),
(3299, 'SC', '17', 'Mont Buxton', 0, 0, 0),
(3300, 'SC', '18', 'Mont Fleuri', 0, 0, 0),
(3301, 'SC', '19', 'Plaisance', 0, 0, 0),
(3302, 'SC', '20', 'Pointe La Rue', 0, 0, 0),
(3303, 'SC', '21', 'Port Glaud', 0, 0, 0),
(3304, 'SC', '22', 'Saint Louis', 0, 0, 0),
(3305, 'SC', '23', 'Takamaka', 0, 0, 0),
(3306, 'SD', '27', 'Al Wusta', 0, 0, 0),
(3307, 'SD', '28', 'Al Istiwa\'iyah', 0, 0, 0),
(3308, 'SD', '29', 'Al Khartum', 0, 0, 1),
(3309, 'SD', '30', 'Ash Shamaliyah', 0, 0, 0),
(3310, 'SD', '31', 'Ash Sharqiyah', 0, 0, 0),
(3311, 'SD', '32', 'Bahr al Ghazal', 0, 0, 0),
(3312, 'SD', '33', 'Darfur', 13.4, 24, 0),
(3313, 'SD', '34', 'Kurdufan', 0, 0, 0),
(3314, 'SE', '01', 'Alvsborgs Lan', 56.437575, 12.889097, 0),
(3315, 'SE', '02', 'Blekinge Lan', 0, 0, 10),
(3316, 'SE', '03', 'Gavleborgs Lan', 0, 0, 62),
(3317, 'SE', '04', 'Goteborgs och Bohus Lan', 0, 0, 0),
(3318, 'SE', '05', 'Gotlands Lan', 0, 0, 10),
(3319, 'SE', '06', 'Hallands Lan', 0, 0, 37),
(3320, 'SE', '07', 'Jamtlands Lan', 0, 0, 49),
(3321, 'SE', '08', 'Jonkopings Lan', 0, 0, 77),
(3322, 'SE', '09', 'Kalmar Lan', 0, 0, 51),
(3323, 'SE', '10', 'Dalarnas Lan', 0, 0, 72),
(3324, 'SE', '11', 'Kristianstads Lan', 56.428945, 13.117327, 0),
(3325, 'SE', '12', 'Kronobergs Lan', 0, 0, 33),
(3326, 'SE', '13', 'Malmohus Lan', 55.60223, 12.987279, 0),
(3327, 'SE', '14', 'Norrbottens Lan', 0, 0, 54),
(3328, 'SE', '15', 'Orebro Lan', 0, 0, 44),
(3329, 'SE', '16', 'Ostergotlands Lan', 0, 0, 39),
(3330, 'SE', '17', 'Skaraborgs Lan', 58.384175, 13.436919, 0),
(3331, 'SE', '18', 'Sodermanlands Lan', 0, 0, 46),
(3332, 'SE', '21', 'Uppsala Lan', 0, 0, 48),
(3333, 'SE', '22', 'Varmlands Lan', 0, 0, 73),
(3334, 'SE', '23', 'Vasterbottens Lan', 0, 0, 86),
(3335, 'SE', '24', 'Vasternorrlands Lan', 0, 0, 47),
(3336, 'SE', '25', 'Vastmanlands Lan', 0, 0, 51),
(3337, 'SE', '26', 'Stockholms Lan', 0, 0, 90),
(3338, 'SE', '27', 'Skane Lan', 0, 0, 159),
(3339, 'SE', '28', 'Vastra Gotaland', 0, 0, 236),
(3340, 'SH', '01', 'Ascension', -7.940109, -14.358208, 0),
(3341, 'SH', '02', 'Saint Helena', 0, 0, 0),
(3342, 'SH', '03', 'Tristan da Cunha', -37.113628, -12.283934, 0),
(3343, 'SI', '01', 'Ajdovscina', 45.88885, 13.903935, 0),
(3344, 'SI', '02', 'Beltinci', 46.6068, 16.2412, 0),
(3345, 'SI', '03', 'Bled', 46.36831, 14.11469, 0),
(3346, 'SI', '04', 'Bohinj', 0, 0, 184),
(3347, 'SI', '05', 'Borovnica', 45.92261, 14.36275, 0),
(3348, 'SI', '06', 'Bovec', 46.33694, 13.55172, 0),
(3349, 'SI', '07', 'Brda', 45.44687, 13.762882, 0);
INSERT INTO `tbl_regions` (`id`, `country`, `code`, `name`, `latitude`, `longitude`, `cities`) VALUES
(3350, 'SI', '08', 'Brezice', 45.90252, 15.59985, 0),
(3351, 'SI', '09', 'Brezovica', 0, 0, 200),
(3352, 'SI', '11', 'Celje', 46.23955, 15.2666, 0),
(3353, 'SI', '12', 'Cerklje na Gorenjskem', 0, 0, 0),
(3354, 'SI', '13', 'Cerknica', 45.79722, 14.36063, 0),
(3355, 'SI', '14', 'Cerkno', 46.12619, 13.98425, 0),
(3356, 'SI', '15', 'Crensovci', 46.57548, 16.28954, 0),
(3357, 'SI', '16', 'Crna na Koroskem', 0, 0, 0),
(3358, 'SI', '17', 'Crnomelj', 45.57516, 15.19419, 0),
(3359, 'SI', '19', 'Divaca', 45.68291, 13.96974, 0),
(3360, 'SI', '20', 'Dobrepolje', 0, 0, 0),
(3361, 'SI', '22', 'Dol pri Ljubljani', 0, 0, 0),
(3362, 'SI', '24', 'Dornava', 46.43526, 15.95499, 0),
(3363, 'SI', '25', 'Dravograd', 46.59028, 15.02837, 0),
(3364, 'SI', '26', 'Duplek', 46.51439, 15.71835, 0),
(3365, 'SI', '27', 'Gorenja Vas-Poljane', 46.10573, 14.14394, 0),
(3366, 'SI', '28', 'Gorisnica', 0, 0, 0),
(3367, 'SI', '29', 'Gornja Radgona', 46.67893, 15.9888, 0),
(3368, 'SI', '30', 'Gornji Grad', 46.29534, 14.80781, 0),
(3369, 'SI', '31', 'Gornji Petrovci', 0, 0, 0),
(3370, 'SI', '32', 'Grosuplje', 45.95653, 14.66033, 0),
(3371, 'SI', '34', 'Hrastnik', 46.14653, 15.0845, 0),
(3372, 'SI', '35', 'Hrpelje-Kozina', 45.60822, 13.93432, 0),
(3373, 'SI', '36', 'Idrija', 45.99968, 14.01831, 0),
(3374, 'SI', '37', 'Ig', 45.95857, 14.52719, 0),
(3375, 'SI', '38', 'Ilirska Bistrica', 45.57012, 14.24184, 0),
(3376, 'SI', '39', 'Ivancna Gorica', 0, 0, 0),
(3377, 'SI', '40', 'Izola-Isola', 0, 0, 0),
(3378, 'SI', '42', 'Jursinci', 46.48496, 15.97223, 0),
(3379, 'SI', '44', 'Kanal', 46.0853, 13.6343, 0),
(3380, 'SI', '45', 'Kidricevo', 0, 0, 0),
(3381, 'SI', '46', 'Kobarid', 46.245005, 13.579075, 0),
(3382, 'SI', '47', 'Kobilje', 0, 0, 0),
(3383, 'SI', '49', 'Komen', 45.81752, 13.7483, 0),
(3384, 'SI', '50', 'Koper-Capodistria', 0, 0, 0),
(3385, 'SI', '51', 'Kozje', 46.07299, 15.56108, 0),
(3386, 'SI', '52', 'Kranj', 46.23668, 14.355915, 0),
(3387, 'SI', '53', 'Kranjska Gora', 46.4832, 13.78589, 0),
(3388, 'SI', '54', 'Krsko', 45.9563, 15.49155, 0),
(3389, 'SI', '55', 'Kungota', 0, 0, 0),
(3390, 'SI', '57', 'Lasko', 46.15434, 15.23581, 0),
(3391, 'SI', '61', 'Ljubljana', 46.05456, 14.50443, 0),
(3392, 'SI', '62', 'Ljubno', 46.34217, 14.83341, 0),
(3393, 'SI', '64', 'Logatec', 45.91217, 14.22831, 0),
(3394, 'SI', '66', 'Loski Potok', 0, 0, 0),
(3395, 'SI', '68', 'Lukovica', 46.167024, 14.686032, 0),
(3396, 'SI', '71', 'Medvode', 46.14444, 14.41181, 0),
(3397, 'SI', '72', 'Menges', 46.15864, 14.56965, 0),
(3398, 'SI', '73', 'Metlika', 45.64857, 15.31298, 0),
(3399, 'SI', '74', 'Mezica', 0, 0, 0),
(3400, 'SI', '76', 'Mislinja', 46.44171, 15.19576, 0),
(3401, 'SI', '77', 'Moravce', 0, 0, 0),
(3402, 'SI', '78', 'Moravske Toplice', 46.69053, 16.22297, 0),
(3403, 'SI', '79', 'Mozirje', 46.34033, 14.96121, 0),
(3404, 'SI', '80', 'Murska Sobota', 46.662855, 16.143385, 0),
(3405, 'SI', '81', 'Muta', 46.60977, 15.16304, 0),
(3406, 'SI', '82', 'Naklo', 46.30009, 14.297552, 0),
(3407, 'SI', '83', 'Nazarje', 0, 0, 0),
(3408, 'SI', '84', 'Nova Gorica', 45.95578, 13.655045, 0),
(3409, 'SI', '86', 'Odranci', 0, 0, 0),
(3410, 'SI', '87', 'Ormoz', 46.405859, 16.153339, 0),
(3411, 'SI', '88', 'Osilnica', 0, 0, 0),
(3412, 'SI', '89', 'Pesnica', 46.617744, 15.673751, 0),
(3413, 'SI', '91', 'Pivka', 45.6829, 14.1973, 0),
(3414, 'SI', '92', 'Podcetrtek', 46.15655, 15.59705, 0),
(3415, 'SI', '94', 'Postojna', 45.791316, 14.288754, 0),
(3416, 'SI', '97', 'Puconci', 0, 0, 0),
(3417, 'SI', '98', 'Racam', 0, 0, 0),
(3418, 'SI', '99', 'Radece', 46.0667, 15.18197, 0),
(3419, 'SI', 'A1', 'Radenci', 46.64041, 16.04427, 0),
(3420, 'SI', 'A2', 'Radlje ob Dravi', 46.61434, 15.22557, 0),
(3421, 'SI', 'A3', 'Radovljica', 46.34531, 14.1713, 0),
(3422, 'SI', 'A6', 'Rogasovci', 0, 0, 0),
(3423, 'SI', 'A7', 'Rogaska Slatina', 46.232489, 15.499192, 0),
(3424, 'SI', 'A8', 'Rogatec', 46.22594, 15.70001, 0),
(3425, 'SI', 'B1', 'Semic', 45.65196, 15.18209, 0),
(3426, 'SI', 'B2', 'Sencur', 46.24234, 14.42114, 0),
(3427, 'SI', 'B3', 'Sentilj', 46.6748, 15.65691, 0),
(3428, 'SI', 'B4', 'Sentjernej', 45.84353, 15.33806, 0),
(3429, 'SI', 'B6', 'Sevnica', 46.01058, 15.30682, 0),
(3430, 'SI', 'B7', 'Sezana', 45.708835, 13.87134, 0),
(3431, 'SI', 'B8', 'Skocjan', 45.9065, 15.29222, 0),
(3432, 'SI', 'B9', 'Skofja Loka', 46.167, 14.315315, 0),
(3433, 'SI', 'C1', 'Skofljica', 45.98451, 14.57416, 0),
(3434, 'SI', 'C2', 'Slovenj Gradec', 46.481447, 15.031987, 0),
(3435, 'SI', 'C4', 'Slovenske Konjice', 46.33689, 15.42299, 0),
(3436, 'SI', 'C5', 'Smarje pri Jelsah', 0, 0, 0),
(3437, 'SI', 'C6', 'Smartno ob Paki', 46.3292, 15.03216, 0),
(3438, 'SI', 'C7', 'Sostanj', 46.38086, 15.04798, 0),
(3439, 'SI', 'C8', 'Starse', 0, 0, 0),
(3440, 'SI', 'C9', 'Store', 0, 0, 0),
(3441, 'SI', 'D1', 'Sveti Jurij', 0, 0, 0),
(3442, 'SI', 'D2', 'Tolmin', 46.184098, 13.734267, 0),
(3443, 'SI', 'D3', 'Trbovlje', 46.152185, 15.045935, 0),
(3444, 'SI', 'D4', 'Trebnje', 45.90765, 15.00592, 0),
(3445, 'SI', 'D5', 'Trzic', 46.367, 14.30885, 0),
(3446, 'SI', 'D6', 'Turnisce', 46.62843, 16.30905, 0),
(3447, 'SI', 'D7', 'Velenje', 46.362975, 15.11395, 0),
(3448, 'SI', 'D8', 'Velike Lasce', 0, 0, 0),
(3449, 'SI', 'E1', 'Vipava', 45.845295, 13.955126, 0),
(3450, 'SI', 'E2', 'Vitanje', 46.38106, 15.29411, 0),
(3451, 'SI', 'E3', 'Vodice', 45.48419, 14.0524, 0),
(3452, 'SI', 'E5', 'Vrhnika', 45.9652, 14.29766, 0),
(3453, 'SI', 'E6', 'Vuzenica', 46.59925, 15.16423, 0),
(3454, 'SI', 'E7', 'Zagorje ob Savi', 46.135333, 14.99566, 0),
(3455, 'SI', 'E9', 'Zavrc', 0, 0, 0),
(3456, 'SI', 'F1', 'Zelezniki', 0, 0, 0),
(3457, 'SI', 'F2', 'Ziri', 46.04701, 14.10966, 0),
(3458, 'SI', 'F3', 'Zrece', 46.37523, 15.38652, 0),
(3459, 'SI', 'G4', 'Dobrova-Horjul-Polhov Gradec', 46.05516, 14.41665, 0),
(3460, 'SI', 'G7', 'Domzale', 46.13606, 14.59767, 0),
(3461, 'SI', 'H4', 'Jesenice', 46.43431, 14.07109, 0),
(3462, 'SI', 'H6', 'Kamnik', 45.95512, 14.40879, 0),
(3463, 'SI', 'H7', 'Kocevje', 45.6431, 14.8607, 0),
(3464, 'SI', 'I2', 'Kuzma', 0, 0, 0),
(3465, 'SI', 'I3', 'Lenart', 46.57702, 15.83262, 0),
(3466, 'SI', 'I5', 'Litija', 46.05942, 14.82245, 0),
(3467, 'SI', 'I6', 'Ljutomer', 46.51834, 16.19556, 0),
(3468, 'SI', 'I7', 'Loska Dolina', 45.62288, 14.46245, 0),
(3469, 'SI', 'I9', 'Luce', 46.353843, 14.735691, 0),
(3470, 'SI', 'J1', 'Majsperk', 46.3515, 15.735, 0),
(3471, 'SI', 'J2', 'Maribor', 46.549225, 15.637075, 0),
(3472, 'SI', 'J5', 'Miren-Kostanjevica', 45.89463, 13.60842, 0),
(3473, 'SI', 'J7', 'Novo Mesto', 45.80565, 15.175815, 0),
(3474, 'SI', 'J9', 'Piran', 45.52217, 13.574225, 0),
(3475, 'SI', 'K5', 'Preddvor', 46.30168, 14.42151, 0),
(3476, 'SI', 'K7', 'Ptuj', 46.42563, 15.886445, 0),
(3477, 'SI', 'L1', 'Ribnica', 45.7407, 14.7289, 0),
(3478, 'SI', 'L3', 'Ruse', 46.53986, 15.50778, 0),
(3479, 'SI', 'L7', 'Sentjur pri Celju', 46.219165, 15.396265, 0),
(3480, 'SI', 'L8', 'Slovenska Bistrica', 46.389915, 15.569075, 0),
(3481, 'SI', 'N2', 'Videm', 46.576224, 16.039306, 0),
(3482, 'SI', 'N3', 'Vojnik', 46.29307, 15.3024, 0),
(3483, 'SI', 'N5', 'Zalec', 46.25197, 15.16514, 0),
(3484, 'SK', '01', 'Banska Bystrica', 0, 0, 81),
(3485, 'SK', '02', 'Bratislava', 0, 0, 41),
(3486, 'SK', '03', 'Kosice', 0, 0, 57),
(3487, 'SK', '04', 'Nitra', 0, 0, 51),
(3488, 'SK', '05', 'Presov', 0, 0, 89),
(3489, 'SK', '06', 'Trencin', 0, 0, 40),
(3490, 'SK', '07', 'Trnava', 0, 0, 41),
(3491, 'SK', '08', 'Zilina', 0, 0, 55),
(3492, 'SL', '01', 'Eastern', 8.208333, -10.941666, 0),
(3493, 'SL', '02', 'Northern', 9.155364, -11.983333, 0),
(3494, 'SL', '03', 'Southern', 7.725, -12.089071, 0),
(3495, 'SL', '04', 'Western Area', 0, 0, 1),
(3496, 'SM', '01', 'Acquaviva', 43.944991, 12.418439, 0),
(3497, 'SM', '02', 'Chiesanuova', 43.904549, 12.4204, 0),
(3498, 'SM', '03', 'Domagnano', 43.952856, 12.469444, 0),
(3499, 'SM', '04', 'Faetano', 43.925494, 12.498368, 0),
(3500, 'SM', '05', 'Fiorentino', 43.911795, 12.456732, 0),
(3501, 'SM', '06', 'Borgo Maggiore', 43.943274, 12.445957, 0),
(3502, 'SM', '07', 'San Marino', 43.932156, 12.448625, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_renewal_frequency`
--

CREATE TABLE `tbl_renewal_frequency` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `frequency_name` varchar(45) DEFAULT NULL,
  `frequency_description` varchar(120) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_resource_type`
--

CREATE TABLE `tbl_resource_type` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `resource_id` bigint(20) UNSIGNED NOT NULL,
  `resource_type` int(11) NOT NULL DEFAULT '1',
  `is_active` int(11) NOT NULL DEFAULT '1',
  `created_by` int(11) DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `date_modified` timestamp NULL DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_risk_areas`
--

CREATE TABLE `tbl_risk_areas` (
  `id` int(11) NOT NULL,
  `area_id` int(11) NOT NULL,
  `risk_group_id` int(11) NOT NULL,
  `risk_assessment_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_risk_assessment`
--

CREATE TABLE `tbl_risk_assessment` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `group_id` bigint(20) NOT NULL,
  `start_id` bigint(20) NOT NULL,
  `category` text NOT NULL,
  `question` text NOT NULL,
  `status` varchar(20) NOT NULL,
  `company_id` bigint(20) NOT NULL,
  `branch_id` bigint(20) DEFAULT NULL,
  `source_id` bigint(20) NOT NULL,
  `source_type` varchar(250) NOT NULL,
  `hazard_type` bigint(20) DEFAULT NULL,
  `hazard_description` text,
  `risk_type` bigint(20) DEFAULT NULL,
  `risk_description` text,
  `pscore` int(11) NOT NULL,
  `control_type` bigint(20) DEFAULT NULL,
  `control_description` text,
  `is_control_in_effect` int(11) DEFAULT NULL,
  `reduced_by` int(11) DEFAULT NULL,
  `new_score` int(11) DEFAULT NULL,
  `risk_notes` text NOT NULL,
  `risk_photo` text NOT NULL,
  `is_active` int(11) DEFAULT '1',
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_risk_assessment_areas`
--

CREATE TABLE `tbl_risk_assessment_areas` (
  `id` int(11) NOT NULL,
  `area_name` varchar(125) DEFAULT NULL,
  `area_sort` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_risk_assessment_category`
--

CREATE TABLE `tbl_risk_assessment_category` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `template_id` bigint(20) DEFAULT NULL,
  `category` varchar(255) DEFAULT NULL,
  `branch_id` bigint(20) DEFAULT NULL,
  `company_id` bigint(20) DEFAULT NULL,
  `is_active` bigint(20) DEFAULT '1',
  `created_by` bigint(20) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_risk_assessment_groups`
--

CREATE TABLE `tbl_risk_assessment_groups` (
  `id` int(11) NOT NULL,
  `risk_type_id` int(11) NOT NULL,
  `group_name` varchar(45) DEFAULT NULL,
  `is_header` tinyint(1) NOT NULL,
  `group_order` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_risk_assessment_old`
--

CREATE TABLE `tbl_risk_assessment_old` (
  `id` int(11) NOT NULL,
  `risk_assessment_no` varchar(150) DEFAULT NULL,
  `risk_assessor` bigint(11) UNSIGNED NOT NULL,
  `user_type_id` int(11) NOT NULL,
  `risk_assessment_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  `is_complete` tinyint(1) NOT NULL DEFAULT '0',
  `assessment_type_id` int(11) NOT NULL,
  `company_id` bigint(20) UNSIGNED NOT NULL,
  `date_created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` bigint(11) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `modified_by` bigint(11) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 KEY_BLOCK_SIZE=4;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_risk_assessment_questions`
--

CREATE TABLE `tbl_risk_assessment_questions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `template_id` varchar(250) DEFAULT NULL,
  `category_id` bigint(20) DEFAULT NULL,
  `question` text,
  `complaint` varchar(10) DEFAULT NULL,
  `resource_involved` bigint(20) DEFAULT NULL,
  `comments` text,
  `status` varchar(20) DEFAULT NULL,
  `photo` text,
  `branch_id` bigint(20) DEFAULT NULL,
  `company_id` bigint(20) DEFAULT NULL,
  `is_active` int(11) DEFAULT '1',
  `created_by` bigint(20) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_risk_assessment_request`
--

CREATE TABLE `tbl_risk_assessment_request` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `source_id` bigint(20) NOT NULL,
  `source` varchar(255) NOT NULL,
  `due_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `priority` varchar(20) DEFAULT NULL,
  `template_id` bigint(20) DEFAULT NULL,
  `person_responsible` bigint(20) NOT NULL,
  `notes` text,
  `requested_status` varchar(20) DEFAULT NULL,
  `upcoming_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `risk_assessor` bigint(20) NOT NULL,
  `branch_id` bigint(20) DEFAULT NULL,
  `company_id` bigint(20) NOT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1',
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_risk_assessment_start`
--

CREATE TABLE `tbl_risk_assessment_start` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `request_id` bigint(20) DEFAULT NULL,
  `risk_type_id` bigint(20) DEFAULT NULL,
  `risk_template_id` bigint(20) DEFAULT NULL,
  `description` text,
  `starting_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `next_revision_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `frequency` varchar(50) DEFAULT NULL,
  `departments_list` text,
  `resource_list` text,
  `occupation_list` text,
  `branch_id` bigint(20) DEFAULT NULL,
  `company_id` bigint(20) DEFAULT NULL,
  `is_draft` int(11) DEFAULT NULL,
  `setupStatus` varchar(10) NOT NULL,
  `hasAssessed` int(11) NOT NULL DEFAULT '0',
  `revisedBy` bigint(20) NOT NULL,
  `is_active` int(11) DEFAULT '1',
  `created_by` bigint(20) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_risk_assessment_templates`
--

CREATE TABLE `tbl_risk_assessment_templates` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `group_id` bigint(20) DEFAULT NULL,
  `type_id` bigint(20) DEFAULT NULL,
  `template` varchar(250) DEFAULT NULL,
  `branch_id` bigint(20) DEFAULT NULL,
  `company_id` bigint(20) DEFAULT NULL,
  `is_draft` int(11) DEFAULT NULL,
  `is_active` int(11) DEFAULT '1',
  `created_by` bigint(20) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_risk_assessment_type`
--

CREATE TABLE `tbl_risk_assessment_type` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `group_id` bigint(20) NOT NULL,
  `type_name` varchar(250) DEFAULT NULL,
  `is_default` int(11) DEFAULT NULL,
  `branch_id` bigint(20) DEFAULT NULL,
  `company_id` bigint(20) DEFAULT NULL,
  `is_active` int(11) DEFAULT '1',
  `created_by` bigint(20) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_risk_assessor`
--

CREATE TABLE `tbl_risk_assessor` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `group_id` bigint(20) NOT NULL,
  `company_id` bigint(20) NOT NULL,
  `branch_id` bigint(20) DEFAULT NULL,
  `dep_id` bigint(20) DEFAULT NULL,
  `occupation` bigint(20) DEFAULT NULL,
  `employee_id` bigint(20) DEFAULT NULL,
  `risk_dep` text,
  `groups_` text,
  `is_active` int(11) DEFAULT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_risk_data`
--

CREATE TABLE `tbl_risk_data` (
  `id` int(11) NOT NULL,
  `activity_no` varchar(20) NOT NULL,
  `hazard` varchar(255) DEFAULT NULL,
  `risk` varchar(255) DEFAULT NULL,
  `score` int(11) DEFAULT NULL,
  `final_score` int(11) DEFAULT NULL,
  `photo` varchar(45) DEFAULT NULL,
  `date_created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` bigint(11) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `modified_by` bigint(11) UNSIGNED DEFAULT NULL,
  `group_id` int(11) NOT NULL,
  `risk_area_id` int(11) NOT NULL,
  `risk_assessment_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_safety_plan`
--

CREATE TABLE `tbl_safety_plan` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `description` varchar(250) DEFAULT NULL,
  `details` text,
  `branch_id` bigint(20) DEFAULT NULL,
  `company_id` bigint(20) DEFAULT NULL,
  `is_active` int(11) DEFAULT '1',
  `created_by` bigint(20) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_service_types`
--

CREATE TABLE `tbl_service_types` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `service_name` varchar(100) NOT NULL,
  `service_description` varchar(255) DEFAULT NULL,
  `note` text,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `service_type` bigint(19) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_service_types`
--

INSERT INTO `tbl_service_types` (`id`, `service_name`, `service_description`, `note`, `created_by`, `modified_by`, `date_modified`, `date_created`, `service_type`) VALUES
(1, 'service', 'Add Service', 'Supplier of service/consylting', NULL, NULL, '2017-05-03 05:58:32', '2017-04-19 10:06:25', NULL),
(2, 'asset', 'Link To Asset Types', 'Assets supplier', NULL, NULL, '2017-05-03 05:58:45', '2017-04-19 10:06:25', NULL),
(3, 'material', 'Link To Material Types', 'Material supplier', NULL, NULL, '2017-05-03 05:58:59', '2017-04-19 10:06:25', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_settings_control`
--

CREATE TABLE `tbl_settings_control` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `group_id` bigint(20) NOT NULL,
  `company_id` bigint(20) NOT NULL,
  `branch_id` bigint(20) DEFAULT NULL,
  `control_type` varchar(255) NOT NULL,
  `reduction` int(10) UNSIGNED DEFAULT NULL,
  `is_active` int(11) DEFAULT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_settings_hazards`
--

CREATE TABLE `tbl_settings_hazards` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `group_id` bigint(20) NOT NULL,
  `company_id` bigint(20) NOT NULL,
  `branch_id` bigint(20) DEFAULT NULL,
  `hazard` varchar(255) NOT NULL,
  `is_active` int(11) DEFAULT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_settings_risk`
--

CREATE TABLE `tbl_settings_risk` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `group_id` bigint(20) NOT NULL,
  `company_id` bigint(20) NOT NULL,
  `branch_id` bigint(20) DEFAULT NULL,
  `risk` varchar(255) NOT NULL,
  `is_active` int(11) DEFAULT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_sheqf_notes`
--

CREATE TABLE `tbl_sheqf_notes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `group_id` bigint(20) DEFAULT NULL,
  `company_id` bigint(20) NOT NULL,
  `branch_id` bigint(20) DEFAULT NULL,
  `source_id` bigint(20) NOT NULL,
  `source_type` varchar(250) NOT NULL,
  `topic` varchar(250) DEFAULT NULL,
  `notes` text,
  `is_active` int(11) DEFAULT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_sheqteam_groups`
--

CREATE TABLE `tbl_sheqteam_groups` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `sheqteam_name` varchar(250) NOT NULL,
  `sheqteam_description` varchar(250) NOT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1',
  `created_by` bigint(20) UNSIGNED NOT NULL,
  `modified_by` bigint(20) UNSIGNED NOT NULL,
  `date_modified` timestamp NULL DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_suppliers`
--

CREATE TABLE `tbl_suppliers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `company_id` bigint(20) UNSIGNED NOT NULL,
  `supplier_name` varchar(235) NOT NULL,
  `status` int(11) NOT NULL,
  `vat_number` varchar(50) DEFAULT NULL,
  `pty_number` varchar(255) DEFAULT NULL,
  `office_number` varchar(45) DEFAULT NULL,
  `contact_person` varchar(255) DEFAULT NULL,
  `cell_number` varchar(20) DEFAULT NULL,
  `supplier_email` varchar(120) DEFAULT NULL,
  `website` varchar(120) DEFAULT NULL,
  `bee_level_id` bigint(20) NOT NULL,
  `certificate_file_id` int(11) DEFAULT NULL,
  `suppier_address` varchar(255) DEFAULT NULL,
  `provience` varchar(45) DEFAULT NULL,
  `business_description` varchar(255) DEFAULT NULL,
  `supporting_document` varchar(255) DEFAULT NULL,
  `supplier_address` varchar(255) DEFAULT NULL,
  `supplier_details` varchar(125) DEFAULT NULL,
  `supplier_town` varchar(120) DEFAULT NULL,
  `street` varchar(150) NOT NULL,
  `suburb` varchar(150) NOT NULL,
  `town` varchar(150) NOT NULL,
  `postal_code` varchar(150) NOT NULL,
  `service_frequency` int(11) DEFAULT NULL,
  `is_construction` tinyint(1) DEFAULT '0',
  `is_high_risk` tinyint(1) DEFAULT '0',
  `compliance_description` varchar(45) DEFAULT NULL,
  `is_audited` tinyint(1) DEFAULT NULL,
  `is_doc_approved` tinyint(1) DEFAULT '0',
  `audit_type` bigint(20) UNSIGNED DEFAULT NULL,
  `audit_frequency` bigint(20) UNSIGNED DEFAULT NULL,
  `next_audit` timestamp NULL DEFAULT NULL,
  `country_id` varchar(6) NOT NULL,
  `region_id` bigint(20) UNSIGNED NOT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NULL DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  `archive_note` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_supplier_administrator`
--

CREATE TABLE `tbl_supplier_administrator` (
  `id` bigint(19) UNSIGNED NOT NULL,
  `admin_name` varchar(120) DEFAULT NULL,
  `admin_surname` varchar(120) DEFAULT NULL,
  `admin_email` varchar(255) DEFAULT NULL,
  `supplier_id` bigint(19) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_supplier_branch`
--

CREATE TABLE `tbl_supplier_branch` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `branch_id` bigint(20) UNSIGNED DEFAULT NULL,
  `supplier_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_supplier_documents`
--

CREATE TABLE `tbl_supplier_documents` (
  `id` bigint(20) NOT NULL,
  `setting_id` bigint(20) UNSIGNED NOT NULL,
  `supplier_id` bigint(20) UNSIGNED NOT NULL,
  `file_id` bigint(20) UNSIGNED DEFAULT NULL,
  `expiry_date` timestamp NULL DEFAULT NULL,
  `is_approved` tinyint(1) DEFAULT '0',
  `created_by` bigint(20) DEFAULT NULL,
  `date_created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` bigint(20) DEFAULT NULL,
  `date_modified` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_supplier_settings`
--

CREATE TABLE `tbl_supplier_settings` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(125) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `date_created` timestamp NULL DEFAULT NULL,
  `modified_by` bigint(20) DEFAULT NULL,
  `date_modified` timestamp NULL DEFAULT NULL,
  `company_id` bigint(19) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_supply_of`
--

CREATE TABLE `tbl_supply_of` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `supplier_id` bigint(20) UNSIGNED NOT NULL,
  `category_id` bigint(20) UNSIGNED DEFAULT NULL,
  `type_id` bigint(20) UNSIGNED DEFAULT NULL,
  `service_type` varchar(125) DEFAULT NULL,
  `notes` text,
  `created_by` bigint(20) DEFAULT NULL,
  `date_created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `service_id` bigint(19) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_system_actions`
--

CREATE TABLE `tbl_system_actions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `source_id` bigint(20) DEFAULT NULL,
  `topic_id` bigint(20) NOT NULL,
  `source_type` varchar(250) DEFAULT NULL,
  `action` text NOT NULL,
  `action_taken` varchar(250) DEFAULT NULL,
  `action_taken_id` bigint(20) DEFAULT NULL,
  `status` varchar(50) NOT NULL,
  `performed_by` bigint(20) NOT NULL,
  `supplier_id` bigint(20) NOT NULL,
  `schedule_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `action_signed_off` int(11) DEFAULT '0',
  `action_signed_by` bigint(20) DEFAULT NULL,
  `branch_id` bigint(20) DEFAULT NULL,
  `company_id` bigint(20) DEFAULT NULL,
  `is_active` int(11) DEFAULT '1',
  `created_by` bigint(20) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_temporary_staff`
--

CREATE TABLE `tbl_temporary_staff` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `branch_id` bigint(20) UNSIGNED NOT NULL,
  `staff_firstname` varchar(145) DEFAULT NULL,
  `staff_lastname` varchar(145) DEFAULT NULL,
  `staff_id` varchar(225) DEFAULT NULL,
  `staff_email` varchar(45) DEFAULT NULL,
  `staff_cellphone` varchar(45) DEFAULT NULL,
  `job_description` text,
  `date_created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `date_modified` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `created_by` bigint(20) DEFAULT NULL,
  `modified_by` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_temporary_staff_induction`
--

CREATE TABLE `tbl_temporary_staff_induction` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `month_valid` bigint(20) DEFAULT '0',
  `file_id` bigint(20) UNSIGNED DEFAULT NULL,
  `responsible_person` bigint(20) DEFAULT NULL,
  `date_created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `date_modified` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `created_by` bigint(20) DEFAULT NULL,
  `modified_by` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE `tbl_users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `username` varchar(120) NOT NULL,
  `email` varchar(145) DEFAULT NULL,
  `role_id` bigint(20) NOT NULL,
  `firstname` varchar(120) DEFAULT NULL,
  `lastname` varchar(120) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1',
  `last_login` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `salt` varchar(45) DEFAULT NULL,
  `ip_address` varchar(60) DEFAULT NULL,
  `db_name` varchar(250) NOT NULL,
  `Company` varchar(250) NOT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `profile_image_id` bigint(20) UNSIGNED DEFAULT NULL,
  `employee_id` bigint(20) UNSIGNED DEFAULT NULL,
  `company_id` bigint(20) DEFAULT NULL,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_created` timestamp NULL DEFAULT NULL,
  `date_modified` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`id`, `username`, `email`, `role_id`, `firstname`, `lastname`, `password`, `is_active`, `last_login`, `salt`, `ip_address`, `db_name`, `Company`, `created_by`, `profile_image_id`, `employee_id`, `company_id`, `modified_by`, `date_created`, `date_modified`) VALUES
(1, 'admin', 'admin@gmail.com', 1, 'admin', 'system', '21232f297a57a5a743894a0e4a801fc3', 1, '2017-10-13 10:06:17', NULL, NULL, 'sheqintel_master', 'Innovatorshill.co.za', 1, NULL, NULL, 1, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user_actions`
--

CREATE TABLE `tbl_user_actions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `action_name` varchar(50) NOT NULL,
  `module_name` varchar(50) DEFAULT 'system',
  `description` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user_clothing`
--

CREATE TABLE `tbl_user_clothing` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type_name` varchar(145) DEFAULT NULL,
  `type_size` varchar(100) DEFAULT NULL,
  `type_description` text,
  `shirt` varchar(100) DEFAULT NULL,
  `pants` varchar(100) DEFAULT NULL,
  `overall` varchar(100) DEFAULT NULL,
  `shoes` varchar(100) DEFAULT NULL,
  `jacket` varchar(100) DEFAULT NULL,
  `gloves` varchar(100) DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `modified_by` bigint(20) DEFAULT NULL,
  `date_created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `date_modified` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `employee_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user_data`
--

CREATE TABLE `tbl_user_data` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `user_type_id` bigint(20) UNSIGNED NOT NULL,
  `field_id` bigint(20) UNSIGNED NOT NULL,
  `field_data` varchar(255) DEFAULT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_created` timestamp NULL DEFAULT NULL,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user_emergency_contact`
--

CREATE TABLE `tbl_user_emergency_contact` (
  `id` bigint(20) NOT NULL,
  `employee_id` bigint(20) UNSIGNED NOT NULL,
  `contact_type` int(11) NOT NULL,
  `name` varchar(145) DEFAULT NULL,
  `contact_number` varchar(145) DEFAULT NULL,
  `relationship` varchar(255) DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `modified_by` bigint(20) DEFAULT NULL,
  `date_created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `date_modified` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user_emergency_medical`
--

CREATE TABLE `tbl_user_emergency_medical` (
  `id` bigint(20) NOT NULL,
  `employee_id` bigint(20) UNSIGNED NOT NULL,
  `medical_aid` varchar(145) DEFAULT NULL,
  `medical_aid_plan` varchar(255) DEFAULT NULL,
  `medical_aid_number` varchar(145) DEFAULT NULL,
  `card_proof` bigint(20) DEFAULT NULL,
  `allergies` text,
  `blood_type` int(11) DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `modified_by` bigint(20) DEFAULT NULL,
  `date_created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `date_modified` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user_types`
--

CREATE TABLE `tbl_user_types` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(50) NOT NULL,
  `description` text,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_created` timestamp NULL DEFAULT NULL,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user_type_actions`
--

CREATE TABLE `tbl_user_type_actions` (
  `action_id` bigint(20) UNSIGNED NOT NULL,
  `user_type_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user_type_fields`
--

CREATE TABLE `tbl_user_type_fields` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_type_id` bigint(20) UNSIGNED NOT NULL,
  `field_name` varchar(255) NOT NULL,
  `field_order` int(11) NOT NULL DEFAULT '1',
  `category_name` varchar(255) NOT NULL,
  `category_order` int(11) NOT NULL DEFAULT '1',
  `field_datatype` varchar(255) NOT NULL DEFAULT 'VARCHAR',
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_created` timestamp NULL DEFAULT NULL,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_modified` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_witness`
--

CREATE TABLE `tbl_witness` (
  `id` int(11) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `id_number` varchar(50) NOT NULL,
  `contact_number` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `company_name` varchar(100) NOT NULL,
  `witness_type` int(11) NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `date_modified` timestamp NULL DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_working_environment`
--

CREATE TABLE `tbl_working_environment` (
  `id` int(11) UNSIGNED NOT NULL,
  `occupation_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '0',
  `created_by` bigint(20) DEFAULT NULL,
  `modified_by` bigint(20) DEFAULT NULL,
  `date_created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `date_modified` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_working_instructions`
--

CREATE TABLE `tbl_working_instructions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `job_description_id` bigint(20) UNSIGNED NOT NULL,
  `description` text,
  `date_created` timestamp NULL DEFAULT NULL,
  `date_modified` timestamp NULL DEFAULT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_working_instructions_documents`
--

CREATE TABLE `tbl_working_instructions_documents` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `working_instruction_id` bigint(20) UNSIGNED NOT NULL,
  `file_id` bigint(20) UNSIGNED NOT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `date_created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `date_modified` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `xtbl_risk_assessment_type`
--

CREATE TABLE `xtbl_risk_assessment_type` (
  `id` int(11) NOT NULL,
  `type_name` varchar(120) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_appointments`
--
ALTER TABLE `tbl_appointments`
  ADD PRIMARY KEY (`id`,`settings_id`,`appointed_employee_id`),
  ADD KEY `fk_tbl_appointments_tbl_appointmnet_settings1_idx` (`settings_id`),
  ADD KEY `fk_tbl_employee_sid_idx` (`appointed_employee_id`);

--
-- Indexes for table `tbl_appointmnet_settings`
--
ALTER TABLE `tbl_appointmnet_settings`
  ADD PRIMARY KEY (`id`,`appointment_group_id`),
  ADD KEY `fk_tbl_appointmnet_settings_tbl_appointment_types1_idx` (`appointment_group_id`),
  ADD KEY `fk_tbl_appointment_document_idx` (`doc_id`),
  ADD KEY `fk_tbl_appointmnet_settings_tbl_companies1` (`company_id`);

--
-- Indexes for table `tbl_assets`
--
ALTER TABLE `tbl_assets`
  ADD PRIMARY KEY (`id`),
  ADD KEY `department_id` (`department_id`),
  ADD KEY `categorytype_id` (`categorytype_id`),
  ADD KEY `resourcetype_id` (`resourcetype_id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `tbl_assets_involved_cause`
--
ALTER TABLE `tbl_assets_involved_cause`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_assets_involved_effects`
--
ALTER TABLE `tbl_assets_involved_effects`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_assets_involved_type`
--
ALTER TABLE `tbl_assets_involved_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_assets_thirdparty`
--
ALTER TABLE `tbl_assets_thirdparty`
  ADD PRIMARY KEY (`id`),
  ADD KEY `incident_id` (`incident_id`);

--
-- Indexes for table `tbl_assets_thirdparty_cause`
--
ALTER TABLE `tbl_assets_thirdparty_cause`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_assets_thirdparty_effects`
--
ALTER TABLE `tbl_assets_thirdparty_effects`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_asset_category`
--
ALTER TABLE `tbl_asset_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_asset_document`
--
ALTER TABLE `tbl_asset_document`
  ADD PRIMARY KEY (`id`),
  ADD KEY `asset_id` (`asset_id`);

--
-- Indexes for table `tbl_asset_documents`
--
ALTER TABLE `tbl_asset_documents`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_asset_financial`
--
ALTER TABLE `tbl_asset_financial`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_asset_life_span`
--
ALTER TABLE `tbl_asset_life_span`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_asset_material`
--
ALTER TABLE `tbl_asset_material`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_asset_photo`
--
ALTER TABLE `tbl_asset_photo`
  ADD PRIMARY KEY (`id`),
  ADD KEY `asset_id` (`asset_id`);

--
-- Indexes for table `tbl_asset_questions`
--
ALTER TABLE `tbl_asset_questions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_asset_relocate`
--
ALTER TABLE `tbl_asset_relocate`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_asset_som`
--
ALTER TABLE `tbl_asset_som`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_asset_type`
--
ALTER TABLE `tbl_asset_type`
  ADD PRIMARY KEY (`id`),
  ADD KEY `category_id` (`category_id`);

--
-- Indexes for table `tbl_audit_type`
--
ALTER TABLE `tbl_audit_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_awareness`
--
ALTER TABLE `tbl_awareness`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_awareness_induction_material`
--
ALTER TABLE `tbl_awareness_induction_material`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_awareness_posters`
--
ALTER TABLE `tbl_awareness_posters`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_awareness_program`
--
ALTER TABLE `tbl_awareness_program`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_awareness_program_responsible_persons`
--
ALTER TABLE `tbl_awareness_program_responsible_persons`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_awareness_types`
--
ALTER TABLE `tbl_awareness_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_bee_levels`
--
ALTER TABLE `tbl_bee_levels`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_blood_type`
--
ALTER TABLE `tbl_blood_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_branches`
--
ALTER TABLE `tbl_branches`
  ADD PRIMARY KEY (`id`,`company_id`),
  ADD KEY `fk_tbl_branches_tbl_companies1_idx` (`company_id`);

--
-- Indexes for table `tbl_clients`
--
ALTER TABLE `tbl_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_client_type_idx` (`client_type`),
  ADD KEY `fk_branch_id_idx` (`branch_id`);

--
-- Indexes for table `tbl_client_company`
--
ALTER TABLE `tbl_client_company`
  ADD PRIMARY KEY (`id`,`client_id`),
  ADD KEY `fk_client_client_idx` (`client_id`);

--
-- Indexes for table `tbl_client_individual`
--
ALTER TABLE `tbl_client_individual`
  ADD PRIMARY KEY (`id`,`client_id`),
  ADD KEY `fk_individual_client_idx` (`client_id`);

--
-- Indexes for table `tbl_client_types`
--
ALTER TABLE `tbl_client_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_companies`
--
ALTER TABLE `tbl_companies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_company_docs`
--
ALTER TABLE `tbl_company_docs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_compliance`
--
ALTER TABLE `tbl_compliance`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_compliance_details`
--
ALTER TABLE `tbl_compliance_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_compliance_documents`
--
ALTER TABLE `tbl_compliance_documents`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_contractor_control`
--
ALTER TABLE `tbl_contractor_control`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_contractor_control_details`
--
ALTER TABLE `tbl_contractor_control_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_contract_control_type`
--
ALTER TABLE `tbl_contract_control_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_corrective_actions`
--
ALTER TABLE `tbl_corrective_actions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_corrective_action_types`
--
ALTER TABLE `tbl_corrective_action_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_countries`
--
ALTER TABLE `tbl_countries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_country_languages`
--
ALTER TABLE `tbl_country_languages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_courses`
--
ALTER TABLE `tbl_courses`
  ADD PRIMARY KEY (`id`,`type_id`,`branch_id`,`group_id`),
  ADD KEY `fk_tbl_courses_tbl_course_seta_idx` (`seta_id`),
  ADD KEY `fk_tbl_courses_tbl_course_types1_idx` (`type_id`),
  ADD KEY `fk_tbl_courses_tbl_branches1_idx` (`branch_id`),
  ADD KEY `fk_tbl_courses_tbl_sheqteam_groups1_idx` (`group_id`);

--
-- Indexes for table `tbl_course_matrix`
--
ALTER TABLE `tbl_course_matrix`
  ADD PRIMARY KEY (`courses_id`,`user_id`,`module_id`),
  ADD KEY `fk_tbl_course_matrix_tbl_matrix_type_idx` (`type_id`),
  ADD KEY `fk_tbl_course_matrix_tbl_courses1_idx` (`courses_id`);

--
-- Indexes for table `tbl_course_seta`
--
ALTER TABLE `tbl_course_seta`
  ADD PRIMARY KEY (`id`,`company_id`),
  ADD KEY `fk_tbl_course_seta_tbl_companies1_idx` (`company_id`);

--
-- Indexes for table `tbl_course_status`
--
ALTER TABLE `tbl_course_status`
  ADD PRIMARY KEY (`id`,`courses_id`),
  ADD KEY `fk_tbl_course_status_tbl_courses_idx` (`courses_id`),
  ADD KEY `fk_tbl_course_status_tbl_suppliers1_idx` (`suppliers_id`);

--
-- Indexes for table `tbl_course_training`
--
ALTER TABLE `tbl_course_training`
  ADD PRIMARY KEY (`id`,`course_id`,`user_id`),
  ADD KEY `fk_tbl_course_training_tbl_course_matrix_idx` (`course_id`,`user_id`),
  ADD KEY `fk_tbl_course_training_tbl_file_documents1_idx` (`file_id`);

--
-- Indexes for table `tbl_course_training_history`
--
ALTER TABLE `tbl_course_training_history`
  ADD PRIMARY KEY (`id`,`course_id`,`user_id`),
  ADD KEY `fk_tbl_course_training_tbl_course_matrix_idx` (`course_id`,`user_id`),
  ADD KEY `fk_tbl_course_training_tbl_file_documents1_idx` (`file_id`);

--
-- Indexes for table `tbl_course_types`
--
ALTER TABLE `tbl_course_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_course_uploads`
--
ALTER TABLE `tbl_course_uploads`
  ADD PRIMARY KEY (`id`,`course_id`),
  ADD KEY `fk_tbl_ppe_uploads_tbl_file_documents1_idx` (`file_id`),
  ADD KEY `fk_tbl_course_uploads_tbl_courses1_idx` (`course_id`),
  ADD KEY `fk_tbl_course_uploads_tbl_documents1_idx` (`doc_id`);

--
-- Indexes for table `tbl_departments`
--
ALTER TABLE `tbl_departments`
  ADD PRIMARY KEY (`id`,`branch_id`,`company_id`),
  ADD KEY `fk_tbl_departments_tbl_branches1_idx` (`branch_id`,`company_id`),
  ADD KEY `company_id` (`company_id`);

--
-- Indexes for table `tbl_department_assets`
--
ALTER TABLE `tbl_department_assets`
  ADD PRIMARY KEY (`department_id`,`assets_id`,`branch_id`,`company_id`),
  ADD KEY `fk_tbl_departments_has_tbl_assets_tbl_assets1_idx` (`assets_id`),
  ADD KEY `fk_tbl_departments_has_tbl_assets_tbl_departments1_idx` (`department_id`,`branch_id`,`company_id`),
  ADD KEY `tbl_department_assets_ibfk_1` (`branch_id`),
  ADD KEY `tbl_department_assets_ibfk_2` (`company_id`);

--
-- Indexes for table `tbl_department_incident`
--
ALTER TABLE `tbl_department_incident`
  ADD PRIMARY KEY (`department_id`,`incident_id`,`branch_id`,`company_id`),
  ADD KEY `fk_tbl_departments_has_tbl_assets_tbl_assets1_idx` (`incident_id`),
  ADD KEY `fk_tbl_departments_has_tbl_assets_tbl_departments1_idx` (`department_id`,`branch_id`,`company_id`),
  ADD KEY `tbl_department_assets_ibfk_1` (`branch_id`),
  ADD KEY `tbl_department_assets_ibfk_2` (`company_id`),
  ADD KEY `department_id` (`department_id`),
  ADD KEY `incident_id` (`incident_id`),
  ADD KEY `branch_id` (`branch_id`),
  ADD KEY `company_id` (`company_id`);

--
-- Indexes for table `tbl_department_locations`
--
ALTER TABLE `tbl_department_locations`
  ADD PRIMARY KEY (`id`,`departments_id`),
  ADD KEY `fk_tbl_department_locations_tbl_departments1_idx` (`departments_id`);

--
-- Indexes for table `tbl_department_manager`
--
ALTER TABLE `tbl_department_manager`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_department_manager_roles`
--
ALTER TABLE `tbl_department_manager_roles`
  ADD PRIMARY KEY (`id`,`manager_id`),
  ADD KEY `fk_tbl_department_manager_roles_tbl_department_manager_idx` (`manager_id`);

--
-- Indexes for table `tbl_department_material`
--
ALTER TABLE `tbl_department_material`
  ADD PRIMARY KEY (`material_id`,`branch_id`,`company_id`),
  ADD KEY `fk_tbl_departments_has_tbl_material_tbl_assets1_idx` (`material_id`),
  ADD KEY `fk_tbl_departments_has_tbl_material_tbl_departments1_idx` (`branch_id`,`company_id`),
  ADD KEY `tbl_department_material_ibfk_2` (`company_id`);

--
-- Indexes for table `tbl_department_occupations`
--
ALTER TABLE `tbl_department_occupations`
  ADD PRIMARY KEY (`employee_id`,`occupation_id`,`department_id`,`branch_id`,`company_id`),
  ADD KEY `fk_tbl_department_occupations_tbl_employee1_idx` (`employee_id`),
  ADD KEY `fk_tbl_department_occupations_tbl_departments1_idx` (`department_id`,`branch_id`,`company_id`),
  ADD KEY `fk_tbl_department_occupations_tbl_occupations1_idx` (`occupation_id`);

--
-- Indexes for table `tbl_documents`
--
ALTER TABLE `tbl_documents`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_document_approvals`
--
ALTER TABLE `tbl_document_approvals`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_tbl_document_approvals_1_idx` (`doc_id`),
  ADD KEY `fk_tbl_document_approvals_2_idx` (`employee_id`);

--
-- Indexes for table `tbl_document_folders`
--
ALTER TABLE `tbl_document_folders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_tbl_document_folders_1_idx` (`company_id`);

--
-- Indexes for table `tbl_document_links`
--
ALTER TABLE `tbl_document_links`
  ADD PRIMARY KEY (`id`,`links_types_id`,`doc_id`),
  ADD KEY `fk_tbl_document_approvals_1_idx` (`doc_id`),
  ADD KEY `fk_tbl_document_links_tbl_document_links_types1_idx` (`links_types_id`);

--
-- Indexes for table `tbl_document_links_types`
--
ALTER TABLE `tbl_document_links_types`
  ADD PRIMARY KEY (`id`,`company_id`),
  ADD KEY `fk_tbl_document_links_types_tbl_companies1_idx` (`company_id`);

--
-- Indexes for table `tbl_document_type`
--
ALTER TABLE `tbl_document_type`
  ADD PRIMARY KEY (`id`,`company_id`),
  ADD KEY `fk_tbl_document_type_tbl_companies1_idx` (`company_id`);

--
-- Indexes for table `tbl_document_version`
--
ALTER TABLE `tbl_document_version`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_tbl_document_version_1_idx` (`doc_id`);

--
-- Indexes for table `tbl_emergency`
--
ALTER TABLE `tbl_emergency`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_emergency_assemblyarea_department`
--
ALTER TABLE `tbl_emergency_assemblyarea_department`
  ADD PRIMARY KEY (`assemblyarea_id`,`department_id`),
  ADD KEY `department_id` (`department_id`),
  ADD KEY `assemblyarea_id` (`assemblyarea_id`);

--
-- Indexes for table `tbl_emergency_authority`
--
ALTER TABLE `tbl_emergency_authority`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_emergency_department`
--
ALTER TABLE `tbl_emergency_department`
  ADD PRIMARY KEY (`emergency_id`,`department_id`),
  ADD KEY `emergency_id` (`emergency_id`),
  ADD KEY `department_id` (`department_id`);

--
-- Indexes for table `tbl_emergency_evacuation`
--
ALTER TABLE `tbl_emergency_evacuation`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_emergency_evacuation_assemblyarea`
--
ALTER TABLE `tbl_emergency_evacuation_assemblyarea`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_emergency_evacuation_department`
--
ALTER TABLE `tbl_emergency_evacuation_department`
  ADD PRIMARY KEY (`evacuation_id`,`department_id`),
  ADD KEY `department_id` (`department_id`),
  ADD KEY `evacuation_id` (`evacuation_id`);

--
-- Indexes for table `tbl_emergency_evacuation_plan`
--
ALTER TABLE `tbl_emergency_evacuation_plan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_emergency_evacuation_signoff`
--
ALTER TABLE `tbl_emergency_evacuation_signoff`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `evacuation_id` (`evacuation_id`);

--
-- Indexes for table `tbl_emergency_setup`
--
ALTER TABLE `tbl_emergency_setup`
  ADD PRIMARY KEY (`id`),
  ADD KEY `emergency_id` (`emergency_id`),
  ADD KEY `appointment` (`appointment`);

--
-- Indexes for table `tbl_employee`
--
ALTER TABLE `tbl_employee`
  ADD PRIMARY KEY (`id`,`company_id`),
  ADD KEY `fk_tbl_employee_tbl_companies_idx` (`company_id`),
  ADD KEY `fk_tbl_employee_tbl_employee_types1_idx` (`employee_type_id`);

--
-- Indexes for table `tbl_employee_types`
--
ALTER TABLE `tbl_employee_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_environmental_electricity_measurement`
--
ALTER TABLE `tbl_environmental_electricity_measurement`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_environmental_electricity_utility`
--
ALTER TABLE `tbl_environmental_electricity_utility`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_environmental_utility_bill`
--
ALTER TABLE `tbl_environmental_utility_bill`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_environmental_water_measurement`
--
ALTER TABLE `tbl_environmental_water_measurement`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_environmental_water_utility`
--
ALTER TABLE `tbl_environmental_water_utility`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_ethnic`
--
ALTER TABLE `tbl_ethnic`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_events`
--
ALTER TABLE `tbl_events`
  ADD PRIMARY KEY (`id`,`type_id`,`company_id`),
  ADD KEY `fk_tbl_event_tbl_event_type_idx` (`type_id`),
  ADD KEY `fk_tbl_event_tbl_companies1_idx` (`company_id`),
  ADD KEY `fk_tbl_events_tbl_suppliers1_idx` (`supplier_id`);

--
-- Indexes for table `tbl_events_attachment`
--
ALTER TABLE `tbl_events_attachment`
  ADD PRIMARY KEY (`id`,`event_id`),
  ADD KEY `fk_tbl_events_attachment_tbl_events1_idx` (`event_id`),
  ADD KEY `fk_tbl_events_attachment_tbl_file_documents1_idx` (`file_id`);

--
-- Indexes for table `tbl_events_notifications`
--
ALTER TABLE `tbl_events_notifications`
  ADD PRIMARY KEY (`id`,`event_id`);

--
-- Indexes for table `tbl_events_signoff`
--
ALTER TABLE `tbl_events_signoff`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_events_type`
--
ALTER TABLE `tbl_events_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_exclusion_from_work`
--
ALTER TABLE `tbl_exclusion_from_work`
  ADD PRIMARY KEY (`id`,`occupation_id`),
  ADD KEY `fk_tbl_working_environment_tbl_occupations1_idx` (`occupation_id`);

--
-- Indexes for table `tbl_exposed_material`
--
ALTER TABLE `tbl_exposed_material`
  ADD PRIMARY KEY (`id`,`working_instruction_id`,`asset_id`),
  ADD KEY `fk_tbl_exposed_to_asset_tbl_working_instructions_idx` (`working_instruction_id`);

--
-- Indexes for table `tbl_exposed_to_asset`
--
ALTER TABLE `tbl_exposed_to_asset`
  ADD PRIMARY KEY (`id`,`working_instruction_id`,`asset_id`),
  ADD KEY `fk_tbl_exposed_to_asset_tbl_working_instructions_idx` (`working_instruction_id`);

--
-- Indexes for table `tbl_fall_protection_plan`
--
ALTER TABLE `tbl_fall_protection_plan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_fall_protection_plan_types`
--
ALTER TABLE `tbl_fall_protection_plan_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_file_documents`
--
ALTER TABLE `tbl_file_documents`
  ADD PRIMARY KEY (`id`,`folders_id`),
  ADD KEY `fk_tbl_file_documents_tbl_document_folders1_idx` (`folders_id`);

--
-- Indexes for table `tbl_finance_approval`
--
ALTER TABLE `tbl_finance_approval`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_finance_approval_quotes`
--
ALTER TABLE `tbl_finance_approval_quotes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_finance_categories`
--
ALTER TABLE `tbl_finance_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_finance_service_providers`
--
ALTER TABLE `tbl_finance_service_providers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_finance_types`
--
ALTER TABLE `tbl_finance_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_financial_loss`
--
ALTER TABLE `tbl_financial_loss`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_financial_loss_asset`
--
ALTER TABLE `tbl_financial_loss_asset`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_financial_loss_client`
--
ALTER TABLE `tbl_financial_loss_client`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_financial_loss_employees`
--
ALTER TABLE `tbl_financial_loss_employees`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_financial_loss_insurance`
--
ALTER TABLE `tbl_financial_loss_insurance`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_financial_loss_material`
--
ALTER TABLE `tbl_financial_loss_material`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_financial_loss_medical`
--
ALTER TABLE `tbl_financial_loss_medical`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_financial_loss_other`
--
ALTER TABLE `tbl_financial_loss_other`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_financial_loss_production`
--
ALTER TABLE `tbl_financial_loss_production`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_financial_loss_settings`
--
ALTER TABLE `tbl_financial_loss_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_frequency`
--
ALTER TABLE `tbl_frequency`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_frequency_type`
--
ALTER TABLE `tbl_frequency_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_gender`
--
ALTER TABLE `tbl_gender`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_global_status`
--
ALTER TABLE `tbl_global_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_health_baseline`
--
ALTER TABLE `tbl_health_baseline`
  ADD PRIMARY KEY (`id`),
  ADD KEY `department_id` (`department_id`),
  ADD KEY `occupations_id` (`occupations_id`);

--
-- Indexes for table `tbl_health_biological`
--
ALTER TABLE `tbl_health_biological`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`),
  ADD KEY `medical_types_id` (`medical_types_id`),
  ADD KEY `frequency_id` (`frequency_id`);

--
-- Indexes for table `tbl_health_entry`
--
ALTER TABLE `tbl_health_entry`
  ADD PRIMARY KEY (`id`),
  ADD KEY `medical_types_id` (`medical_types_id`);

--
-- Indexes for table `tbl_health_medical_practitioner`
--
ALTER TABLE `tbl_health_medical_practitioner`
  ADD PRIMARY KEY (`id`),
  ADD KEY `practitioner_type_id` (`practitioner_type_id`);

--
-- Indexes for table `tbl_health_medical_practitioner_type`
--
ALTER TABLE `tbl_health_medical_practitioner_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_health_medical_status`
--
ALTER TABLE `tbl_health_medical_status`
  ADD PRIMARY KEY (`id`,`user_id`,`baseline_id`),
  ADD KEY `baseline_id` (`baseline_id`),
  ADD KEY `medical_practitioner` (`medical_practitioner`);

--
-- Indexes for table `tbl_health_medical_types`
--
ALTER TABLE `tbl_health_medical_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_health_periodic`
--
ALTER TABLE `tbl_health_periodic`
  ADD PRIMARY KEY (`id`),
  ADD KEY `frequency_id` (`frequency_id`),
  ADD KEY `medical_types_id` (`medical_types_id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `tbl_health_survey`
--
ALTER TABLE `tbl_health_survey`
  ADD PRIMARY KEY (`id`),
  ADD KEY `department_id` (`department_id`),
  ADD KEY `company_id` (`company_id`);

--
-- Indexes for table `tbl_health_survey_document`
--
ALTER TABLE `tbl_health_survey_document`
  ADD PRIMARY KEY (`id`,`survey_id`),
  ADD KEY `event_id` (`survey_id`);

--
-- Indexes for table `tbl_improvement_log`
--
ALTER TABLE `tbl_improvement_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_incident`
--
ALTER TABLE `tbl_incident`
  ADD PRIMARY KEY (`id`),
  ADD KEY `incident_category_id` (`incident_category_id`),
  ADD KEY `incident_type_id` (`incident_type_id`),
  ADD KEY `department_id` (`department_id`),
  ADD KEY `investigator_id` (`investigator_id`);

--
-- Indexes for table `tbl_incident_assets`
--
ALTER TABLE `tbl_incident_assets`
  ADD PRIMARY KEY (`id`,`asset_id`,`incident_id`),
  ADD KEY `asset_id` (`asset_id`),
  ADD KEY `incident_id` (`incident_id`);

--
-- Indexes for table `tbl_incident_assets_cause`
--
ALTER TABLE `tbl_incident_assets_cause`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_incident_authorities`
--
ALTER TABLE `tbl_incident_authorities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_incident_category`
--
ALTER TABLE `tbl_incident_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_incident_documents`
--
ALTER TABLE `tbl_incident_documents`
  ADD PRIMARY KEY (`id`),
  ADD KEY `incident_id` (`incident_id`);

--
-- Indexes for table `tbl_incident_final_medical`
--
ALTER TABLE `tbl_incident_final_medical`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`),
  ADD KEY `incident_id` (`incident_id`);

--
-- Indexes for table `tbl_incident_medical`
--
ALTER TABLE `tbl_incident_medical`
  ADD PRIMARY KEY (`id`,`incident_id`,`person_involved_id`),
  ADD KEY `id` (`id`),
  ADD KEY `incident_id` (`incident_id`);

--
-- Indexes for table `tbl_incident_medical_appointment`
--
ALTER TABLE `tbl_incident_medical_appointment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `tbl_incident_medical_documents`
--
ALTER TABLE `tbl_incident_medical_documents`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_incident_person_involved`
--
ALTER TABLE `tbl_incident_person_involved`
  ADD PRIMARY KEY (`id`,`incident_id`,`person_involved_id`,`person_involved_type`),
  ADD KEY `incident_id` (`incident_id`),
  ADD KEY `person_involved_id` (`person_involved_id`),
  ADD KEY `person_involved_type` (`person_involved_type`);

--
-- Indexes for table `tbl_incident_photo`
--
ALTER TABLE `tbl_incident_photo`
  ADD PRIMARY KEY (`id`),
  ADD KEY `incident_id` (`incident_id`);

--
-- Indexes for table `tbl_incident_publicperson`
--
ALTER TABLE `tbl_incident_publicperson`
  ADD PRIMARY KEY (`id`),
  ADD KEY `incident_id` (`incident_id`);

--
-- Indexes for table `tbl_incident_type`
--
ALTER TABLE `tbl_incident_type`
  ADD PRIMARY KEY (`id`),
  ADD KEY `category_id` (`category_id`);

--
-- Indexes for table `tbl_incident_visitors`
--
ALTER TABLE `tbl_incident_visitors`
  ADD PRIMARY KEY (`id`),
  ADD KEY `incident_id` (`incident_id`);

--
-- Indexes for table `tbl_incident_witness`
--
ALTER TABLE `tbl_incident_witness`
  ADD PRIMARY KEY (`id`,`incident_id`,`witness_id`,`witness_type`),
  ADD KEY `incident_id` (`incident_id`);

--
-- Indexes for table `tbl_inspections`
--
ALTER TABLE `tbl_inspections`
  ADD PRIMARY KEY (`id`),
  ADD KEY `inspectiontype_id` (`inspectiontype_id`),
  ADD KEY `department_id` (`department_id`);

--
-- Indexes for table `tbl_inspections_active`
--
ALTER TABLE `tbl_inspections_active`
  ADD PRIMARY KEY (`id`),
  ADD KEY `inspection_id` (`inspection_id`);

--
-- Indexes for table `tbl_inspections_active_document`
--
ALTER TABLE `tbl_inspections_active_document`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_inspections_conformance`
--
ALTER TABLE `tbl_inspections_conformance`
  ADD PRIMARY KEY (`id`),
  ADD KEY `inspection_id` (`inspection_id`),
  ADD KEY `department_id` (`department_id`);

--
-- Indexes for table `tbl_inspections_document`
--
ALTER TABLE `tbl_inspections_document`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_inspections_frequency`
--
ALTER TABLE `tbl_inspections_frequency`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_inspections_signoff`
--
ALTER TABLE `tbl_inspections_signoff`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_inspections_type`
--
ALTER TABLE `tbl_inspections_type`
  ADD PRIMARY KEY (`id`),
  ADD KEY `frequency_id` (`frequency_id`),
  ADD KEY `linkform_id` (`linkform_id`),
  ADD KEY `group_id` (`group_id`),
  ADD KEY `appointment_id` (`appointment_id`);

--
-- Indexes for table `tbl_investigations`
--
ALTER TABLE `tbl_investigations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_investigation_assessment`
--
ALTER TABLE `tbl_investigation_assessment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_investigation_evidence`
--
ALTER TABLE `tbl_investigation_evidence`
  ADD PRIMARY KEY (`id`,`folders_id`);

--
-- Indexes for table `tbl_investigators`
--
ALTER TABLE `tbl_investigators`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_jobs`
--
ALTER TABLE `tbl_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_jobs_list`
--
ALTER TABLE `tbl_jobs_list`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_job_description`
--
ALTER TABLE `tbl_job_description`
  ADD PRIMARY KEY (`id`,`occupation_id`),
  ADD KEY `fk_tbl_job_description_tbl_occupations_idx` (`occupation_id`);

--
-- Indexes for table `tbl_job_tasks_uploads`
--
ALTER TABLE `tbl_job_tasks_uploads`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_tbl_ppe_uploads_tbl_file_documents1_idx` (`file_id`),
  ADD KEY `fk_tbl_course_uploads_tbl_documents1_idx` (`doc_id`),
  ADD KEY `fk_tbl_job_tasks_uploads_tbl_job_description1_idx` (`task_id`);

--
-- Indexes for table `tbl_languages`
--
ALTER TABLE `tbl_languages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_language_data`
--
ALTER TABLE `tbl_language_data`
  ADD PRIMARY KEY (`id`,`language_id`),
  ADD KEY `fk_tbl_language_data_tbl_languages1_idx` (`language_id`);

--
-- Indexes for table `tbl_material`
--
ALTER TABLE `tbl_material`
  ADD PRIMARY KEY (`id`),
  ADD KEY `category_id` (`category_id`),
  ADD KEY `type_id` (`type_id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `tbl_material_category`
--
ALTER TABLE `tbl_material_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_material_department_store`
--
ALTER TABLE `tbl_material_department_store`
  ADD PRIMARY KEY (`material_id`,`department_id`),
  ADD KEY `material_id` (`material_id`),
  ADD KEY `department_id` (`department_id`);

--
-- Indexes for table `tbl_material_department_use`
--
ALTER TABLE `tbl_material_department_use`
  ADD PRIMARY KEY (`material_id`,`department_id`),
  ADD KEY `material_id` (`material_id`),
  ADD KEY `department_id` (`department_id`);

--
-- Indexes for table `tbl_material_documents`
--
ALTER TABLE `tbl_material_documents`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_material_properties_emergency`
--
ALTER TABLE `tbl_material_properties_emergency`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_material_som`
--
ALTER TABLE `tbl_material_som`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_material_type`
--
ALTER TABLE `tbl_material_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_material_types`
--
ALTER TABLE `tbl_material_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_material_unit`
--
ALTER TABLE `tbl_material_unit`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_material_usage`
--
ALTER TABLE `tbl_material_usage`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_matrix_type`
--
ALTER TABLE `tbl_matrix_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_method_statements`
--
ALTER TABLE `tbl_method_statements`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_method_statements_types`
--
ALTER TABLE `tbl_method_statements_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_modules`
--
ALTER TABLE `tbl_modules`
  ADD PRIMARY KEY (`id`,`module_id`);

--
-- Indexes for table `tbl_module_item`
--
ALTER TABLE `tbl_module_item`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_near_miss`
--
ALTER TABLE `tbl_near_miss`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_near_miss_types`
--
ALTER TABLE `tbl_near_miss_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_non_conformance`
--
ALTER TABLE `tbl_non_conformance`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_non_conformance_types`
--
ALTER TABLE `tbl_non_conformance_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_notes`
--
ALTER TABLE `tbl_notes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_objectives`
--
ALTER TABLE `tbl_objectives`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_objectives_frequency`
--
ALTER TABLE `tbl_objectives_frequency`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_objectives_performance_frequency`
--
ALTER TABLE `tbl_objectives_performance_frequency`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_objectives_status`
--
ALTER TABLE `tbl_objectives_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_objectives_strategy`
--
ALTER TABLE `tbl_objectives_strategy`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_objectives_types`
--
ALTER TABLE `tbl_objectives_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_occupations`
--
ALTER TABLE `tbl_occupations`
  ADD PRIMARY KEY (`id`,`department_id`),
  ADD KEY `fk_occupation_department1` (`department_id`);

--
-- Indexes for table `tbl_online_inspections`
--
ALTER TABLE `tbl_online_inspections`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_online_inspection_assets`
--
ALTER TABLE `tbl_online_inspection_assets`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_online_inspection_assets_assessment`
--
ALTER TABLE `tbl_online_inspection_assets_assessment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_online_inspection_category`
--
ALTER TABLE `tbl_online_inspection_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_online_inspection_details`
--
ALTER TABLE `tbl_online_inspection_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_online_inspection_leg_iso`
--
ALTER TABLE `tbl_online_inspection_leg_iso`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_online_inspection_materials`
--
ALTER TABLE `tbl_online_inspection_materials`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_online_inspection_materials_assessment`
--
ALTER TABLE `tbl_online_inspection_materials_assessment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_online_inspection_plan`
--
ALTER TABLE `tbl_online_inspection_plan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_online_inspection_questions`
--
ALTER TABLE `tbl_online_inspection_questions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_online_inspection_questions_assessment`
--
ALTER TABLE `tbl_online_inspection_questions_assessment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_person_involved_affected`
--
ALTER TABLE `tbl_person_involved_affected`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_person_involved_effects`
--
ALTER TABLE `tbl_person_involved_effects`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_physical_requirements`
--
ALTER TABLE `tbl_physical_requirements`
  ADD PRIMARY KEY (`id`,`occupation_id`),
  ADD KEY `fk_tbl_working_environment_tbl_occupations1_idx` (`occupation_id`);

--
-- Indexes for table `tbl_ppe`
--
ALTER TABLE `tbl_ppe`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_tbl_ppe_tbl_suppliers1_idx1` (`supplier_id`),
  ADD KEY `fk_tbl_ppe_tbl_ppe_types1_idx1` (`ppe_type_id`);

--
-- Indexes for table `tbl_ppe_issue_frequency`
--
ALTER TABLE `tbl_ppe_issue_frequency`
  ADD PRIMARY KEY (`id`,`requirement_id`),
  ADD UNIQUE KEY `requirement_id_UNIQUE` (`requirement_id`),
  ADD KEY `fk_tbl_ppe_issue_frequency_tbl_ppe_requirements_idx` (`requirement_id`);

--
-- Indexes for table `tbl_ppe_requirements`
--
ALTER TABLE `tbl_ppe_requirements`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_tbl_ppe_requirements_tbl_occupations1_idx` (`occupation_id`),
  ADD KEY `fk_tbl_ppe_requirements_tbl_ppe1_idx1` (`ppe_id`),
  ADD KEY `fk_tbl_ppe_requirements_tbl_frequency_type1_idx` (`frequency_id`);

--
-- Indexes for table `tbl_ppe_types`
--
ALTER TABLE `tbl_ppe_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_ppe_uploads`
--
ALTER TABLE `tbl_ppe_uploads`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_tbl_ppe_uploads_tbl_ppe_idx` (`ppe_id`),
  ADD KEY `fk_tbl_ppe_uploads_tbl_file_documents1_idx` (`file_id`);

--
-- Indexes for table `tbl_preventative_asset_list`
--
ALTER TABLE `tbl_preventative_asset_list`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_preventative_asset_status`
--
ALTER TABLE `tbl_preventative_asset_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_preventative_jobs`
--
ALTER TABLE `tbl_preventative_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_preventative_maintenance`
--
ALTER TABLE `tbl_preventative_maintenance`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_preventative_maintenance_asset`
--
ALTER TABLE `tbl_preventative_maintenance_asset`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_preventative_parts`
--
ALTER TABLE `tbl_preventative_parts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_preventative_types`
--
ALTER TABLE `tbl_preventative_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_recommendations`
--
ALTER TABLE `tbl_recommendations`
  ADD PRIMARY KEY (`id`,`risk_data_id`,`type_id`),
  ADD KEY `fk_tbl_recommendation_types_idx` (`type_id`),
  ADD KEY `fk_tbl_recommendations_tbl_recommendation_types1_idx` (`risk_data_id`);

--
-- Indexes for table `tbl_recommendation_types`
--
ALTER TABLE `tbl_recommendation_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_regions`
--
ALTER TABLE `tbl_regions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_renewal_frequency`
--
ALTER TABLE `tbl_renewal_frequency`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_risk_areas`
--
ALTER TABLE `tbl_risk_areas`
  ADD PRIMARY KEY (`id`,`risk_group_id`,`area_id`,`risk_assessment_id`),
  ADD KEY `fk_tbl_risk_areas_tbl_risk_assessment_areas1_idx` (`area_id`),
  ADD KEY `fk_tbl_risk_areas_tbl_risk_assessment1_idx` (`risk_assessment_id`),
  ADD KEY `fk_tbl_groups` (`risk_group_id`);

--
-- Indexes for table `tbl_risk_assessment`
--
ALTER TABLE `tbl_risk_assessment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_risk_assessment_areas`
--
ALTER TABLE `tbl_risk_assessment_areas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_risk_assessment_category`
--
ALTER TABLE `tbl_risk_assessment_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_risk_assessment_groups`
--
ALTER TABLE `tbl_risk_assessment_groups`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_tbl_tbl_risk_assessment_type1` (`risk_type_id`);

--
-- Indexes for table `tbl_risk_assessment_old`
--
ALTER TABLE `tbl_risk_assessment_old`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `risk_assessment_no_UNIQUE` (`risk_assessment_no`),
  ADD KEY `fk_tbl_companies_company_id` (`company_id`);

--
-- Indexes for table `tbl_risk_assessment_questions`
--
ALTER TABLE `tbl_risk_assessment_questions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_risk_assessment_request`
--
ALTER TABLE `tbl_risk_assessment_request`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_risk_assessment_start`
--
ALTER TABLE `tbl_risk_assessment_start`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_risk_assessment_templates`
--
ALTER TABLE `tbl_risk_assessment_templates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_risk_assessment_type`
--
ALTER TABLE `tbl_risk_assessment_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_risk_assessor`
--
ALTER TABLE `tbl_risk_assessor`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_risk_data`
--
ALTER TABLE `tbl_risk_data`
  ADD PRIMARY KEY (`id`,`risk_assessment_id`,`risk_area_id`,`group_id`),
  ADD KEY `fk_tbl_risk_data_tbl_risk_areas1_idx` (`risk_area_id`),
  ADD KEY `fk_tbl_risk_data_risk_areasassrisk2` (`risk_assessment_id`),
  ADD KEY `fk_tbl_risk_data_risk_areasassrisk3` (`group_id`);

--
-- Indexes for table `tbl_safety_plan`
--
ALTER TABLE `tbl_safety_plan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_settings_control`
--
ALTER TABLE `tbl_settings_control`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_settings_hazards`
--
ALTER TABLE `tbl_settings_hazards`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_settings_risk`
--
ALTER TABLE `tbl_settings_risk`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_sheqf_notes`
--
ALTER TABLE `tbl_sheqf_notes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_sheqteam_groups`
--
ALTER TABLE `tbl_sheqteam_groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_suppliers`
--
ALTER TABLE `tbl_suppliers`
  ADD PRIMARY KEY (`id`,`company_id`),
  ADD KEY `fk_company_id_idx` (`company_id`);

--
-- Indexes for table `tbl_supplier_administrator`
--
ALTER TABLE `tbl_supplier_administrator`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_tbl_supplier_administrator_tbl_suppliers_idx` (`supplier_id`);

--
-- Indexes for table `tbl_supplier_branch`
--
ALTER TABLE `tbl_supplier_branch`
  ADD PRIMARY KEY (`id`,`supplier_id`),
  ADD KEY `fk_tbl_suppier_branch_tbl_suppliers_idx` (`supplier_id`);

--
-- Indexes for table `tbl_supplier_documents`
--
ALTER TABLE `tbl_supplier_documents`
  ADD PRIMARY KEY (`id`,`setting_id`,`supplier_id`),
  ADD KEY `fk_tbl_supplier_approval_documents_tbl_supplier_settings_idx` (`setting_id`),
  ADD KEY `fk_tbl_supplier_approval_documents_tbl_suppliers1_idx` (`supplier_id`),
  ADD KEY `fk_tbl_supplier_approval_documents_tbl_file_documents1_idx` (`file_id`);

--
-- Indexes for table `tbl_supplier_settings`
--
ALTER TABLE `tbl_supplier_settings`
  ADD PRIMARY KEY (`id`,`company_id`),
  ADD KEY `fk_supplier_setting_key_idx` (`company_id`);

--
-- Indexes for table `tbl_supply_of`
--
ALTER TABLE `tbl_supply_of`
  ADD PRIMARY KEY (`id`,`supplier_id`,`service_id`),
  ADD KEY `fk_tbl_supply_of_tbl_suppliers1_idx` (`supplier_id`),
  ADD KEY `fk_tbl_supply_of_tbl_service_types1_idx` (`service_id`);

--
-- Indexes for table `tbl_system_actions`
--
ALTER TABLE `tbl_system_actions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_temporary_staff`
--
ALTER TABLE `tbl_temporary_staff`
  ADD PRIMARY KEY (`id`,`user_id`,`branch_id`),
  ADD KEY `fk_tbl_temporary_staff_tbl_users1_idx` (`user_id`),
  ADD KEY `fk_tbl_temporary_staff_tbl_branches1_idx` (`branch_id`);

--
-- Indexes for table `tbl_temporary_staff_induction`
--
ALTER TABLE `tbl_temporary_staff_induction`
  ADD PRIMARY KEY (`id`,`user_id`),
  ADD KEY `fk_tbl_temporary_staff_induction_tbl_file_documents_idx` (`file_id`),
  ADD KEY `fk_tbl_temporary_staff_induction_tbl_temporary_staff1_idx` (`user_id`);

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_tbl_users_tbl_file_documents1_idx` (`profile_image_id`),
  ADD KEY `fk_tbl_users_tbl_employee1_idx` (`employee_id`,`company_id`);

--
-- Indexes for table `tbl_user_actions`
--
ALTER TABLE `tbl_user_actions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_user_clothing`
--
ALTER TABLE `tbl_user_clothing`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_tbl_user_clothing_tbl_employee1_idx` (`employee_id`);

--
-- Indexes for table `tbl_user_data`
--
ALTER TABLE `tbl_user_data`
  ADD PRIMARY KEY (`id`,`user_id`,`user_type_id`,`field_id`),
  ADD KEY `fk_tbl_user_data_tbl_users1_idx` (`user_id`,`user_type_id`),
  ADD KEY `fk_tbl_user_data_tbl_user_type_fields1_idx` (`field_id`,`user_type_id`);

--
-- Indexes for table `tbl_user_emergency_contact`
--
ALTER TABLE `tbl_user_emergency_contact`
  ADD PRIMARY KEY (`id`,`contact_type`),
  ADD KEY `fk_tbl_user_emergency_contact_tbl_employee1_idx` (`employee_id`);

--
-- Indexes for table `tbl_user_emergency_medical`
--
ALTER TABLE `tbl_user_emergency_medical`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_tbl_user_emergency_medical_tbl_employee1_idx` (`employee_id`);

--
-- Indexes for table `tbl_user_types`
--
ALTER TABLE `tbl_user_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_user_type_actions`
--
ALTER TABLE `tbl_user_type_actions`
  ADD PRIMARY KEY (`action_id`,`user_type_id`),
  ADD KEY `fk_tbl_actions_has_tbl_user_types_tbl_user_types1_idx` (`user_type_id`),
  ADD KEY `fk_tbl_actions_has_tbl_user_types_tbl_actions_idx` (`action_id`);

--
-- Indexes for table `tbl_user_type_fields`
--
ALTER TABLE `tbl_user_type_fields`
  ADD PRIMARY KEY (`id`,`user_type_id`),
  ADD KEY `fk_tbl_user_type_fields_tbl_user_types1_idx` (`user_type_id`);

--
-- Indexes for table `tbl_witness`
--
ALTER TABLE `tbl_witness`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_working_environment`
--
ALTER TABLE `tbl_working_environment`
  ADD PRIMARY KEY (`id`,`occupation_id`),
  ADD KEY `fk_tbl_working_environment_tbl_occupations1_idx` (`occupation_id`);

--
-- Indexes for table `tbl_working_instructions`
--
ALTER TABLE `tbl_working_instructions`
  ADD PRIMARY KEY (`id`,`job_description_id`),
  ADD KEY `fk_tbl_working_instructions_tbl_job_description1_idx` (`job_description_id`);

--
-- Indexes for table `tbl_working_instructions_documents`
--
ALTER TABLE `tbl_working_instructions_documents`
  ADD PRIMARY KEY (`id`,`working_instruction_id`,`file_id`),
  ADD KEY `fk_tbl_working_instructions_documents_tbl_working_instructi_idx` (`working_instruction_id`),
  ADD KEY `fk_tbl_working_instructions_documents_tbl_file_documents1_idx` (`file_id`);

--
-- Indexes for table `xtbl_risk_assessment_type`
--
ALTER TABLE `xtbl_risk_assessment_type`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_appointments`
--
ALTER TABLE `tbl_appointments`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_appointmnet_settings`
--
ALTER TABLE `tbl_appointmnet_settings`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_assets_involved_cause`
--
ALTER TABLE `tbl_assets_involved_cause`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_assets_involved_effects`
--
ALTER TABLE `tbl_assets_involved_effects`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_assets_involved_type`
--
ALTER TABLE `tbl_assets_involved_type`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_assets_thirdparty`
--
ALTER TABLE `tbl_assets_thirdparty`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_assets_thirdparty_cause`
--
ALTER TABLE `tbl_assets_thirdparty_cause`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_assets_thirdparty_effects`
--
ALTER TABLE `tbl_assets_thirdparty_effects`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_asset_category`
--
ALTER TABLE `tbl_asset_category`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_asset_document`
--
ALTER TABLE `tbl_asset_document`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_asset_documents`
--
ALTER TABLE `tbl_asset_documents`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_asset_financial`
--
ALTER TABLE `tbl_asset_financial`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_asset_life_span`
--
ALTER TABLE `tbl_asset_life_span`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_asset_material`
--
ALTER TABLE `tbl_asset_material`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_asset_photo`
--
ALTER TABLE `tbl_asset_photo`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_asset_questions`
--
ALTER TABLE `tbl_asset_questions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_asset_relocate`
--
ALTER TABLE `tbl_asset_relocate`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_asset_som`
--
ALTER TABLE `tbl_asset_som`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_asset_type`
--
ALTER TABLE `tbl_asset_type`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_awareness`
--
ALTER TABLE `tbl_awareness`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_awareness_induction_material`
--
ALTER TABLE `tbl_awareness_induction_material`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_awareness_posters`
--
ALTER TABLE `tbl_awareness_posters`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_awareness_program`
--
ALTER TABLE `tbl_awareness_program`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_awareness_program_responsible_persons`
--
ALTER TABLE `tbl_awareness_program_responsible_persons`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_awareness_types`
--
ALTER TABLE `tbl_awareness_types`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_bee_levels`
--
ALTER TABLE `tbl_bee_levels`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `tbl_blood_type`
--
ALTER TABLE `tbl_blood_type`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tbl_branches`
--
ALTER TABLE `tbl_branches`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_clients`
--
ALTER TABLE `tbl_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_client_company`
--
ALTER TABLE `tbl_client_company`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_client_individual`
--
ALTER TABLE `tbl_client_individual`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_client_types`
--
ALTER TABLE `tbl_client_types`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tbl_companies`
--
ALTER TABLE `tbl_companies`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_company_docs`
--
ALTER TABLE `tbl_company_docs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_compliance`
--
ALTER TABLE `tbl_compliance`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_compliance_details`
--
ALTER TABLE `tbl_compliance_details`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_compliance_documents`
--
ALTER TABLE `tbl_compliance_documents`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_contractor_control`
--
ALTER TABLE `tbl_contractor_control`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_contractor_control_details`
--
ALTER TABLE `tbl_contractor_control_details`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_contract_control_type`
--
ALTER TABLE `tbl_contract_control_type`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_corrective_actions`
--
ALTER TABLE `tbl_corrective_actions`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_corrective_action_types`
--
ALTER TABLE `tbl_corrective_action_types`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_countries`
--
ALTER TABLE `tbl_countries`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=240;
--
-- AUTO_INCREMENT for table `tbl_country_languages`
--
ALTER TABLE `tbl_country_languages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=136;
--
-- AUTO_INCREMENT for table `tbl_courses`
--
ALTER TABLE `tbl_courses`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_course_seta`
--
ALTER TABLE `tbl_course_seta`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_course_status`
--
ALTER TABLE `tbl_course_status`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_course_training`
--
ALTER TABLE `tbl_course_training`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_course_training_history`
--
ALTER TABLE `tbl_course_training_history`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_course_types`
--
ALTER TABLE `tbl_course_types`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tbl_course_uploads`
--
ALTER TABLE `tbl_course_uploads`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_departments`
--
ALTER TABLE `tbl_departments`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_department_locations`
--
ALTER TABLE `tbl_department_locations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_department_manager`
--
ALTER TABLE `tbl_department_manager`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_department_manager_roles`
--
ALTER TABLE `tbl_department_manager_roles`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_documents`
--
ALTER TABLE `tbl_documents`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_document_approvals`
--
ALTER TABLE `tbl_document_approvals`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_document_folders`
--
ALTER TABLE `tbl_document_folders`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_document_links`
--
ALTER TABLE `tbl_document_links`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_document_links_types`
--
ALTER TABLE `tbl_document_links_types`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_document_type`
--
ALTER TABLE `tbl_document_type`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_document_version`
--
ALTER TABLE `tbl_document_version`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_emergency`
--
ALTER TABLE `tbl_emergency`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_emergency_authority`
--
ALTER TABLE `tbl_emergency_authority`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_emergency_evacuation`
--
ALTER TABLE `tbl_emergency_evacuation`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_emergency_evacuation_assemblyarea`
--
ALTER TABLE `tbl_emergency_evacuation_assemblyarea`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_emergency_evacuation_plan`
--
ALTER TABLE `tbl_emergency_evacuation_plan`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_emergency_evacuation_signoff`
--
ALTER TABLE `tbl_emergency_evacuation_signoff`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_emergency_setup`
--
ALTER TABLE `tbl_emergency_setup`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_employee`
--
ALTER TABLE `tbl_employee`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_employee_types`
--
ALTER TABLE `tbl_employee_types`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tbl_environmental_electricity_measurement`
--
ALTER TABLE `tbl_environmental_electricity_measurement`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_environmental_electricity_utility`
--
ALTER TABLE `tbl_environmental_electricity_utility`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_environmental_utility_bill`
--
ALTER TABLE `tbl_environmental_utility_bill`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_environmental_water_measurement`
--
ALTER TABLE `tbl_environmental_water_measurement`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_environmental_water_utility`
--
ALTER TABLE `tbl_environmental_water_utility`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_ethnic`
--
ALTER TABLE `tbl_ethnic`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tbl_events`
--
ALTER TABLE `tbl_events`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_events_attachment`
--
ALTER TABLE `tbl_events_attachment`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_events_notifications`
--
ALTER TABLE `tbl_events_notifications`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_events_signoff`
--
ALTER TABLE `tbl_events_signoff`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_events_type`
--
ALTER TABLE `tbl_events_type`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_exclusion_from_work`
--
ALTER TABLE `tbl_exclusion_from_work`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_exposed_material`
--
ALTER TABLE `tbl_exposed_material`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_exposed_to_asset`
--
ALTER TABLE `tbl_exposed_to_asset`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_fall_protection_plan`
--
ALTER TABLE `tbl_fall_protection_plan`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_fall_protection_plan_types`
--
ALTER TABLE `tbl_fall_protection_plan_types`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_file_documents`
--
ALTER TABLE `tbl_file_documents`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_finance_approval`
--
ALTER TABLE `tbl_finance_approval`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_finance_approval_quotes`
--
ALTER TABLE `tbl_finance_approval_quotes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_finance_categories`
--
ALTER TABLE `tbl_finance_categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_finance_service_providers`
--
ALTER TABLE `tbl_finance_service_providers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_finance_types`
--
ALTER TABLE `tbl_finance_types`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_financial_loss`
--
ALTER TABLE `tbl_financial_loss`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_financial_loss_asset`
--
ALTER TABLE `tbl_financial_loss_asset`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_financial_loss_client`
--
ALTER TABLE `tbl_financial_loss_client`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_financial_loss_employees`
--
ALTER TABLE `tbl_financial_loss_employees`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_financial_loss_insurance`
--
ALTER TABLE `tbl_financial_loss_insurance`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_financial_loss_material`
--
ALTER TABLE `tbl_financial_loss_material`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_financial_loss_medical`
--
ALTER TABLE `tbl_financial_loss_medical`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_financial_loss_other`
--
ALTER TABLE `tbl_financial_loss_other`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_financial_loss_production`
--
ALTER TABLE `tbl_financial_loss_production`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_financial_loss_settings`
--
ALTER TABLE `tbl_financial_loss_settings`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_frequency`
--
ALTER TABLE `tbl_frequency`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `tbl_frequency_type`
--
ALTER TABLE `tbl_frequency_type`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tbl_gender`
--
ALTER TABLE `tbl_gender`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tbl_global_status`
--
ALTER TABLE `tbl_global_status`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_health_baseline`
--
ALTER TABLE `tbl_health_baseline`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_health_biological`
--
ALTER TABLE `tbl_health_biological`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_health_entry`
--
ALTER TABLE `tbl_health_entry`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_health_medical_practitioner`
--
ALTER TABLE `tbl_health_medical_practitioner`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_health_medical_practitioner_type`
--
ALTER TABLE `tbl_health_medical_practitioner_type`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_health_medical_status`
--
ALTER TABLE `tbl_health_medical_status`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_health_medical_types`
--
ALTER TABLE `tbl_health_medical_types`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_health_periodic`
--
ALTER TABLE `tbl_health_periodic`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_health_survey`
--
ALTER TABLE `tbl_health_survey`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_health_survey_document`
--
ALTER TABLE `tbl_health_survey_document`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_improvement_log`
--
ALTER TABLE `tbl_improvement_log`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_incident`
--
ALTER TABLE `tbl_incident`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_incident_assets`
--
ALTER TABLE `tbl_incident_assets`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_incident_assets_cause`
--
ALTER TABLE `tbl_incident_assets_cause`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_incident_authorities`
--
ALTER TABLE `tbl_incident_authorities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_incident_category`
--
ALTER TABLE `tbl_incident_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_incident_documents`
--
ALTER TABLE `tbl_incident_documents`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_incident_final_medical`
--
ALTER TABLE `tbl_incident_final_medical`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_incident_medical`
--
ALTER TABLE `tbl_incident_medical`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_incident_medical_appointment`
--
ALTER TABLE `tbl_incident_medical_appointment`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_incident_medical_documents`
--
ALTER TABLE `tbl_incident_medical_documents`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_incident_person_involved`
--
ALTER TABLE `tbl_incident_person_involved`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_incident_photo`
--
ALTER TABLE `tbl_incident_photo`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_incident_publicperson`
--
ALTER TABLE `tbl_incident_publicperson`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_incident_type`
--
ALTER TABLE `tbl_incident_type`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_incident_visitors`
--
ALTER TABLE `tbl_incident_visitors`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_incident_witness`
--
ALTER TABLE `tbl_incident_witness`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_inspections`
--
ALTER TABLE `tbl_inspections`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_inspections_active`
--
ALTER TABLE `tbl_inspections_active`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_inspections_active_document`
--
ALTER TABLE `tbl_inspections_active_document`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_inspections_conformance`
--
ALTER TABLE `tbl_inspections_conformance`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_inspections_document`
--
ALTER TABLE `tbl_inspections_document`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_inspections_frequency`
--
ALTER TABLE `tbl_inspections_frequency`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tbl_inspections_signoff`
--
ALTER TABLE `tbl_inspections_signoff`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_inspections_type`
--
ALTER TABLE `tbl_inspections_type`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_investigations`
--
ALTER TABLE `tbl_investigations`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_investigation_assessment`
--
ALTER TABLE `tbl_investigation_assessment`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_investigation_evidence`
--
ALTER TABLE `tbl_investigation_evidence`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_investigators`
--
ALTER TABLE `tbl_investigators`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_jobs`
--
ALTER TABLE `tbl_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_jobs_list`
--
ALTER TABLE `tbl_jobs_list`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_job_description`
--
ALTER TABLE `tbl_job_description`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_job_tasks_uploads`
--
ALTER TABLE `tbl_job_tasks_uploads`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_languages`
--
ALTER TABLE `tbl_languages`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_language_data`
--
ALTER TABLE `tbl_language_data`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=104;
--
-- AUTO_INCREMENT for table `tbl_material_category`
--
ALTER TABLE `tbl_material_category`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_material_documents`
--
ALTER TABLE `tbl_material_documents`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_material_properties_emergency`
--
ALTER TABLE `tbl_material_properties_emergency`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_material_som`
--
ALTER TABLE `tbl_material_som`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_material_type`
--
ALTER TABLE `tbl_material_type`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_material_types`
--
ALTER TABLE `tbl_material_types`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_material_unit`
--
ALTER TABLE `tbl_material_unit`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_material_usage`
--
ALTER TABLE `tbl_material_usage`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_matrix_type`
--
ALTER TABLE `tbl_matrix_type`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tbl_method_statements`
--
ALTER TABLE `tbl_method_statements`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_method_statements_types`
--
ALTER TABLE `tbl_method_statements_types`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_modules`
--
ALTER TABLE `tbl_modules`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_module_item`
--
ALTER TABLE `tbl_module_item`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_near_miss`
--
ALTER TABLE `tbl_near_miss`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_near_miss_types`
--
ALTER TABLE `tbl_near_miss_types`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_non_conformance`
--
ALTER TABLE `tbl_non_conformance`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_non_conformance_types`
--
ALTER TABLE `tbl_non_conformance_types`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_notes`
--
ALTER TABLE `tbl_notes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_objectives`
--
ALTER TABLE `tbl_objectives`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_objectives_frequency`
--
ALTER TABLE `tbl_objectives_frequency`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tbl_objectives_performance_frequency`
--
ALTER TABLE `tbl_objectives_performance_frequency`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tbl_objectives_status`
--
ALTER TABLE `tbl_objectives_status`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_objectives_strategy`
--
ALTER TABLE `tbl_objectives_strategy`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_objectives_types`
--
ALTER TABLE `tbl_objectives_types`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_occupations`
--
ALTER TABLE `tbl_occupations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_online_inspections`
--
ALTER TABLE `tbl_online_inspections`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_online_inspection_assets`
--
ALTER TABLE `tbl_online_inspection_assets`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_online_inspection_assets_assessment`
--
ALTER TABLE `tbl_online_inspection_assets_assessment`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_online_inspection_category`
--
ALTER TABLE `tbl_online_inspection_category`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_online_inspection_details`
--
ALTER TABLE `tbl_online_inspection_details`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_online_inspection_leg_iso`
--
ALTER TABLE `tbl_online_inspection_leg_iso`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_online_inspection_materials`
--
ALTER TABLE `tbl_online_inspection_materials`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_online_inspection_materials_assessment`
--
ALTER TABLE `tbl_online_inspection_materials_assessment`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_online_inspection_plan`
--
ALTER TABLE `tbl_online_inspection_plan`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_online_inspection_questions`
--
ALTER TABLE `tbl_online_inspection_questions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_online_inspection_questions_assessment`
--
ALTER TABLE `tbl_online_inspection_questions_assessment`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_person_involved_affected`
--
ALTER TABLE `tbl_person_involved_affected`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_person_involved_effects`
--
ALTER TABLE `tbl_person_involved_effects`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_physical_requirements`
--
ALTER TABLE `tbl_physical_requirements`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_ppe`
--
ALTER TABLE `tbl_ppe`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_ppe_issue_frequency`
--
ALTER TABLE `tbl_ppe_issue_frequency`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_ppe_requirements`
--
ALTER TABLE `tbl_ppe_requirements`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_ppe_types`
--
ALTER TABLE `tbl_ppe_types`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_ppe_uploads`
--
ALTER TABLE `tbl_ppe_uploads`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_preventative_asset_list`
--
ALTER TABLE `tbl_preventative_asset_list`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_preventative_asset_status`
--
ALTER TABLE `tbl_preventative_asset_status`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_preventative_jobs`
--
ALTER TABLE `tbl_preventative_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_preventative_maintenance`
--
ALTER TABLE `tbl_preventative_maintenance`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_preventative_maintenance_asset`
--
ALTER TABLE `tbl_preventative_maintenance_asset`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_preventative_parts`
--
ALTER TABLE `tbl_preventative_parts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_preventative_types`
--
ALTER TABLE `tbl_preventative_types`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_recommendations`
--
ALTER TABLE `tbl_recommendations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_recommendation_types`
--
ALTER TABLE `tbl_recommendation_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_regions`
--
ALTER TABLE `tbl_regions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3503;
--
-- AUTO_INCREMENT for table `tbl_renewal_frequency`
--
ALTER TABLE `tbl_renewal_frequency`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_risk_areas`
--
ALTER TABLE `tbl_risk_areas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_risk_assessment`
--
ALTER TABLE `tbl_risk_assessment`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_risk_assessment_areas`
--
ALTER TABLE `tbl_risk_assessment_areas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_risk_assessment_category`
--
ALTER TABLE `tbl_risk_assessment_category`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_risk_assessment_groups`
--
ALTER TABLE `tbl_risk_assessment_groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_risk_assessment_old`
--
ALTER TABLE `tbl_risk_assessment_old`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_risk_assessment_questions`
--
ALTER TABLE `tbl_risk_assessment_questions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_risk_assessment_request`
--
ALTER TABLE `tbl_risk_assessment_request`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_risk_assessment_start`
--
ALTER TABLE `tbl_risk_assessment_start`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_risk_assessment_templates`
--
ALTER TABLE `tbl_risk_assessment_templates`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_risk_assessment_type`
--
ALTER TABLE `tbl_risk_assessment_type`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_risk_assessor`
--
ALTER TABLE `tbl_risk_assessor`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_risk_data`
--
ALTER TABLE `tbl_risk_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_safety_plan`
--
ALTER TABLE `tbl_safety_plan`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_settings_control`
--
ALTER TABLE `tbl_settings_control`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_settings_hazards`
--
ALTER TABLE `tbl_settings_hazards`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_settings_risk`
--
ALTER TABLE `tbl_settings_risk`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_sheqf_notes`
--
ALTER TABLE `tbl_sheqf_notes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_sheqteam_groups`
--
ALTER TABLE `tbl_sheqteam_groups`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_suppliers`
--
ALTER TABLE `tbl_suppliers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_supplier_administrator`
--
ALTER TABLE `tbl_supplier_administrator`
  MODIFY `id` bigint(19) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_supplier_branch`
--
ALTER TABLE `tbl_supplier_branch`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_supplier_documents`
--
ALTER TABLE `tbl_supplier_documents`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_supplier_settings`
--
ALTER TABLE `tbl_supplier_settings`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_supply_of`
--
ALTER TABLE `tbl_supply_of`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_system_actions`
--
ALTER TABLE `tbl_system_actions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_temporary_staff`
--
ALTER TABLE `tbl_temporary_staff`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_temporary_staff_induction`
--
ALTER TABLE `tbl_temporary_staff_induction`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_user_actions`
--
ALTER TABLE `tbl_user_actions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_user_clothing`
--
ALTER TABLE `tbl_user_clothing`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_user_data`
--
ALTER TABLE `tbl_user_data`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_user_emergency_contact`
--
ALTER TABLE `tbl_user_emergency_contact`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_user_emergency_medical`
--
ALTER TABLE `tbl_user_emergency_medical`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_user_types`
--
ALTER TABLE `tbl_user_types`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_user_type_fields`
--
ALTER TABLE `tbl_user_type_fields`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_witness`
--
ALTER TABLE `tbl_witness`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_working_environment`
--
ALTER TABLE `tbl_working_environment`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_working_instructions`
--
ALTER TABLE `tbl_working_instructions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_working_instructions_documents`
--
ALTER TABLE `tbl_working_instructions_documents`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `xtbl_risk_assessment_type`
--
ALTER TABLE `xtbl_risk_assessment_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `tbl_assets`
--
ALTER TABLE `tbl_assets`
  ADD CONSTRAINT `tbl_assets_ibfk_1` FOREIGN KEY (`categorytype_id`) REFERENCES `tbl_asset_category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tbl_assets_ibfk_2` FOREIGN KEY (`resourcetype_id`) REFERENCES `tbl_asset_type` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tbl_assets_ibfk_3` FOREIGN KEY (`department_id`) REFERENCES `tbl_departments` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tbl_assets_ibfk_4` FOREIGN KEY (`id`) REFERENCES `tbl_asset_material` (`id`);

--
-- Constraints for table `tbl_assets_thirdparty`
--
ALTER TABLE `tbl_assets_thirdparty`
  ADD CONSTRAINT `tbl_assets_thirdparty_ibfk_1` FOREIGN KEY (`incident_id`) REFERENCES `tbl_incident` (`id`);

--
-- Constraints for table `tbl_asset_document`
--
ALTER TABLE `tbl_asset_document`
  ADD CONSTRAINT `tbl_asset_document_ibfk_1` FOREIGN KEY (`asset_id`) REFERENCES `tbl_assets` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_asset_photo`
--
ALTER TABLE `tbl_asset_photo`
  ADD CONSTRAINT `tbl_asset_photo_ibfk_1` FOREIGN KEY (`asset_id`) REFERENCES `tbl_assets` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tbl_asset_type`
--
ALTER TABLE `tbl_asset_type`
  ADD CONSTRAINT `tbl_asset_type_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `tbl_asset_category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tbl_branches`
--
ALTER TABLE `tbl_branches`
  ADD CONSTRAINT `fk_tbl_branches_tbl_companies1` FOREIGN KEY (`company_id`) REFERENCES `tbl_companies` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `tbl_clients`
--
ALTER TABLE `tbl_clients`
  ADD CONSTRAINT `fk_branch_id` FOREIGN KEY (`branch_id`) REFERENCES `tbl_branches` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_client_type` FOREIGN KEY (`client_type`) REFERENCES `tbl_client_types` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_client_company`
--
ALTER TABLE `tbl_client_company`
  ADD CONSTRAINT `fk_client_client_id` FOREIGN KEY (`client_id`) REFERENCES `tbl_clients` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_client_individual`
--
ALTER TABLE `tbl_client_individual`
  ADD CONSTRAINT `fk_individual_client_id` FOREIGN KEY (`client_id`) REFERENCES `tbl_clients` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_courses`
--
ALTER TABLE `tbl_courses`
  ADD CONSTRAINT `fk_tbl_courses_tbl_branches1` FOREIGN KEY (`branch_id`) REFERENCES `tbl_branches` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_tbl_courses_tbl_course_seta` FOREIGN KEY (`seta_id`) REFERENCES `tbl_course_seta` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_tbl_courses_tbl_course_types1` FOREIGN KEY (`type_id`) REFERENCES `tbl_course_types` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_tbl_courses_tbl_sheqteam_groups1` FOREIGN KEY (`group_id`) REFERENCES `tbl_sheqteam_groups` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_course_matrix`
--
ALTER TABLE `tbl_course_matrix`
  ADD CONSTRAINT `fk_tbl_course_matrix_tbl_courses1` FOREIGN KEY (`courses_id`) REFERENCES `tbl_courses` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_tbl_course_matrix_tbl_matrix_type` FOREIGN KEY (`type_id`) REFERENCES `tbl_matrix_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_course_seta`
--
ALTER TABLE `tbl_course_seta`
  ADD CONSTRAINT `fk_tbl_course_seta_tbl_companies1` FOREIGN KEY (`company_id`) REFERENCES `tbl_companies` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_course_status`
--
ALTER TABLE `tbl_course_status`
  ADD CONSTRAINT `fk_tbl_course_status_tbl_courses1` FOREIGN KEY (`courses_id`) REFERENCES `tbl_courses` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_tbl_course_status_tbl_suppliers1` FOREIGN KEY (`suppliers_id`) REFERENCES `tbl_suppliers` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_course_training`
--
ALTER TABLE `tbl_course_training`
  ADD CONSTRAINT `fk_tbl_course_training_tbl_course_matrix` FOREIGN KEY (`course_id`,`user_id`) REFERENCES `tbl_course_matrix` (`courses_id`, `user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_tbl_course_training_tbl_file_documents1` FOREIGN KEY (`file_id`) REFERENCES `tbl_file_documents` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_course_uploads`
--
ALTER TABLE `tbl_course_uploads`
  ADD CONSTRAINT `fk_tbl_course_uploads_tbl_courses1` FOREIGN KEY (`course_id`) REFERENCES `tbl_courses` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_tbl_course_uploads_tbl_documents1` FOREIGN KEY (`doc_id`) REFERENCES `tbl_documents` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_tbl_ppe_uploads_tbl_file_documents10` FOREIGN KEY (`file_id`) REFERENCES `tbl_file_documents` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_departments`
--
ALTER TABLE `tbl_departments`
  ADD CONSTRAINT `fk_tbl_departments_tbl_branches1` FOREIGN KEY (`branch_id`,`company_id`) REFERENCES `tbl_branches` (`id`, `company_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `tbl_departments_ibfk_1` FOREIGN KEY (`branch_id`) REFERENCES `tbl_branches` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tbl_departments_ibfk_2` FOREIGN KEY (`company_id`) REFERENCES `tbl_companies` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tbl_department_assets`
--
ALTER TABLE `tbl_department_assets`
  ADD CONSTRAINT `fk_tbl_departments_has_tbl_assets_tbl_departments1` FOREIGN KEY (`department_id`,`branch_id`,`company_id`) REFERENCES `tbl_departments` (`id`, `branch_id`, `company_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tbl_department_assets_ibfk_1` FOREIGN KEY (`branch_id`) REFERENCES `tbl_branches` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tbl_department_assets_ibfk_2` FOREIGN KEY (`company_id`) REFERENCES `tbl_companies` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tbl_department_assets_ibfk_3` FOREIGN KEY (`department_id`) REFERENCES `tbl_departments` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tbl_department_assets_ibfk_4` FOREIGN KEY (`assets_id`) REFERENCES `tbl_assets` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tbl_department_incident`
--
ALTER TABLE `tbl_department_incident`
  ADD CONSTRAINT `tbl_department_incident_ibfk_1` FOREIGN KEY (`department_id`) REFERENCES `tbl_documents` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tbl_department_incident_ibfk_2` FOREIGN KEY (`incident_id`) REFERENCES `tbl_incident` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tbl_department_incident_ibfk_3` FOREIGN KEY (`branch_id`) REFERENCES `tbl_branches` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tbl_department_incident_ibfk_4` FOREIGN KEY (`company_id`) REFERENCES `tbl_companies` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tbl_department_locations`
--
ALTER TABLE `tbl_department_locations`
  ADD CONSTRAINT `fk_tbl_department_locations_tbl_departments1` FOREIGN KEY (`departments_id`) REFERENCES `tbl_departments` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_department_material`
--
ALTER TABLE `tbl_department_material`
  ADD CONSTRAINT `tbl_department_material_ibfk_1` FOREIGN KEY (`branch_id`) REFERENCES `tbl_branches` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tbl_department_material_ibfk_2` FOREIGN KEY (`company_id`) REFERENCES `tbl_companies` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tbl_department_material_ibfk_4` FOREIGN KEY (`material_id`) REFERENCES `tbl_material` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tbl_department_occupations`
--
ALTER TABLE `tbl_department_occupations`
  ADD CONSTRAINT `fk_tbl_department_occupations_tbl_departments1` FOREIGN KEY (`department_id`,`branch_id`,`company_id`) REFERENCES `tbl_departments` (`id`, `branch_id`, `company_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_tbl_department_occupations_tbl_employee1` FOREIGN KEY (`employee_id`) REFERENCES `tbl_employee` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_tbl_department_occupations_tbl_occupations1` FOREIGN KEY (`occupation_id`) REFERENCES `tbl_occupations` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_document_approvals`
--
ALTER TABLE `tbl_document_approvals`
  ADD CONSTRAINT `fk_tbl_document_approvals_1` FOREIGN KEY (`doc_id`) REFERENCES `tbl_documents` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_tbl_document_approvals_2` FOREIGN KEY (`employee_id`) REFERENCES `tbl_users` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `tbl_document_folders`
--
ALTER TABLE `tbl_document_folders`
  ADD CONSTRAINT `fk_tbl_document_folders_1` FOREIGN KEY (`company_id`) REFERENCES `tbl_companies` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tbl_document_links`
--
ALTER TABLE `tbl_document_links`
  ADD CONSTRAINT `fk_tbl_document_links_tbl_document_links_types1` FOREIGN KEY (`links_types_id`) REFERENCES `tbl_document_links_types` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_tbl_document_links_tbl_documents1` FOREIGN KEY (`doc_id`) REFERENCES `tbl_documents` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_document_links_types`
--
ALTER TABLE `tbl_document_links_types`
  ADD CONSTRAINT `fk_tbl_document_links_types_tbl_companies1` FOREIGN KEY (`company_id`) REFERENCES `tbl_companies` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_document_type`
--
ALTER TABLE `tbl_document_type`
  ADD CONSTRAINT `fk_tbl_document_type_tbl_companies1` FOREIGN KEY (`company_id`) REFERENCES `tbl_companies` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_emergency_assemblyarea_department`
--
ALTER TABLE `tbl_emergency_assemblyarea_department`
  ADD CONSTRAINT `tbl_emergency_assemblyarea_department_ibfk_1` FOREIGN KEY (`department_id`) REFERENCES `tbl_departments` (`id`),
  ADD CONSTRAINT `tbl_emergency_assemblyarea_department_ibfk_2` FOREIGN KEY (`assemblyarea_id`) REFERENCES `tbl_emergency_evacuation_assemblyarea` (`id`);

--
-- Constraints for table `tbl_emergency_department`
--
ALTER TABLE `tbl_emergency_department`
  ADD CONSTRAINT `tbl_emergency_department_ibfk_1` FOREIGN KEY (`department_id`) REFERENCES `tbl_departments` (`id`),
  ADD CONSTRAINT `tbl_emergency_department_ibfk_2` FOREIGN KEY (`emergency_id`) REFERENCES `tbl_emergency` (`id`);

--
-- Constraints for table `tbl_emergency_evacuation_department`
--
ALTER TABLE `tbl_emergency_evacuation_department`
  ADD CONSTRAINT `fk_tbl_emergency_evacuation_department_tbl_departments_id` FOREIGN KEY (`department_id`) REFERENCES `tbl_departments` (`id`),
  ADD CONSTRAINT `fk_tbl_emergency_evacuation_department_tbl_emergency_evacuati_id` FOREIGN KEY (`evacuation_id`) REFERENCES `tbl_emergency_evacuation` (`id`);

--
-- Constraints for table `tbl_emergency_evacuation_signoff`
--
ALTER TABLE `tbl_emergency_evacuation_signoff`
  ADD CONSTRAINT `fk_tbl_emergency_evacuation_signoff_tbl_emergency_evacuation_id` FOREIGN KEY (`evacuation_id`) REFERENCES `tbl_emergency_evacuation` (`id`);

--
-- Constraints for table `tbl_emergency_setup`
--
ALTER TABLE `tbl_emergency_setup`
  ADD CONSTRAINT `tbl_emergency_setup_ibfk_1` FOREIGN KEY (`emergency_id`) REFERENCES `tbl_emergency` (`id`),
  ADD CONSTRAINT `tbl_emergency_setup_ibfk_2` FOREIGN KEY (`appointment`) REFERENCES `tbl_appointmnet_settings` (`id`);

--
-- Constraints for table `tbl_employee`
--
ALTER TABLE `tbl_employee`
  ADD CONSTRAINT `fk_tbl_employee_tbl_companies` FOREIGN KEY (`company_id`) REFERENCES `tbl_companies` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_tbl_employee_tbl_employee_types1` FOREIGN KEY (`employee_type_id`) REFERENCES `tbl_employee_types` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_exclusion_from_work`
--
ALTER TABLE `tbl_exclusion_from_work`
  ADD CONSTRAINT `fk_tbl_working_environment_tbl_occupations10` FOREIGN KEY (`occupation_id`) REFERENCES `tbl_occupations` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_exposed_material`
--
ALTER TABLE `tbl_exposed_material`
  ADD CONSTRAINT `fk_tbl_exposed_to_asset_tbl_working_instructions0` FOREIGN KEY (`working_instruction_id`) REFERENCES `tbl_working_instructions` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_exposed_to_asset`
--
ALTER TABLE `tbl_exposed_to_asset`
  ADD CONSTRAINT `fk_tbl_exposed_to_asset_tbl_working_instructions` FOREIGN KEY (`working_instruction_id`) REFERENCES `tbl_working_instructions` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_file_documents`
--
ALTER TABLE `tbl_file_documents`
  ADD CONSTRAINT `fk_tbl_file_documents_tbl_document_folders1` FOREIGN KEY (`folders_id`) REFERENCES `tbl_document_folders` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_health_baseline`
--
ALTER TABLE `tbl_health_baseline`
  ADD CONSTRAINT `tbl_health_baseline_ibfk_1` FOREIGN KEY (`department_id`) REFERENCES `tbl_departments` (`id`),
  ADD CONSTRAINT `tbl_health_baseline_ibfk_2` FOREIGN KEY (`occupations_id`) REFERENCES `tbl_occupations` (`id`);

--
-- Constraints for table `tbl_health_medical_status`
--
ALTER TABLE `tbl_health_medical_status`
  ADD CONSTRAINT `tbl_health_medical_status_ibfk_1` FOREIGN KEY (`medical_practitioner`) REFERENCES `tbl_health_medical_practitioner` (`id`),
  ADD CONSTRAINT `tbl_health_medical_status_ibfk_2` FOREIGN KEY (`baseline_id`) REFERENCES `tbl_health_baseline` (`id`);

--
-- Constraints for table `tbl_incident`
--
ALTER TABLE `tbl_incident`
  ADD CONSTRAINT `tbl_incident_ibfk_3` FOREIGN KEY (`department_id`) REFERENCES `tbl_departments` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tbl_incident_ibfk_4` FOREIGN KEY (`incident_category_id`) REFERENCES `tbl_sheqteam_groups` (`id`),
  ADD CONSTRAINT `tbl_incident_ibfk_5` FOREIGN KEY (`incident_type_id`) REFERENCES `tbl_incident_type` (`id`);

--
-- Constraints for table `tbl_incident_assets`
--
ALTER TABLE `tbl_incident_assets`
  ADD CONSTRAINT `tbl_incident_assets_ibfk_1` FOREIGN KEY (`asset_id`) REFERENCES `tbl_asset_material` (`id`),
  ADD CONSTRAINT `tbl_incident_assets_ibfk_2` FOREIGN KEY (`incident_id`) REFERENCES `tbl_incident` (`id`);

--
-- Constraints for table `tbl_incident_documents`
--
ALTER TABLE `tbl_incident_documents`
  ADD CONSTRAINT `tbl_incident_documents_ibfk_1` FOREIGN KEY (`incident_id`) REFERENCES `tbl_incident` (`id`);

--
-- Constraints for table `tbl_incident_final_medical`
--
ALTER TABLE `tbl_incident_final_medical`
  ADD CONSTRAINT `tbl_incident_final_medical_ibfk_1` FOREIGN KEY (`incident_id`) REFERENCES `tbl_incident` (`id`);

--
-- Constraints for table `tbl_incident_medical`
--
ALTER TABLE `tbl_incident_medical`
  ADD CONSTRAINT `tbl_incident_medical_ibfk_1` FOREIGN KEY (`incident_id`) REFERENCES `tbl_incident` (`id`);

--
-- Constraints for table `tbl_incident_person_involved`
--
ALTER TABLE `tbl_incident_person_involved`
  ADD CONSTRAINT `tbl_incident_person_involved_ibfk_1` FOREIGN KEY (`incident_id`) REFERENCES `tbl_incident` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tbl_incident_photo`
--
ALTER TABLE `tbl_incident_photo`
  ADD CONSTRAINT `tbl_incident_photo_ibfk_1` FOREIGN KEY (`incident_id`) REFERENCES `tbl_incident` (`id`);

--
-- Constraints for table `tbl_incident_publicperson`
--
ALTER TABLE `tbl_incident_publicperson`
  ADD CONSTRAINT `tbl_incident_publicperson_ibfk_1` FOREIGN KEY (`incident_id`) REFERENCES `tbl_incident` (`id`);

--
-- Constraints for table `tbl_incident_type`
--
ALTER TABLE `tbl_incident_type`
  ADD CONSTRAINT `tbl_incident_type_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `tbl_sheqteam_groups` (`id`);

--
-- Constraints for table `tbl_incident_visitors`
--
ALTER TABLE `tbl_incident_visitors`
  ADD CONSTRAINT `tbl_incident_visitors_ibfk_1` FOREIGN KEY (`incident_id`) REFERENCES `tbl_incident` (`id`);

--
-- Constraints for table `tbl_incident_witness`
--
ALTER TABLE `tbl_incident_witness`
  ADD CONSTRAINT `tbl_incident_witness_ibfk_1` FOREIGN KEY (`incident_id`) REFERENCES `tbl_incident` (`id`);

--
-- Constraints for table `tbl_job_description`
--
ALTER TABLE `tbl_job_description`
  ADD CONSTRAINT `fk_tbl_job_description_tbl_occupations1` FOREIGN KEY (`occupation_id`) REFERENCES `tbl_occupations` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_job_tasks_uploads`
--
ALTER TABLE `tbl_job_tasks_uploads`
  ADD CONSTRAINT `fk_tbl_course_uploads_tbl_documents10` FOREIGN KEY (`doc_id`) REFERENCES `tbl_documents` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_tbl_job_tasks_uploads_tbl_job_description1` FOREIGN KEY (`task_id`) REFERENCES `tbl_job_description` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_tbl_ppe_uploads_tbl_file_documents100` FOREIGN KEY (`file_id`) REFERENCES `tbl_file_documents` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_language_data`
--
ALTER TABLE `tbl_language_data`
  ADD CONSTRAINT `fk_tbl_language_data_tbl_languages1` FOREIGN KEY (`language_id`) REFERENCES `tbl_languages` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `tbl_material_department_store`
--
ALTER TABLE `tbl_material_department_store`
  ADD CONSTRAINT `tbl_material_department_store_ibfk_1` FOREIGN KEY (`department_id`) REFERENCES `tbl_departments` (`id`),
  ADD CONSTRAINT `tbl_material_department_store_ibfk_2` FOREIGN KEY (`material_id`) REFERENCES `tbl_material` (`id`);

--
-- Constraints for table `tbl_material_department_use`
--
ALTER TABLE `tbl_material_department_use`
  ADD CONSTRAINT `tbl_material_department_use_ibfk_1` FOREIGN KEY (`department_id`) REFERENCES `tbl_departments` (`id`),
  ADD CONSTRAINT `tbl_material_department_use_ibfk_2` FOREIGN KEY (`material_id`) REFERENCES `tbl_material` (`id`);

--
-- Constraints for table `tbl_occupations`
--
ALTER TABLE `tbl_occupations`
  ADD CONSTRAINT `fk_occupation_department1` FOREIGN KEY (`department_id`) REFERENCES `tbl_departments` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tbl_physical_requirements`
--
ALTER TABLE `tbl_physical_requirements`
  ADD CONSTRAINT `fk_tbl_working_environment_tbl_occupations100` FOREIGN KEY (`occupation_id`) REFERENCES `tbl_occupations` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_ppe`
--
ALTER TABLE `tbl_ppe`
  ADD CONSTRAINT `fk_tbl_ppe_tbl_ppe_types1` FOREIGN KEY (`ppe_type_id`) REFERENCES `tbl_ppe_types` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_tbl_ppe_tbl_suppliers1` FOREIGN KEY (`supplier_id`) REFERENCES `tbl_suppliers` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_ppe_issue_frequency`
--
ALTER TABLE `tbl_ppe_issue_frequency`
  ADD CONSTRAINT `fk_tbl_ppe_issue_frequency_tbl_ppe_requirements` FOREIGN KEY (`requirement_id`) REFERENCES `tbl_ppe_requirements` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_ppe_requirements`
--
ALTER TABLE `tbl_ppe_requirements`
  ADD CONSTRAINT `fk_tbl_ppe_requirements_tbl_frequency_type1` FOREIGN KEY (`frequency_id`) REFERENCES `tbl_frequency_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_tbl_ppe_requirements_tbl_occupations1` FOREIGN KEY (`occupation_id`) REFERENCES `tbl_occupations` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_tbl_ppe_requirements_tbl_ppe1` FOREIGN KEY (`ppe_id`) REFERENCES `tbl_ppe` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_ppe_uploads`
--
ALTER TABLE `tbl_ppe_uploads`
  ADD CONSTRAINT `fk_tbl_ppe_uploads_tbl_file_documents1` FOREIGN KEY (`file_id`) REFERENCES `tbl_file_documents` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_tbl_ppe_uploads_tbl_ppe1` FOREIGN KEY (`ppe_id`) REFERENCES `tbl_ppe` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_recommendations`
--
ALTER TABLE `tbl_recommendations`
  ADD CONSTRAINT `fk_tbl_recommendation_types` FOREIGN KEY (`type_id`) REFERENCES `tbl_recommendation_types` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_tbl_recommendations_tbl_recommendation_types1` FOREIGN KEY (`risk_data_id`) REFERENCES `tbl_risk_data` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_risk_areas`
--
ALTER TABLE `tbl_risk_areas`
  ADD CONSTRAINT `fk_tbl_groups` FOREIGN KEY (`risk_group_id`) REFERENCES `tbl_risk_assessment_groups` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_tbl_risk_assessment1` FOREIGN KEY (`risk_assessment_id`) REFERENCES `tbl_risk_assessment_old` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_tbl_risk_assessment_areas1` FOREIGN KEY (`area_id`) REFERENCES `tbl_risk_assessment_areas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_risk_assessment_groups`
--
ALTER TABLE `tbl_risk_assessment_groups`
  ADD CONSTRAINT `fk_tbl_tbl_risk_assessment_type1` FOREIGN KEY (`risk_type_id`) REFERENCES `xtbl_risk_assessment_type` (`id`);

--
-- Constraints for table `tbl_risk_assessment_old`
--
ALTER TABLE `tbl_risk_assessment_old`
  ADD CONSTRAINT `fk_tbl_companies_company_id` FOREIGN KEY (`company_id`) REFERENCES `tbl_companies` (`id`);

--
-- Constraints for table `tbl_risk_data`
--
ALTER TABLE `tbl_risk_data`
  ADD CONSTRAINT `fk_tbl_risk_data_risk_areasassrisk` FOREIGN KEY (`risk_area_id`) REFERENCES `tbl_risk_areas` (`area_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_tbl_risk_data_risk_areasassrisk2` FOREIGN KEY (`risk_assessment_id`) REFERENCES `tbl_risk_areas` (`risk_assessment_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_tbl_risk_data_risk_areasassrisk3` FOREIGN KEY (`group_id`) REFERENCES `tbl_risk_areas` (`risk_group_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_suppliers`
--
ALTER TABLE `tbl_suppliers`
  ADD CONSTRAINT `fk_company_id` FOREIGN KEY (`company_id`) REFERENCES `tbl_companies` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_supplier_administrator`
--
ALTER TABLE `tbl_supplier_administrator`
  ADD CONSTRAINT `fk_tbl_supplier_administrator_tbl_suppliers` FOREIGN KEY (`supplier_id`) REFERENCES `tbl_suppliers` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_supplier_branch`
--
ALTER TABLE `tbl_supplier_branch`
  ADD CONSTRAINT `fk_tbl_suppier_branch_tbl_suppliers` FOREIGN KEY (`supplier_id`) REFERENCES `tbl_suppliers` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_supplier_documents`
--
ALTER TABLE `tbl_supplier_documents`
  ADD CONSTRAINT `fk_tbl_supplier_approval_documents_tbl_file_documents1` FOREIGN KEY (`file_id`) REFERENCES `tbl_file_documents` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_tbl_supplier_approval_documents_tbl_supplier_settings` FOREIGN KEY (`setting_id`) REFERENCES `tbl_supplier_settings` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_tbl_supplier_approval_documents_tbl_suppliers1` FOREIGN KEY (`supplier_id`) REFERENCES `tbl_suppliers` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_supplier_settings`
--
ALTER TABLE `tbl_supplier_settings`
  ADD CONSTRAINT `fk_supplier_setting_key` FOREIGN KEY (`company_id`) REFERENCES `tbl_companies` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_temporary_staff`
--
ALTER TABLE `tbl_temporary_staff`
  ADD CONSTRAINT `fk_tbl_temporary_staff_tbl_branches1` FOREIGN KEY (`branch_id`) REFERENCES `tbl_branches` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_tbl_temporary_staff_tbl_users1` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_temporary_staff_induction`
--
ALTER TABLE `tbl_temporary_staff_induction`
  ADD CONSTRAINT `fk_tbl_temporary_staff_induction_tbl_file_documents` FOREIGN KEY (`file_id`) REFERENCES `tbl_file_documents` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_tbl_temporary_staff_induction_tbl_temporary_staff1` FOREIGN KEY (`user_id`) REFERENCES `tbl_temporary_staff` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
