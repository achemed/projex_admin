<?php
/**
 * Created by PhpStorm.
 * User: Douglas
 * Date: 5/10/2017
 * Time: 3:15 PM
 */

class Package_Module_Class
{



    public static function getSingleModules($module_id = null){

        global $db;

        $allAllowedSql = "SELECT tbl_modules.*
                                FROM  tbl_modules 
                                 WHERE parent_id > 0 ";
        if(!empty($module_id))
        {
            $allAllowedSql .= " and id = ".$db->sqs($module_id);
        }
        $modules = $db->getRow($allAllowedSql);
        if($modules){
            return $modules;
        }else{
            return false;
        }
    }
    //

    public static function getAllSystemModules(){

        global $db;

        $allAllowedSql = "select * from tbl_modules where tbl_modules.is_active = 1 and tbl_modules.parent_id <> 0 and tbl_modules.global_view = 0";
        $modules = $db->getAll($allAllowedSql);

        if($modules){
            return $modules;
        }else{
            return false;
        }
    }


    public static function getAllModules($mainModule = true){

        global $db;

        $allAllowedSql = "SELECT tbl_modules.*
                                FROM  tbl_modules 
                                 WHERE ";

        if($mainModule){
            $allAllowedSql .= " parent_id = 0 ";

        }else{
            $allAllowedSql .= " parent_id > 0 ";
        }
        $allAllowedSql .= " AND  global_view = 0 ";
        $allAllowedSql .= " ORDER BY sort,parent_id";
        $modules = $db->getAll($allAllowedSql);

        if($modules){
            return $modules;
        }else{
            return false;
        }
    }

    public static function getAllGlobalModules($mainModule = true){

        global $db;

        $allAllowedSql = "SELECT tbl_modules.*
                                FROM  tbl_modules 
                               WHERE ";

        if($mainModule){
            $allAllowedSql .= " parent_id = 0 ";

        }else{
            $allAllowedSql .= " parent_id > 0 ";
        }

        $allAllowedSql .= " AND  global_view = 1 ";

        $allAllowedSql .= " ORDER BY sort,parent_id";
        $modules = $db->getAll($allAllowedSql);

        if($modules){
            return $modules;
        }else{
            return false;
        }
    }
    public static function getModulesSubModules($moduleId = null){

        global $db;

        $allAllowedSql = "SELECT tbl_modules.*
                                FROM  tbl_modules 
                                WHERE parent_id = ".$db->sqs($moduleId) ." ORDER BY sort,parent_id";

        $modules = $db->getAll($allAllowedSql);

        if($modules){
            return $modules;
        }else{
            return false;
        }
    }

    /**
     * Method to get sub menus
     * @param null $module_parent_id
     * @param null $roleId
     * @return array
     */
    private static function check_sub_modules_permission($module_parent_id = null,$roleId = null)
    {
        global $db;

        $allChildMenuSql = "SELECT   tbl_modules.*,tbl_permissions.role_id
                                            FROM  tbl_modules
                                            INNER JOIN tbl_permissions ON tbl_permissions.module_id = tbl_modules.id
                                            WHERE tbl_modules.parent_id = ".$db->sqs($module_parent_id)."
                                            AND tbl_permissions.role_id =".$db->sqs($roleId);

        //echo $allChildMenuSql;
        // exit;
        $allChildMenuSql .= " ORDER BY tbl_modules.sort,tbl_modules.parent_id ";
        $child = $db->getAll($allChildMenuSql);

        if ($child){
            //return $child;
            $array = array();
            foreach ($child as $page) {
                $array[$page['id']] = $page;
            }
            return $array;
        }
    }

    public static function has_module_permission($moduleId = null,$roleId = null)
    {
        global $db;
        //if no module_id is null, allow access
        if($moduleId==null || $roleId ==1)
        {
            return true;
        }

        $permissionSql = "SELECT   tbl_modules.*
                                            FROM  tbl_permissions
                                            INNER JOIN tbl_modules ON tbl_modules.id = tbl_permissions.module_id
                                            WHERE tbl_permissions.module_id= ".$db->sqs($moduleId)."
                                            AND tbl_permissions.role_id =".$db->sqs($roleId);
        /*$permissionSql = "SELECT   tbl_modules.*
                                            FROM  tbl_permissions
                                            INNER JOIN tbl_modules ON tbl_modules.id = tbl_permissions.module_id
                                            WHERE tbl_modules.module_name= ".$db->sqs($moduleName)."
                                            AND tbl_permissions.role_id =".$db->sqs($roleId);*/

        $isPermission = $db->getRow($permissionSql);

        if ($isPermission)
        {
            return true;

        }else{
            return false;
        }
    }

    public static function has_module_package($moduleId = null,$packageId = null){
            global $db;

            $permissionSql = "SELECT   tbl_modules.*
                                            FROM  tbl_package_modules
                                            INNER JOIN tbl_modules ON tbl_modules.id = tbl_package_modules.modules_id
                                            WHERE tbl_package_modules.modules_id= ".$db->sqs($moduleId)."
                                            AND tbl_package_modules.package_id =".$db->sqs($packageId);
            $isPermission = $db->getRow($permissionSql);

            if ($isPermission)
            {
                return true;

            }else{
                return false;
            }
    }

    public static function getModulesActions($moduleId = null){

        global $db;

        $allActionsSql = "SELECT tbl_module_actions.*
                                FROM  tbl_module_actions 
                                WHERE module_id = ".$db->sqs($moduleId);

        $modulesActionResults = $db->getAll($allActionsSql);

        if($modulesActionResults){
            return $modulesActionResults;
        }else{
            return false;
        }
    }

    /**
     * @param array $permission_data
     * @param array $sub_permissions
     * @param array $global_permission_data
     * @param array $global_sub_permissions
     * @param array $packageData
     * @return bool
     */
    static function addPackageModules($permission_data = array(), $sub_permissions = array(),$global_permission_data = array(),$global_sub_permissions = array(),$packageData = array())
    {
        global $db;
        $success = false;
        $db->startTransaction();
        //Clean the database for now...

        $SQL = "DELETE FROM tbl_package_modules WHERE package_id = ".$db->sqs($packageData['package_id']);
        $db->query($SQL);
            if(!empty($permission_data)){
                foreach ($permission_data as $allowed_main_module) {
                    $sql = "INSERT INTO tbl_package_modules (
                              `modules_id`,
                              `package_id`                              
                            ) VALUES ("
                        .$db->sqs($allowed_main_module).", "
                        .$db->sqs($packageData['package_id']).")";
                    $success  = $db->query($sql);
                }
            }

            if(!empty($sub_permissions) && $success){
                foreach ($sub_permissions as $allowed_main_module) {
                    $sql = "INSERT INTO tbl_package_modules (
                              `modules_id`,
                              `package_id`                            
                            ) VALUES ("
                        .$db->sqs($allowed_main_module).", "
                        .$db->sqs($packageData['package_id']).")";
                    $success = $db->query($sql);
                }
            }



            if(!empty($global_permission_data)){
                foreach ($global_permission_data as $allowed_main_module) {
                    $sql = "INSERT INTO tbl_package_modules (
                              `modules_id`,
                              `package_id`                              
                            ) VALUES ("
                        .$db->sqs($allowed_main_module).", "
                        .$db->sqs($packageData['package_id']).")";
                    $success  = $db->query($sql);
                }
            }

            if(!empty($global_sub_permissions) && $success){
                foreach ($global_sub_permissions as $allowed_main_module) {
                    $sql = "INSERT INTO tbl_package_modules (
                              `modules_id`,
                              `package_id`                            
                            ) VALUES ("
                        .$db->sqs($allowed_main_module).", "
                        .$db->sqs($packageData['package_id']).")";
                    $success = $db->query($sql);
                }
            }

            if($success){
                $db->commit();
                return true;
            }else{
                return false;
            }

    }


    static function addModulePermissionsActions($roleData,$permission_data = array(), $sub_permissions = array(),$permission_action_data = array(),$global_permission_data = array(),$global_sub_permissions = array(),$global_permission_action_data = array())
    {
        global $db;
        $success = false;
        $db->startTransaction();

        $sql = "INSERT INTO tbl_roles (
                                  role_name ,
                                  role_description,
                                  created_by                                
                            ) VALUES ("
            .$db->sqs($roleData['role_name']).", "
            .$db->sqs($roleData['role_description'])." , "
            .$db->sqs($_SESSION['user_id']).")";



        if($result = $db->query($sql)){
            $success = true;
            $roleId  = $db->insertId();

            if(!empty($permission_data)){
                foreach ($permission_data as $allowed_main_module) {
                    $sql = "INSERT INTO tbl_permissions (
                              `module_id`,
                              `role_id`                              
                            ) VALUES ("
                        .$db->sqs($allowed_main_module).", "
                        .$db->sqs($roleId).")";
                    $success  = $db->query($sql);
                }
            }

            if(!empty($sub_permissions) && $success){
                foreach ($sub_permissions as $allowed_main_module) {
                    $sql = "INSERT INTO tbl_permissions (
                              `module_id`,
                              `role_id`                            
                            ) VALUES ("
                        .$db->sqs($allowed_main_module).", "
                        .$db->sqs($roleId).")";
                    $success = $db->query($sql);
                }
            }

            if($success && !empty($permission_action_data)) {
                foreach ($permission_action_data as $permission_action) {
                    list($module, $action) = explode('|', $permission_action);

                    $sql = "INSERT INTO tbl_permission_actions (
                              role_id,
                              action_id,
                              module_id                          
                            ) VALUES ("
                        .$db->sqs($roleId).", "
                        .$db->sqs($action).", "
                        .$db->sqs($module).")";
                    $success = $db->query($sql);
                }
            }

            if(!empty($global_permission_data)){
                foreach ($global_permission_data as $allowed_main_module) {
                    $sql = "INSERT INTO tbl_permissions (
                              `module_id`,
                              `role_id`                              
                            ) VALUES ("
                        .$db->sqs($allowed_main_module).", "
                        .$db->sqs($roleId).")";
                    $success  = $db->query($sql);
                }
            }

            if(!empty($global_sub_permissions) && $success){
                foreach ($global_sub_permissions as $allowed_main_module) {
                    $sql = "INSERT INTO tbl_permissions (
                              `module_id`,
                              `role_id`                            
                            ) VALUES ("
                        .$db->sqs($allowed_main_module).", "
                        .$db->sqs($roleId).")";
                    $success = $db->query($sql);
                }
            }

            if($success && !empty($global_permission_action_data)) {
                foreach ($global_permission_action_data as $permission_action) {
                    list($module, $action) = explode('|', $permission_action);

                    $sql = "INSERT INTO tbl_permission_actions (
                              role_id,
                              action_id,
                              module_id                          
                            ) VALUES ("
                        .$db->sqs($roleId).", "
                        .$db->sqs($action).", "
                        .$db->sqs($module).")";
                    $success = $db->query($sql);
                }
            }

            if($success){
                $db->commit();
                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }

    }

    /**
     * @param $roleData
     * @param array $permission_data
     * @param array $sub_permissions
     * @param array $permission_action_data
     * @param array $g_permission_data
     * @param array $g_sub_permissions
     * @param array $g_permission_action_data
     * @return bool
     */
    static function updateModulePermissionsActions($roleData,$permission_data = array(), $sub_permissions = array(),$permission_action_data = array(),$g_permission_data = array(),$g_sub_permissions = array(),$g_permission_action_data = array())
    {

        global $db;
        $success = false;
        $roleId = $roleData['roleId'];

        $db->startTransaction();
        $sql = "UPDATE  tbl_roles  SET role_name =" .$db->sqs($roleData['role_name']).", 
                                  role_description =" .$db->sqs($roleData['role_description']).", 
                                  modified_by   =" .$db->sqs($_SESSION['user_id'])."
                                  WHERE role_id = " .$db->sqs($roleData['roleId']);

        $success = $db->query($sql);


        $SQL = "DELETE FROM tbl_permissions WHERE role_id = ".$db->sqs($roleId);
        $result = $db->query($SQL);
        if(!empty($permission_data)){

            foreach ($permission_data as $allowed_main_module) {
                $sql = "INSERT INTO tbl_permissions (
                              `module_id`,
                              `role_id`                              
                            ) VALUES ("
                    .$db->sqs($allowed_main_module).", "
                    .$db->sqs($roleId).")";
                $success  = $db->query($sql);
            }
        }

        if(!empty($g_permission_data)){

            foreach ($g_permission_data as $allowed_main_module) {
                $sql = "INSERT INTO tbl_permissions (
                              `module_id`,
                              `role_id`                              
                            ) VALUES ("
                    .$db->sqs($allowed_main_module).", "
                    .$db->sqs($roleId).")";
                $success  = $db->query($sql);
            }
        }

        //$SQL = "DELETE FROM tbl_permissions WHERE role_id = ".$db->sqs($roleId);
        //$result = $db->query($SQL);
        if(!empty($sub_permissions)){

            foreach ($sub_permissions as $allowed_main_module) {
                $sql = "INSERT INTO tbl_permissions (
                              `module_id`,
                              `role_id`                            
                            ) VALUES ("
                    .$db->sqs($allowed_main_module).", "
                    .$db->sqs($roleId).")";
                $success = $db->query($sql);
            }
        }

        if(!empty($g_sub_permissions)){

            foreach ($g_sub_permissions as $allowed_main_module) {
                $sql = "INSERT INTO tbl_permissions (
                              `module_id`,
                              `role_id`                            
                            ) VALUES ("
                    .$db->sqs($allowed_main_module).", "
                    .$db->sqs($roleId).")";
                $success = $db->query($sql);
            }
        }

        $SQL = "DELETE FROM tbl_permission_actions WHERE role_id = ".$db->sqs($roleId);
        $result = $db->query($SQL);
        if(!empty($permission_action_data)) {
            foreach ($permission_action_data as $permission_action) {
                list($module, $action) = explode('|', $permission_action);

                $sql = "INSERT INTO tbl_permission_actions (
                              role_id,
                              action_id,
                              module_id                          
                            ) VALUES ("
                    .$db->sqs($roleId).", "
                    .$db->sqs($action).", "
                    .$db->sqs($module).")";
                $success = $db->query($sql);
            }
        }

        if(!empty($g_permission_action_data)) {
            foreach ($g_permission_action_data as $permission_action) {
                list($module, $action) = explode('|', $permission_action);

                $sql = "INSERT INTO tbl_permission_actions (
                              role_id,
                              action_id,
                              module_id                          
                            ) VALUES ("
                    .$db->sqs($roleId).", "
                    .$db->sqs($action).", "
                    .$db->sqs($module).")";
                $success = $db->query($sql);
            }
        }

        $db->commit();
        return $success;
    }

    public static function has_module_action_permission($module_id, $action_id,$role_id)
    {
        global $db;
        //if no module_id is null, allow access
        if($module_id==null)
        {
            return true;
        }


        $allActionsSql = "SELECT tbl_permission_actions.*
                                FROM  tbl_permission_actions 
                                WHERE module_id = ".$db->sqs($module_id)."
                                AND  action_id = ".$db->sqs($action_id)."
                                AND  role_id = ".$db->sqs($role_id);

        $modulesActionResults = $db->getRow($allActionsSql);

        if($modulesActionResults){
            return true;
        }else{
            return false;
        }
    }

    public static function updateParentMenu($menuData = array())
    {
        global $db;

        $updateParentMenuSql = "UPDATE tbl_modules SET 
                                                      module_description = ".$db->sqs($menuData['name']).",
                                                      module_url = ".$db->sqs($menuData['url']).",
                                                      sort = ".$db->sqs($menuData['sort']).",
                                                      icon = ".$db->sqs($menuData['icon']).",
                                                      is_module_parent_id = 1
                                                  
                                      WHERE id=".$db->sqs($menuData['id']);

        $updateResults = $db->query($updateParentMenuSql);

        if($updateResults){
            return true;
        }else{
            return false;
        }
    }

    public static function updateParentNoChildMenu($menuData = array())
    {
        global $db;

        $updateParentMenuSql = "UPDATE tbl_modules SET 
                                                      is_module_parent_id = 0        
                                      WHERE id=".$db->sqs($menuData['id']);

        $updateResults = $db->query($updateParentMenuSql);

        if($updateResults){
            return true;
        }else{
            return false;
        }
    }


    public static function updateChildMenu($menuData = array())
    {
        global $db;


        $updateParentMenuSql = "UPDATE tbl_modules SET 
                                                      module_description = ".$db->sqs($menuData['name']).",
                                                      module_url = ".$db->sqs($menuData['url']).",
                                                      sort = ".$db->sqs($menuData['sort']).",
                                                      icon = ".$db->sqs($menuData['icon']).",
                                                      is_module_parent_id = 0,
                                                      parent_id =".$db->sqs($menuData['parent_id'])."
                                                  
                                      WHERE id=".$db->sqs($menuData['id']);

        $updateResults = $db->query($updateParentMenuSql);

        if($updateResults){
            return true;
        }else{
            return false;
        }
    }

    /**
     *
     */
    public static function logModuleAccess($module = null, $moduleAction = null){
        global $db;

        $is_global = 0;
        $branch_id = 0;
        if(isset($_SESSION['show_global_menu']) && $_SESSION['show_global_menu'] == true){

            $is_global = 1;
        }

        if(isset($_SESSION['branch_id']) && $_SESSION['branch_id'] > 0){

            $branch_id = $_SESSION['branch_id'];
        }

        if(isset($_SESSION['user_id'])){
            $sql = "INSERT INTO tbl_user_module_logs (
                              module_name,
                              module_action,
                              is_global,
                              branch_id,
                             created_by
                            ) VALUES ("
                .$db->sqs($module).", "
                .$db->sqs($moduleAction).", "
                .$db->sqs($is_global).", "
                .$db->sqs($branch_id).", "
                .$db->sqs($_SESSION['user_id']).")";
            $success = $db->query($sql);
        }

    }


    public static function getUserLogModuleAccess($userId = null){
        global $db;
        $allActionsSql = "SELECT tbl_user_module_logs.*
                                FROM  tbl_user_module_logs 
                                WHERE created_by = ".$db->sqs($userId)." ORDER BY tbl_user_module_logs.date_created DESC";

        $modulesActionResults = $db->getAll($allActionsSql);

        if($modulesActionResults){
            return $modulesActionResults;
        }else{
            return false;
        }

    }

    /**
     * Escape
     * @param $string
     * @return string
     */
    public static function e($string)
    {
        return htmlentities($string);
    }

}