<?php

//Include the common file
require_once 'config.php';

global $objUsers;


//Get the action to perform
$action	= (isset($_REQUEST['action'])) ? $_REQUEST['action'] : 'defaultAction';

//Get the action to perform
$module	= (isset($_REQUEST['module'])) ? $_REQUEST['module'] : 'default';

//If not logged in set action to login.
if(isset($_SESSION['loggedin'])){
    
} else {
    if($action != 'processAdminlogin'){
        $action = 'defaultAction';
        $module = 'default';
    }
}

//CHECK IF USER IS ALLOWED TO PERFORM ACTION


//Start the output buffer
ob_start();

//Include the main content template based on action 
$actionOutput = processAction($module, $action);	


if(!empty($actionOutput)){
    include($actionOutput);
}

//Get the output from the output buffer
$output = ob_flush();	

//Clean the output buffer and turn it off
ob_end_clean();


/**
 * Main function of the index script. It processes requests and returns the appropriate response.
 * 
 * @param string $action The request 
 * @param string $module The module or section of functionality the request relates to
 * @return string $output The template to be displayed.
 */
function processAction($module, $action)
{
    
    if($module == 'default'){
        return controller::$action();
    } else {
        if(class_exists($module)){
            return $module::$action();
        } else {
            return FALSE;
        }    
    }
}