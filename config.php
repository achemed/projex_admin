<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
//Start the session

if(isset($_SESSION)){
    unset($_SESSION);
    session_unset();
    session_destroy();
}
session_start();

$base_url = null;
function is_https()
{
    if ( ! empty($_SERVER['HTTPS']) && strtolower($_SERVER['HTTPS']) !== 'off')
    {
        return TRUE;
    }
    elseif (isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https')
    {
        return TRUE;
    }
    elseif ( ! empty($_SERVER['HTTP_FRONT_END_HTTPS']) && strtolower($_SERVER['HTTP_FRONT_END_HTTPS']) !== 'off')
    {
        return TRUE;
    }

    return FALSE;
}

if (isset($_SERVER['HTTP_HOST']) && preg_match('/^((\[[0-9a-f:]+\])|(\d{1,3}(\.\d{1,3}){3})|[a-z0-9\-\.]+)(:\d+)?$/i', $_SERVER['HTTP_HOST']))
{
    $base_url = (is_https() ? 'https' : 'http').'://'.$_SERVER['HTTP_HOST']
        .substr($_SERVER['SCRIPT_NAME'], 0, strpos($_SERVER['SCRIPT_NAME'], basename($_SERVER['SCRIPT_FILENAME'])));
}
else
{
    $base_url = 'http://localhost/';
}

function isLocalhost($whitelist = ['127.0.0.1', '::1']) {
    return in_array($_SERVER['REMOTE_ADDR'], $whitelist);
}

$host_url = rtrim($base_url,"/");;
//Define the root dir
define( 'ROOT', dirname(__file__) );
define( 'TEMPLATES', ROOT.'/frontend/templates/');
define( 'BASE_URL',$host_url);
define( 'HOSTNAME',$host_url);
define( 'SALT1', 'asdfjhaufhawefasdf');
define( 'DEFAULT_LANG', 'English');
define( 'LANGUAGES', ROOT.'/languages/');
define('FOLDER_DIRECTORY','/documents/');
define('FOLDER_DIRECTORY_ROOT',ROOT.FOLDER_DIRECTORY);
define('COMPANY_DOCUMENTS_PATH','/frontend/documents/');
define('COMPANY_DOCS_PATH','/frontend/doc/');
define('SITE_UPLOADS_PATH','/frontend/media/uploads/');
define('SITE_IMAGE_PATH',HOSTNAME.'/frontend/media/images/');
define('ICON_IMAGE_FOLDER','file_icons/');
define('DEFAULT_FOLDER_VIEW','gridView');
define('DOCX_TEMP_PATH', ROOT.FOLDER_DIRECTORY);
define('DB_PREFIX', 'sheqintel_');
define('MASTER_DB', DB_PREFIX.'master');
///global types
//use \PhpOffice\PhpWord\Settings;
//Settings::loadConfig('vendor\phpoffice\phpword\phpword.ini.dist');

//Define modules array

$modulesArray = array('legal','regions','mail_template','support','package','module','admin_users','company_client','company','folders','company_client_users','update','sheq');
//Include classes
include_once(ROOT.'/classes/db_class.php');
include_once(ROOT.'/libraries/package_module_class.php');
//include_once(ROOT.'/classes/language_class.php');
include_once(ROOT.'/classes/template_class.php');

foreach($modulesArray as $module){
    include_once(ROOT.'/classes/' . $module . '_class.php');
}
    
//Define global db obj
global $db;

$config = array();
// DB Settings
$config['db']                                   = array();
$config['db']['host']                           = 'localhost';
if(isLocalhost()){
    $config['db']['username']                       = 'root';
    $config['db']['password']                       = '';
}else {
    $config['db']['username']                       = 'root';
    $config['db']['password']                       = 'ureyus22';
}

$config['db']['database']                       = MASTER_DB;
$config['db']['port']                           = 3306;


$db = new db_class( $config['db'] );

$db->connect();


global $objUsers;
if(!is_object($objUsers)){
    $objUsers = new admin_users_class();
}

global $objCompanies;
if(!is_object($objCompanies)){
    $objCompanies = new company();
}

global $objFolders;
if(!is_object($objFolders)){
    $objFolders = new folders_class();
}

global $objTemplate;
if(!is_object($objTemplate)){
    $objTemplate = new template_class();
}

global $objClient;

if(!is_object($objClient)){
    $objClient = new company_client_class();
}

global $objClientUser;

if(!is_object($objClientUser)){
    $objClientUser = new company_client_users_class();
}


global $objUpdates;

if(!is_object($objUpdates)){
    $objUpdates = new update_class();
}

global $objModules;

if(!is_object($objModules)){
    $objModules = new module_class();
}

global $objPackages;
if(!is_object($objPackages)){
    $objPackages = new package_class();
}


global $objSupport;
if(!is_object($objSupport)){
    $objSupport = new support_class();
}

global $objMailTemp;
if(!is_object($objMailTemp)){
    $objMailTemp = new mail_template_class();
}

global $objRegions;
if(!is_object($objRegions)){
    $objRegions = new regions();
}


global $objLegal;
if(!is_object($objLegal)){
    $objLegal = new legal_class();
}

global $objSheq;
if(!is_object($objSheq)){
    $objSheq = new sheq_class();
}
//near_misses
/**
/**
 * Autoloader function to automatically load module controllers
 *
 * @param type $class
 */
spl_autoload_register(function($class)
{
        include_once(ROOT . '/controllers/' . $class . '.php');
});