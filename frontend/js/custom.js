$(document).ready(function() {
    
//create tabs 
$("#companyinfoform").show();
$("#additionalbranchform").show();
$("#departmentsform").show();
$("#occupationsform").show();
$("#companyinfo").click(function(){
    $("#companyinfoform").show();
    $("#additionalbranchform").hide();
    $("#departmentsform").hide();
    $("#occupationsform").hide();
});
$("#additionalbranches").click(function(){
    $("#additionalbranchform").show();
    $("#companyinfoform").hide();
    $("#departmentsform").hide();
    $("#occupationsform").hide();
});

$("#departments").click(function(){
    $("#departmentsform").show();
    $("#additionalbranchform").hide();
    $("#companyinfoform").hide();
    $("#occupationsform").hide();
});
$("#occupationsform").click(function(){
	$("#occupationsform").show();
    $("#additionalbranchform").hide();
    $("#companyinfoform").hide();
    $("#departmentsform").hide();
});

//tooltip
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();
});
//toggle department form
$("#departmentsform").hide();
$("#addDepartment").click(function(){
    $("#departmentsform").toggle('slow');
});
$("#additionalbranchform").hide();
$("#addBranch").click(function(){
    $("#additionalbranchform").toggle('slow');
});
$("#occupationForm").hide();
$("#addOccupation").click(function(){
    $("#occupationForm").toggle('slow');
});
$("#employeeForm").hide();
$("#addEmployee").click(function(){
    $("#employeeForm").toggle('slow');
});
$("#systemUserForm").hide();
$("#addNewUser").click(function(){
    $("#systemUserForm").toggle('slow');
});
$("#addInspectionQualityForm").hide();
$("#addInspectionQuality").click(function(){
    $("#addInspectionQualityForm").toggle('slow');
});
//hide and show for registration form
$("#occupation").hide();
$("#optionsoccupation").change(function(){
   var occupation = $("#optionsoccupation" ).val();
   if(occupation === 'other'){
       $("#occupation").show();
   }else{
       $("#occupation").hide();
   }
});
//company type
$("#hideNumberofBranch").hide();
$("#hideNumberofCompanies").hide();

$("#companytype").change(function(){
   var companytype = $("#companytype" ).val();
   
   if(companytype == 2){
       $("#hideNumberofBranch").show();
       $("#hideNumberofCompanies").hide();
   } else if (companytype == 3){
       $("#hideNumberofBranch").hide();
       $("#hideNumberofCompanies").show();
   }else{
       $("#hideNumberofBranch").hide();
       $("#hideNumberofCompanies").hide();
   }
});
//hide and show for employee form
$("#otherOccupationName").hide();
$("#occupationName").change(function(){
   var occupationName = $("#occupationName" ).val();
   if(occupationName === 'other'){
       $("#otherOccupationName").show();
   }else{
       $("#otherOccupationName").hide();
   }
});

//hide and show for employee form
$("#newOccupationName").hide();
$("#occupationName").change(function(){
   var occupationName = $("#occupationName").val();
   if(occupationName === 'other'){
       $("#newOccupationName").show();
   }else{
       $("#newOccupationName").hide();
   }
});


//hide 
$("#othersupplierType").hide();
$("#supplierType").change(function(){
   var occupation = $("#supplierType" ).val();
   if(occupation === 'other'){
       $("#othersupplierType").show();
   }else{
       $("#othersupplierType").hide();
   }
});
$("#addNewCourseForm").hide();
$("#addNewCourse").click(function(){
    $("#addNewCourseForm").toggle('slow');
});

$("#medicalForm").hide();
$("#addMedical").click(function(){
    $("#medicalForm").toggle('slow');
});

$("#ppeHistoryForm").hide();
$("#addPPEHistory").click(function(){
    $("#ppeHistoryForm").toggle('slow');
});

$("#appointmentForm").hide();
$("#addAppointment").click(function(){
    $("#appointmentForm").toggle('slow');
    $("#addInspectionForm").hide('slow');
    
});

$("#addInspectionForm").hide();
$("#addInspection").click(function(){
    $("#addInspectionForm").toggle('slow');
    $("#appointmentForm").hide('slow');
    
});

$("#lodgeCorrctiveActionForm").hide();
$("#lodgeCorrctiveAction").click(function(){
    $("#lodgeCorrctiveActionForm").toggle('slow');
});

$("#environmental").hide();
$("#quality").hide();
$("#otherIncidents").hide();

$("#incidentCategory").change(function(){
   var incidentCategory = $("#incidentCategory" ).val();
   switch (incidentCategory) {
        case '1':
            $("#healthandsafety").show();
            $("#environmental").hide();
            $("#quality").hide();
            $("#otherIncidents").hide();
            break;
        case '2':
            $("#environmental").show();
            $("#healthandsafety").hide();
            $("#quality").hide();
            $("#otherIncidents").hide();
            break;
        case '3':
            $("#quality").show();
            $("#healthandsafety").hide();
            $("#environmental").hide();
            $("#otherIncidents").hide();
            break;
        case '4':
            $("#otherIncidents").show();
            $("#healthandsafety").hide();
            $("#environmental").hide();
            $("#quality").hide();
            break;
        default:
            $("#healthandsafety").show();
            break;
    }
});

$("#optInducedhumanError").show();
$("#optDesign").hide();
$("#optAcqusition").hide();
$("#optDepartment").hide();
$("#optSupervision").hide();
$("#optMaintenance").hide();


$("#resourceOptions").change(function(){
   var resourceOptions = $("#resourceOptions").val();
   switch (resourceOptions) {
        case '1':
            $("#optInducedhumanError").show('slow');
            $("#optDesign").hide();
            $("#optAcqusition").hide();
            $("#optDepartment").hide();
            $("#optSupervision").hide();
            $("#optMaintenance").hide();
            break;
        case '2':
            $("#optDesign").show('slow');
            $("#optInducedhumanError").hide();
            $("#optAcqusition").hide();
            $("#optDepartment").hide();
            $("#optSupervision").hide();
            $("#optMaintenance").hide();
            break;
        case '3':
            $("#optAcqusition").show('slow');
            $("#optInducedhumanError").hide();
            $("#optDesign").hide();
            $("#optDepartment").hide();
            $("#optSupervision").hide();
            $("#optMaintenance").hide();
            break;
        case '4':
            $("#optDepartment").show('slow');
            $("#optDesign").hide();
            $("#optAcqusition").hide();
            $("#optInducedhumanError").hide();
            $("#optSupervision").hide();
            $("#optMaintenance").hide();
            break;
        case '5':
            $("#optMaintenance").show('slow');
            $("#optInducedhumanError").hide();
            $("#optDesign").hide();
            $("#optAcqusition").hide();
            $("#optDepartment").hide();
            $("#optSupervision").hide();
            break;
        case '6':
            $("#optSupervision").show('slow');
            $("#optInducedhumanError").hide();
            $("#optDesign").hide();
            $("#optAcqusition").hide();
            $("#optMaintenance").hide();
            $("#optDepartment").hide();
            break;
    }
});

$("#optTaskExperience").hide();
$("#optOrganizational").hide();
$("#optPersonalCapabilities").hide();
$("#optTrainignEducation").hide();
$("#optSupervisions").hide();
$("#optBehaiour").hide();

$("#causeAnalyses").change(function(){
   var causeAnalyses = $("#causeAnalyses").val();
   switch (causeAnalyses) {
        case '1':
            $("#optHumanError").show('slow');
            $("#optTaskExperience").hide();
            $("#optOrganizational").hide();
            $("#optPersonalCapabilities").hide();
            $("#optTrainignEducation").hide();
            $("#optSupervisions").hide();
            $("#optBehaiour").hide();
            break;
        case '2':
            $("#optTaskExperience").show('slow');
            $("#optHumanError").hide();
            $("#optOrganizational").hide();
            $("#optPersonalCapabilities").hide();
            $("#optTrainignEducation").hide();
            $("#optSupervisions").hide();
            $("#optBehaiour").hide();
            break;
        case '3':
            $("#optOrganizational").show('slow');
            $("#optHumanError").hide();
            $("#optTaskExperience").hide();
            $("#optPersonalCapabilities").hide();
            $("#optTrainignEducation").hide();
            $("#optSupervisions").hide();
            $("#optBehaiour").hide();
            break;
        case '4':
            $("#optPersonalCapabilities").show('slow');
            $("#optHumanError").hide();
            $("#optTaskExperience").hide();
            $("#optOrganizational").hide();
            $("#optTrainignEducation").hide();
            $("#optSupervisions").hide();
            $("#optBehaiour").hide();
            break;
        case '5':
            $("#optTrainignEducation").show('slow');
            $("#optHumanError").hide();
            $("#optTaskExperience").hide();
            $("#optOrganizational").hide();
            $("#optPersonalCapabilities").hide();
            $("#optSupervisions").hide();
            $("#optBehaiour").hide();
            break;
        case '6':
            $("#optSupervisions").show('slow');
            $("#optHumanError").hide();
            $("#optTaskExperience").hide();
            $("#optOrganizational").hide();
            $("#optPersonalCapabilities").hide();
            $("#optTrainignEducation").hide();
            $("#optBehaiour").hide();
            break;
        case '7':
            $("#optBehaiour").show('slow');
            $("#optHumanError").hide();
            $("#optTaskExperience").hide();
            $("#optOrganizational").hide();
            $("#optPersonalCapabilities").hide();
            $("#optTrainignEducation").hide();
            $("#optSupervisions").hide();
            break;
        default:
            $("#optHumanError").show();
            break;
    }
});

$("#addPersonForm").hide();
$("#addResourceForm").hide();
$("#addWitnessForm").hide();

$("#addPerson").click(function(){
    $("#addPersonForm").toggle('slow');
    $("#addResourceForm").hide('slow');
    $("#addWitnessForm").hide('slow');
});

$("#addResource").click(function(){
    $("#addResourceForm").toggle('slow');
    $("#addPersonForm").hide('slow');
    $("#addWitnessForm").hide('slow');
});

$("#addWitness").click(function(){
    $("#addWitnessForm").toggle('slow');
    $("#addPersonForm").hide('slow');
    $("#addWitnessForm").hide('slow');
});


$("#addNewCompanyForm").hide();
$("#addNewCompany").click(function(){
        $("#addNewCompanyForm").toggle('slow');
});


//hide and show for suppliers/clients-customers form
    $("#company").hide();
    $("#companyIndividual").change(function(){
        var companyIndividual = $("#companyIndividual" ).val();
        if(companyIndividual === 'company'){
            $("#company").show();
            $("#company :input").prop('required',true);
        }else{
            $("#company").hide();
            $("#company :input").prop('required',null);
        }
    });
    $("#individual").hide();
    $("#companyIndividual").change(function(){
        var companyIndividual = $("#companyIndividual" ).val();
        if(companyIndividual === 'individual'){
            $("#individual").show();
            $("#individual :input").prop('required',true);
        }else{
            $("#individual").hide();
            $("#individual :input").prop('required',null);
        }
    });

//Date pickers
$("#appointmentDate").datepicker({dateFormat:"yy/mm/dd"}).datepicker();
$("#expiryDate").datepicker({dateFormat:"yy/mm/dd"}).datepicker();
$("#inspectionDate").datepicker({dateFormat:"yy/mm/dd"}).datepicker();
$("#trainingDate").datepicker({dateFormat:"yy/mm/dd"}).datepicker();
$("#expiryDate").datepicker({dateFormat:"yy/mm/dd"}).datepicker();
$("#licenseCompletionDate").datepicker({dateFormat:"yy/mm/dd"}).datepicker();
$("#licenseExpiryDate").datepicker({dateFormat:"yy/mm/dd"}).datepicker();
$("#registrationExpiryDate").datepicker({dateFormat:"yy/mm/dd"}).datepicker();
$("#medicalDate").datepicker({dateFormat:"yy/mm/dd"}).datepicker();
$("#scheduleDate").datepicker({dateFormat:"yy/mm/dd"}).datepicker();
$("#committeesMeetingDate").datepicker({dateFormat:"yy/mm/dd"}).datepicker();
$("#agendaDate").datepicker({dateFormat:"yy/mm/dd"}).datepicker();
$("#meetingTime").datepicker();
$("#incidentTime").timepicker();
$("#correctiveActionDate").datepicker({dateFormat:"yy/mm/dd"}).datepicker();
$("#correctiveActionTargetDate").datepicker({dateFormat:"yy/mm/dd"}).datepicker();

tinymce.init({
    selector: '.tinyMCEselector',
    menubar: false
});

});