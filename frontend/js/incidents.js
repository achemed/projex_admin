$(document).ready(function(){
    
    $("#incidentDate").datetimepicker({dateFormat:"yy/mm/dd", controlType: 'select',oneLine: true, timeFormat: 'hh:mm tt', maxDate: new Date(),changeYear:true, changeMonth:true,yearRange:"1900:3000"}).datetimepicker();
    $("#investigationDate").datetimepicker({dateFormat:"yy/mm/dd", controlType: 'select',oneLine: true, timeFormat: 'hh:mm tt', minDate: new Date(),changeYear:true, changeMonth:true,yearRange:"1900:3000"}).datetimepicker();
    
    $("#environment").hide();
    $("#quality").hide();
    $("#food").hide();

$("#incidentGroup").change(function(){
    
   var incidentReportType = $("#incidentGroup" ).val();
   switch (incidentReportType) {
        case '1':
            $("#ohs").show();
            $("#environment").hide();
            $("#quality").hide();
            $("#food").hide();
            break;
        case '2':
            $("#environment").show();
            $("#ohs").hide();
            $("#quality").hide();
            $("#food").hide();
            break;
        case '3':
            $("#quality").show();
            $("#ohs").hide();
            $("#environment").hide();
            $("#food").hide();
            break;
        case '4':
            $("#food").show();
            $("#ohs").hide();
            $("#environment").hide();
            $("#quality").hide();
            break;
        default:
            $("#ohs").show();
            break;
    }
});
    
    $("#addNewResourceBtn").click(function(){
        $("#addNewResource-modal").modal('toggle');
        $("#resourceDescription").val('');
    });
    $("#addNewCauseBtn").click(function(){
        $("#addNewCause-modal").modal('toggle');
        $("#newCause").val('');
    });
    $("#addNewEmployeeBtn").click(function(){
        $("#addNewEmployee-modal").modal('toggle');
    });
    $("#addNewCustomerBtn").click(function(){
        $("#addNewCustomer-modal").modal('toggle');
    });
    $("#addNewServiceProviderBtn").click(function(){
        $("#addNewServiceProvider-modal").modal('toggle');
    });
    $("#addNewSupplierBtn").click(function(){
        $("#addNewSupplier-modal").modal('toggle');
    });

    $('input[name="medicalTreatment"]').click(function(){
        if($(this).attr("value")==="0"){
            $(".referenceBox").not(".medical").hide();
            $(".medical").show();
        }else{
            $(".medical").hide();
        }
    });

    $("#employee").hide();
    $("#customer").hide();
    $("#supplier").hide();
    $("#visitor").hide();
    $("#public").hide();
    
    $("#witnessPersonType").change(function(){
       var witnessPersonType = $("#witnessPersonType").val();
       if(witnessPersonType === '1'){
           $("#employee").show();
       }else{
           $("#employee").hide();
       }

       if(witnessPersonType === '2'){
           $("#customer").show();
       }else{
           $("#customer").hide();
       }

       if(witnessPersonType === '3'){
           $("#supplier").show();
       }else{
           $("#supplier").hide();
       }

       if(witnessPersonType === '4'){
           $("#visitor").show();
       }else{
           $("#visitor").hide();
       }
       if(witnessPersonType === '5'){
           $("#public").show();
       }else{
           $("#public").hide();
       }
    });

    $("#incidentEmployee").hide();
    $("#incidentCustomer").hide();
    $("#incidentSupplier").hide();
    $("#incidentVisitor").hide();
    $("#incidentPublic").hide();
    
    $("#personType").change(function(){
       var personType = $("#personType" ).val();
       if(personType === '1'){
           $("#incidentEmployee").show();
       }else{
           $("#incidentEmployee").hide();
       }

       if(personType === '2'){
           $("#incidentCustomer").show();
       }else{
           $("#incidentCustomer").hide();
       }

       if(personType === '3'){
           $("#incidentSupplier").show();
       }else{
           $("#incidentSupplier").hide();
       }

       if(personType === '4'){
           $("#incidentVisitor").show();
       }else{
           $("#incidentVisitor").hide();
       }

       if(personType === '5'){
           $("#incidentPublic").show();
       }else{
           $("#incidentPublic").hide();
       }
    });

    $('#customerCompanyType').change(function(){
        var companyType = $(this).val();
        if(companyType === '1'){
            $("#hidecustomerCompanyName").removeClass('hidden');
            $("#hidecustomerNameSurname").addClass('hidden');
        }else{
            $("#hidecustomerNameSurname").removeClass('hidden');
            $("#hidecustomerCompanyName").addClass('hidden');
        }
    });
    
    $('#witnessCompanyType').change(function(){
        var companyType = $(this).val();
        if(companyType === '1'){
            $("#witnessHidecustomerCompanyName").removeClass('hidden');
            $("#witnesshidecustomerNameSurname").addClass('hidden');
        }else{
            $("#witnesshidecustomerNameSurname").removeClass('hidden');
            $("#witnessHidecustomerCompanyName").addClass('hidden');
        }
    });
    
    $('input[name="personInjured"]').click(function () {
        if ($(this).attr("value") === "1") {
            $(".incidentBox2").not(".injuries").hide();
            $(".injuries").show();
        } else {
            $(".injuries").hide();
        }
    });
    
    $('body').on('change', '#incidentGroup', function(){
        var group_id = $(this).val();
        showIncidentTypesByGroupId(group_id, 'incidentType');
        $('#incidentType').selectpicker('refresh');
    });
    
   /**
    * Method to show incident types by group id
    * @param {String} groupid
    * @param {String} selectorid
    * @return {Array}
    */
   function showIncidentTypesByGroupId(groupid, selectorid){
        var opt = '';
        var selector = "#"+selectorid;
        $.ajax({
            type: "POST",
            url : "index.php?module=incidents&action=getIncidentTypesByCategoryId",
            dataType:'json',
            data :'groups='+groupid,
                success: function(data){
                    $(selector).find('option').remove();
                    if(data.length > 0){
                        $.each(data, function(key, val) {
                            opt = $('<option/>');
                            opt.val(val.id);
                            opt.text(val.type_name);
                            $(selector).append(opt);
                        }); 
                        $(selector).selectpicker('refresh');
                    }else{
                       $(selector).find('option').remove();
                    }
                        
                },
           error: function(){
               console.log('Search failed');
           }
       });  
   }
   
    $('body').on('change', '#incidentDepartment', function(){
        var departmentid = $(this).find("option:selected").val();
        showManagersByDepartmentId(departmentid, 'incidentDepartmentManager');
        $('#incidentDepartmentManager').selectpicker('refresh');
    });
   /**
    * Method to show incident types by group id
    * @param {String} departmentid
    * @param {String} selectorid
    * @return {Array}
    */
   function showManagersByDepartmentId(departmentid, selectorid){
        var opt = '';
        var selector = "#"+selectorid;
        $.ajax({
            type: "POST",
            url : "index.php?module=incidents&action=getDepartmentManager",
            dataType:'json',
            data :'department_id='+departmentid,
                success: function(data){
                    $(selector).find('option').remove();
                    if(data.length > 0){
                        $.each(data, function(key, val) {
                            opt = $('<option/>');
                            opt.val(val.id);
                            opt.text(val.firstname+ ' '+val.lastname);
                            $(selector).append(opt);
                        }); 
                        $(selector).selectpicker('refresh');
                    }else{
                       $(selector).find('option').remove();
                    }
                        
                },
           error: function(){
               console.log('Search failed');
           }
       });  
   }
   
   //reported person
   $('body').on('change', '#reportedPersonDepartment', function(){
        var departmentid = $(this).val();
        showEmployeeByDepartmentId(departmentid, 'reportedPerson', 'reportedPersonNotFound');
        $('#reportedPerson').selectpicker('refresh');
   });
   //person involved 
   $('body').on('change', '#personInvolvedEmployeeDepartment', function(){
        var departmentid = $(this).val();
        showEmployeeByDepartmentId(departmentid, 'personInvolvedEmployeeName', 'personInvolvedEmployeeNotFound');
        $('#personInvolvedEmployeeName').selectpicker('refresh');
   });
   //witness involved
   $('body').on('change', '#witnessEmployeeDepartment', function(){
        var departmentid = $(this).val();
        showEmployeeByDepartmentId(departmentid, 'witnessEmployeeName', 'NotFoundwitnessEmployee');
        $('#witnessEmployeeName').selectpicker('refresh');
   });
   
   /**
    * Method to show incident types by group id
    * @param {String} departmentid
    * @param {String} selectorid
    * @param {String} notfoundselector
    * @return {Array}
    */
   function showEmployeeByDepartmentId(departmentid, selectorid, notfoundselector){
        var opt = '';
        var selector = "#"+selectorid;
        var notfoundselectorid = "#"+notfoundselector;
        $.ajax({
            type: "POST",
            url : "index.php?module=incidents&action=showEmployeeByDepartmentId",
            dataType:'json',
            data :'departmentid='+departmentid,
                success: function(data){
                    $(selector).find('option').remove();
                    $(notfoundselectorid).addClass("hidden");
                    if(data.length > 0){
                            opt = $('<option/>');
                            opt.val('');
                            opt.text('Please select one');
                            $(selector).append(opt);
                        $.each(data, function(key, val) {
                            opt = $('<option/>');
                            opt.val(val.id);
                            opt.text(val.firstname +' '+ val.lastname);
                            $(selector).append(opt);
                        }); 
                        $(selector).selectpicker('refresh');
                    }else{
                       $(notfoundselectorid).removeClass("hidden");
                       $(selector).find('option').remove();
                       $(selector).selectpicker('refresh');
                    }     
                },
           error: function(){
               console.log('Search failed');
           }
       });  
   }
   //asset types
   $('body').on('change', '#incidentAssetCategory', function(){
        var group_id = $(this).val();
        showAssetTypesByGroupId(group_id, 'incidentAssetType');
        $('#incidentAssetType').selectpicker('refresh');
    });
    
    $('body').on('change', '#incidentResourceCategory', function(){
        var group_id = $(this).val();
        showAssetTypesByGroupId(group_id, 'incidentResourceType');
        $('#incidentResourceType').selectpicker('refresh');
    });
   /**
    * Method to show incident types by group id
    * @param {String} groupid
    * @param {String} selectorid
    * @return {Array}
    */
   function showAssetTypesByGroupId(groupid, selectorid){
        var opt = '';
        var selector = "#"+selectorid;
        $.ajax({
            type: "POST",
            url : "index.php?module=assets&action=showAssetTypes",
            dataType:'json',
            data :'categoryId='+groupid,
                success: function(data){
                    $(selector).find('option').remove();
                    if(data.length > 0){
                        
                        $.each(data, function(key, val) {
                            opt = $('<option/>');
                            opt.val(val.id);
                            opt.text(val.name);
                            $(selector).append(opt);
                        });
                        
                        opt = $('<option/>');
                        opt.val('0');
                        opt.text('Other');
                        $(selector).append(opt); 
                        
                        $(selector).selectpicker('refresh');
                    }else{
                       $(selector).find('option').remove();
                    }
                        
                },
           error: function(){
               console.log('Search failed');
           }
       });  
    }
    
    $('body').on('change', '#incidentAssetType', function(){
        searchAssetInvolved('incidentAssetDescription','NotFoundAssetInvolved');
        $('#incidentAssetDescription').selectpicker('refresh');
    });
    
    /**
     * Method to search asset involved
     * @param {String} selectorid
     * @param {String} notfoundselectorid
     */
    function searchAssetInvolved(selectorid, notfoundselectorid){
        var opt = '';
        var selector = "#"+selectorid;
        var notfoundselector = "#"+notfoundselectorid;
        $.ajax({
            type: "POST",
            url : "index.php?module=incidents&action=searchAssetInvolved",
            dataType:'json',
            data:$('#addAssetFormInvolved').serialize(),
            success: function (data){
                $(selector).find('option').remove();
                $(notfoundselector).addClass('hidden');
                if(data.length > 0){
                    $.each(data, function(key, val) {
                        opt = $('<option/>');
                        opt.val(val.id);
                        opt.text(val.description);
                        $(selector).append(opt);
                    }); 
                    $(selector).selectpicker('refresh');
                }else{
                   $(notfoundselector).removeClass('hidden');
                   $(selector).find('option').remove();
                }
            },
            error: function(){
                console.log('Asset Involved Error');
            }
        });  
    }
 
/*-------------------------------------------End of forms ---------------------------------*/    
    /**
     * Call functions for person involved employee
     */  
    $('body').on('change', '#personInvolvedEmployeeName', function(){
        var personInvolvedID = $(this).find("option:selected").val();
        $('#personInvolvedID').val(personInvolvedID);
    });
   
    $('body').on('change', '#employeeCustomerCompanyName', function(){
        var personInvolvedID = $(this).find("option:selected").val();
        $('#personInvolvedID').val(personInvolvedID);
    });
    
    $('body').on('change', '#customerNameSurname', function(){
        var personInvolvedID = $(this).find("option:selected").val();
        $('#personInvolvedID').val(personInvolvedID);
    });
    
    $('body').on('change', '#personInvolvedSupplier', function(){
        var personInvolvedID = $(this).find("option:selected").val();
        $('#personInvolvedID').val(personInvolvedID);
    });
    
    
   /**
     * Call functions for witness involved employee
     */  
    $('body').on('change', '#witnessEmployeeName', function(){
        var witnessInvolvedID = $(this).find("option:selected").val();
        $('#witnessInvolvedID').val(witnessInvolvedID);
    });
    
    $('body').on('change', '#customerCompanyName', function(){
        var witnessInvolvedID = $(this).find("option:selected").val();
        $('#witnessInvolvedID').val(witnessInvolvedID);
    });
    
    $('body').on('change', '#customerNameSurname', function(){
        var witnessInvolvedID = $(this).find("option:selected").val();
        $('#witnessInvolvedID').val(witnessInvolvedID);
    });
    
    $('body').on('change', '#witnessSupplier', function(){
        var witnessInvolvedID = $(this).find("option:selected").val();
        $('#witnessInvolvedID').val(witnessInvolvedID);
    });
    
});