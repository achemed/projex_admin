<?php
/**
 * @package projex
 * @author Sunnyboy Mathole <sunny@innovatorshill.co.za>
 * @copyright (c) 2016, Innovators Hill
 * @license 
 */

include_once('classes/page_class.php');

$objPage->startHeader();
$objPage->setTitle($title);

//include stylesheet
$externalstylesheet = array( HOSTNAME . '/vendor/components/bootstrap/css/bootstrap.min.css',
                             HOSTNAME . '/vendor/select2/select2/dist/css/select2.min.css',
                             HOSTNAME . '/vendor/components/jqueryui/themes/smoothness/jquery-ui.css',
                             HOSTNAME . '/vendor/bootstrap-select/bootstrap-select/dist/css/bootstrap-select.min.css',
                             HOSTNAME . '/vendor/grimmlink/toastr/build/toastr.min.css',
                             HOSTNAME . '/vendor/fullcalendar/fullcalendar/dist/fullcalendar.min.css',
                             HOSTNAME . '/vendor/fullcalendar/fullcalendar/dist/fullcalendar.print.min.css',
                             HOSTNAME . '/vendor/components/font-awesome/css/font-awesome.min.css',
                             HOSTNAME . '/vendor/datatables/datatables/media/css/dataTables.bootstrap.min.css',
                             HOSTNAME . '/vendor/trentrichardson/jquery-timepicker-addon/dist/jquery-ui-timepicker-addon.min.css');

$stylesheet = array( 'main.css',
                     'responsive.css',
                     'sweetalert.css',
                     //'jquery-ui-1.10.4.custom.min.css', Questionable ??
                    );

$objPage->includeExternalStyle($externalstylesheet);
$objPage->includeStyle($stylesheet);

$externaljavascript = array(HOSTNAME .'/vendor/components/jquery/jquery.min.js',
                            HOSTNAME .'/vendor/components/jqueryui/ui/jquery-ui.js',
                            HOSTNAME .'/vendor/components/bootstrap/js/bootstrap.min.js',
                            HOSTNAME .'/vendor/select2/select2/dist/js/select2.min.js',
                            HOSTNAME .'/vendor/trentrichardson/jquery-timepicker-addon/dist/jquery-ui-timepicker-addon.js',
                            HOSTNAME .'/vendor/tinymce/tinymce/tinymce.min.js',
                            HOSTNAME .'/vendor/bootstrap-select/bootstrap-select/dist/js/bootstrap-select.min.js',
                            HOSTNAME .'/vendor/grimmlink/toastr/build/toastr.min.js',
                            HOSTNAME .'/vendor/moment/moment/jquery/jquery.min.js',
                            HOSTNAME .'/vendor/fullcalendar/fullcalendar/dist/fullcalendar.min.js',
                            HOSTNAME .'/vendor/datatables/datatables/media/js/dataTables.bootstrap.min.js',
                            HOSTNAME .'/vendor/thatsus/jquery-circle-progress/dist/circle-progress.js'
                            );

$javascript = array( 'jquery-migrate-1.2.1.min.js',
                     'jquery.metisMenu.js',
		     'jquery.menu.js',
		     'main.js',
		     'Chart.bundle.js',
		     'utils.js',
		     'jquery.bootstrap.wizard.js',
		     'bootstrap-rating.js',
		     'custom.js',
		     'bootstrap-modal-popover.js',
                     'jquery.tablecheckbox.js',
                     'jquery.ckie.js',
                     'sheq.documents.js',
                     'sweetalert.js');

$objPage->includeExternalScript($externaljavascript);
$objPage->includeScript($javascript);
?>  

<script> var AJAX_ROOT = "<?php echo BASE_URL; ?>/index.php?";
    
function getMoreInfoDropDownList(element,next_element,url)
   {
     var selected_id = $(element).val();
     var obj = $(next_element);
     var ur = url;;
     $.ajax({
        type:"GET",
        url:ur,
        dataType:"json",
        data:{group_id:selected_id},
        success:function(result)
        {
             var rs = JSON.stringify(result);
             var action = JSON.parse(rs);
             $(obj).find('option').remove();   
             var row = "";
             $(action).each(function(i,val)
              {        
                  row = "<option value='"+val.option_key+"'>"+val.value+"</option>";
                  $(obj,' .selectpicker').append(row);                                 
              });                
              
              $(obj,' .selectpicker').selectpicker('refresh');
        }
     });
   }
</script>  
<?php
$objPage->endHeader();
