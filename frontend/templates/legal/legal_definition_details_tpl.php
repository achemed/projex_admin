<?php
$title = 'Legal';
include_once('frontend/templates/headers/default_header_tpl.php');

?>

<?php
/*
 * Include side menu from the include file
 */

include_once('frontend/templates/menus/main-menu.php');
?>
<!--navigation-->
<?php
/*
 * Include  main menu from the include file
 */

include_once('frontend/templates/menus/side-menu.php');
?>
<!--End of navigation-->


<div class="page-content">

    <!--sub menu-->
    <ul class="nav nav-tabs nav-tabs-line">
        <li ><a href="index.php?module=legal&action=viewLegalAbout&id=<?php echo $data['legal_id']; ?>">About</a></li>
        <li ><a href="index.php?module=legal&action=viewLegalDetails&id=<?php echo $data['legal_id']; ?>">Law</a></li>
        <li class="active"><a href="index.php?module=legal&action=viewAboutLegalDefinitions&id=<?php echo $data['legal_id']; ?>">Definitions</a></li>
    </ul>
    <!--End of sub menu-->
    <div class="panel-group">
        <div class="panel panel-default">
            <div class="panel-heading clearfix">
                <div class="panel-title pull-left">Definition</div>
                <div class="panel-heading-icons">
                    <a data-toggle="sheqModal" href="index.php?module=modal_request&action=modalAddLegalDefinition&id=<?php echo $data['legal_id']; ?>" class="btn btn-red btn-circle"><i class="fa fa-plus"></i></a>
                </div>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table width="100%" class="table table-hover" id="dataTables-example">
                        <thead>
                        <tr>
                            <th>Definition</th>
                            <th>Details</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php

                        global $objRegions;
                        if($data['definitionInfo']){ foreach($data['definitionInfo'] as $legal):

                            //$region = $objRegions->getCountryInfo($legal['country_id']);
                            ?>
                            <tr>
                                <td><?php echo $legal['content']; ?></td>
                                <td><?php echo $legal['definition']; ?></td>
                                <td>
                                </td>

                            </tr>
                        <?php endforeach;
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <!--End of panel-default-->
    </div>
</div>
<!--End of main col-->
<?php
include_once('frontend/templates/footers/default_footer_tpl.php');
?>
<script>
    $('body').on('click','[data-toggle="sheqModal"]',function(e){

        $('#sheqModal').remove();
        $('.modal-backdrop').remove();
        e.preventDefault();
        var $this=$(this),
            $remote=$this.data('remote')||$this.attr('href'),

            $modal=$('<div class="modal fade" id="sheqModal"  role="dialog"><div class="modal-body"></div></div>');
        $('body').append($modal);
        //$modal.modal();
        $modal.modal({backdrop: 'static', keyboard: false});
        $modal.load($remote);
    });
</script>
