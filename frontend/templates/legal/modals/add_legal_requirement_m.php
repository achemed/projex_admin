<?php
/**
 * Created by PhpStorm.
 * User: Innovatorshill
 * Date: 3/30/2017
 * Time: 10:21 AM
 */

?>
<!--Add Link Modal -->
<div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Add Legal</h4>
        </div>
        <div class="modal-body">
            <form class="form-horizontal" method="post" name="addLinkForm" id="addLinkForm" action="index.php?module=legal&action=addLegalRequirement">
                <div class="form-group">
                    <label class="control-label col-sm-4" for="notes">Header</label>
                    <div class="col-lg-6 col-md-4 col-sm-8">
                        <textarea rows="5" class="form-control" name="header" id="header"  placeholder=""></textarea>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-4" for="notes">Sub No</label>
                    <div class="col-lg-6 col-md-4 col-sm-8">
                        <input type="text" class="form-control"  name="sub_no" id="sub_no" >
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-4" for="notes">Legal Description</label>
                    <div class="col-lg-6 col-md-4 col-sm-8">
                        <textarea rows="5" class="form-control" name="description" id="description"  placeholder=""></textarea>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-offset-4 col-sm-8">
                        <input type="hidden"  class="form-control"  name="legal_id" value="<?php echo $data['legal_id']; ?>">
                        <button type="submit" id="addDocumentBtn" class="btn btn-success">Submit</button>
                    </div>
                </div>
            </form>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
    </div>
</div>
<script>
    $('.selectpicker').selectpicker('refresh');
</script>
<!--End Of Modal -->