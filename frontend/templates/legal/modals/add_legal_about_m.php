<?php
/**
 * Created by PhpStorm.
 * User: Innovatorshill
 * Date: 3/30/2017
 * Time: 10:21 AM
 */

?>
<!--Add Link Modal -->
<div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">About</h4>
        </div>
        <div class="modal-body">
            <form class="form-horizontal"  enctype="multipart/form-data" method="post" name="addLinkForm" id="addLinkForm" action="index.php?module=legal&action=addLegalAbout">

                <div class="form-group">
                    <label class="control-label col-sm-4" for="notes">About</label>
                    <div class="col-lg-6 col-md-4 col-sm-8">
                        <textarea rows="5" class="form-control" name="about" id="about"  placeholder="" required></textarea>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-4" for="notes">Effective Date</label>
                    <div class="col-lg-6 col-md-4 col-sm-8">
                        <input class="form-control" name="effective_date" id="effective_date"  placeholder="">
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-4" for="notes">Effective Date</label>
                    <div class="col-lg-6 col-md-4 col-sm-8">
                        <input type="file" name="doc_upload" id="doc_upload"/>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-4 col-sm-8">
                        <input type="hidden"  class="form-control"  name="legal_id" value="<?php echo $data['legal_id']; ?>">
                        <button type="submit" id="addDocumentBtn" class="btn btn-success">Submit</button>
                    </div>
                </div>
            </form>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
    </div>
</div>
<script>
    $('.selectpicker').selectpicker('refresh');

    //effectiveDate
    $("#effective_date").datepicker({
        dateFormat:"yy/mm/dd",
        minDate: new Date(),
        changeYear:true,
        changeMonth:true,
        yearRange:"1900:3000"}).datepicker();
</script>
<!--End Of Modal -->