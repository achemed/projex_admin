<?php
$title = 'Legal';
include_once('frontend/templates/headers/default_header_tpl.php');

?>

<?php
/*
 * Include side menu from the include file
 */

include_once('frontend/templates/menus/main-menu.php');
?>
<!--navigation-->
<?php
/*
 * Include  main menu from the include file
 */

include_once('frontend/templates/menus/side-menu.php');
?>
<!--End of navigation-->


<div class="page-content">

    <!--sub menu-->
    <ul class="nav nav-tabs nav-tabs-line">
        <li class="active"><a href="index.php?module=legal&action=viewLegalAbout&id=<?php echo $data['legal_id']; ?>">About</a></li>
        <li ><a href="index.php?module=legal&action=viewLegalDetails&id=<?php echo $data['legal_id']; ?>">Law</a></li>
        <li><a href="index.php?module=legal&action=viewAboutLegalDefinitions&id=<?php echo $data['legal_id']; ?>">Definitions</a></li>
    </ul>
    <!--End of sub menu-->
    <div class="panel-group">
        <div class="panel panel-default">
            <div class="panel-heading clearfix">
                <div class="panel-title pull-left">Definition</div>
                <div class="panel-heading-icons">
                    <?php if($data['aboutInfo']['about'] == ""){ ?>
                    <a data-toggle="sheqModal" href="index.php?module=modal_request&action=modalAddLegalAbout&id=<?php echo $data['legal_id']; ?>" class="btn btn-red btn-circle"><i class="fa fa-plus"></i></a>
                    <?php }else{ ?>
                        <a data-toggle="sheqModal" href="index.php?module=modal_request&action=modalEditLegalAbout&id=<?php echo $data['aboutInfo']['id']; ?>" class="btn btn-red btn-circle"><i class="fa fa-pencil"></i></a>
                    <?php } ?>
                </div>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table no-margin">
                        <tbody>
                        <tr>
                            <td class="bold">About</td>
                            <td>
                                <?php echo $data['aboutInfo']['about']; ?>
                            </td>
                        </tr>
                        <tr>
                            <td class="bold">Effective Date</td>
                            <td>
                                <?php echo date("j F Y",strtotime($data['aboutInfo']['effective_date'])); ?>
                            </td>
                        </tr>
                        <tr>
                            <td class="bold">Last Update</td>
                            <td><?php echo date("j F Y",strtotime($data['aboutInfo']['date_modified'])); ?></td>
                        </tr>
                        <tr>
                            <td class="bold">Download Law</td>
                            <td>
                                <?php if($data['aboutInfo']['file_id'] > 1){ ?>
                                    <a data-toggle="sheqModal"  href="index.php?module=modal_request&action=modalViewDocumentInfo&id=<?php echo $data['aboutInfo']['file_id']; ?>"><span class="custom-label label  inline-block info">View Certificate</span></a>
                                <?php }else{  ?>
                                    ----
                                <?php }  ?>
                            </td>
                        </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <!--End of panel-default-->
    </div>
</div>
<!--End of main col-->
<?php
include_once('frontend/templates/footers/default_footer_tpl.php');
?>
<script>
    $('body').on('click','[data-toggle="sheqModal"]',function(e){

        $('#sheqModal').remove();
        $('.modal-backdrop').remove();
        e.preventDefault();
        var $this=$(this),
            $remote=$this.data('remote')||$this.attr('href'),

            $modal=$('<div class="modal fade" id="sheqModal"  role="dialog"><div class="modal-body"></div></div>');
        $('body').append($modal);
        //$modal.modal();
        $modal.modal({backdrop: 'static', keyboard: false});
        $modal.load($remote);
    });
</script>
