<?php
$title = 'Package Details';
include_once('frontend/templates/headers/default_header_tpl.php');

?>

<?php
/*
 * Include side menu from the include file
 */

include_once('frontend/templates/menus/main-menu.php');
?>
<!--navigation-->
<?php
/*
 * Include  main menu from the include file
 */

include_once('frontend/templates/menus/side-menu.php');
?>
<!--End of navigation-->
<style>
    .form-control.height-auto {
        height: auto;
    }
    .xlist-unstyled {
        padding-left: 0;
        list-style: none;
    }
    .list-unstyled li > .xlist-unstyled {
        margin-left: 15px;
    }
    .list-unstyled li > .list-unstyled {
        margin-left: 25px;
    }
    .scroller {
        max-height: 300px;
        overflow-y: scroll;
    }

    .modal-lg {
        /*max-width: 1000px;*/
        width: 70%;
        padding: 10px;
        margin: 0px;
        margin: 0 auto;
    }

</style>
<div class="page-content">
    <div class="row">
        <div class="col-md-12">
            <div class="panel">
                <div class="panel-body">
                    <h5><b>Package Name</b> : <?php echo  $data['packageInfo']['package_name']; ?></h5>
                    <h5><b>Package Description</b> : <?php echo $data['packageInfo']['package_details']; ?></h5>
                </div>
            </div>
        </div>
    </div>
    <div class="panel-group">
        <div class="panel panel-default">
            <div class="panel-heading clearfix">
                <div class="panel-title pull-left">Package Modules</div>
                <div class="panel-heading-icons">
                </div>
            </div>
            <div class="panel-body">

                <form class="form-horizontal" method="post" name="addRoleForm" action="index.php?module=package&action=addPackageModules">
                    <input type="hidden" name="package_id" value="<?php echo $data['packageInfo']['id']; ?>" />
                    <div class="col-md-12">
                        <div class="col-md-6">
                            <div class="form-group">
                                <span><h5>Modules</h5></span>
                                <!--
                                <label class="control-label col-sm-4" for="Roles">Modules Permissions
                                    <span class="required">*</span>
                                </label>-->
                                <div class="">
                                    <div class="form-control height-auto">
                                        <div class="scroller" style="height:350px;" data-always-visible="1">
                                            <ul class="list-unstyled">
                                                <?php
                                                if($data['parent_modules'] )
                                                foreach ($data['parent_modules'] as $module) {
                                                    ?>
                                                    <li>

                                                        <input type="checkbox" name="permissions[]" value="<?php echo $module['id']; ?>" class="module_checkboxes "
                                                            <?php echo (package_module_class::has_module_package($module['id'],$data['packageInfo']['id'])) ? ' checked' : ''; ?>
                                                        />
                                                        <span class="text-info"><?php echo  $module['module_description'] ;?> :</span>
                                                        <span class="text-warning"><?php echo $module['module_description']; ?></span>

                                                        <ul class="xlist-unstyled">
                                                            <?php  $data['submenus_modules'] =  package_module_class::getModulesSubModules($module['id']); ?>
                                                            <?php if($data['submenus_modules']) foreach ($data['submenus_modules'] as $sub_module) { ?>
                                                                <li>
                                                                    <input type="checkbox" name="sub_permissions[]" value="<?php echo $sub_module['id']; ?>" class="module_checkboxes "
                                                                        <?php echo (package_module_class::has_module_package($sub_module['id'],$data['packageInfo']['id'])) ? ' checked' : ''; ?>
                                                                    />
                                                                    <span class="text-info"><?php echo  $sub_module['module_description'] ;?>:</span>
<!--
                                                                    <ul class="list-unstyled">
                                                                        <?php
/*
                                                                        $module_actions= package_module_class::getModulesActions($sub_module['id']);
                                                                        if($module_actions) {
                                                                            foreach ($module_actions as $module_action) {
                                                                                ?>
                                                                                <li>
                                                                                    <input type="checkbox"
                                                                                           name="permissions_actions[]"
                                                                                           value="<?php echo $module_action['module_id'] . "|" . $module_action['id']; ?>"/>
                                                                                    <span
                                                                                        class="text-info"><?php echo $module_action['action_name']; ?></span>
                                                                                </li>
                                                                                <?php
                                                                            }
                                                                        }
*/
                                                                        ?>
                                                                    </ul>
                                                                    -->
                                                                </li>
                                                            <?php } ?>
                                                        </ul>
                                                    </li>
                                                    <?php
                                                }
                                                ?>
                                            </ul>
                                        </div>
                                    </div>
                                    <span class="help-block text-red">	 select one or more modules/permissions...</span>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <span><h5>Global Modules</h5></span>
                                <!--
                                <label class="control-label col-sm-4" for="Roles">Global Modules Permissions
                                    <span class="required">*</span>
                                </label>-->
                                <div class="">
                                    <div class="form-control height-auto">
                                        <div class="scroller" style="height:350px;" data-always-visible="1">
                                            <ul class="list-unstyled">
                                                <?php
                                                if($data['global_parent_modules'] )
                                                foreach ($data['global_parent_modules'] as $module) {
                                                    ?>
                                                    <li>

                                                        <input type="checkbox" name="global_permissions[]" value="<?php echo $module['id']; ?>" class="module_checkboxes "
                                                            <?php echo (package_module_class::has_module_package($module['id'],$data['packageInfo']['id'])) ? ' checked' : ''; ?>
                                                        />
                                                        <span class="text-info"><?php echo  $module['module_description'] ;?> :</span>
                                                        <span class="text-warning"><?php echo $module['module_description']; ?></span>

                                                        <ul class="xlist-unstyled">
                                                            <?php  $data['submenus_modules'] =  package_module_class::getModulesSubModules($module['id']); ?>
                                                            <?php if($data['submenus_modules']) foreach ($data['submenus_modules'] as $sub_module) { ?>
                                                                <li>
                                                                    <input type="checkbox" name="global_sub_permissions[]" value="<?php echo $sub_module['id']; ?>" class="module_checkboxes "
                                                                        <?php echo (package_module_class::has_module_package($sub_module['id'],$data['packageInfo']['id'])) ? ' checked' : ''; ?>
                                                                    />
                                                                    <span class="text-info"><?php echo  $sub_module['module_description'] ;?>:</span>
                                                                    <!--
                                                                    <ul class="list-unstyled">
                                                                        <?php
                                                                    /*
                                                                        $module_actions= package_module_class::getModulesActions($sub_module['id']);
                                                                        if($module_actions) {
                                                                            foreach ($module_actions as $module_action) {
                                                                                ?>
                                                                                <li>
                                                                                    <input type="checkbox"
                                                                                           name="global_permissions_actions[]"
                                                                                           value="<?php echo $module_action['module_id'] . "|" . $module_action['id']; ?>"/>
                                                                                    <span
                                                                                        class="text-info"><?php echo $module_action['action_name']; ?></span>
                                                                                </li>
                                                                                <?php
                                                                            }
                                                                        }
                                                                    */
                                                                        ?>
                                                                    </ul>
                                                                    -->
                                                                </li>
                                                            <?php } ?>
                                                        </ul>
                                                    </li>
                                                    <?php
                                                }
                                                ?>
                                            </ul>
                                        </div>
                                    </div>
                                    <span class="help-block text-red">	 select one or more modules/permissions...</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--end -->
                    <div class="form-group">
                        <button type="submit" id="addApprovalBtn" class="btn btn-success">Submit</button>
                    </div>

                </form>

            </div>
        </div>

        <!--End of panel-default-->
    </div>
</div>
<!--End of main col-->
<?php
include_once('frontend/templates/footers/default_footer_tpl.php');
?>
<script>


    $(".module_checkboxes").change(function()
    {
        if ($(this).prop('checked'))
        {
            $(this).parent().find('input[type=checkbox]').not(':disabled').prop('checked', true);
        }
        else
        {
            $(this).parent().find('input[type=checkbox]').not(':disabled').prop('checked', false);
        }
    });

    $('body').on('click','[data-toggle="sheqModal"]',function(e){

        $('#sheqModal').remove();
        $('.modal-backdrop').remove();
        e.preventDefault();
        var $this=$(this),
            $remote=$this.data('remote')||$this.attr('href'),

            $modal=$('<div class="modal fade" id="sheqModal"  role="dialog"><div class="modal-body"></div></div>');
        $('body').append($modal);
        //$modal.modal();
        $modal.modal({backdrop: 'static', keyboard: false});
        $modal.load($remote);
    });
    $(function() {
        tinymce.init({
            selector: '#docContent',
            height: 500,
            theme: 'modern',
            plugins: [
                'advlist autolink lists link image charmap print preview hr anchor pagebreak',
                'searchreplace wordcount visualblocks visualchars code fullscreen',
                'insertdatetime media nonbreaking save table contextmenu directionality',
                'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc'
            ],
            toolbar1: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
            toolbar2: 'print preview media | forecolor backcolor emoticons | codesample',
            image_advtab: true,
            templates: [
                { title: 'Test template 1', content: 'Test 1' },
                { title: 'Test template 2', content: 'Test 2' }
            ],
            content_css: [
                '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
                '//www.tinymce.com/css/codepen.min.css'
            ]
        });
    });

</script>
