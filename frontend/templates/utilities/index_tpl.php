<?php
$title = 'Company Information';
include_once('frontend/templates/headers/default_header_tpl.php');

?>

<?php
/*
 * Include side menu from the include file
 */

include_once('frontend/templates/menus/main-menu.php');
?>
<!--navigation-->
<?php
/*
 * Include  main menu from the include file
 */

include_once('frontend/templates/menus/side-menu.php');
?>
<!--End of navigation-->

<div class="page-content">
    <div class="panel-group">
        <div class="panel panel-default">
            <div class="panel-heading clearfix">
                <div class="panel-title pull-left">Updates</div>
                <div class="panel-heading-icons">
                    <a data-toggle="sheqModal" href="index.php?module=modal_request&action=modalQuickUpdateCompaniesScript" class="btn btn-red btn-circle"><i class="fa fa-plus"></i></a>
                </div>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table width="100%" class="table table-hover" id="dataTables-example">
                        <thead>
                        <tr>
                            <th>Company Name</th>
                            <th>Database Name</th>
                            <th>Last Updated</th>
                            <th></th>

                        </tr>
                        </thead>
                        <tbody>
                        <?php

                        if($data['allClients']){ foreach($data['allClients'] as $user):
                            $status = "";
                            switch($user['is_active'])
                            {
                                case 1:
                                    $status = "Active";
                                    break;
                                case 0 :
                                    $status = "In Active";
                                    break;
                            }
                            ?>
                            <tr>
                                <td><?php echo $user['company_name']; ?></td>
                                <td><?php echo $user['db_name']; ?></td>
                                <td><?php echo $user['last_db_updated']; ?></td>
                                <td>
                                    <a  data-toggle="sheqModal" href="index.php?module=modal_request&action=modalViewUpdateStrings&id=<?php echo $user['id'];?>">
                                        <span class="custom-label label  inline-block info">
                                        View
                                        </span>
                                    </a>
                                </td>

                            </tr>
                        <?php endforeach;
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <!--End of panel-default-->
    </div>
</div>
<!--End of main col-->
<?php
include_once('frontend/templates/footers/default_footer_tpl.php');
?>
<script>
    $('body').on('click','[data-toggle="sheqModal"]',function(e){

        $('#sheqModal').remove();
        $('.modal-backdrop').remove();
        e.preventDefault();
        var $this=$(this),
            $remote=$this.data('remote')||$this.attr('href'),

            $modal=$('<div class="modal fade" id="sheqModal"  role="dialog"><div class="modal-body"></div></div>');
        $('body').append($modal);
        //$modal.modal();
        $modal.modal({backdrop: 'static', keyboard: false});
        $modal.load($remote);
    });
</script>
