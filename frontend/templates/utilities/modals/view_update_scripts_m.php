<?php
/**
 * Created by PhpStorm.
 * User: Douglas
 * Date: 7/9/2017
 * Time: 2:08 PM
 */
?>
<!--View Link Modal -->
<div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">View Updates</h4>
        </div>
        <div class="modal-body">
            <div class="table-responsive">
                <table width="100%" class="table table-hover">
                    <thead>
                    <tr>
                        <th>Script</th>
                        <th>Date</th>
                        <th>modified by</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php if($data['allUpdates']) : foreach ($data['allUpdates']  as $link): ?>
                        <tr>
                            <td><?php echo $link['query_string']; ?></td>
                            <td><?php echo $link['date_created']; ?></td>
                            <td><?php echo $link['created_by']; ?></td>
                        </tr>
                    <?php endforeach; endif; ?>
                    </tbody>
                </table>

            </div>


        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
    </div>

</div>
<!--End Of Modal -->
