<?php
/**
 * Created by PhpStorm.
 * User: Innovatorshill
 * Date: 3/30/2017
 * Time: 10:21 AM
 */

?>
<!--Add Link Modal -->
<div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Update Company Script</h4>
        </div>
        <div class="modal-body">
            <form class="form-horizontal" method="post" name="addLinkForm" id="addLinkForm" action="index.php?module=updates&action=runQuickUpdate">
                <div class="form-group">
                    <label class="control-label col-sm-4" for="type">Company</label>
                    <div class="col-lg-6 col-md-4 col-sm-8">

                        <select class="selectpicker" name="companyId[]"  id="companyId"  multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select 1 or more company"data-header="Close">
                            <option value="">Select Comapny</option>
                            <?php
                            if(!empty($data['companies'])) {
                                foreach($data['companies'] as $company){
                                    echo '<option value="'.$company['id'].'">'.$company['company_name'].'</option>';
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-4" for="notes">Script</label>
                    <div class="col-lg-6 col-md-4 col-sm-8">
                        <textarea rows="5" class="form-control" name="script" id="script" required placeholder=""></textarea>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-offset-4 col-sm-8">
                        <button type="submit" id="addDocumentBtn" class="btn btn-success">Submit</button>
                    </div>
                </div>
            </form>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
    </div>
</div>
<script>
    $('.selectpicker').selectpicker('refresh');
</script>
<!--End Of Modal -->