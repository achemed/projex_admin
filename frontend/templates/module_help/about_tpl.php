<?php
$title = 'Help Module';
include_once('frontend/templates/headers/default_header_tpl.php');

?>

<?php
/*
 * Include side menu from the include file
 */

include_once('frontend/templates/menus/main-menu.php');
?>
<!--navigation-->
<?php
/*
 * Include  main menu from the include file
 */

include_once('frontend/templates/menus/side-menu.php');

global $objModules;
$data['moduleInfo'] = $objModules->getModuleHelpInfo($_GET['id']);
$data['moduleDetails'] = $objModules->getModuleDetailsInfo($_GET['id']);
$data['moduleAttachments'] = $objModules->getAllModuleAttachmentsInfo($_GET['id']);
?>
<!--End of navigation-->

<div class="page-content">

      <div class="row">
            <div class="col-lg-12">
            <ul class="nav nav-tabs nav-tabs-line">
                <li><a href="<?php echo BASE_URL;?>/index.php?action=viewAllModules&module=module_help"><span class="fa fa-arrow-left"></span> Help</a></li>            
            </ul>
            </div>
        </div>
    <div class="row">
        <div class="col-md-12">
            <div class="panel">
                <div class="panel-body">
                    <h5><b>Description</b> : <?php echo $data['moduleInfo']['module_description']; ?></h5>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
            <div class="col-lg-12">
            <ul class="nav nav-tabs nav-tabs-line">
                <li class="active"><a href="<?php echo BASE_URL;?>/index.php?action=viewAllModuleAbout&module=module_help&id=<?php echo $_GET['id'];?>">About</a></li> 
                <li><a href="<?php echo BASE_URL;?>/index.php?action=viewAllModuleSteps&module=module_help&id=<?php echo $_GET['id'];?>">Steps</a></li>
                <li><a href="<?php echo BASE_URL;?>/index.php?action=viewAllModuleRecommendedSettings&module=module_help&id=<?php echo $_GET['id'];?>">Recommended Settings</a></li>            
            </ul>
            </div>
        </div>
    <div class="panel-group">
        <div class="panel panel-default">
            <div class="panel-heading clearfix">
                <div class="panel-title pull-left">About</div>
                <div class="panel-heading-icons">
                </div>
            </div>
            <div class="panel-body">
                <form class="form-horizontal" method="post" name="addLinkForm" id="addLinkForm" action="index.php?module=module_help&action=updateHelpModule">
                <textarea type="text" class="form-control" name="docContent" id="docContent"><?php echo isset($data['moduleDetails']["notes"]) ? $data['moduleDetails']["notes"] : "";?></textarea>
                <br/>
                <div class="form-group">
                    <div class="col-sm-offset-4 col-sm-8">
                        <input type="hidden" name="help_id" value="<?php echo $data['moduleInfo']['id']; ?>">
                        <button type="submit" id="addDocumentBtn" class="btn btn-success">Submit</button>
                    </div>
                </div>
                </form>
            </div>
        </div>

        <!--End of panel-default-->
    </div>





    <div class="panel-group">
        <div class="panel panel-default">
            <div class="panel-heading clearfix">
                <div class="panel-title pull-left">Attachments</div>
                <div class="panel-heading-icons">
                    <a data-toggle="sheqModal" href="index.php?module=modal_request&action=modalAddModuleHelpAttachment&id=<?php echo $_GET['id'];?>" class="btn btn-red btn-circle"><i class="fa fa-plus"></i></a>
                </div>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table width="100%" class="table table-hover" id="dataTables-example">
                        <thead>
                        <tr>
                            <th>Description</th>
                            <th>Date Added</th>
                            <th>Attachment</th>
                            <th></th>

                        </tr>
                        </thead>
                        <tbody>
                        <?php

                        if($data['moduleAttachments']){ foreach($data['moduleAttachments'] as $info):

                            $fileInfoUrl = "";
                            if(!empty($info['filename']))
                            {
                                $fileInfoUrl = "<a target='_blank' class='btn btn-xs btn-primary' href='".BASE_URL.'/frontend/media/uploads/'.$info['filename']."'>View</a>";
                            }
                            
                            ?>
                            <tr>
                                <td><?php echo $info['description']; ?></td>
                                <td><?php echo $info['date_created']; ?></td>
                                <td><?php echo $fileInfoUrl; ?></td>
                                <td>                            
                                </td>

                            </tr>
                        <?php endforeach;
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <!--End of panel-default-->
    </div>

</div>
<!--End of main col-->
<?php
include_once('frontend/templates/footers/default_footer_tpl.php');
?>
<script>
    $('body').on('click','[data-toggle="sheqModal"]',function(e){

        $('#sheqModal').remove();
        $('.modal-backdrop').remove();
        e.preventDefault();
        var $this=$(this),
            $remote=$this.data('remote')||$this.attr('href'),

            $modal=$('<div class="modal fade" id="sheqModal"  role="dialog"><div class="modal-body"></div></div>');
        $('body').append($modal);
        //$modal.modal();
        $modal.modal({backdrop: 'static', keyboard: false});
        $modal.load($remote);
    });
    $(function() {
        tinymce.init({
            selector: '#docContent',
            height: 500,
            theme: 'modern',
            plugins: [
                'advlist autolink lists link image charmap print preview hr anchor pagebreak',
                'searchreplace wordcount visualblocks visualchars code fullscreen',
                'insertdatetime media nonbreaking save table contextmenu directionality',
                'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc'
            ],
            toolbar1: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
            toolbar2: 'print preview media | forecolor backcolor emoticons | codesample',
            image_advtab: true,
            templates: [
                { title: 'Test template 1', content: 'Test 1' },
                { title: 'Test template 2', content: 'Test 2' }
            ],
            content_css: [
                '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
                '//www.tinymce.com/css/codepen.min.css'
            ]
        });
    });

</script>
