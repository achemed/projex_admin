<?php
/**
 * Created by PhpStorm.
 * User: Innovatorshill
 * Date: 3/30/2017
 * Time: 10:21 AM
 */

global $objModules;
$allQuestions = $objModules->getAllModuleSteps($data['id']);
$totalSettings = 0;
if($allQuestions) : foreach ( $allQuestions as $flow):
                        $totalSettings++;
                        endforeach;
endif;?>
<!--Add Link Modal -->
<div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Add</h4>
        </div>
        <div class="modal-body">
            <form class="form-horizontal" enctype="multipart/form-data" 
            method="post" action="index.php?module=module_help&action=addHelpModuleSteps">
            <input type="hidden" name="help_id" value="<?php echo $data['id'];?>"/>

                <div class="form-group">
                <label class="control-label col-sm-4" for="description">Description</label>
                <div class="col-lg-12 col-md-4 col-sm-8">
                    <textarea cols="5" rows="5" class="form-control" required="true" name="description"></textarea>
                </div>

            </div>

            <?php
                    if($totalSettings == 0)
                    {
                    ?>
                    <input type="hidden" name="position" value="1" readonly="true"/>
                    <input type="hidden" name="process_first" value="Yes" readonly="true"/>
                    <?php }
                    if($totalSettings > 0)
                    {?>
                    <div class="form-group">
                        <label class="control-label col-sm-4" for="position">After Question</label>
                        <div class="col-lg-12 col-md-4 col-sm-8">
                           <select name="position" id="position"  required="true" class="form-control">
                                                <option value>Select a type</option>
                                                  <?php                                                                                               
                                                  $counter = 0;
                                                  if($allQuestions)
                                                  {
                                                       foreach($allQuestions as $group)
                                                        {
                                                           $counter++;
                                                     //      if($counter > 1)
                                                           {
                                                            ?>
                                                                <option value="<?php echo $group["parent_id"];?>"><?php echo "Step: ".$group["parent_id"].' - '.$group["description"];?></option>
                                                            <?php
                                                           }
                                                        }
                                                      }?>     
                           </select>
                        </div>
                    </div>   
                    <?php
                    }?>



                <div class="modal-custom-h5"><span><h5>Attachments</h5></span></div>

             <div class="card" >
                                    <div class="card-header clearfix">Uploads <span class="panel-heading-icons">
                    <a href="#spawn_upload" id="spawn_new_upload" class="btn btn-red btn-circle"><i class="fa fa-plus"></i></a>
                 </span></div>
                  <div class="card-body" id="file_element_block">
                      <table width="100%" class="table table-hover file_table">
                          <thead>
                          <tr>
                              <th></th>
                              <th></th>
                          </tr>
                          </thead>
                          <tbody>
                          <tr>

                          </tr>
                          </tbody>
                      </table>                                      
                  </div>
              </div>    

                <div class="form-group">
                    <div class="col-sm-offset-4 col-sm-8">
                        <button type="submit" id="addDocumentBtn" class="btn btn-success">Submit</button>
                    </div>
                </div>
            </form>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
    </div>
</div>
<script>
    $('.selectpicker').selectpicker('refresh');
    $(document).ready(function()
    {
        $("#spawn_new_upload").on("click",function()
        {
            var file_element = document.createElement("input");
            file_element.setAttribute("type","file");
            file_element.setAttribute("name","uploadPhoto[]");

            var removeElement = document.createElement("a");
            removeElement.href = "#remove";
            removeElement.innerHTML = "<span class='fa fa-trash'></span>";
            removeElement.onclick = function()
            {
                var obj = $(this).parent().parent();
                obj.fadeOut();
            };

            var row = "<tr><td></td><td></td></tr>"
            $(".file_table tbody").append(row);
            $(".file_table tbody tr:last td:eq(0)").append(file_element);
            $(".file_table tbody tr:last td:eq(1)").append(removeElement);
        });

    });
</script>
<!--End Of Modal -->