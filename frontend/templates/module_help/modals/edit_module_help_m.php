<?php
/**
 * Created by PhpStorm.
 * User: Innovatorshill
 * Date: 3/30/2017
 * Time: 10:21 AM
 */

?>
<!--Add Link Modal -->
<div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Help Module</h4>
        </div>
        <div class="modal-body">
            <form class="form-horizontal" method="post" name="addLinkForm" id="addLinkForm" action="index.php?module=module_help&action=editHelpModule">

                <!--<div class="form-group">
                    <label class="control-label col-sm-4" for="notes">Module</label>
                    <div class="col-lg-6 col-md-4 col-sm-8">
                        <input class="form-control" name="module_name" id="module_name" value="<?php //echo $data['helperInfo']['module_name'] ;?>">
                    </div>
                </div>-->
                <div class="form-group">
                    <label class="control-label col-sm-4" for="notes">Description</label>
                    <div class="col-lg-6 col-md-4 col-sm-8">
                        <textarea rows="5" class="form-control" name="description" id="description" ><?php echo $data['helperInfo']['module_description'] ;?></textarea>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-offset-4 col-sm-8">
                        <input type="hidden" name="help_id" id="help_id" value="<?php echo $data['helperInfo']['id'] ;?>">
                        <button type="submit" id="addDocumentBtn" class="btn btn-success">Submit</button>
                    </div>
                </div>
            </form>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
    </div>
</div>
<script>
    $('.selectpicker').selectpicker('refresh');
</script>
<!--End Of Modal -->