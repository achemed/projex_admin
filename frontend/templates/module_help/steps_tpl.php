<?php
$title = 'Help Module';
include_once('frontend/templates/headers/default_header_tpl.php');

?>

<?php
/*
 * Include side menu from the include file
 */

include_once('frontend/templates/menus/main-menu.php');
?>
<!--navigation-->
<?php
/*
 * Include  main menu from the include file
 */

include_once('frontend/templates/menus/side-menu.php');

global $objModules;
$data['moduleInfo'] = $objModules->getModuleHelpInfo($_GET['id']);
$data['moduleDetails'] = $objModules->getModuleDetailsInfo($_GET['id']);
$data['moduleSteps'] = $objModules->getAllModuleSteps($_GET['id']);
?>
<!--End of navigation-->

<div class="page-content">

      <div class="row">
            <div class="col-lg-12">
            <ul class="nav nav-tabs nav-tabs-line">
                <li><a href="<?php echo BASE_URL;?>/index.php?action=viewAllModules&module=module_help"><span class="fa fa-arrow-left"></span> Help</a></li>            
            </ul>
            </div>
        </div>
    <div class="row">
        <div class="col-md-12">
            <div class="panel">
                <div class="panel-body">
                    <h5><b>Description</b> : <?php echo $data['moduleInfo']['module_description']; ?></h5>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
            <div class="col-lg-12">
            <ul class="nav nav-tabs nav-tabs-line">
                <li ><a href="<?php echo BASE_URL;?>/index.php?action=viewAllModuleAbout&module=module_help&id=<?php echo $_GET['id'];?>">About</a></li> 
                <li class="active"><a href="<?php echo BASE_URL;?>/index.php?action=viewAllModuleSteps&module=module_help&id=<?php echo $_GET['id'];?>">Steps</a></li>
                <li><a href="<?php echo BASE_URL;?>/index.php?action=viewAllModuleRecommendedSettings&module=module_help&id=<?php echo $_GET['id'];?>">Recommended Settings</a></li>            
            </ul>
            </div>
        </div>






    <div class="panel-group">
        <div class="panel panel-default">
            <div class="panel-heading clearfix">
                <div class="panel-title pull-left">Steps</div>
                <div class="panel-heading-icons">
                    <a data-toggle="sheqModal" href="index.php?module=modal_request&action=modalAddModuleStep&id=<?php echo $_GET['id'];?>" class="btn btn-red btn-circle"><i class="fa fa-plus"></i></a>
                </div>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table width="100%" class="table table-hover" id="dataTables-example">
                        <thead>
                        <tr>
                            <th>Step</th>
                            <th>Description</th>
                            <th>Attachment</th>
                            <th></th>

                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $counter = 0;
                        if($data['moduleSteps']){ foreach($data['moduleSteps'] as $steps):

                            $counter++;
                           
                            ?>
                            <tr>
                                <td><?php echo $counter;?></td>
                                <td><?php echo $steps['description']; ?></td>                                
                                <td>
                                    <a class="btn btn-xs btn-primary" data-toggle="sheqModal" href="index.php?module=modal_request&action=viewSignedOffDetails&signOffSection=ModuleHelper&id=<?php echo $steps["id"]; ?>">View</a>
                                    </td>                          
                                </td>

                            </tr>
                        <?php endforeach;
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <!--End of panel-default-->
    </div>

</div>
<!--End of main col-->
<?php
include_once('frontend/templates/footers/default_footer_tpl.php');
?>
<script>
    $('body').on('click','[data-toggle="sheqModal"]',function(e){

        $('#sheqModal').remove();
        $('.modal-backdrop').remove();
        e.preventDefault();
        var $this=$(this),
            $remote=$this.data('remote')||$this.attr('href'),

            $modal=$('<div class="modal fade" id="sheqModal"  role="dialog"><div class="modal-body"></div></div>');
        $('body').append($modal);
        //$modal.modal();
        $modal.modal({backdrop: 'static', keyboard: false});
        $modal.load($remote);
        });   

</script>
