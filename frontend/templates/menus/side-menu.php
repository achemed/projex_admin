<!--BEGIN BACK TO TOP-->
        <a id="totop" href="#"><i class="fa fa-angle-up"></i></a>
    <!--END BACK TO TOP-->

<!--START WRAPPER-->
<div id="wrapper">
    
<!--START SIDEBAR MENU-->
<nav id="sidebar" role="navigation" data-step="2" data-intro="Template has &lt;b&gt;many navigation styles&lt;/b&gt;"data-position="right" class="navbar-default navbar-static-side">
    <div class="sidebar-collapse menu-scroll">
        <ul id="side-menu" class="nav">
            <!--Header logo and text-->
            <!--
            <div class="navbar nav_title" style="border: 0;">
                <li>
                    <a href="index.php?action=defaultAction&module=users" style="padding:0px !important;" class="site_title">
                    <img class="img-responsive" style="padding:0px;max-width: 60%;" src="frontend/media/images/logo_gwr.png" alt="..."><!--<span class="">GRW</span>--</a>
                </li>
                
            </div>
            <div class="clearfix"></div>
            -->
            <!--End Header logo and text-->
            <!--Start of Improvement-->
            <li>
                <a href="index.php?module=clients&action=viewAllClients"><i class="fa fa-check-square-o fa-fw"><div class="icon-bg bg-primary"></div></i><span class="menu-title">Company</span></a>
            </li>
            <li>
                <a href="index.php?module=updates&action=viewAllUpdates"><i class="fa fa fa-database fa-fw"><div class="icon-bg bg-primary"></div></i><span class="menu-title">Utilities</span></a>
            </li>
            <li>
                <a href="index.php?module=module_help&action=viewAllModules"><i class="fa fa fa-info-circle fa-fw"><div class="icon-bg bg-primary"></div></i><span class="menu-title">Module Helper</span></a>
            </li>
            <li>
                <a href="index.php?module=clients&action=viewAllTransactLogs"><i class="fa fa-check-square-o fa-fw"><div class="icon-bg bg-primary"></div></i><span class="menu-title">Transaction Log</span></a>
            </li>
            <li>
                <a href="index.php?module=package&action=viewAllPackages"><i class="fa fa-check-square-o fa-fw"><div class="icon-bg bg-primary"></div></i><span class="menu-title">Packages</span></a>
            </li>

            <li>
                <a href="index.php?module=mail_template&action=viewAllTemplates"><i class="fa fa-check-square-o fa-fw"><div class="icon-bg bg-primary"></div></i><span class="menu-title">Mail Template</span></a>
            </li>

            <li>
                <a href="index.php?module=legal&action=viewAllLegals"><i class="fa fa-check-square-o fa-fw"><div class="icon-bg bg-primary"></div></i><span class="menu-title">Legal</span></a>
            </li>

            <li>
                <a href="index.php?module=awareness&action=viewAllAwareness"><i class="fa fa-check-square-o fa-fw"><div class="icon-bg bg-primary"></div></i><span class="menu-title">Awareness</span></a>
            </li>
            <li>
                <a href="index.php?module=support&action=viewAllSupportTickets"><i class="fa fa-info fa-fw"><div class="icon-bg bg-primary"></div></i><span class="menu-title">Support</span></a>
            </li>
            <li>
                <a href="index.php?module=sheq&action=viewAllSHEQOptions"><i class="fa fa-info-circle fa-fw"><div class="icon-bg bg-primary"></div></i><span class="menu-title">SHEQ Club</span></a>
            </li>

            <!--End of Company-->
        </ul>
    </div>
</nav>
<!--END SIDE MENU-->
<div id="page-wrapper">