<?php
global $objUsers;
global $objCompanies;
?>
<!--BEGIN TITLE & BREADCRUMB PAGE-->
    <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
<!--
        <div class="page-header pull-left">
            <div class="page-title">
                <ul class="nav-header pull-left">
                    <li><a id="menu-toggle" href="#"><button class="btn btn-default btn-custom-header" data-toggle="layout" data-action="sidebar_mini_toggle" type="button"><i class="fa fa-ellipsis-v"></i></button></a></li>
                    <li><button class="btn btn-default btn-custom-header" data-toggle="modal" data-target="#action-modal" type="button"><i class="fa fa-th"></i></button></li>
                </ul>
            </div>
        </div>

        -->
        <div class="page-header pull-left">

            <div class="page-title">
                <ul class="nav-header pull-left">
                    <li>
                        <a id="menu-toggle" href="#"><button class="btn btn-default btn-custom-header" data-toggle="layout" data-action="sidebar_mini_toggle" type="button">
                                <i class="fa fa-ellipsis-v"></i>
                            </button></a>
                    </li>
                   
                    <?php if(isset($_SESSION['company_id']) && !empty($_SESSION['company_id'])){ ?>
                    <li><a href="index.php?action=defaultAction&module=users"><img class="brand-logo" width="50px" height="50px" src="http://www.sheq24software.co.za/frontend/images/SHEQ24LOGOLIGHT.png" alt="..."></a></li>
                    <?php }else{ ?>
                        <li><a href="index.php?action=defaultAction&module=users"><img  width="200px" height="50px" src="http://www.sheq24software.co.za/frontend/images/SHEQ24LOGOLIGHT.png" alt="..." ></a></li>
                    <?php } ?>
                </ul>
            </div>
        </div>

        <!-- Modal -->        
           <ul class="nav navbar-nav navbar-right m-n hidden-xs nav-user user">          
           <li class="dropdown">                          
               <a href="#" class="dropdown-toggle" data-toggle="dropdown"> 
                   <span class="thumb-sm avatar pull-left"> <img class="thumbnail-img" src="frontend/media/images/user_image.png" alt="..."> </span> <?php echo $_SESSION['firstname'] . ' '. $_SESSION['lastname']; ?> <b class="caret"></b>
               </a> 
               <ul id="dropdown-menu" class="dropdown-menu dropdown-lg animated fadeInRight"> 
                   <li><a href="<?php echo BASE_URL; ?>/index.php?action=logout&module=auth">LogOut</a></li>
               </ul> 
           </li>
           </ul>
        <div class="clearfix">
        </div>


    <!--END TITLE & BREADCRUMB PAGE-->
    
    <!-- major modals -->
    <div class="modal fade" id="notificationDisplay-modal" role="dialog">
    <div class="modal-dialog" style="width:90% !important;">
        <!-- Modal content-->
        <div class="modal-content">  
        <div class="modal-body" style="padding:0px;">
            <form class="form-horizontal" method="post">
                <div class="mail-box">
                  <aside class="sm-side" style="margin:0px;">
                      <ul class="inbox-nav inbox-divider">
                          <li class="active loader_item" id="inbox">
                              <a href="#"><i class="fa fa-inbox"></i> Inbox <span class="label label-primary pull-right inbox_total"></span></a>
                          </li>
                          <li class="loader_item" id="outbox">
                              <a href="#"><i class="fa fa-envelope-o"></i> Sent Request <span class="label label-primary pull-right outbox_total"></span></a>
                          </li>
                          <li class="loader_item" id="trash">
                              <a href="#"><i class=" fa fa-trash-o"></i> Trash <span class="label label-primary pull-right trash_total"></span></a>
                          </li>
                      </ul>
                      <ul class="nav nav-pills nav-stacked labels-info inbox-divider">
                          <li> <h4>Work Space</h4> </li>
                          <li> <a href="#"> <i class=" fa fa-sign-blank text-danger"></i> Non-Conformance </a> </li>
                          <li> <a href="#"> <i class=" fa fa-sign-blank text-success"></i> Incident </a> </li>
                          <li> <a href="#"> <i class=" fa fa-sign-blank text-info "></i> Corrective Action </a>
                          </li>
                          <li> <a href="#"> <i class=" fa fa-sign-blank text-warning "></i> Maintenance </a>
                          </li>
                          <li> <a href="#"> <i class=" fa fa-sign-blank text-primary "></i> Investigation </a>
                          </li>
                          <li> <a href="#"> <i class=" fa fa-sign-blank text-primary "></i> Improvement </a>
                          </li>
                          <li> <a href="#"> <i class=" fa fa-sign-blank text-primary "></i> Online Inspection </a>
                          </li>
                          <li> <a href="#"> <i class=" fa fa-sign-blank text-primary "></i> Objective </a>
                          </li>
                          <li> <a href="#"> <i class=" fa fa-sign-blank text-primary "></i> Training </a>    
                          </li>
                          <li> <a href="#"> <i class=" fa fa-sign-blank text-primary "></i> Communication </a>    
                          </li>
                      </ul>
                      <div class="inbox-body text-center" style="display:none;">
                          <div class="btn-group">
                              <a class="btn mini btn-primary" href="javascript:;">
                                  <i class="fa fa-plus"></i>
                              </a>
                          </div>
                          <div class="btn-group">
                              <a class="btn mini btn-success" href="javascript:;">
                                  <i class="fa fa-phone"></i>
                              </a>
                          </div>
                          <div class="btn-group">
                              <a class="btn mini btn-info" href="javascript:;">
                                  <i class="fa fa-cog"></i>
                              </a>
                          </div>
                      </div>

                  </aside>
                  <aside class="lg-side" style="margin:0px;">
                      <div class="inbox-body mail_body" style="padding:0px;">
                                                    
                      </div>
                  </aside>
       </div>
     </form>        
            </div>   
        </div>
        </div>
    </div>
    </div>
