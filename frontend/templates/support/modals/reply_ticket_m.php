<?php
/**
 * Created by PhpStorm.
 * User: Douglas
 * Date: 16/5/2017
 * Time: 1:57 PM
 */
?>

<!-- Add Working Instruction Modal -->
<div class="modal-dialog modal-md">
    <!-- Modal content-->
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Support Reply</h4>
        </div>
        <div class="modal-body">
            <form class="form-horizontal" name="occupationForm" method="post" action="<?php echo BASE_URL; ?>/index.php?module=support&action=replySupportTicket">


                <div class="form-group">
                    <label class="control-label col-sm-4" for="notes">Description</label>
                    <div class="col-lg-6 col-md-4 col-sm-8">
                        <textarea rows="5" class="form-control" name="description" id="description"  placeholder=""></textarea>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-offset-4 col-sm-8">
                        <input type="hidden" name="db_name" value="<?php echo $data['db_name']; ?>" />
                        <input type="hidden" name="support_id" value="<?php echo $data['id']; ?>" />
                        <button type="submit" class="btn btn-success">Save</button>
                        <button type="reset" class="btn btn-default">Reset</button>
                    </div>
                </div>
            </form>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
    </div>
</div>
<!--End Of Modal -->

