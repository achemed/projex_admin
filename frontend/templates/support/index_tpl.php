<?php
$title = 'Support';
include_once('frontend/templates/headers/default_header_tpl.php');

?>

<?php
/*
 * Include side menu from the include file
 */

include_once('frontend/templates/menus/main-menu.php');
?>
<!--navigation-->
<?php
/*
 * Include  main menu from the include file
 */

include_once('frontend/templates/menus/side-menu.php');
?>
<!--End of navigation-->

<div class="page-content">

    <!--sub menu-->
    <ul class="nav nav-tabs nav-tabs-line">
        <li class="active"><a href="index.php?module=support&action=viewAllSupportTickets">All</a></li>
    </ul>
    <!--End of sub menu-->
    <div class="card">
        <div class="card-header clearfix">Support
            <!--
            <span class="panel-heading-icons">
                    <a data-toggle="sheqModal" href="index.php?module=modal_request&action=modalAddSupportTicket" class="btn btn-red btn-circle"><i class="fa fa-plus"></i></a>
            </span>
            -->
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table width="100%" class="table table-hover">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Status</th>
                        <th>Priority</th>
                        <th>Date</th>
                        <th>User Name</th>
                        <th>Email</th>
                        <th>Support Type</th>
                        <th>Description / Module</th>
                        <th>More Details</th>
                        <th>Uploads</th>
                        <th>BISON Action Taken</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    global $objProducts;

                    if($data['tickets']) : foreach ( $data['tickets'] as $ticket): ?>
                        <?php
                        global $objUsers;
                        $userInfo = $objUsers->getUserDetails($ticket['created_by']);

                        if($ticket['status_id']==1){
                            $status_name = "Open";
                        }else{
                            $status_name = "Closed";
                        }

                        if($ticket['priority_id']==1){
                            $priorite_name = "Low";
                        }elseif($ticket['priority_id']==2){
                            $priorite_name = "Medium";
                        }else{
                            $priorite_name = "High";
                        }

                        if($ticket['type_id']==1){
                            $type_name = "Help";
                        }elseif($ticket['type_id']==2){
                            $type_name = "Error";
                        }else{
                            $type_name = "Updgrade";
                        }
                        ?>
                        <tr>
                            <td><?php echo $ticket['id']; ?></td>
                            <td><?php echo $status_name; ?></td>
                            <td><?php echo $priorite_name; ?></td>
                            <td><?php echo date("j F Y", strtotime($ticket['date_created'])); ?></td>
                            <td><?php echo $ticket['user_name']; ?></td>
                            <td><?php echo $ticket['user_email']; ?></td>
                            <td><?php echo $type_name; ?></td>
                            <td><?php echo $ticket['description']; ?></td>
                            <td><?php echo $ticket['details']; ?></td>
                            <td>
                                <a  href="index.php?module=support&action=viewSupportFiles&id=<?php echo $ticket['id'];?>">
                                        <span class="btn btn-xs btn-info">
                                        view
                                        </span>
                                </a>
                            </td>
                            <td><?php echo $ticket['response']; ?></td>
                            <td>
                            <a data-toggle="sheqModal" href="index.php?module=modal_request&action=modalReplySupportTicket&db_name=<?php echo $ticket['db_name'] ?>&id=<?php echo $ticket['client_support_id']; ?>" class="btn btn-red btn-circle"><i class="fa fa-plus"></i></a>
                            </td>
                        </tr>
                    <?php endforeach; endif; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<?php
/*
 *
 */
include_once('frontend/templates/footers/default_footer_tpl.php');
?>
<script>

    $('body').on('click','[data-toggle="sheqModal"]',function(e){

        $('#sheqModal').remove();
        $('.modal-backdrop').remove();
        e.preventDefault();
        var $this=$(this), $remote=$this.data('remote')||$this.attr('href'),
            $modal=$('<div class="modal fade" id="sheqModal"  role="dialog"><div class="modal-dialog" ><div class="modal-body"></div></div></div>');
        $('body').append($modal);

        $modal.modal({backdrop: 'static', keyboard: false});
        $modal.load($remote);
    });
</script>