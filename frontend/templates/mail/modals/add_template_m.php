<?php
/**
 * Created by PhpStorm.
 * User: Innovatorshill
 * Date: 3/30/2017
 * Time: 10:21 AM
 */

?>
<style>
    .modal-lg {
        max-width: 100%;
        width: 60%;
        padding: 10px;
        margin: auto;
    }
</style>
<!--Add Link Modal -->
<div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Templates</h4>
        </div>
        <div class="modal-body">
            <form class="form-horizontal" method="post" name="addLinkForm" id="addLinkForm" action="index.php?module=mail_template&action=addTemplate">

                <div class="form-group">
                    <label class="control-label col-sm-4" for="notes">Name</label>
                    <div class="col-lg-6 col-md-4 col-sm-8">
                        <input class="form-control" name="template_name" id="template_name" required placeholder="">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-4" for="notes">Description</label>
                    <div class="col-lg-6 col-md-4 col-sm-8">
                        <textarea rows="5" class="form-control" name="description" id="description"  placeholder=""></textarea>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-4" for="notes">Content</label>
                </div>
                <div class="form-group">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <textarea  class="form-control" name="content" id="content"  placeholder=""></textarea>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-offset-4 col-sm-8">
                        <button type="submit" id="addDocumentBtn" class="btn btn-success">Submit</button>
                    </div>
                </div>
            </form>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
    </div>
</div>
<script>
    $('.selectpicker').selectpicker('refresh');

    $(function() {
        tinymce.init({
            selector: '#content',
            height: 500,
            theme: 'modern',
            plugins: [
                'advlist autolink lists link image charmap print preview hr anchor pagebreak',
                'searchreplace wordcount visualblocks visualchars code fullscreen',
                'insertdatetime media nonbreaking save table contextmenu directionality',
                'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc'
            ],
            toolbar1: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
            toolbar2: 'print preview media | forecolor backcolor emoticons | codesample',
            image_advtab: true,
            templates: [
                { title: 'Test template 1', content: 'Test 1' },
                { title: 'Test template 2', content: 'Test 2' }
            ],
            content_css: [
                '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
                '//www.tinymce.com/css/codepen.min.css'
            ]
        });
    });
</script>
<!--End Of Modal -->