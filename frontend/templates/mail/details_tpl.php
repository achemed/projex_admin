<?php
$title = 'Template';
include_once('frontend/templates/headers/default_header_tpl.php');

?>

<?php
/*
 * Include side menu from the include file
 */

include_once('frontend/templates/menus/main-menu.php');
?>
<!--navigation-->
<?php
/*
 * Include  main menu from the include file
 */

include_once('frontend/templates/menus/side-menu.php');
?>
<!--End of navigation-->

<div class="page-content">
    <div class="row">
        <div class="col-md-12">
            <div class="panel">
                <div class="panel-body">
                    <h5><b>Template Name</b> : <?php echo  $data['templateInfo']['template_name']; ?></h5>
                    <h5><b>Template Description</b> : <?php echo $data['templateInfo']['description']; ?></h5>
                </div>
            </div>
        </div>
    </div>
    <div class="panel-group">
        <div class="panel panel-default">
            <div class="panel-heading clearfix">
                <div class="panel-title pull-left">Template</div>
                <div class="panel-heading-icons">
                </div>
            </div>
            <div class="panel-body">
                <form class="form-horizontal" method="post" name="addLinkForm" id="addLinkForm" action="index.php?module=mail_template&action=editTemplate">
                    <textarea type="text" class="form-control" name="content" id="content"><?php echo isset($data['templateInfo']["content"]) ? html_entity_decode($data['templateInfo']["content"]): "";?></textarea>
                    <div class="form-group">
                        <div class="col-sm-offset-4 col-sm-8">
                            <input type="hidden" name="template_id" value="<?php echo $data['templateInfo']['id']; ?>">
                            <button type="submit" id="addDocumentBtn" class="btn btn-success">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <!--End of panel-default-->
    </div>
</div>
<!--End of main col-->
<?php
include_once('frontend/templates/footers/default_footer_tpl.php');
?>
<script>
    $('body').on('click','[data-toggle="sheqModal"]',function(e){

        $('#sheqModal').remove();
        $('.modal-backdrop').remove();
        e.preventDefault();
        var $this=$(this),
            $remote=$this.data('remote')||$this.attr('href'),

            $modal=$('<div class="modal fade" id="sheqModal"  role="dialog"><div class="modal-body"></div></div>');
        $('body').append($modal);
        //$modal.modal();
        $modal.modal({backdrop: 'static', keyboard: false});
        $modal.load($remote);
    });
    $(function() {
        tinymce.init({
            selector: '#content',
            height: 500,
            theme: 'modern',
            plugins: [
                'advlist autolink lists link image charmap print preview hr anchor pagebreak',
                'searchreplace wordcount visualblocks visualchars code fullscreen',
                'insertdatetime media nonbreaking save table contextmenu directionality',
                'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc'
            ],
            toolbar1: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
            toolbar2: 'print preview media | forecolor backcolor emoticons | codesample',
            image_advtab: true,
            templates: [
                { title: 'Test template 1', content: 'Test 1' },
                { title: 'Test template 2', content: 'Test 2' }
            ],
            content_css: [
                '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
                '//www.tinymce.com/css/codepen.min.css'
            ]
        });
    });

</script>
