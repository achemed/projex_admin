<?php
/**
 * Created by PhpStorm.
 * User: Innovatorshill
 * Date: 3/30/2017
 * Time: 10:21 AM
 */

?>
<!--Add Link Modal -->
<div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Package</h4>
        </div>
        <div class="modal-body">
            <form class="form-horizontal" method="post" name="addLinkForm" id="addLinkForm" action="index.php?module=clients&action=editClientPackage">

                <div class="form-group">
                    <label class="control-label col-sm-4" for="notes">Package</label>
                    <div class="col-lg-6 col-md-4 col-sm-8">
                        <select class="form-control" required="true" id="package_id" name="package_id" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select package">
                            <option value selected>Please select a package</option>
                            <?php
                            foreach($data['packages'] as $group)
                            {
                                if($data['clientInfo']['package_id'] == $group["id"]) {
                                    echo "<option value='{$group["id"]}' selected>{$group['package_name']}</option>";
                                }else{
                                    echo "<option value='{$group["id"]}'>{$group['package_name']}</option>";

                                }
                            }?>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-offset-4 col-sm-8">
                        <input type="hidden" name="client_id" value="<?php echo $data['clientInfo']['id']  ; ?>" />
                        <button type="submit"  class="btn btn-success">Update</button>
                    </div>
                </div>
            </form>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
    </div>
</div>
<script>
    $('.selectpicker').selectpicker('refresh');
</script>
<!--End Of Modal -->