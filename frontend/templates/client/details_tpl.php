<?php
$title = 'Package Details';
include_once('frontend/templates/headers/default_header_tpl.php');

?>

<?php
/*
 * Include side menu from the include file
 */

include_once('frontend/templates/menus/main-menu.php');
?>
<!--navigation-->
<?php
/*
 * Include  main menu from the include file
 */

include_once('frontend/templates/menus/side-menu.php');
?>
<!--End of navigation-->

<div class="page-content">
    <div class="row">
        <div class="col-md-12">
            <div class="panel">
                <div class="panel-body">
                    <h5><b>Company Name</b> : <?php echo  $data['clientInfo']['company_name']; ?></h5>

                </div>
            </div>
        </div>
    </div>

        <div class="col-lg-12 col-md-12">
            <div class="row">
            <div class="co-lg-4 col-md-4">
                <div class="panel-group">
                <div class="panel panel-default">
                    <div class="panel-heading clearfix">
                        <div class="panel-title pull-left">Package</div>
                        <div class="panel-heading-icons">
                            <a data-toggle="sheqModal"  href="index.php?module=modal_request&action=modalEditClientDetails&id=<?php echo $data['clientInfo']['id'];?>" class="btn btn-red btn-circle"><i class="fa fa-pencil"></i></a>
                        </div>
                    </div>
                    <div class="panel-body text-center"">
                        <?php
                        global $objPackages;
                        $packageInfo = $objPackages->getPackageInfo($data['clientInfo']['package_id']);
                        ?>
                        <h3 style="color:#5e5e5e !important"><b>
                        <?php echo ($packageInfo['package_name']) ? $packageInfo['package_name'] :"--"; ?>
                            </b>
                        </h3>
                    </div>
                </div>

                <!--End of panel-default-->
            </div>
            </div>
            <div class="col-lg-8 col-md-8">
                <div class="panel-group">
                    <div class="panel panel-default">
                        <div class="panel-heading clearfix">
                            <div class="panel-title pull-left">Groups</div>
                            <div class="panel-heading-icons">
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table width="100%" class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th>Group Name</th>
                                        <th>Types Description</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    if($data['groups']): foreach($data['groups'] as $group):
                                        ?>
                                        <tr>
                                            <td><?php echo $group["sheqteam_name"];?></td>
                                            <td><?php echo $group["sheqteam_description"];?></td>
                                            <td>
                                                <div class="btn-group">
                                                    <button style="color: white; text-decoration: none; background-color: #17a589; font-size: 11px;" class="btn dropdown-toggle" href="#" data-toggle="dropdown">
                                                        Action <span class="caret"></span>
                                                    </button>
                                                    <ul class="dropdown-menu stay-open pull-right" role="menu" style="min-width: 150px;">
                                                        <li><a data-toggle="sheqModal" href="index.php?module=modal_request&action=modalEditCompanyGroups&id=<?php echo $group["id"]; ?>"><span class="glyphicon glyphicon-pencil addglyphicon"></span> Edit</a></li>
                                                        <li class="sweet-4"><a  data-toggle="sheqModal" href="index.php?module=modal_request&action=modalEditCompanyGroups&id=<?php echo $group["id"]; ?>"><span class="glyphicon glyphicon-trash addglyphicon"></span> Delete</a></li>

                                                    </ul>
                                                </div>
                                            </td>
                                        </tr>
                                    <?php endforeach;endif; ?>
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>

                    <!--End of panel-default-->
                </div>
            </div>
        </div>
        </div>

</div>
<!--End of main col-->
<?php
include_once('frontend/templates/footers/default_footer_tpl.php');
?>
<script>
    $('body').on('click','[data-toggle="sheqModal"]',function(e){

        $('#sheqModal').remove();
        $('.modal-backdrop').remove();
        e.preventDefault();
        var $this=$(this),
            $remote=$this.data('remote')||$this.attr('href'),

            $modal=$('<div class="modal fade" id="sheqModal"  role="dialog"><div class="modal-body"></div></div>');
        $('body').append($modal);
        //$modal.modal();
        $modal.modal({backdrop: 'static', keyboard: false});
        $modal.load($remote);
    });

</script>
