<?php
$title = 'Company Information';
include_once('frontend/templates/headers/default_header_tpl.php');

?>

<?php
/*
 * Include side menu from the include file
 */

include_once('frontend/templates/menus/main-menu.php');
?>
    <!--navigation-->
    <?php
    /*
     * Include  main menu from the include file
     */

    include_once('frontend/templates/menus/side-menu.php');
    ?>
    <!--End of navigation-->

<div class="page-content">

        <ul class="nav nav-tabs nav-tabs-line">
        <li class="active"><a href="index.php?module=clients&action=viewAllClients">Active</a></li>
        <li><a href="index.php?module=clients&action=viewAllInActiveClients">Not Active</a></li>
    </ul>   
        <div class="panel-group">
            <div class="panel panel-default">
                <div class="panel-heading clearfix">Company Information 
						<span class="panel-heading-icons">
							<a href="#addNonConformance-modal" id="addNewCompany" class="btn btn-red btn-circle"><i class="fa fa-plus"></i></a>
						 </span></div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table width="100%" class="table table-hover" id="dataTables-example">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Company Name</th>
                                <th>Firstname</th>
								<th>Lastname</th>
                                <th>user name</th>
								<th>Date Created</th>
                                <th>Package</th>
								<th>Status</th>
                                <th>Club Member</th>
                                <!--
							    <th>#</th>
							    -->
                            </tr>
                            </thead>
                            <tbody>
                            <?php 
                            global $objPackages;
                            if($data['allClients']){ foreach($data['allClients'] as $user):

                                $packageInfo = $objPackages->getPackageInfo($user['package_id']);
								$status = "";	
								switch($user['is_active'])
								{
									case 1:
									$status = "Active";
									break;
									case 0 :
									$status = "In Active";
									break;
								}
                                
							?>
                                <tr>
                                        <td><?php echo $user['id']; ?></td>
                                        <td><?php echo $user['company_name']; ?></td>
                                        <td><?php echo $user['firstname']; ?></td>
                                        <td><?php echo $user['lastname']; ?></td>
                                        <td><?php echo $user['username']; ?></td>
										<td><?php echo $user['date_created']; ?></td>
                                        <td>

                                            <a  class="btn btn-xs btn-info"  href="index.php?module=clients&action=viewClientDetails&id=<?php echo $user['id'];?>">
                                            <span>
                                                 <?php echo ($packageInfo['package_name']) ? $packageInfo['package_name'] :"--"; ?>
                                            </span>
                                            </td>
                                        <td>
										<select user_id="<?php echo $user['id'];?>" class="company_status" class="form-control btn-group">
											<option value="1" <?php if($status == "Active"){echo "Selected";}?>>Activate</option>
											<option value="0" <?php if($status == "In Active"){echo "Selected";}?>>Deactivate</option>
										</select></td>
                                        <td>
                                        <select user_id="<?php echo $user['id'];?>" class="company_club_member" class="form-control btn-group">
                                            <option value="1" <?php if($user['company_club_member'] == 1){echo "Selected";}?>>Yes</option>
                                            <option value="0" <?php if($user['company_club_member'] == 0){echo "Selected";}?>>No</option>
                                        </select></td>
                                    <!--
										<td>
                                            <a  data-toggle="sheqModal" href="index.php?module=modal_request&action=modalEditClientDetails&id=<?php //echo $user['id'];?>">
                                        <span class="btn btn-xs btn-info">
                                        Edit
                                        </span>
                                            </a>
                                        </td>
                                    -->

                                </tr>
                                <?php endforeach; 
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!--End of panel panel-default-->
        </div>
        <!--End of panel-group-->
        <!-- Add Company form -->
    <div class="panel panel-default" id="addNewCompanyForm">
        <form enctype="multipart/form-data" class="form-horizontal" name="companyinfo" id="addNewCompanyform" method="post" action="<?php echo BASE_URL; ?>/index.php?module=clients&action=addClient">
		    <div class="panel panel-default">
                    <div class="panel-heading">Administrator Information</div>
                    <div class="panel-body">
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="companyName">Firstname</label>
                            <div class="col-lg-4 col-md-4 col-sm-8">
                                <input type="text" class="form-control" id="firstname" name="firstname" required placeholder="Enter your firstname">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="vatNumber">Lastname</label>
                            <div class="col-lg-4 col-md-4 col-sm-8">
                                <input type="text" class="form-control" name="lastname" id="lastname" required placeholder="Enter your lastname">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="vatNumber">Email Address</label>
                            <div class="col-lg-4 col-md-4 col-sm-8">
                                <input type="email" class="form-control" name="email" id="email" required placeholder="Enter your email">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-sm-4" for="ptyNumber">Username</label>
                            <div class="col-lg-4 col-md-4 col-sm-8">
                                <input type="text" class="form-control" name="username" id="username" required placeholder="Enter your username">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="ptyNumber">Password</label>
                            <div class="col-lg-4 col-md-4 col-sm-8">
                                <input type="password" class="form-control" name="password" id="password" required placeholder="Enter your password">
                            </div>
                        </div>
						<div class="form-group">
                            <label class="control-label col-sm-4" for="ptyNumber">Company</label>
                            <div class="col-lg-4 col-md-4 col-sm-8">
                                <input type="text" class="form-control" name="company_name" id="company_name" required placeholder="Enter your Company Name">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="notes">Package</label>
                            <div class="col-lg-4 col-md-4 col-sm-4">
                                <select class="form-control" required="true" id="package_id" name="package_id" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Select package">
                                    <option value selected>Please select a package</option>
                                    <?php
                                    foreach($data['packages'] as $group)
                                    {
                                            echo "<option value='{$group["id"]}'>{$group['package_name']}</option>";
                                    }?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="ptyNumber">Database</label>
                            <div class="col-lg-4 col-md-4 col-sm-8">
                                <input type="text" class="form-control" name="db_name" id="db_name" required placeholder="Enter your db_name">
                            </div>
                        </div>	
						<div class="form-group">
                            <label class="control-label col-sm-4" for="addAdmin"></label>
                            <div class="col-lg-4 col-md-4 col-sm-8">
                                <input type="submit" class="btn btn-success" name="addAdmin" value="Add Administrator"/>
                            </div>
                        </div>							
                    </div>
                </div>            
        </form>
    </div>
</div>

<?php
/*
 *
 */
include_once('frontend/templates/footers/default_footer_tpl.php');
include_once('frontend/templates/footers/regions_ajax_tpl.php');
?>
<script>
 $(document).ready(function()
 {
	$(".company_status").on("change",function()
	{
			var id = $(this).attr("user_id");
			var status = $(this).val();
			var url_link = "<?php echo BASE_URL;?>/index.php?action=changeAccountStatus&module=auth"
			$.ajax({
				url:url_link,
				type:"POST",
				data:{admin_id:id,account_status:status},
				success:function(result)
				{
					if(parseInt(result) == 200)
					{
                        toastr.success(
                          "Message","Account successfully updated",
                          {
                            timeOut: 10,
                            fadeOut: 10,
                            onHidden: function () {
                                window.location.reload();
                              }
                          }
                        );
					}
					else
					{
						toastr.warning("Message","Failed updating Account");
					}
				}
			});
	});


    $(".company_club_member").on("change",function()
    {
            var id = $(this).attr("user_id");
            var status = $(this).val();
            var url_link = "<?php echo BASE_URL;?>/index.php?action=changeAccountClubMemberStatus&module=auth"
            $.ajax({
                url:url_link,
                type:"POST",
                data:{admin_id:id,account_status:status},
                success:function(result)
                {
                    if(parseInt(result) == 200)
                    {
                        toastr.success("Message","Account successfully updated");
                    }
                    else
                    {
                        toastr.warning("Message","Failed updating Account");
                    }
                }
            });
    });
 });

 $('body').on('click','[data-toggle="sheqModal"]',function(e){

     $('#sheqModal').remove();
     $('.modal-backdrop').remove();
     e.preventDefault();
     var $this=$(this),
         $remote=$this.data('remote')||$this.attr('href'),

         $modal=$('<div class="modal fade" id="sheqModal"  role="dialog"><div class="modal-body"></div></div>');
     $('body').append($modal);
     //$modal.modal();
     $modal.modal({backdrop: 'static', keyboard: false});
     $modal.load($remote);
 });
</script>


