<?php
$title = 'Company Information';
include_once('frontend/templates/headers/default_header_tpl.php');

?>

<?php
/*
 * Include side menu from the include file
 */

include_once('frontend/templates/menus/main-menu.php');
?>
    <!--navigation-->
    <?php
    /*
     * Include  main menu from the include file
     */

    include_once('frontend/templates/menus/side-menu.php');
    ?>
    <!--End of navigation-->


<div class="page-content">    
        <div class="panel-group">
            <div class="panel panel-default">
                <div class="panel-heading clearfix">Transaction Logs 
						</div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table width="100%" class="table table-hover" id="dataTables-example">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Company Name</th>
                                <th>Firstname</th>
								<th>Lastname</th>
								<th>Module</th>
								<th>Action</th>
								<th>Date</th>
							    <th>#</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            if($data['allClients']){ foreach($data['allClients'] as $user):
								$status = "";	
								switch($user['is_active'])
								{
									case 1:
									$status = "Active";
									break;
									case 0 :
									$status = "In Active";
									break;
								}
							?>
                                <tr>
                                        <td><?php echo $user['id']; ?></td>
                                        <td><?php echo $user['company_name']; ?></td>
                                        <td><?php echo $user['firstname']; ?></td>
                                        <td><?php echo $user['lastname']; ?></td>
										<td>Corrective Action</td>
										<td>Signed Off</td>
										<td><?php echo $user['date_created']; ?></td>
										<th></th>
                                </tr>
                                <?php endforeach; 
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!--End of panel panel-default-->
        </div>
        <!--End of panel-group-->
        <!-- Add Company form -->
    <div class="panel panel-default" id="addNewCompanyForm">
        <form enctype="multipart/form-data" class="form-horizontal" name="companyinfo" id="addNewCompanyform" method="post" action="<?php echo BASE_URL; ?>/index.php?module=companies&action=addCompany">
		    <div class="panel panel-default">
                    <div class="panel-heading">Administrator Information</div>
                    <div class="panel-body">
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="companyName">Firstname</label>
                            <div class="col-lg-4 col-md-4 col-sm-8">
                                <input type="text" class="form-control" id="firstname" name="firstname" required placeholder="Enter your firstname">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="vatNumber">Lastname</label>
                            <div class="col-lg-4 col-md-4 col-sm-8">
                                <input type="text" class="form-control" name="lastname" id="lastname" required placeholder="Enter your lastname">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="ptyNumber">Username</label>
                            <div class="col-lg-4 col-md-4 col-sm-8">
                                <input type="text" class="form-control" name="username" id="username" required placeholder="Enter your username">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="ptyNumber">Password</label>
                            <div class="col-lg-4 col-md-4 col-sm-8">
                                <input type="password" class="form-control" name="password" id="password" required placeholder="Enter your password">
                            </div>
                        </div>
						<div class="form-group">
                            <label class="control-label col-sm-4" for="ptyNumber">Company</label>
                            <div class="col-lg-4 col-md-4 col-sm-8">
                                <input type="text" class="form-control" name="company_name" id="company_name" required placeholder="Enter your Company Name">
                            </div>
                        </div>	
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="ptyNumber">Database</label>
                            <div class="col-lg-4 col-md-4 col-sm-8">
                                <input type="text" class="form-control" name="db_name" id="db_name" required placeholder="Enter your db_name">
                            </div>
                        </div>	
						<div class="form-group">
                            <label class="control-label col-sm-4" for="addAdmin"></label>
                            <div class="col-lg-4 col-md-4 col-sm-8">
                                <input type="submit" class="btn btn-success" name="addAdmin" value="Add Administrator"/>
                            </div>
                        </div>							
                    </div>
                </div>            
        </form>
    </div>
</div>

<?php
/*
 *
 */
include_once('frontend/templates/footers/default_footer_tpl.php');
include_once('frontend/templates/footers/regions_ajax_tpl.php');
?>
<script>
 $(document).ready(function()
 {
	$(".company_status").on("change",function()
	{
			var id = $(this).attr("user_id");
			var status = $(this).val();
			var url_link = "<?php echo BASE_URL;?>/index.php?action=changeAccountStatus&module=users"
			$.ajax({
				url:url_link,
				type:"POST",
				data:{admin_id:id,account_status:status},
				success:function(result)
				{
					if(parseInt(result) == 200)
					{
						toastr.success("Message","Account successfully updated");
					}
					else
					{
						toastr.warning("Message","Failed updating Account");
					}
				}
			});
	});
 });
</script>


