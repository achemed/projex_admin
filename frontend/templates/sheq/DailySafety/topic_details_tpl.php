<?php
$title = 'Company Information';
include_once('frontend/templates/headers/default_header_tpl.php');

?>

<?php
/*
 * Include side menu from the include file
 */

include_once('frontend/templates/menus/main-menu.php');
?>
    <!--navigation-->
    <?php
    /*
     * Include  main menu from the include file
     */

    include_once('frontend/templates/menus/side-menu.php');
    ?>
    <!--End of navigation-->

<div class="page-content">

      

       <div class="panel">
            <div class="panel-body">
                <h4>Date: <?php echo $data['topicInfo']['daily_date'];?></h4>
                <h4>Topic: <?php echo $data['topicInfo']['topic'];?></h4>
            </div>
         </div>

    <ul class="nav nav-tabs nav-tabs-line">
        <li><a href="index.php?module=sheq&action=view_sheq_daily_safety_topics">Back</a></li>
    </ul>


   <?php
   global $objSheq;
   $data['moduleDetails'] = array();
   $data['moduleDetails'] = $objSheq->getAllDailySafetyTopicDescription($_GET['id']);
   ?>

    <div class="panel-group">
        <div class="panel panel-default">
           <div class="panel-heading clearfix">Information</div>    
           <div class="panel-body">

              <form class="form-horizontal" method="post" name="addLinkForm" id="addLinkForm" action="index.php?module=sheq&action=updateDailySafetyTopicDescription">
                <input type="hidden" name="topic_id" value="<?php echo $_GET['id'];?>"/>
                <textarea type="text" class="form-control" name="docContent" id="docContent"><?php echo isset($data['moduleDetails']["notes"]) ? $data['moduleDetails']["notes"] : "";?></textarea>
                <br/>
                <div class="form-group">
                    <div class="col-sm-offset-4 col-sm-8">
                        <input type="hidden" name="help_id" value="<?php echo $data['moduleInfo']['id']; ?>">
                        <button type="submit" name="btnAdd" class="btn btn-success">Submit</button>
                    </div>
                </div>
             </form>
           </div>
        </div>
    </div>        

        <div class="panel-group">
            <div class="panel panel-default">
                <div class="panel-heading clearfix">Questions
						<span class="panel-heading-icons">
							<a data-toggle="sheqModal" href="index.php?module=modal_request&action=modalAddDailySafetyTopicQuestion&id=<?php echo $_GET['id'];?>" class="btn btn-red btn-circle"><i class="fa fa-plus"></i></a>
						 </span></div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table width="100%" class="table table-hover" id="dataTables-example">
                            <thead>
                            <tr>
                                <th>Question</th>
                                <th>Answer</th>                                
                            </tr>
                            </thead>
                            <tbody>
                            <?php 
                            if($data['allQuesetions']){ foreach($data['allQuesetions'] as $info):
                                
                                
							?>
                                <tr>
                                        <td><?php echo $info['question']; ?></td>
                                        <td><?php echo $info['answer']; ?></td>
                                        <td></td>                                        
                                </tr>
                                <?php endforeach; 
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!--End of panel panel-default-->
        </div>
        <!--End of panel-group-->
       
</div>

<?php
/*
 *
 */
include_once('frontend/templates/footers/default_footer_tpl.php');
?>
<script>
    $('body').on('click','[data-toggle="sheqModal"]',function(e){

        $('#sheqModal').remove();
        $('.modal-backdrop').remove();
        e.preventDefault();
        var $this=$(this),
            $remote=$this.data('remote')||$this.attr('href'),

            $modal=$('<div class="modal fade" id="sheqModal"  role="dialog"><div class="modal-body"></div></div>');
        $('body').append($modal);
        //$modal.modal();
        $modal.modal({backdrop: 'static', keyboard: false});
        $modal.load($remote);
    });


    $(function() {
        tinymce.init({
            selector: '#docContent',
            height: 300,
            theme: 'modern',
            plugins: [
                'advlist autolink lists link image charmap print preview hr anchor pagebreak',
                'searchreplace wordcount visualblocks visualchars code fullscreen',
                'insertdatetime media nonbreaking save table contextmenu directionality',
                'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc'
            ],
            toolbar1: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
            toolbar2: 'print preview media | forecolor backcolor emoticons | codesample',
            image_advtab: true,
            templates: [
                { title: 'Test template 1', content: 'Test 1' },
                { title: 'Test template 2', content: 'Test 2' }
            ],
            content_css: [
                '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
                '//www.tinymce.com/css/codepen.min.css'
            ]
        });
    });
</script>


