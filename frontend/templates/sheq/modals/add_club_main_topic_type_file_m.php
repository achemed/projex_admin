<?php
/**
 * Created by PhpStorm.
 * User: Innovatorshill
 * Date: 3/30/2017
 * Time: 10:21 AM
 */

?>
<!--Add Link Modal -->
<div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Add</h4>
        </div>
        <div class="modal-body">
            <form class="form-horizontal"  enctype="multipart/form-data" method="post" name="addLinkForm" id="addLinkForm" action="index.php?module=sheq&action=addMainClubTopicTypeFile&section=<?php echo $data['section'];?>">

                <input type="hidden" name="sheqclub_section" value="<?php echo $data['section'];?>"/>
                <input type="hidden" name="type_id" value="<?php echo $data['id'];?>"/>

                <div class="form-group">
                    <label class="control-label col-sm-4" for="description">Description</label>
                    <div class="col-lg-6 col-md-4 col-sm-8">
                        <input type="text" class="form-control" name="description" id="description"  />
                    </div>
                </div>


                <div class="form-group">
                    <label class="control-label col-sm-4" for="attachment">Attachment</label>
                    <div class="col-lg-6 col-md-4 col-sm-8">
                        <input type="file" name="uploadPhoto[]"/>
                    </div>
                </div>
                
                <div class="form-group">
                    <div class="col-sm-offset-4 col-sm-8">
                        <button type="submit" name="btnAdd" class="btn btn-success">Submit</button>
                    </div>
                </div>
            </form>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
    </div>
</div>
<script>
    $('.selectpicker').selectpicker('refresh');
</script>
<!--End Of Modal -->