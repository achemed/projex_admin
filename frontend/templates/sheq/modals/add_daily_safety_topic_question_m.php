<?php
/**
 * Created by PhpStorm.
 * User: Innovatorshill
 * Date: 3/30/2017
 * Time: 10:21 AM
 */

?>
<!--Add Link Modal -->
<div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Add</h4>
        </div>
        <div class="modal-body">
            <form class="form-horizontal"  enctype="multipart/form-data" method="post" name="addLinkForm" id="addLinkForm" action="index.php?module=sheq&action=addDailySafetyTopicQuestions">

                <input type="hidden" name="topic_id" value="<?php echo $data['topic_id'];?>"/>

                <div class="form-group">
                    <label class="control-label col-sm-4" for="question">Question</label>
                    <div class="col-lg-6 col-md-4 col-sm-8">
                        <textarea type="text" rows="5" class="form-control" name="question"></textarea>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-4" for="notes">Answer</label>
                    <div class="col-lg-6 col-md-4 col-sm-8">
                        <select class="form-control" name="answer">
                            <option value disabled selected>Please select a option</option>
                            <option>True</option>
                            <option>False</option>
                        </select>
                    </div>
                </div>

                
                <div class="form-group">
                    <div class="col-sm-offset-4 col-sm-8">
                        <button type="submit" name="btnAdd" class="btn btn-success">Submit</button>
                    </div>
                </div>
            </form>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
    </div>
</div>
<script>
    $('.selectpicker').selectpicker('refresh');
</script>
<!--End Of Modal -->