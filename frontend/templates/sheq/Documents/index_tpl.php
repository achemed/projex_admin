<?php
$title = 'Company Information';
include_once('frontend/templates/headers/default_header_tpl.php');

?>

<?php
/*
 * Include side menu from the include file
 */

include_once('frontend/templates/menus/main-menu.php');
?>
    <!--navigation-->
    <?php
    /*
     * Include  main menu from the include file
     */

    include_once('frontend/templates/menus/side-menu.php');
    ?>
    <!--End of navigation-->
<style>
    .switch label {
  cursor: pointer; }
  .switch label input[type=checkbox] {
    opacity: 0;
    width: 0;
    height: 0; }
    .switch label input[type=checkbox]:checked + .lever {
      background-color: #dccfe2; }
    .switch label input[type=checkbox]:checked + .lever:after {
      background-color: #aa66cc;
      left: 1.5rem; }
    .switch label input[type=checkbox]:checked:not(:disabled) ~ .lever:active:after {
      -webkit-box-shadow: 0 0.0625rem 0.1875rem 0.0625rem rgba(0, 0, 0, 0.4), 0 0 0 0.9375rem rgba(170, 102, 204, 0.1);
      box-shadow: 0 0.0625rem 0.1875rem 0.0625rem rgba(0, 0, 0, 0.4), 0 0 0 0.9375rem rgba(170, 102, 204, 0.1); }
    .switch label input[type=checkbox]:not(:disabled) ~ .lever:active:after {
      -webkit-box-shadow: 0 0.0625rem 0.1875rem 0.0625rem rgba(0, 0, 0, 0.4), 0 0 0 0.9375rem rgba(0, 0, 0, 0.08);
      box-shadow: 0 0.0625rem 0.1875rem 0.0625rem rgba(0, 0, 0, 0.4), 0 0 0 0.9375rem rgba(0, 0, 0, 0.08); }
    .switch label input[type=checkbox]:disabled + .lever {
      cursor: default; }
    .switch label input[type=checkbox]:disabled + .lever:after, .switch label input[type=checkbox]:disabled:checked + .lever:after {
      background-color: #BDBDBD; }
  .switch label .lever {
    content: "";
    display: inline-block;
    position: relative;
    background-color: #818181;
    -webkit-border-radius: 0.9375rem;
    border-radius: 0.9375rem;
    margin-right: 0.625rem;
    vertical-align: middle;
    margin: 0 1rem;
    width: 2.5rem;
    height: 0.9375rem;
    -webkit-transition: background 0.3s ease;
    -o-transition: background 0.3s ease;
    transition: background 0.3s ease; }
    .switch label .lever:after {
      content: "";
      position: absolute;
      display: inline-block;
      background-color: #F1F1F1;
      -webkit-border-radius: 1.3125rem;
      border-radius: 1.3125rem;
      left: -0.3125rem;
      top: -0.1875rem;
      -webkit-box-shadow: 0 0.0625rem 0.1875rem 0.0625rem rgba(0, 0, 0, 0.4);
      box-shadow: 0 0.0625rem 0.1875rem 0.0625rem rgba(0, 0, 0, 0.4);
      width: 1.3125rem;
      height: 1.3125rem;
      -webkit-transition: left 0.3s ease, background 0.3s ease, -webkit-box-shadow 1s ease;
      transition: left 0.3s ease, background 0.3s ease, -webkit-box-shadow 1s ease;
      -o-transition: left 0.3s ease, background 0.3s ease, box-shadow 1s ease;
      transition: left 0.3s ease, background 0.3s ease, box-shadow 1s ease;
      transition: left 0.3s ease, background 0.3s ease, box-shadow 1s ease, -webkit-box-shadow 1s ease; }</style>
<div class="page-content">

        <ul class="nav nav-tabs nav-tabs-line">
        <li><a href="index.php?module=sheq&action=viewAllSHEQOptions">Back</a></li>
    </ul>   

    <div class="panel">
            <div class="panel-body text-center">
                <h2 style="color:#dc6767;font-weight: bold;">Document</h2>
            </div>
         </div>
        <div class="panel-group">
            <div class="panel panel-default">
                <div class="panel-heading clearfix">Document Groups 
            <span class="panel-heading-icons">
              <a data-toggle="sheqModal" href="index.php?module=modal_request&action=modalAddSheqClubMainTopic&section=Documents&returnUrl=view_sheq_documents" class="btn btn-red btn-circle"><i class="fa fa-plus"></i></a>
             </span></div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table width="100%" class="table table-hover" id="dataTables-example">
                            <thead>
                            <tr>
                                <th>Group</th>  
                                <td>Published</td>                              
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php 
                            global $objSheq;
                            $allTopics = $objSheq->getAllSheqClubMain(true,"Documents");

                            if($allTopics){ foreach($allTopics as $info):
                                
                                
              ?>
                                <tr>
                                        <td><?php echo $info['topic']; ?></td>
                                        <td>
                                     <div class="switch">
                                          <label>
                                            In Active
                                            <input type="checkbox" class="update_inspection_template_state" state_row="<?php echo $info["id"];?>" <?php

                                                if($info['is_published'] != 0)
                                                {
                                                    echo 'checked';
                                                }?>>
                                            <span class="lever"></span> Active
                                          </label>
                                        </div>
                                 </td>
                                        <td><a href="index.php?action=view_more_info&module=sheq&id=<?php echo $info['id'];?>&section=Documents" class="btn btn-xs btn-primary">More</a></td>                                        
                                </tr>
                                <?php endforeach; 
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!--End of panel panel-default-->
        </div>
        <!--End of panel-group-->
       
</div>

<?php
/*
 *
 */
include_once('frontend/templates/footers/default_footer_tpl.php');
?>
<script>
    $(document).ready(function(){


    $(".update_inspection_template_state").on("click",function()
    {
        var topic_id = $(this).attr("state_row");
         
        if($(this).attr("checked"))
        {
            var value = 1;
        }
        else
        {
            var value = 0;  
        }
        $.ajax({
            url:'<?php echo BASE_URL;?>/index.php?action=update_main_club_topic_status&module=sheq',
            type:'post',
            data:{topic_id:topic_id,status:value},
            success:function(result)
            {
                if(parseInt(result) == 200)
                {
                   toastr.success("Updated");
                }
            }
        });
    });

    $('body').on('click','[data-toggle="sheqModal"]',function(e)
    {

        $('#sheqModal').remove();
        $('.modal-backdrop').remove();
        e.preventDefault();
        var $this=$(this),
            $remote=$this.data('remote')||$this.attr('href'),

            $modal=$('<div class="modal fade" id="sheqModal"  role="dialog"><div class="modal-body"></div></div>');
        $('body').append($modal);
        //$modal.modal();
        $modal.modal({backdrop: 'static', keyboard: false});
        $modal.load($remote);
    });
});
</script>

