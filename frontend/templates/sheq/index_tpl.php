<?php
$title = 'Legal';
include_once('frontend/templates/headers/default_header_tpl.php');

?>

<?php
/*
 * Include side menu from the include file
 */

include_once('frontend/templates/menus/main-menu.php');
?>
<!--navigation-->
<?php
/*
 * Include  main menu from the include file
 */

include_once('frontend/templates/menus/side-menu.php');
?>
<!--End of navigation-->

<div class="page-content">

        <div class="panel">
            <div class="panel-body text-center">
                <h2 style="color:#dc6767;font-weight: bold;">SHEQ Club</h2>
            </div>
         </div>
                <?php

                $allSavedModules = array(
                                                "1"=>"Documents",
                                                "2"=>"Legal",
                                                "3"=>"Daily Safety",
                                                "4"=>"Training",
                                                "5"=>"Posters",
                                                "6"=>"Toolbox Talks",
                                                "7"=>"Risk/Controls",
                                                "8"=>"ISO Help",
                                                "9"=>"Templates");

                ?>
                <div class="rows sheqdiv">
                    <?php
                    foreach($allSavedModules as $item => $val)
                    {
                        $url = "#";
                        
                        switch ($item) {

                            case 1:
                                $url = "index.php?action=view_sheq_documents&module=sheq";
                                break;

                            case 2:
                                $url = "index.php?action=view_sheq_legal&module=sheq";
                                break;    
                        
                            case 3:
                                $url = "index.php?action=view_sheq_daily_safety_topics&module=sheq";
                                break;

                           case 4:
                                $url = "index.php?action=view_sheq_training&module=sheq";
                                break;

                           case 5:
                                $url = "index.php?action=view_sheq_posters&module=sheq";
                                break; 

                           case 6:
                                $url = "index.php?action=view_sheq_toolbox_talks&module=sheq";
                                break;      


                                         
                            
                            default:
                                # code...
                                break;
                        }
                    ?>                        
                    <a href="<?php echo $url;?>" class="col-lg-3 text-center" style="height:100px;margin-bottom:10px;margin-right:10px;border:1px solid #ccc;background:#2c343f;color:#fff;">
                        <h5 style="margin-top:40px;"><?php echo $val;?></h5> 
                    </a>                    
                    <?php
                    }
                    ?>
                </div>            
</div>
<style>
    .sheqdiv a:hover{
        background:#dc6767;
        box-shadow: 0px 0px 2px #dc6767;
        border:2px solid #dc6767;
    }
    .sheqdiv a:hover > h5{
        color:#dc6767;
    }
</style>
<!--End of main col-->
<?php
include_once('frontend/templates/footers/default_footer_tpl.php');
?>

