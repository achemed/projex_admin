<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2016/12/13
 * Time: 10:33 AM
 */

?>
<script type="text/javascript">
    $(document).ready(function() {

        $(document).ready(function() {
            $("#countryName").select2({
                allowClear: true,
                width:'100%'
            });

            $("#regionName").select2({
                    allowClear: true,
                    width: '100%'
                }
            );

            $("#branchCountryName").select2({
                    allowClear: true,
                    width: '100%'
                }
            );

            $("#branchRegionName").select2({
                    allowClear: true,
                    width: '100%'
                }
            );

            $("#headOfficeCountry").select2({
                    allowClear: true,
                    width: '100%'
                }
            );
            $("#headOfficeProvince").select2({
                    allowClear: true,
                    width: '100%'
                }
            );

            $("#individualHeadOfficeCountry").select2({
                    allowClear: true,
                    width: '100%'
                }
            );
            $("#individualHeadOfficeProvince").select2({
                    allowClear: true,
                    width: '100%'
                }
            );
        });

        $('#countryName').change(function() {
            var opt = '';
            var country_id = '';
            country_id = $(this).find(':selected').val()

            $.ajax({
                type: "POST",
                url: "<?php echo BASE_URL.'/index.php?module=ajax_request&action=ajaxCountryRegions';?>",
                data: {'countryId': country_id},
                dataType: "json", //return type expected as json
                success: function(regions) {

                    $('#regionName').empty();
                    $("<option  value=0>&nbsp;</option>").prependTo("#regionName");
                    if (regions.length > 0) {
                        $.each(regions, function(key, val) {
                            opt = $('<option/>');
                            opt.val(val.id);
                            opt.text(val.name);
                            $('#regionName').append(opt);
                        });
                        $("#regionName").attr("disabled", false);
                    } else {

                        $("#regionName").attr("disabled", true);
                    }
                }
            });
        });

        $('#branchCountryName').change(function() {
            var opt = '';
            var country_id = '';
            country_id = $(this).find(':selected').val()

            $.ajax({
                type: "POST",
                url: "<?php echo BASE_URL.'/index.php?module=ajax_request&action=ajaxCountryRegions';?>",
                data: {'countryId': country_id},
                dataType: "json", //return type expected as json
                success: function(regions) {

                    $('#branchRegionName').empty();
                    $("<option  value=0>&nbsp;</option>").prependTo("#branchRegionName");
                    if (regions.length > 0) {
                        $.each(regions, function(key, val) {
                            opt = $('<option/>');
                            opt.val(val.id);
                            opt.text(val.name);
                            $('#branchRegionName').append(opt);
                        });
                        $("#branchRegionName").attr("disabled", false);
                    } else {

                        $("#branchRegionName").attr("disabled", true);
                    }
                }
            });
        });

        $('#headOfficeCountry').change(function() {
            var opt = '';
            var country_id = '';
            country_id = $(this).find(':selected').val()

            $.ajax({
                type: "POST",
                url: "<?php echo BASE_URL.'/index.php?module=ajax_request&action=ajaxCountryRegions';?>",
                data: {'countryId': country_id},
                dataType: "json", //return type expected as json
                success: function(regions) {

                    $('#headOfficeProvince').empty();
                    $("<option  value=0>&nbsp;</option>").prependTo("#headOfficeProvince");
                    if (regions.length > 0) {
                        $.each(regions, function(key, val) {
                            opt = $('<option/>');
                            opt.val(val.id);
                            opt.text(val.name);
                            $('#headOfficeProvince').append(opt);
                        });
                        $("#headOfficeProvince").attr("disabled", false);
                    } else {

                        $("#headOfficeProvince").attr("disabled", true);
                    }
                }
            });
        });


        $('#individualHeadOfficeCountry').change(function() {
            var opt = '';
            var country_id = '';
            country_id = $(this).find(':selected').val()

            $.ajax({
                type: "POST",
                url: "<?php echo BASE_URL.'/index.php?module=ajax_request&action=ajaxCountryRegions';?>",
                data: {'countryId': country_id},
                dataType: "json", //return type expected as json
                success: function(regions) {

                    $('#individualHeadOfficeProvince').empty();
                    $("<option  value=0>&nbsp;</option>").prependTo("#individualHeadOfficeProvince");
                    if (regions.length > 0) {
                        $.each(regions, function(key, val) {
                            opt = $('<option/>');
                            opt.val(val.id);
                            opt.text(val.name);
                            $('#individualHeadOfficeProvince').append(opt);
                        });
                        $("#individualHeadOfficeProvince").attr("disabled", false);
                    } else {

                        $("#individualHeadOfficeProvince").attr("disabled", true);
                    }
                }
            });
        });





    });
</script>