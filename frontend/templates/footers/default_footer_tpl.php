<?php
/**
 * @package projex
 * @author Sunnyboy Mathole <sunny@innovatorshill.co.za>
 * @copyright (c) 2016, Innovators Hill
 * @license
 */

//include the template
include_once('classes/page_class.php');
?>
    <div id="footer">
        <div class="copyright">
            <a href="http://themifycloud.com"></a></div>
    </div>
    <!--END FOOTER-->
    </div>
    <!--END PAGE WRAPPER-->
    </div>
    </div>
<?php
if(isset($_GET['message'])){
    $message = $_GET['message'];
    $alert = $_GET['type'];
    ?>
    <script type="text/javascript">
        $(document).ready(function(){
            toastr.options = {
                "closeButton": true,
                "debug": false,
                "positionClass": "toast-top-right",
                "onclick": null,
                "showDuration": "300",
                "hideDuration": "1000",
                "timeOut": "5000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            }
            toastr.<?php echo $alert; ?>("<?php echo $message; ?>", "<?php echo "Message"; ?>");
        });
    </script>
<?php } ?>
    <script>
        $(document).ready(function()
        {

            $('body').on('shown.bs.dropdown','.table-responsive',function(e){
                var $table = $(this),
                    $menu = $(e.target).find('.dropdown-menu'),
                    tableOffsetHeight = $table.offset().top + $table.height(),
                    menuOffsetHeight = $menu.offset().top + $menu.outerHeight(true);

                if (menuOffsetHeight > tableOffsetHeight)
                    $table.css("padding-bottom", menuOffsetHeight + 5 - tableOffsetHeight);
            });

            $('body').on('hide.bs.dropdown','.table-responsive',function(e){
                $(this).css("padding-bottom", 0);
            });
/*
            $(".user-companies li a").click(function(e){
                e.preventDefault();
                var company_id;
                company_id = $(this).data('value');
                $.ajax({
                    type: "POST",
                    url: "W//echo BASE_URL.'/index.php?module=ajax_request&action=ajaxCompanyBranches';?>",
                    data: {'companyId': company_id},
                    dataType: "json", //return type expected as json
                    success: function(branches) {
                        location.reload(true);
                    }
                });
            });

            $(".user-allowed-branch li a").click(function(e){
                e.preventDefault();
                var branch_id;
                branch_id = $(this).data('value');

                $.ajax({
                    type: "POST",
                    url: "//echo BASE_URL.'/index.php?module=ajax_request&action=ajaxDefaultBranch';?>",
                    data: {'branchId': branch_id},
                    dataType: "json", //return type expected as json
                    success: function(branches) {

                        location.reload(true);
                    }
                });

            });*/
        });
    </script>
<?php $objPage->getFooter(); ?>