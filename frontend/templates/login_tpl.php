<?php 
$title = 'Login';
include_once('frontend/templates/headers/default_header_tpl.php');
?>
<style>
body {
    background: #232323;
    background-size: cover;
}
.panel{
    border:1px solid #ccc;
    background:#fff;

}
</style>
<div class="container-fluid">
    <div class="row">
    <div class="col-md-12 text-center">
	<br/><br/><br/>
        <img src="frontend/images/SHEQ24LOGOLIGHTWhite.png" alt="logo" width="520px" height="130px" />
		<br/><br/>
    </div>
    </div>
</div>
<div class="container">
    <div class="row">
    <div class="col-md-6 col-md-offset-3">
	<div class="login-panel panel panel-default">
	<div class="panel-heading">Admin Console</div>
        <div class="panel-body">
        <form class="form-horizontal" name="loginForm" id="loginform" action="index.php" method="post">
            <div class="form-group">
                <label class="control-label col-sm-4" for="username">Username/Email</label>
                <div class="col-sm-8">
                <input type="text" class="form-control" name="username" id="username" required placeholder="Enter your username">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="password">Password</label>
                <div class="col-sm-8">
                <input type="password" class="form-control" name="password" id="password" required placeholder="Enter your password">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-4 col-sm-8">
                    <input name="action" value="processAdminlogin" type="hidden">
                    <input name="module" value="auth" type="hidden">
                    <button type="submit" name="login" class="btn btn-success">Login</button>
                </div>
            </div>
        </form>
        </div>
	</div>
        </div>
    </div><!--End of row-->
</div><!--End of container-->
<?php include_once('frontend/templates/footers/default_footer_tpl.php');?>