<div class="modal-dialog modal-lg modal-notify modal-success">
    <!-- Modal content-->
    <div class="modal-content">
        <div class="modal-header">
            <p class="heading lead">Attachments</p>

            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true" class="white-text">×</span>
                            </button>
        </div>
        <div class="modal-body">
             <?php
            if(!empty($data["allFiles"]))
            {?>            

             <div class="table-responsive">
                <table width="100%" class="table table-hover">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>File</th>
                        <th></th>                        
                    </tr>
                    </thead>
                    <tbody>

                    <?php
                    global $objProject;
                     if($data['allFiles']): foreach($data['allFiles'] as $file): 

                               


                               $full_path = BASE_URL.SITE_UPLOADS_PATH.$file["filename"]; 
                                ?>
                        <tr>
                            <td><?php echo $file["id"];?></td>
                            <td><a target='_blank' href="<?php echo $full_path;?>"><?php echo $file['filename'];?></a></td>
                            <td><?php
                            if(empty($data['history'])){?><a class="delete_generic_file" section="<?php echo $_GET['signOffSection'];?>" ncid="<?php echo $file["id"];?>" href="#deleteFile"><span class="fa fa-trash"></span></a><?php
                        }?></td>
                        </tr>
                    <?php endforeach; endif;?>

                    </tbody>
                </table>
            </div>

            <?php
             }?>
</div>

</div>
<script>
    $(document).ready(function()
    {

        $(".delete_generic_file").on("click",function()
        {
                var row = $(this).parent().parent().parent();
                var noc_id = $(this).attr('ncid');
                var sectionInfo = $(this).attr('section');
                swal({
                      title: "Are you sure you want to delete this record?",
                     // text: "You may restore this record, by requesting a restore action by your administrator!",
                      type: "input",
                      showCancelButton: true,
                      confirmButtonClass: 'btn-danger',
                      confirmButtonText: 'Yes, delete it!',
                      closeOnConfirm: false,
                      //closeOnCancel: false
                      inputPlaceholder: "Reason For Deleting"
                    },
                    function (inputValue) 
                    {
                    if (inputValue === false)return false;
                    if (inputValue === "") {
                      swal.showInputError("You need to provide a reason for deleting!");
                      return false;
                    }
                    //swal("Deleted!", "Reason For Deletion: " + inputValue, "success");  
                    var url = "<?php echo BASE_URL;?>/index.php?action=removeDetailsFile&module=modal_request";
                    $.ajax({
                        type:'post',
                        url:url,
                        data:{Section:sectionInfo,ncid:noc_id,reason:inputValue},
                        success:function(result)
                        {                                
                            if(result == "succeed")
                            {
                                swal("Deleted!", "", "success");  
                                $(row).fadeOut('slow');
                            }
                            else
                            {
                                swal("Deletion failed","Please make sure you have Sufficient permissions to delete this record", "error");
                            }
                        }
                    });
                    
            });       
        });
    });
</script>