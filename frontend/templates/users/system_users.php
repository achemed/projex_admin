<?php 
/*
 * Header file
 */
$title = 'Upcoming';
include_once('frontend/templates/headers/default_header_tpl.php');
?> 
<div class="container-fluid">
<!--navigation--> 
<?php 
/*
 * Include  main menu from the include file
 */
include_once('frontend/templates/menus/side-menu.php'); 
?>
<?php 
/*
 * Include side menu from the include file
 */
include_once('frontend/templates/menus/main-menu.php'); 

?> 
<!--End of navigation--> 
<div class="col-lg-10">
<!-- start code -->
<div class="row">  
    <div class="col-lg-12">
    <ul class="nav nav-pills topbar-menu">
        <li class="active"><a href="system_users.php">System Users</a></li>
     </ul>
    </div>
</div>


<div class="panel panel-default">
<div class="panel-heading">System Users</div>
<div class="panel-body">
<div class="table-responsive">
<table width="100%" class="table table-hover">
    <thead>
    <tr>
        <th>Branch</th>
        <th>Department</th>
        <th>Name Surname</th>
        <th>Occupation</th>
        <th>User Type Location</th>
        <th>User Type Management</th>
        <th>E-mail</th>
        <th></th>
        <th></th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td>Cape Town Branch</td>
        <td>IT</td>
        <td><a href="#">Sunnyboy Mathole</a></td>
        <td>Software Developer</td>
        <td>Department</td>
        <td>Admin</td>
        <td>sunny@innovatorshill.co.za</td>
        <td><a href="system_edit.html"><span class="glyphicon glyphicon-pencil"></span></a></td>
        <td><a href="#"><span class="glyphicon glyphicon-trash"></span></a></td>
    </tr>
    <tr>
        <td>Johannesburg Branch</td>
        <td>Finance</td>
        <td><a href="#">Warren Windvogel</a></td>
        <td>Accountant</td>
        <td>Branch</td>
        <td>User</td>
        <td>wwaren@innovatorshill.co.za</td>
        <td><a href="system_edit.html"><span class="glyphicon glyphicon-pencil"></span></a></td>
        <td><a href="#"><span class="glyphicon glyphicon-trash"></span></a></td>
    </tr>
    </tbody>
</table>
</div>
</div>
</div>
<!--End of panel panel-default-->

<div class="panel panel-default">
<div class="panel-body">
<form class="form-inline" method="post">
    <div class="form-group">
    <button type="button" id="addNewUser" class="btn btn-success">Add New System User</button>
    </div>
</form>
</div>
</div>


<div class="panel panel-default" id="systemUserForm">
<div class="panel-heading">Add system user</div>
<div class="panel-body">
<form class="form-horizontal" name="systemUserForm">
    <div class="form-group">
        <label class="control-label col-sm-4" for="userFullname">Name Surname</label>
        <div class="col-lg-4 col-md-4 col-sm-8">
        <select class="selectpicker" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" title="Search name and surname">
                <option>Sunny Mathole</option>
                <option>Nick Botha</option>
                <option>Warren Windvogel</option>
        </select> 
        </div>
    </div>
    <br/>
    <div class="form-group">
        <label class="control-label col-sm-4" for="userTypeLocation">User Type Location</label>
        <div class="col-lg-4 col-md-4 col-sm-8">
        <select class="form-control" required name="userTypeLocation">
            <option value selected disabled>Please select a user type location</option>
            <option>Department</option>
            <option>Branch</option>
            <option>Company</option>
        </select>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-4" for="userTypeManagement">User Type Management</label>
        <div class="col-lg-4 col-md-4 col-sm-8">
        <select class="form-control" required name="userTypeLocation">
            <option value selected disabled>Please select a user type management</option>
            <option>Admin</option>
            <option>User</option>
        </select>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-4" for="userRestrictions">Restrictions</label>
        <div class="col-lg-4 col-md-4 col-sm-8">
        <select class="selectpicker" multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true" multiple title="Select 1 or more restrictions" data-header="Close">
            <option>Health</option>
            <option>Document Editing</option>
        </select>
        </div>
    </div>
    <br/>
    <div class="form-group">
        <label class="control-label col-sm-4" for="email">confirm E-mail Address</label>
        <div class="col-lg-4 col-md-4 col-sm-8">
            <input type="email" class="form-control" name="email" id="email" required placeholder="Please confirm your email address">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-4" for="passwrd">Password</label>
        <div class="col-lg-4 col-md-4 col-sm-8">
         <input type="password" class="form-control" name="passwrd" id="passwrd" required>
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-offset-4 col-sm-8">
        <button type="submit" class="btn btn-success">Add & Send Mail</button>
        </div>
    </div>
</form>
</div>
</div>  
<!--End of panel panel-default-->
        </div>
</div><!--End of container-fluid -->

<?php
/*
 * 
 */
include_once('frontend/templates/footers/default_footer_tpl.php');
?>  
    