<?php
$title = 'View Users';
include_once('frontend/templates/headers/default_header_tpl.php');
include_once('frontend/templates/menus/side-menu.php');
?>

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-2"></div>
        <!--Start of main col-->
        <div class="col-lg-8">
            <div class="panel-group">
                <div class="panel panel-default">
                <div class="panel-heading"><h4>Search User</h4></div>
                <div class="panel-body"> 
                    <div class="container quicksearch">
                        <div class="col-lg-4 col-md-4 col-sm-8">
                        <input type="text" class="form-control" id="searchUser" name="searchUser" placeholder="Search user by full name or username" required>
                        </div>
                        <div class="col-lg-8 col-md-8 col-sm-8">
                            <button id="buttonSearch" type="submit" name="buttonSearch" class="btn btn-success">Search User</button>
                        </div>
                        <div id="result"></div>
                    </div>    
                </div>
                </div>
                <div class="panel panel-default">
                <div class="panel-heading"><h4>User management</h4></div>
                <div class="panel-body"> 
                    <div class="table-responsive">          
                    <table class="table">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>First name</th>
                          <th>Last name</th>
                          <th>Username</th>
                          <th>Date created</th>
                          <th></th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody>
                          <?php
                          $count = 1;
                          
                            foreach($data['users'] as $userDetail){
                            echo "<tr>
                                    <td>" . $count . "</td>
                                    <td>" . $userDetail['firstname'] . "</td>
                                    <td>" . $userDetail['lastname'] . "</td>
                                    <td>" . $userDetail['username'] . "</td>
                                    <td>" . $userDetail['date_created'] . "</td>
                                    <td><a href=\"index.php?module=users&action=editUser&id=" . $userDetail['id']. "\"><span style=\"color:green;\" class=\"glyphicon glyphicon-pencil\"></span></a></td>";
                                    if($userDetail['is_active']){         
                                        echo "<td><a data-toggle=\"tooltip\" title=\"Click to deactivate this user.\" class=\"tooltipcustom\" href='" . BASE_URL . "/index.php?action=deactivateUser&id=" . $userDetail['id'] . "'><span style=\"color:red;\" class=\"glyphicon glyphicon-remove\"></span></a></td>";
                                    } else {  
                                        echo "<td><a data-toggle=\"tooltip\" title=\"Click to activate this client.\" class=\"tooltipcustom\" href='" . BASE_URL . "/index.php?action=activateUser&id=" . $userDetail['id'] . "'><span style=\"color:green;\" class=\"glyphicon glyphicon-ok\"></span></a></td>";
                                    }       
                                 echo "</tr>";
                                $count ++;
                            }
                            ?>
                      </tbody>
                    </table>
                    </div>                    
                </div>
                </div>
                <div class="panel panel-default" >
                <div class="panel-body"> 
                    <form method="post" name="addAssets" action="index.php">
                        <input type="hidden" name="action" value="addUser">
                        <input type="hidden" name="module" value="users">
                        <button type="submit" class="btn btn-success">Add New User</button>
                    </form>   
                </div>
                </div>   
                <!--End of panel-default-->
            </div>
        </div>
        <!--End of main col-->
        <div class="col-lg-2"></div>
    </div>
</div>
<script>
    $(document).ready(function(){
        function search(){
            var searchterm = $("#searchUser").val();
            if(searchterm !== ""){
                $("#result").html("<img alt='ajax search' src='ajax-loader.gif' />");
                $.ajax({
                    type:"post",
                    url:"frontend/templates/users/search_user.php",
                    data:"searchterm="+searchterm,
                    success:function(data){
                        $("#result").html(data);
                        $("#searchUser").val("");
                    }
                });
            }
        }
        
        $("#buttonSearch").click(function(){
            search();
        });
        
        $("#searchUser").keyup(function(e){
            if(e.keyCode == 13){
               search();  
            }
        });
        
    });
</script>
<?php include_once('frontend/templates/footers/default_footer_tpl.php');?>